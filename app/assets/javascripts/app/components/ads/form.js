import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  AutoComplete,
} from 'material-ui';
import { paperStyle } from '../common/styles';
import ImagesComponent from '../common/images_component';
import { show, upsert } from '../../services/posts';
import VideosComponent from "../common/videos_component";

class AdForm extends Component {
  state = {
    post: {
      text: '',
      link: {url: ''},
      attachments: [],
      photos: [],
      videos: [],
      type: 'ads',
      images: [],
    },
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveAd();
  }

  _retrieveAd = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      res.post.created_at = new Date(res.post.created_at);
      res.post.updated_at = new Date(res.post.updated_at);
      res.post.photos = [];
      res.post.videos = [];
      res.post.attachments.map(item => {
        switch (item.type){
          case 'photo':
            res.post.photos.push(item);
            break;
          case 'video':
            res.post.videos.push(item);
            break;
        }
      });
      this.setState({
        post: res.post
      })
    })
  };

  _handleChange = (key,value) => {
    const { post } = this.state;
    const { link } = this.state.post;
    if(key === 'link'){
      this.setState({
        post: {
          ...post, link: {
            ...link, url: value
          }
        }
      })
    } else {
      this.setState({
        post: {
          ...post, [key]: value,
        },
        validationErrors: {
          ...this.state.validationErrors,
          [key]: null
        }
      }, () => {
        // after state update
      })
    };
  }


  _handleSubmit = event => {
    event.preventDefault();
    const { post } = this.state;
    upsert(post)
      .success(res => {
        location.hash = '#/ads';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        let errors = {};
        for (var i in res.validation_errors){
          let err = [];
          res.validation_errors[i].map(item => {err.push(item['message'])});
          errors[i] = err;
        };
        this.setState({
          validationErrors: errors
        })
      })
  };

  updateMedia = (files, type)=> {
    switch (type){
      case 'photo':
        this.setState({
          post: {
            ...this.state.post,
            photos: files
          }
        });
        break;
      case 'video':
        this.setState({
          post: {
            ...this.state.post,
            videos: files
          }
        });
        break;
    }
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { post, progress, validationErrors } = this.state;
    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ControlLabel><h2>{ post.id ? I18n.t('advertisement.edit') : I18n.t('advertisement.new') }</h2></ControlLabel>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href='#/ads'
              className='pull-right'
              secondary={true}
              label={ I18n.t('actions.back') }
            />
          </Col>
        </Row>

        <br/>
        <form onSubmit={this._handleSubmit}>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('advertisement.fields.text') }
              fullWidth={true}
              value={post.text || ''}
              onChange={(_,val) => this._handleChange('text', val)}
              errorText={ (validationErrors.text || '') }
            />
          </FormGroup>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('advertisement.fields.link') }
              fullWidth={true}
              multiLine={true}
              value={post.link && post.link.url ? post.link.url : ''}
              onChange={(_,val) => this._handleChange('link', val)}
              errorText={ (validationErrors.link || '') }
            />
          </FormGroup>
          <FormGroup>
            <ControlLabel>{ I18n.t('advertisement.fields.images') }</ControlLabel>
            <br/>
            <ImagesComponent value={post.photos} update={(files, type) => {this.updateMedia(files, type)}} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>{ I18n.t('advertisement.fields.videos') }</ControlLabel>
            <br/>
            <VideosComponent value={post.videos} update={(files, type) => {this.updateMedia(files, type)}}/>
          </FormGroup>
          <Col sm={4} smOffset={8} className="text-right">
            <br/>
            <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
            <RaisedButton
              type='submit'
              primary={true}
              className='pull-right'
              label={ I18n.t('actions.submit') }
              disabled={isLoading}
            />
          </Col>
          <Clearfix />
        </form>
      </Paper>
    )
  }
}

export default connect(state => state)(AdForm)
