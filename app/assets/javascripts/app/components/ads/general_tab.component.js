import React, { Component } from 'react';
import {
  FormGroup,
  ControlLabel,
  Clearfix, Row, Col
} from 'react-bootstrap'
import { Card, CardActions, CardHeader,
  CardMedia, CardTitle, CardText, Toggle
} from 'material-ui';
import ImageGallery from 'react-image-gallery';
import Videos from '../common/videos.component';
export default class GeneralTabComponent extends Component {
  state = {
    post: {
      attachments: [],
      images: [],
      link: {},
    },
  };

  componentDidMount() {
    const { post } = this.props;
    if (post) {
      this.setState({
        post: post,
      })
    }
  }

  render() {

    const {post} = this.props;

    return(
      <FormGroup>
        <br/>
        <FormGroup>
          <Row>
            <Col sm={2}>
              <ControlLabel>
                Created By Admin:
              </ControlLabel>
            </Col>
            <Col sm={10}>
              <Toggle
                toggled={post.is_admin_created || false}
              />
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <ControlLabel>{ I18n.t('post.fields.text') }</ControlLabel>
          <br/>
          <span className="form-control-static">{ post.text }</span>
          <hr/>
        </FormGroup>
        <FormGroup>
          <ControlLabel>{ I18n.t('post.fields.link') }</ControlLabel>
          <br/>
          <span className="form-control-static">{ post.link && post.link.url ? post.link.url : '-'}</span>
          <hr/>
        </FormGroup>
        <FormGroup>
          <ControlLabel>{ I18n.t('post.fields.created_at') }</ControlLabel>
          <br/>
          <span className="form-control-static">{ post.created_at }</span><hr/>
        </FormGroup>
        <FormGroup>
          <ControlLabel>{ I18n.t('post.fields.updated_at') }</ControlLabel>
          <br/>
          <span className="form-control-static">
              { post.updated_at }
          </span>
        </FormGroup>
        <hr/>
        <FormGroup>
          <Row>
            <Col sm={12}>
              <ControlLabel>{ I18n.t('post.fields.attachments') }</ControlLabel>
            </Col>
          </Row>
        </FormGroup>
        <br/>
        <br/>
        <FormGroup>
          <Row>
            <Col sm={4}>
                <ImageGallery items={post.images.filter(item => item.type == 'photo')}/>
                <Clearfix/>
              </Col>
            <Col sm={7}>
              <Videos value={post.images.filter(item => item.type == 'video')}/>
              <Clearfix/>
            </Col>
          </Row>
        </FormGroup>
      </FormGroup>
    )
  }
}
