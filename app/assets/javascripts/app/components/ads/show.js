import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  FormGroup,
  ControlLabel,
  Row,
  Col,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Avatar,
  IconButton,
  Tabs,
  Tab,
  FlatButton,
  Dialog,
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import { ActionVisibility, ActionDelete, SocialNotificationsActive } from 'material-ui/svg-icons';
import { paperStyle } from '../common/styles';
import {destroy, send_push, show} from '../../services/posts';
import PropTypes from "prop-types";
import GeneralTabComponent from './general_tab.component';

class Ad extends Component {
  state = {
    post: {
      is_admin_created: false,
      reports: [],
      likes: [],
      comments: [],
      attachments: [],
      cms_notifications: [],
      images: [],
      link: {},
      user: {
        id: 0,
        first_name: '',
        last_name:  '',
        avatar: '',
        role: '',
      },
    },
    showConfirm: false,
    showConfirmPush: false
  };

  componentWillMount() {
    this._retrieveAd();
  }

  _retrieveAd = () => {
    const { id } = this.props.params;
    show(id).success(res => {
      this.setState({
        post: res.post
      })
    })
  };

  prepareToDestroy = record => {
    this.setState({
      showConfirm: true
    })
  };

  prepareToPush = record => {
    this.setState({
      showConfirmPush: true
    })
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  closeConfirmPush = () => {
    this.setState({ showConfirmPush: false })
  };


  handleDelete = () => {
    const { id } = this.props.params;
    destroy(id).success(res => {
      this.closeConfirm();
      this.props.history.push('/ads')
    });
  };

  handlePushSend = () => {
    const { id } = this.props.params;
    send_push(id).success(res => {
      this.closeConfirmPush();
      this.props.history.push('/ads')
    });
  };

  render() {
    const { post, showConfirm, showConfirmPush } = this.state;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={9}>
            <ul className="breadcrumb">
              <li>{ I18n.t('ad.label') }</li>
            </ul>
          </Col>
          <Col sm={3}>
            <IconButton onTouchTap={this.prepareToDestroy.bind(post.id)}>
              <ActionDelete color="#c62828" />
            </IconButton>
            <IconButton  onTouchTap={this.prepareToPush.bind(post.id)}>
              <SocialNotificationsActive  color="#388e3c" />
            </IconButton>
            <RaisedButton
              href='#/ads'
              className='pull-right'
              secondary={true}
              label={ I18n.t('actions.back') }
            />
          </Col>
        </Row>
        <Card>
          <CardHeader
            title={ I18n.t('admin_panel.cards.user') }
            actAsExpander={true}
            showExpandableButton={true}
          />
          <CardText>
            <FormGroup>
              <Row>
                <Col sm={12}>
                  <Col xs={6} md={2}>
                    <Avatar src={post.user.avatar} size={50}/>
                  </Col>
                  <Col sm={8}>
                    <Row>
                      <Col sm={12}>
                        <Col sm={6}>
                          <Row>
                            <Col sm={5}>{I18n.t('user.fields.first_name')}</Col>
                            <Col sm={6}><ControlLabel>{post.user.first_name || '-'}</ControlLabel></Col>
                          </Row>
                        </Col>
                        <Col sm={6}>
                          <Row>
                            <Col sm={5}>{I18n.t('user.fields.last_name')}</Col>
                            <Col sm={6}><ControlLabel>{post.user.last_name || '-'}</ControlLabel></Col>
                          </Row>
                        </Col>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={6} md={2}>
                    <IconButton onTouchTap={() => location.hash = `#/users/${post.user.role}/${post.user.id}`}>
                      <ActionVisibility color={palette.primary1Color}/>
                    </IconButton>
                  </Col>
                </Col>
              </Row>
            </FormGroup>
          </CardText>
        </Card>
        <Tabs>
          <Tab label={I18n.t('post.fields.general')}>
            <GeneralTabComponent
              post = {post}
            />
          </Tab>
        </Tabs>
        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('you_are_going_to_remove') } advertisement.
        </Dialog>
        <Clearfix/>

        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirmPush}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handlePushSend}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirmPush}
          onRequestClose={this.closeConfirmPush}
        >
          { I18n.t('you_are_going_to_send_push') } advertisement.
        </Dialog>
      </Paper>
    )
  }
}

Ad.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Ad)
