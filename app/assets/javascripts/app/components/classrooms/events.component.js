import React, { Component } from 'react';
import { Col, Row, Clearfix } from 'react-bootstrap';
import {
  Table, TableBody,
  TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn, Paper
} from 'material-ui'
import Pagination from 'rc-pagination';
import Select from "rc-select";
import {paperStyle} from "../common/styles";
import en_US from "rc-pagination/lib/locale/en_US";

export default class EventsComponent extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    events: [],
    count: 0
  };

  componentWillReceiveProps(nextProps) {
    const { events, count } = nextProps;
    if (events) {
      this.setState({
        events: events,
        count: count
      })
    }
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this.props.changePage(page));
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this.props.changePerPage(per_page));
  };

  render() {
    const { events, count } = this.state;
    const { page, per_page } = this.state.filters;

    return(
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={6}>

          </Col>
        </Row>

        <Table >
          <TableHeader displaySelectAll={false} adjustForCheckbox={false} >
            <TableRow>
              <TableHeaderColumn>{ I18n.t('classroom.events.title') }</TableHeaderColumn>
              <TableHeaderColumn> { I18n.t('classroom.events.description') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('classroom.events.time_from') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('classroom.events.time_to') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('classroom.events.created_at') }</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              events.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{ item.title  }</TableRowColumn>
                    <TableRowColumn>{ item.description  || '-'}</TableRowColumn>
                    <TableRowColumn>{ item.time_from  }</TableRowColumn>
                    <TableRowColumn>{ item.time_to}</TableRowColumn>
                    <TableRowColumn>{ item.created_at  }</TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Clearfix/>
      </Paper>
    )
  }
}
