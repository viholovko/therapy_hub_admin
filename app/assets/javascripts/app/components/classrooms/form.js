import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
} from 'material-ui';
import { paperStyle } from '../common/styles';
import ImageComponent from '../common/image.component';
import { FormErrorMessage } from '../common/form-error-message.component'
import { show, upsert } from '../../services/classrooms';

class ClassroomForm extends Component {
    state = {
      classroom: {
        name: '',
        assessment_link: '',
      },
      validationErrors: {},
    };

  componentWillMount() {
    this._retrieveClassroom();
  }

  _retrieveClassroom = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      res.classroom.created_at = new Date(res.classroom.created_at);
      res.classroom.updated_at = new Date(res.classroom.updated_at);
      this.setState({
        classroom: res.classroom
      })
    })
  };

  _handleChange = (key,value) => {
    const { classroom } = this.state;

    this.setState({
      classroom: {
          ...classroom,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  _handleSubmit = event => {
    event.preventDefault();
    const { classroom } = this.state;
    upsert(classroom)
      .success(res => {
        location.hash = '#/classrooms';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
          this.setState({
          validationErrors: res.validation_errors
        })
      })
  };

  updateAssessmentFile = file => {
    this.setState({
      classroom: {
      ...this.state.classroom,
        assessment_file: file
      }
    })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { classroom, progress, validationErrors } = this.state;

    return (
        <Paper style={paperStyle} zDepth={1}>
          <Row>
            <Col sm={6}>
              <ul className="breadcrumb">
                <li><a href='#/classrooms'>{ I18n.t('classroom.header') }</a></li>
                {
                  classroom.id
                  ?
                    <li>{ I18n.t('classroom.edit') }</li>
                  :
                    <li>{ I18n.t('classroom.new') }</li>
                }
              </ul>
            </Col>
            <Col sm={6}>
              <RaisedButton
                href='#/classrooms'
                className='pull-right'
                secondary={true}
                label={ I18n.t('actions.back') }
              />
            </Col>
          </Row>

          <br/>
          <form onSubmit={this._handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('classroom.fields.name') }
                fullWidth={true}
                value={classroom.name}
                onChange={(_,val) => this._handleChange('name', val)}
                errorText={ validationErrors.name }
              />
            </FormGroup>
            <FormGroup>
              <ImageComponent value={classroom.assessment_file} update={this.updateAssessmentFile} />
              <Clearfix/>
              <FormErrorMessage errors={ validationErrors.assessment_file  } />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('classroom.fields.assessment_link') }
                fullWidth={true}
                value={classroom.assessment_link}
                onChange={(_,val) => this._handleChange('assessment_link', val)}
                errorText={ validationErrors.assessment_link }
              />
            </FormGroup>
            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton
                type='submit'
                primary={true}
                className='pull-right'
                label={ I18n.t('actions.submit') }
                disabled={isLoading}
              />
            </Col>
            <Clearfix />
          </form>
        </Paper>
    )
  }
}

export default connect(state => state)(ClassroomForm)
