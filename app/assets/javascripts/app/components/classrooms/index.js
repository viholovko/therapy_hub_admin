import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  IconButton,
  Paper,
} from 'material-ui';
import {
  ActionVisibility,
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import { all, destroy } from '../../services/classrooms';

class Classrooms extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    classrooms: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveClassrooms();
  }

  _retrieveClassrooms = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        classrooms: res.classrooms,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveClassrooms);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveClassrooms);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveClassrooms)
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  handleDelete = () => {
    const { selectedRecord } = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveClassrooms();
      this.closeConfirm();
    });
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { classrooms, showConfirm, count } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <li>{ I18n.t('classroom.header') }</li>
            </ul>
          </Col>
        </Row>

        <Row>
          <Col sm={6}>
            <Pagination
                selectComponentClass={Select}
                onChange={this.handlePageChange}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={['10','20','50']}
                pageSize={per_page}
                onShowSizeChange={this.handleShowSizeChange}
                current={page}
                total={count}
                locale={en_US}
            />
          </Col>
          <Col sm={6}>

          </Col>
        </Row>

        <Filters
          columns={[
            {label: I18n.t('classroom.fields.name'), key: 'name', type: 'string' }
          ]}
          update={this.updateFilters}
        />

        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
                <TableHeaderColumn>{ I18n.t('classroom.fields.name') }</TableHeaderColumn>
                <TableHeaderColumn>{ I18n.t('classroom.fields.created_at') }</TableHeaderColumn>
              <TableHeaderColumn>

              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              classrooms.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{ item.name  }</TableRowColumn>
                    <TableRowColumn>{ item.assessment_link  }</TableRowColumn>
                    <TableRowColumn>{ item.created_at  }</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'right'}}>
                      <IconButton onTouchTap={() => location.hash = `#/classroom/${item.id}`}>
                        <ActionVisibility color={palette.primary1Color} />
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Clearfix/>
      </Paper>
    )
  }
}

Classrooms.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Classrooms)
