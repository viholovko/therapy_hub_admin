import React, { Component } from 'react';
import { Row, Col, FormGroup, ControlLabel } from 'react-bootstrap';
import { Avatar, IconButton, Checkbox, Dialog, FlatButton } from 'material-ui'
import { ActionVisibility, ActionDelete } from 'material-ui/svg-icons';
import { remove_user_from_classroom } from '../../services/classrooms';

export default class MemberComponent extends Component {
  state = {
    member: {
      first_name: '',
      last_name: '',
      phone_number: '',
      email: '',
      accepted: false,
      role_classroom: '',
    },
    palette: [],
    showConfirm: false
  };

  componentDidMount() {
    const { member, palette } = this.props;
    if (member) {
      this.setState({
        member: member,
        palette: palette
      })
    }
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  closeConfirm = () => {
    this.setState({
      showConfirm: false
    })
  };

  handleDelete = () => {
    const { selectedRecord } = this.state;
    const { onDelete } = this.props;

    onDelete(selectedRecord);
    // remove_user_from_classroom(selectedRecord).success(res => {
      this.closeConfirm();
    // })
  }

  render() {
    const { member, palette, showConfirm } = this.state;

    return(
      <FormGroup>
        <Row>
          <Col sm={12}>
            <Col sm={10}><ControlLabel>{I18n.t(`classroom.fields.${ member.role_classroom }`)}</ControlLabel></Col>
            <Col sm={2}>
              <IconButton onTouchTap={() => location.hash = `#/users/${ member.role }/${ member.id }`}>
                <ActionVisibility color={ palette.primary1Color }/>
              </IconButton>
              <IconButton onTouchTap={this.prepareToDestroy.bind(this,member)}>
                <ActionDelete color="#c62828" />
              </IconButton>
            </Col>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Col xs={6} md={2}>
              <Avatar src={ member.avatar } size={60}/>
            </Col>
            <Col sm={6}>
              <Row>
                <Col sm={12}>
                  <Col sm={6}>
                    <Row>
                      <Col sm={6}>{ I18n.t('user.fields.first_name') }</Col>
                      <Col sm={6}><ControlLabel>{ member.first_name || '-' }</ControlLabel></Col>
                    </Row>
                  </Col>
                  <Col sm={6}>
                    <Row>
                      <Col sm={5}>{ I18n.t('user.fields.first_name') }</Col>
                      <Col sm={6}><ControlLabel>{ member.last_name || '-' }</ControlLabel></Col>
                    </Row>
                  </Col>
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <Col sm={6}>
                    <Row>
                      <Col sm={6}>{ I18n.t('user.fields.phone_number') }</Col>
                      <Col sm={6}><ControlLabel>{ member.phone_number || '-' }</ControlLabel></Col>
                    </Row>
                  </Col>
                  <Col sm={6}>
                    <Row>
                      <Col sm={5}>{ I18n.t('user.fields.email') }</Col>
                      <Col sm={6}><ControlLabel> { member.email || '-' }</ControlLabel></Col>
                    </Row>
                  </Col>
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <Col sm={6}>
                    <Row>
                      <Col sm={6}>{ I18n.t('user.fields.role') }</Col>
                      <Col sm={6}><ControlLabel>{ member.role }</ControlLabel></Col>
                    </Row>
                  </Col>
                  <Col sm={6}>
                    <Row>
                      <Col sm={6}>{ I18n.t('classroom.fields.accepted') }</Col>
                      <Col sm={6}><Checkbox checked={ member.accepted }></Checkbox></Col>
                    </Row>
                  </Col>
                </Col>
              </Row>
              <Row>
                <Col sm={12}>
                  <Col sm={6}>
                    <Row>
                      <Col sm={6}>{ I18n.t('classroom.fields.owner') }</Col>
                      <Col sm={6}><Checkbox checked={ member.id === member.owner_id }></Checkbox></Col>
                    </Row>
                  </Col>
                </Col>
              </Row>
            </Col>
          </Col>
        </Row>
        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('you_are_going_to_remove') } post.
        </Dialog>
      </FormGroup>
    )
  }
}
