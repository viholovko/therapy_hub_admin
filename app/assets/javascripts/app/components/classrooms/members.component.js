import React, { Component } from 'react';
import {  FormGroup } from 'react-bootstrap';
import { Card, CardHeader, CardText } from 'material-ui'
import MemberComponent from './member.component';

export default class MembersComponent extends Component {
  state = {
    members: [],
    palette: []
  };


  removeItem(e) {
    let filteredArray = this.state.members.filter(item => item !== e)
    this.setState({members: filteredArray});
  }
  // removeElement = (item) => {
  //   const { members} = this.state;
  //   console.log('-------------------------');
  //   console.log(item);
  //   console.log('-------------------------');
  //   console.log(members);
  //   console.log('-------------------------');
  //
  //
  //   var members = this.state.members.filter(function(member) { return member.id != item.id });
  //   this.setState({
  //     members: members
  //   });
  //   console.log(members);
  // }

  componentWillReceiveProps(nextProps) {
    const { members, palette} = nextProps;
    if (members) {
      this.setState({
        members: members,
        palette: palette
      })
    }
  };


  render(){
    const { members, palette } = this.state;

    return(
      <Card expanded={this.state.expanded} onExpandChange={this.handleExpandChange}>
        <CardHeader
          title="Members"
          actAsExpander={true}
          showExpandableButton={true}
        />
        <CardText expandable={true}>
          {
            members.map(item => {
              return(
                <FormGroup key = {item.id}>
                  <MemberComponent
                    member = {item}
                    palette = {palette}
                    onDelete = {(item => this.removeItem(item))}
                  />
                </FormGroup>
              )
            })
          }
        </CardText>
      </Card>
    )
  }
}
