import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, Row, Col } from 'react-bootstrap';
import { Paper, RaisedButton, Tabs, Tab } from 'material-ui';
import { paperStyle } from '../common/styles';
import { show } from '../../services/classrooms';
import { all as getAllTasks } from '../../services/tasks';
import { all as getAllEvents } from '../../services/events';
import PropTypes from "prop-types";
import TasksComponent from "./tasks.component";
import EventsComponent from "./events.component";
import MembersComponent from "./members.component";

class Classroom extends Component {

  constructor(props){
    super(props);
    this.state = {
      filters: {
        page: 1,
        per_page: 10
      },
      classroom: {
        assessment_file: {},
        members: []
      },
      tasks: [],
      events: [],
      tasks_count: 0,
      events_count: 0

    }
  };

  componentWillMount() {
    this._retrieveClassroom();
    this._retrieveTasks();
    this._retrieveEvents();
  }



  _retrieveClassroom = () => {
    const { id } = this.props.params;
    let members = [];
    show(id).success(res => {
        res.classroom.student.id    ? members.push(res.classroom.student)     : null;
        res.classroom.parent.id     ? members.push(res.classroom.parent)      : null;
        res.classroom.teacher.id    ? members.push(res.classroom.teacher)     : null;
        res.classroom.caseworker.id ? members.push(res.classroom.caseworker)  : null;
      this.setState({
        classroom: {
          ...res.classroom,
          members: members
        }
      })
    })
  };

  _retrieveTasks = () => {
    const {page, per_page} = this.state.filters;
    getAllTasks({classroom_id: this.props.params.id, page: page, per_page: per_page}).success(res => {
      this.setState({
        tasks: res.tasks,
        tasks_count: res.count
      })
    })
  };

  _retrieveEvents = () => {
    const {page, per_page} = this.state.filters;
    getAllEvents({classroom_id: this.props.params.id, page: page, per_page: per_page}).success(res => {
      this.setState({
        events: res.events,
        events_count: res.count
      })
    })
  };

  handlePageChange = (type ,page) => {
    this.setState({
      filters: {
        ...this.state.filters, page
      }
    }, () => type === 'events' ? this._retrieveEvents() : this._retrieveTasks());
  };

  handleShowSizeChange = (type, per_page) => {
    this.setState({
      filters: {
        ...this.state.filters, page: 1, per_page
      }
    }, () => type === 'events' ? this._retrieveEvents() : this._retrieveTasks());
  };

  render() {
    const { classroom, tasks, events, tasks_count, events_count } = this.state;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <li>{ I18n.t('classroom.label') }</li>
            </ul>
          </Col>
          <Col sm={6}>
            <RaisedButton
              href='#/classrooms'
              className='pull-right'
              secondary={true}
              label={ I18n.t('actions.back') }
            />
          </Col>
        </Row>
        <FormGroup>
          <Row>
            <Col sm={12}>
              <Col sm={8}>
                <Row>
                  <Col sm={12}>
                    <Col sm={6}>
                      <Row>
                        <Col sm={5}>{I18n.t('classroom.fields.name')}</Col>
                        <Col sm={6}><ControlLabel>{ classroom.name || '-'}</ControlLabel></Col>
                      </Row>
                    </Col>
                    <Col sm={6}>
                      <Row>
                        <Col sm={5}>{I18n.t('classroom.fields.assessment_link')}</Col>
                        <Col sm={6}><ControlLabel>{classroom.assessment_link || '-'}</ControlLabel></Col>
                      </Row>
                    </Col>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <Col sm={6}>
                      <Row>
                        <Col sm={5}>{I18n.t('classroom.fields.created_at')}</Col>
                        <Col sm={6}><ControlLabel>{classroom.created_at || '-'}</ControlLabel></Col>
                      </Row>
                    </Col>
                    <Col sm={6}>
                      <Row>
                        <Col sm={5}>{I18n.t('classroom.fields.updated_at')}</Col>
                        <Col sm={6}><ControlLabel>{classroom.updated_at || '-'}</ControlLabel></Col>
                      </Row>
                    </Col>
                  </Col>
                </Row>
              </Col>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <MembersComponent
            members = {classroom.members}
            palette = {palette}
          />
        </FormGroup>
        <FormGroup>
          <Tabs>
            <Tab label={I18n.t('classroom.fields.tasks')} >
              <TasksComponent
                tasks = {tasks}
                count = {tasks_count}
                changePage = {page => {this.handlePageChange('tasks', page)}}
                changePerPage = {per_page => {this.handleShowSizeChange('tasks', per_page)}}
              />
            </Tab>
            <Tab label={I18n.t('classroom.fields.events')} >
              <EventsComponent
                events = {events}
                count = {events_count}
                changePage = {page => {this.handlePageChange('events', page)}}
                changePerPage = {per_page => {this.handleShowSizeChange('events', per_page)}}
              />
            </Tab>
          </Tabs>
        </FormGroup>

      </Paper>
    )
  }
}

Classroom.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Classroom)
