import React, { Component } from 'react';
import {
  Row,
  Col,
  Clearfix
} from 'react-bootstrap';
import Pagination from 'rc-pagination';
import {
  Table, TableBody,
  TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn,
  Paper
} from 'material-ui'
import Select from "rc-select";
import {paperStyle} from "../common/styles";
import en_US from "rc-pagination/lib/locale/en_US";

export default class TasksComponent extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    tasks: [],
    count: 0
  };

  componentWillReceiveProps(nextProps) {
    const { tasks, count } = nextProps;
    if (tasks) {
      this.setState({
        tasks: tasks,
        count: count
      })
    }
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this.props.changePage(page));
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this.props.changePerPage(per_page));
  };

  render() {
    const { tasks, count } = this.state;
    const { page, per_page } = this.state.filters;

    return(
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={6}>

          </Col>
        </Row>

        <Table >
          <TableHeader displaySelectAll={false} adjustForCheckbox={false} >
            <TableRow>
              <TableHeaderColumn>{ I18n.t('classroom.tasks.title') }</TableHeaderColumn>
              <TableHeaderColumn> { I18n.t('classroom.tasks.description') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('classroom.tasks.due_date') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('classroom.tasks.completed') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('classroom.tasks.completed_at') }</TableHeaderColumn>
              <TableHeaderColumn>{ I18n.t('classroom.tasks.created_at') }</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              tasks.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{ item.title  }</TableRowColumn>
                    <TableRowColumn>{ item.description  || '-'}</TableRowColumn>
                    <TableRowColumn>{ item.due_date  }</TableRowColumn>
                    <TableRowColumn>{ item.completed ? <span style={{color: "#009900"}}> Yes</span> : <span style={{color: "#c62828"}}>No</span> }</TableRowColumn>
                    <TableRowColumn>{ item.completed ? <span style={{color: "#009900"}}> { item.completed_at }</span> : <span style={{color: "#c62828"}}>-</span>} </TableRowColumn>
                    <TableRowColumn>{ item.created_at  }</TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Clearfix/>
      </Paper>
    )
  }
}
