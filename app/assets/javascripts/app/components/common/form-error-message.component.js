import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField'
export class FormErrorMessage extends Component {
  render() {
    let { errors } = this.props;
    if(!errors){
      errors = [];
    }
    return(
      <div className={`form-error-message ${ errors.length > 0 ? 'active' : ''}`}>
  <div className="error-div">
      <hr aria-hidden="true" id="image-hr"></hr>
      <hr aria-hidden="true" id="error-hr" style={{transform: `scaleX(${errors.length > 0 ? 1 : 0})`}}></hr>
    </div>
    <div className="form-error-message-text">{ errors.join('. ')}</div>
    </div>
  )
  }
}
FormErrorMessage.propTypes = {
  errors: PropTypes.arrayOf(PropTypes.string)
};