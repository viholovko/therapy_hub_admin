import LogoutIcon from './logout'
import DashboardIcon from './dashboard';
import EmailSettingsIcon from './email_settings';
import PostIcon from './post';
import AdsIcon from './ads';
import ClassroomIcon from './classroom';
import PostFeedbackIcon from './post_feedback';

export {
  LogoutIcon,
  DashboardIcon,
  EmailSettingsIcon,
  PostIcon,
  AdsIcon,
  ClassroomIcon,
  PostFeedbackIcon
}
