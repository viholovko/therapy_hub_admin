import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import {
  Table, TableBody,
  TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn,
  IconButton
} from 'material-ui'
import {ActionVisibility} from 'material-ui/svg-icons'
import DropZone from 'react-dropzone';

export default class ClassroomsComponent extends Component {
  state = {
    classrooms: [],
    palette: []
  };

  componentWillReceiveProps(nextProps) {
    const { classrooms, palette } = nextProps;
    if (classrooms) {
      this.setState({
        classrooms: classrooms,
        palette: palette
      })
    }
  }

  render() {

    const { classrooms, palette } = this.state;

    return(
      <Table style={{ tableLayout: 'auto', height:'200px' }} fixedHeader={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>
              { I18n.t('classroom.fields.name') }
            </TableHeaderColumn>
            <TableHeaderColumn>
              { I18n.t('classroom.fields.assessment_link') }
            </TableHeaderColumn>
            <TableHeaderColumn>
              { I18n.t('classroom.fields.created_at') }
            </TableHeaderColumn>
            <TableHeaderColumn>
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            classrooms.map(item => {
              return (
                <TableRow key={item.id}>
                  <TableRowColumn>{ item.name  }</TableRowColumn>
                  <TableRowColumn>{ item.assessment_link  }</TableRowColumn>
                  <TableRowColumn>{ item.created_at  }</TableRowColumn>
                  <TableRowColumn className='text-right'>
                    <IconButton onTouchTap={() => location.hash = `#/classroom/${item.id}`}>
                      <ActionVisibility color={palette.primary1Color}/>
                    </IconButton>
                  </TableRowColumn>
                </TableRow>
              )
            })
          }
        </TableBody>
      </Table>
    )
  }
}
