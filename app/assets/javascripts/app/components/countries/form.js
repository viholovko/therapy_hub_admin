import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  Toggle,
} from 'material-ui';
import { paperStyle } from '../common/styles';
import Image from '../common/image.component'
import { FormErrorMessage } from '../common/form-error-message.component'
import { show, upsert } from '../../services/countries';

class CountryForm extends Component {
    state = {
      country: {
        name: '',
        phone_code: '',
        alpha2_code: '',
        alpha3_code: '',
        numeric_code: '',
        selected: false,
      },
      validationErrors: {},
    };

  componentWillMount() {
    this._retrieveCountry();
  }

  _retrieveCountry = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      res.country.created_at = new Date(res.country.created_at);
      res.country.updated_at = new Date(res.country.updated_at);
      this.setState({
        country: res.country
      })
    })
  };

  _handleChange = (key,value) => {
    const { country } = this.state;

    this.setState({
      country: {
          ...country,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  _handleSubmit = event => {
    event.preventDefault();
    const { country } = this.state;
    upsert(country)
      .success(res => {
        location.hash = '#/countries';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
          this.setState({
          validationErrors: res.validation_errors
        })
      })
  };

  updateFlag = file => {
    this.setState({
      country: {
      ...this.state.country,
        flag: file
      }
    })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { country, progress, validationErrors } = this.state;

    return (
        <Paper style={paperStyle} zDepth={1}>
          <Row>
            <Col sm={6}>
              <ul className="breadcrumb">
                <li><a href='#/countries'>{ I18n.t('country.header') }</a></li>
                {
                  country.id
                  ?
                    <li>{ I18n.t('country.edit') }</li>
                  :
                    <li>{ I18n.t('country.new') }</li>
                }
              </ul>
            </Col>
            <Col sm={6}>
              <RaisedButton
                href='#/countries'
                className='pull-right'
                secondary={true}
                label={ I18n.t('actions.back') }
              />
            </Col>
          </Row>

          <br/>
          <form onSubmit={this._handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('country.fields.name') }
                fullWidth={true}
                value={country.name}
                onChange={(_,val) => this._handleChange('name', val)}
                errorText={ validationErrors.name }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('country.fields.phone_code') }
                fullWidth={true}
                value={country.phone_code}
                onChange={(_,val) => this._handleChange('phone_code', val)}
                errorText={ validationErrors.phone_code }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('country.fields.alpha2_code') }
                fullWidth={true}
                value={country.alpha2_code}
                onChange={(_,val) => this._handleChange('alpha2_code', val)}
                errorText={ validationErrors.alpha2_code }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('country.fields.alpha3_code') }
                fullWidth={true}
                value={country.alpha3_code}
                onChange={(_,val) => this._handleChange('alpha3_code', val)}
                errorText={ validationErrors.alpha3_code }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('country.fields.numeric_code') }
                fullWidth={true}
                value={country.numeric_code}
                onChange={(_,val) => this._handleChange('numeric_code', val)}
                errorText={ validationErrors.numeric_code }
              />
            </FormGroup>
            <FormGroup>
              <Image value={country.flag}
                     onChange={(val) => this._handleChange('flag', val)}/>
              <Clearfix/>
              <FormErrorMessage errors={ validationErrors.flag  } />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Selected</ControlLabel>
              <Toggle
                defaultToggled={ country.selected }
                onToggle={ (_, val) => this._handleChange('selected', val) }
              />
              <Clearfix/>
              <FormErrorMessage errors={ validationErrors.selected  } />
            </FormGroup>
            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton
                type='submit'
                primary={true}
                className='pull-right'
                label={ I18n.t('actions.submit') }
                disabled={isLoading}
              />
            </Col>
            <Clearfix />
          </form>
        </Paper>
    )
  }
}

export default connect(state => state)(CountryForm)
