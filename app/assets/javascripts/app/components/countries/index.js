import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  Checkbox,
  CircularProgress
} from 'material-ui';
import SortingTh from '../common/sorting_th';
import {
  ImageEdit,
  ActionDelete,
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import {all, activate, destroy} from '../../services/countries'

class Countries extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    countries: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveCountries();
  }

  _retrieveCountries = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        countries: res.countries,
        count: res.count
      });
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveCountries);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveCountries);
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveCountries)
  };

  _changeSelected = (id, isSelected) => {
    let country = {
      id: id,
      selected: isSelected
    };
    activate(country)
      .success(res => {
        this._retrieveCountries()
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        this.setState({
          validationErrors: res.validation_errors
        });
      });
  };

  prepareToAction = (record, action) => {
    this.setState({
      selectedRecord: record,
      showConfirm: true,
      action: action
    })
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  handleAction = () => {
    const { selectedRecord, action } = this.state;
    switch (action) {
      case 'destroy':
        destroy(selectedRecord.id).success(res => {
          this._retrieveCountries();
          this.closeConfirm();
        });
        break;
    }
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { countries, count, showConfirm } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <li>{ I18n.t('headers.countries') }</li>
            </ul>
          </Col>
        </Row>

        <Row>
          <Col md={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col md={4}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36} />
            <RaisedButton
              href='#/country/new'
              className='pull-right'
              primary={true}
              label={I18n.t('country.new')}
            />
          </Col>
        </Row>

        <br/>
        <Filters columns={[
          { label: I18n.t('country.fields.name'), key: 'name', type: 'string' },
          { label: I18n.t('country.fields.phone_code'), key: 'phone_code', type: 'string' },
          { label: I18n.t('country.fields.alpha2_code'), key: 'alpha2_code', type: 'string' },
          { label: I18n.t('country.fields.alpha3_code'), key: 'alpha3_code', type: 'string' },
          { label: I18n.t('country.fields.numeric_code'), key: 'numeric_code', type: 'string' },
          { label: I18n.t('country.fields.not_selected'), key: 'not_selected', type: 'boolean'},
          { label: I18n.t('country.fields.selected'), key: 'selected', type: 'boolean'}
        ]}
                 update={this.updateFilters}
        />
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('country.fields.selected') }
                >
                  { I18n.t('country.fields.selected') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('country.fields.flag') }
                >
                  { I18n.t('country.fields.flag') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('country.fields.name') }
                >
                  { I18n.t('country.fields.name') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('country.fields.phone_code') }
                >
                  { I18n.t('country.fields.phone_code') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('country.fields.alpha2_code') }
                >
                  { I18n.t('country.fields.alpha2_code') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('country.fields.alpha3_code') }
                >
                  { I18n.t('country.fields.alpha3_code') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh
                  update={this.updateFilters}
                  column={ I18n.t('country.fields.numeric_code') }
                >
                  { I18n.t('country.fields.numeric_code') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>

              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              countries.map((item, index) => {
                return (
                  <TableRow key={index}>
                    <TableRowColumn
                      style={{width: '100px'}}
                    >
                      <Checkbox checked={item.selected}
                                onCheck={(_, isCheck) => {this._changeSelected(item.id, isCheck)}}
                      />
                    </TableRowColumn>
                    <TableRowColumn>
                      {
                        item.flag ?
                          <img src={item.flag} style={{ width: '80px'}}/>
                          :
                          null
                      }
                    </TableRowColumn>
                    <TableRowColumn>{item.name}</TableRowColumn>
                    <TableRowColumn>{item.phone_code}</TableRowColumn>
                    <TableRowColumn>{ item.alpha2_code  }</TableRowColumn>
                    <TableRowColumn>{ item.alpha3_code  }</TableRowColumn>
                    <TableRowColumn>{ item.numeric_code  }</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'right'}}>
                      <IconButton onTouchTap={() => location.hash = `#/country/${item.id}/edit`}>
                        <ImageEdit color={palette.accent1Color} />
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToAction.bind(this,item, 'destroy')}>
                      <ActionDelete color="#c62828" />
                    </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Clearfix/>
        <Dialog
          title={I18n.t('country.messages.remove_dialog_title')}
          actions={[
            <FlatButton onTouchTap={this.closeConfirm} label={I18n.t('actions.cancel')}/>,
            <FlatButton secondary={true} onTouchTap={this.handleAction} label={I18n.t('actions.confirm')} />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('country.messages.remove_dialog_body') }
        </Dialog>
      </Paper>
    )
  }
}

Countries.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Countries)