import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix,
  Button
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { FormErrorMessage } from '../common/form-error-message.component'
import { show, upsert } from '../../services/frequently_asked_question';

class FrequentlyAskedQuestionForm extends Component {
    state = {
      frequently_asked_question: {
        title: '',
        description: '',
        content: ''
      },
      validationErrors: {}
    };

  componentWillMount() {
    this._retrieveFrequentlyAskedQuestions();
  }

  _retrieveFrequentlyAskedQuestions = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      res.frequently_asked_question.created_at = new Date(res.frequently_asked_question.created_at);
      res.frequently_asked_question.updated_at = new Date(res.frequently_asked_question.updated_at);
      this.setState({
        frequently_asked_question: res.frequently_asked_question
      })
    })
  };

  _handleChange = (key,value) => {
    const { frequently_asked_question } = this.state;

    this.setState({
      frequently_asked_question: {
          ...frequently_asked_question,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  _handleSubmit = event => {
    event.preventDefault();
    const { frequently_asked_question } = this.state;
    upsert(frequently_asked_question)
      .success(res => {
        location.hash = '#/frequently_asked_questions';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
          this.setState({
          validationErrors: res.validation_errors
        })
      })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { frequently_asked_question, progress, validationErrors } = this.state;

    return (
        <Paper style={paperStyle} zDepth={1}>
          <Row>
            <Col sm={6}>
              <h2>&nbsp;{ I18n.t('frequently_asked_question.form')}</h2>
            </Col>
            <Col sm={6}>
              <RaisedButton
                href='#/frequently_asked_questions'
                className='pull-right'
                secondary={true}
                label={ I18n.t('actions.back') }
              />
            </Col>
          </Row>
          <br/>
          <form onSubmit={this._handleSubmit}>
            <FormGroup>
              <Row>
                <Col sm={2}>
                  <ControlLabel>
                    { I18n.t('frequently_asked_question.fields.title')}
                  </ControlLabel>
                </Col>
                <Col sm={10}>
                  <TextField
                    hintText= { I18n.t('frequently_asked_question.fields.title')}
                    fullWidth={true}
                    value={frequently_asked_question.title}
                    onChange={(_,val) => this._handleChange('title', val)}
                    errorText={ validationErrors.title }
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={2}>
                  <ControlLabel>
                    { I18n.t('frequently_asked_question.fields.description')}
                  </ControlLabel>
                </Col>
                <Col sm={10}>
                  <TextField
                    hintText={ I18n.t('frequently_asked_question.fields.description')}
                    fullWidth={true}
                    value={frequently_asked_question.description}
                    onChange={(_,val) => this._handleChange('description', val)}
                    errorText={ validationErrors.title }
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={2}>
                  <ControlLabel>
                    { I18n.t('frequently_asked_question.fields.content')}
                  </ControlLabel>
                </Col>
                <Col sm={10}>
                  <TextField
                    hintText={ I18n.t('frequently_asked_question.fields.content')}
                    fullWidth={true}
                    value={frequently_asked_question.content}
                    onChange={(_,val) => this._handleChange('content', val)}
                    errorText={ validationErrors.content }
                  />
                </Col>
              </Row>
            </FormGroup>
            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save Frequently Asked Question" disabled={isLoading} />
            </Col>
            <Clearfix />
          </form>
        </Paper>
    )
  }
}

export default connect(state => state)(FrequentlyAskedQuestionForm)
