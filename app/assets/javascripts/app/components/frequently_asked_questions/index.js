import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  CircularProgress,
} from 'material-ui';
import {
  ActionVisibility,
  ImageEdit,
  ActionDelete
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../common/sorting_th';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import { all, destroy } from '../../services/frequently_asked_question';

class FrequentlyAskedQuestions extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10
    },
    frequently_asked_questions: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrieveFrequentlyAskedQuestions();
  }

  _retrieveFrequentlyAskedQuestions = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        frequently_asked_questions: res.frequently_asked_questions,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveFrequentlyAskedQuestions);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveFrequentlyAskedQuestions);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveFrequentlyAskedQuestions)
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  handleDelete = () => {
    const { selectedRecord } = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveFrequentlyAskedQuestions();
      this.closeConfirm();
    });
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { frequently_asked_questions, showConfirm, count } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <h2>{ I18n.t('frequently_asked_question.header')}</h2>
        <Row>
          <Col sm={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={4} className="text-right" style={{minHeight:61}}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36} />
            <RaisedButton href='#/frequently_asked_question/new' className='pull-right'
                          primary={true} label={ I18n.t('frequently_asked_question.new')}
            />
          </Col>
        </Row>
        <Filters columns={[{ label: 'Title', key: 'title', type: 'string' },]} update={this.updateFilters} />
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn><SortingTh update={this.updateFilters} column='title'>{ I18n.t('frequently_asked_question.fields.title')}</SortingTh></TableHeaderColumn>
              <TableHeaderColumn><SortingTh update={this.updateFilters} column='description'>{ I18n.t('frequently_asked_question.fields.description')}</SortingTh></TableHeaderColumn>
              <TableHeaderColumn><SortingTh update={this.updateFilters} column='created_at'>{ I18n.t('frequently_asked_question.fields.created_at')}</SortingTh></TableHeaderColumn>
              <TableHeaderColumn>Actions</TableHeaderColumn>

            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              frequently_asked_questions.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{ item.title }</TableRowColumn>
                    <TableRowColumn>{ item.description }</TableRowColumn>
                    <TableRowColumn>{ item.created_at }</TableRowColumn>
                    <TableRowColumn className='text-right'>

                      <IconButton onTouchTap={() => location.hash = `#/frequently_asked_question/${item.id}`}><ActionVisibility color={palette.primary1Color} /></IconButton>
                      <IconButton onTouchTap={() => location.hash = `#/frequently_asked_question/${item.id}/edit`}><ImageEdit color={palette.accent1Color} /></IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this,item)}><ActionDelete color="#c62828" /></IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('remove_dialog.remove_dialog_title')}
          actions={[
            <FlatButton onTouchTap={this.closeConfirm} label={ I18n.t('remove_dialog.remove_dialog_cancel')}/>,
            <FlatButton secondary={true} onTouchTap={this.handleDelete} label={ I18n.t('remove_dialog.remove_dialog_confirm')} />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          You are going to remove { I18n.t('frequently_asked_question.show')}?
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

FrequentlyAskedQuestions.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(FrequentlyAskedQuestions)
