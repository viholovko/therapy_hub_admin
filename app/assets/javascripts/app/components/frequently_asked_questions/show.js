import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, Row, Col, Clearfix } from 'react-bootstrap';
import {
  Paper,
  RaisedButton
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show } from '../../services/frequently_asked_question';

class FrequentlyAskedQuestion extends Component {
  state = {
    frequently_asked_question: {
      title: '',
      description: '',
      content: ''
    },
  };

  componentWillMount() {
    this._retrieveFrequentlyAskedQuestion();
  }

  _retrieveFrequentlyAskedQuestion = () => {
    const { id } = this.props.params;
    show(id).success(res => {
      this.setState({
        frequently_asked_question: res.frequently_asked_question
      })
    })
  };

  render() {
    const { frequently_asked_question } = this.state;

    return (
        <Paper style={paperStyle} zDepth={1}>
          <Row>
            <Col sm={6}>
              <h2>&nbsp;{ I18n.t('frequently_asked_question.show')}</h2>
            </Col>
            <Col sm={6}>
              <RaisedButton
                href='#/frequently_asked_questions'
                className='pull-right'
                secondary={true}
                label={ I18n.t('actions.back') }
              />
            </Col>
          </Row>
          <br/>
          <FormGroup>
            <Row>
              <Col sm={2}>
                <ControlLabel>{ I18n.t('frequently_asked_question.fields.title')}</ControlLabel>
              </Col>
              <Col sm={10}>
                <span className="form-control-static">
                  { frequently_asked_question.title }
                </span>
              </Col>
            </Row>
            <hr/>
          </FormGroup>
          <FormGroup>
            <Row>
              <Col sm={2}>
                <ControlLabel>{ I18n.t('frequently_asked_question.fields.description')}</ControlLabel>
              </Col>
              <Col sm={10}>
                <span className="form-control-static">
                  { frequently_asked_question.description }
                </span>
              </Col>
            </Row>
            <hr/>
          </FormGroup>
          <FormGroup>
            <Row>
              <Col sm={2}>
                <ControlLabel>{ I18n.t('frequently_asked_question.fields.content')}</ControlLabel>
              </Col>
              <Col sm={10}>
                <span className="form-control-static">
                  { frequently_asked_question.content }
                </span>
              </Col>
            </Row>
            <hr/>
          </FormGroup>
          <FormGroup>
            <Row>
              <Col sm={2}>
                <ControlLabel>{ I18n.t('frequently_asked_question.fields.created_at')}</ControlLabel>
              </Col>
              <Col sm={10}>
                <span className="form-control-static">
                  { frequently_asked_question.created_at }
                </span>
              </Col>
            </Row>
            <hr/>
          </FormGroup>
          <FormGroup>
            <Row>
              <Col sm={2}>
                <ControlLabel>{ I18n.t('frequently_asked_question.fields.updated_at')}</ControlLabel>
              </Col>
              <Col sm={10}>
                <span className="form-control-static">
                  { frequently_asked_question.updated_at }
                </span>
              </Col>
            </Row>
            <hr/>
          </FormGroup>
        </Paper>
    )
  }
}

export default connect(state => state)(FrequentlyAskedQuestion)
