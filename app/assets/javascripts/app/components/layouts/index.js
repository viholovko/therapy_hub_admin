import React, { Component } from "react";
import { Grid, Row, Col } from 'react-bootstrap';
import {AppBar, Drawer, MenuItem, IconButton, Popover, Menu} from 'material-ui';
import { logout } from '../../services/sessions';
import { LogoutIcon, DashboardIcon, PostIcon, AdsIcon, ClassroomIcon } from '../common/icons';
import { SocialGroup, ContentContentPaste, NavigationArrowDropRight, ActionSettings } from 'material-ui/svg-icons'
import { store } from '../../create_store';
const { dispatch } = store;
import { connect } from 'react-redux';

class HomePage extends Component {
  state = { open: false };

  handleToggle = () => this.setState({open: !this.state.open});

  _selectLanguage = (language) => {
    this.setState({
      languagePopoverOpen: false
    })
    dispatch({type: 'SET_LANGUAGE', language});
  }

  render() {
    return (
        <div className="dashboard-page">
          <AppBar
              title="iDyslexic"
              onLeftIconButtonTouchTap={ this.handleToggle }
          >
            <IconButton
                style={{marginTop: '10px'}}
                onTouchTap={(event => this.setState({languagePopoverOpen: true, anchorEl: event.currentTarget}))}
            >
              <div className={`flag ${ this.props.app.main.language }`}></div>
            </IconButton>
            <Popover
                open={this.state.languagePopoverOpen}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                onRequestClose={() => this.setState({languagePopoverOpen: false})}
            >
              <Menu>
                <MenuItem onTouchTap={() => this._selectLanguage('ua')}>
                  <div className="flag ua"></div>
                  <span style={{marginLeft: '10px'}}>UKR</span>
                </MenuItem>
                <MenuItem onTouchTap={() => this._selectLanguage('en')}>
                  <div className="flag en"></div>
                  <span style={{marginLeft: '10px'}}>ENG</span>
                </MenuItem>
              </Menu>
            </Popover>

            <IconButton
              style={{marginTop: '10px'}}
              onTouchTap={logout}
            >
              <LogoutIcon/>
            </IconButton>
          </AppBar>
          <Drawer
            docked={false}
            width={300}
            open={this.state.open}
            onRequestChange={(open) => this.setState({open})}
          >
            <MenuItem onTouchTap={this.handleToggle} href='#/' leftIcon={<DashboardIcon />}>
              { I18n.t('headers.dashboard') }
            </MenuItem>

            {/*generated routes:*/}
            <MenuItem onTouchTap={this.handleToggle} href='#/classrooms' leftIcon={<ClassroomIcon />}>
              { I18n.t('classroom.header') }
            </MenuItem>
            <MenuItem onTouchTap={this.handleToggle} href='#/posts' leftIcon={<PostIcon />}>
              { I18n.t('post.header') }
            </MenuItem>
            <MenuItem onTouchTap={this.handleToggle} href='#/ads' leftIcon={<AdsIcon />}>
              { I18n.t('ad.header') }
            </MenuItem>

            <MenuItem
              primaryText={I18n.t('headers.users')}
              leftIcon={<SocialGroup/>}
              rightIcon={<NavigationArrowDropRight/>}
              menuItems={[
                <MenuItem onTouchTap={this.handleToggle} href='#/users/admin'>{ I18n.t('admin.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/mentor'>{ I18n.t('mentor.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/caseworker'>{ I18n.t('caseworker.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/teacher'>{ I18n.t('teacher.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/parent'>{ I18n.t('parent.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/student'>{ I18n.t('student.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/users/not_upgraded'>{ I18n.t('not_upgraded.header') }</MenuItem>,
              ]}
            />
            <MenuItem
              primaryText={I18n.t('headers.static_content')}
              leftIcon={<ContentContentPaste/>}
              rightIcon={<NavigationArrowDropRight/>}
              menuItems={[
                <MenuItem onTouchTap={this.handleToggle} href='#/about'>{ I18n.t('about.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/terms'>{ I18n.t('term.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/privacy_policy'>{ I18n.t('privacy_policy.header') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/frequently_asked_questions'>{ I18n.t('frequently_asked_question.header')}</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/payment_plans'>{ I18n.t('headers.payment_plans') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/countries'>{ I18n.t('country.header') }</MenuItem>
              ]}
            />
            <MenuItem
              primaryText={I18n.t('headers.system_settings')}
              leftIcon={<ActionSettings/>}
              rightIcon={<NavigationArrowDropRight/>}
              menuItems={[
                <MenuItem onTouchTap={this.handleToggle} href='#/email_sender'>{ I18n.t('headers.email_settings') }</MenuItem>,
                <MenuItem onTouchTap={this.handleToggle} href='#/system_settings'>{ I18n.t('headers.system_settings') }</MenuItem>
              ]}
            />
          </Drawer>
          <Grid>
            <Row>
              <Col md={12}>
                <br/>
                {this.props.children}
              </Col>
            </Row>
          </Grid>
        </div>
    );
  }

}

export default connect(state => state)(HomePage);
