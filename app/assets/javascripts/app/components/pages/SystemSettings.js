import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Col,
  FormGroup,
  Clearfix
} from 'react-bootstrap';
import {Paper, RaisedButton, TextField, CircularProgress, RadioButton, RadioButtonGroup} from 'material-ui';
import { paperStyle } from '../common/styles';
import FileSelect from '../common/file_select_component';
import { show, update } from '../../services/system_settings';

class SystemSettingsForm extends Component {
  state = {
    system_settings: {
      ios_push_environment: 'sandbox',
      apple_purchase_password: '',
      ios_push_apns_host: '',
      ios_push_password: '',
      ios_push_certificate: {},

      android_push_token: '',
      android_purchase_package_name: '',
      android_purchase_product_id: '',
      android_purchase_refresh_token: '',
      android_purchase_client_id: '',
      android_purchase_client_secret: '',
      android_purchase_redirect_uri: '',

      twilio_account_sid: '',
      twilio_auth_token: '',
      twilio_phone_number: '',

      s3_bucket: '',
      aws_access_key_id: '',
      aws_secret_access_key: '',
      s3_region: ''
    }
  };

  componentWillMount() {
    this._retrieveSystemSettings();
  }

  _retrieveSystemSettings = () => {
    show().success(res => {
      this.setState({
        system_settings: res.system_settings
      });
    })
  };

  handleChange = (key,value) => {
    this.setState({
      system_settings: {
        ...this.state.system_settings,
        [key]: value
      }
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { system_settings } = this.state;
    update(system_settings)
      .success(res => {

      })
      .progress(value => {
        this.setState({ progress: value })
      })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { system_settings, progress } = this.state;

    return (
      <div>

        <Paper style={paperStyle} zDepth={1}>
          <h2>{ I18n.t('system_settings.ios_push_environment') }</h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <FormGroup>
              <RadioButtonGroup
                name="ios_push_environment"
                defaultSelected="sandbox"
                valueSelected={system_settings.ios_push_environment}
                value={system_settings.ios_push_environment}
                onChange={(_,val) => this.handleChange('ios_push_environment', val)}
              >
                <RadioButton
                  value="production"
                  label={ I18n.t('system_settings.ios_push_environment_production') }
                />
                <RadioButton
                  value="sandbox"
                  label={ I18n.t('system_settings.ios_push_environment_sandbox') }
                />
              </RadioButtonGroup>
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.ios_push_apns_host') }
                fullWidth={true}
                value={system_settings.ios_push_apns_host || 'gateway.sandbox.push.apple.com'}
                onChange={(_,val) => this.handleChange('ios_push_apns_host', val)}
              />
            </FormGroup>


            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.password') }
                type = 'password'
                hintText=''
                fullWidth={true}
                value={system_settings.ios_push_password || ''}
                onChange={(_,val) => this.handleChange('ios_push_password', val)}
              />
            </FormGroup>
            <FormGroup>
              <FileSelect accept={'.pem'}
                          files={[system_settings.ios_push_certificate]}
                          onChange={(files)=>this.handleChange('ios_push_certificate', files[0])}
                          errorUpdate={()=>({})}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.apple_purchase_password') }
                hintText=''
                fullWidth={true}
                value={system_settings.apple_purchase_password || ''}
                onChange={(_,val) => this.handleChange('apple_purchase_password', val)}
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
            </Col>
            <Clearfix/>
          </form>
        </Paper>
        <br/>

        <Paper style={paperStyle} zDepth={1}>
          <h2>{ I18n.t('system_settings.android_purchases') }</h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_push_token') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_push_token || ''}
                onChange={(_,val) => this.handleChange('android_push_token', val)}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_package_name') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_package_name || ''}
                onChange={(_,val) => this.handleChange('android_purchase_package_name', val)}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_product_id') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_product_id || ''}
                onChange={(_,val) => this.handleChange('android_purchase_product_id', val)}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_refresh_token') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_refresh_token || ''}
                onChange={(_,val) => this.handleChange('android_purchase_refresh_token', val)}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_client_id') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_client_id || ''}
                onChange={(_,val) => this.handleChange('android_purchase_client_id', val)}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_client_secret') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_client_secret || ''}
                onChange={(_,val) => this.handleChange('android_purchase_client_secret', val)}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.android_purchase_redirect_uri') }
                hintText=''
                fullWidth={true}
                value={system_settings.android_purchase_redirect_uri || ''}
                onChange={(_,val) => this.handleChange('android_purchase_redirect_uri', val)}
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
            </Col>
            <Clearfix/>
          </form>
        </Paper>
        <br/>

        <Paper style={paperStyle} zDepth={1}>
          <h2>{ I18n.t('system_settings.twillio') }</h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.twilio_account_sid') }
                hintText=''
                fullWidth={true}
                value={system_settings.twilio_account_sid || ''}
                onChange={(_,val) => this.handleChange('twilio_account_sid', val)}
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.twilio_auth_token') }
                hintText=''
                fullWidth={true}
                value={system_settings.twilio_auth_token || ''}
                onChange={(_,val) => this.handleChange('twilio_auth_token', val)}
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.twilio_phone_number') }
                hintText=''
                fullWidth={true}
                value={system_settings.twilio_phone_number || ''}
                onChange={(_,val) => this.handleChange('twilio_phone_number', val)}
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
            </Col>
            <Clearfix/>
          </form>
        </Paper>
        <br/>
        <Paper style={paperStyle} zDepth={1}>
          <h2>{ I18n.t('system_settings.aws') }</h2>
          <br/>
          <form onSubmit={this.handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.s3_bucket') }
                hintText=''
                fullWidth={true}
                value={system_settings.s3_bucket || ''}
                onChange={(_,val) => this.handleChange('s3_bucket', val)}
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.aws_access_key_id') }
                hintText=''
                fullWidth={true}
                value={system_settings.aws_access_key_id || ''}
                onChange={(_,val) => this.handleChange('aws_access_key_id', val)}
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.aws_secret_access_key') }
                hintText=''
                fullWidth={true}
                value={system_settings.aws_secret_access_key || ''}
                onChange={(_,val) => this.handleChange('aws_secret_access_key', val)}
              />
            </FormGroup>

            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('system_settings.s3_region') }
                hintText='eu-central-1'
                fullWidth={true}
                value={system_settings.s3_region || ''}
                onChange={(_,val) => this.handleChange('s3_region', val)}
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton type='submit' primary={true} className='pull-right' label="Save" disabled={isLoading} />
            </Col>
            <Clearfix/>
          </form>
        </Paper>
      </div>
    )
  }
}

export default connect(state => state)(SystemSettingsForm)
