import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Col,
  Row,
  FormGroup,
  Clearfix,
  ControlLabel,
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  AutoComplete
} from 'material-ui';
import { paperStyle } from '../../common/styles';
import Image from '../../common/image.component';
import { show, upsert } from '../../../services/user';
import { FormErrorMessage } from '../../common/form-error-message.component';
import { all as getAllCountries }  from '../../../services/countries';

class UserForm extends Component {
  state = {
    user: {
      first_name: '',
      last_name: '',
      avatar: null,
      country: null,
      phone_number: ''
    },
    validationErrors: {},
    countries: []
  };

  componentWillMount() {
    this._retrieveUser();
    this._retrieveCountrySearch();
  }

  _retrieveUser = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      this.setState({
        user: res.user
      })
    })
  };

  handleChange = (key, value) => {
    const { user } = this.state;

    this.setState({
      user: {
        ...user,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    })
  };

  handleSubmit = event => {
    event.preventDefault();
    const { user } = this.state;
    const {role} = this.props.params;

    upsert({...user, role: role})
      .success(res => {
        location.hash = `#/users/${role}`;
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        let errors = {};
        for (var i in res.validation_errors){
          let err = [];
          res.validation_errors[i].map(item => {err.push(item['message'])});
          errors[i] = err;
        };
        this.setState({
          validationErrors: errors
        })
      })
  };

  _retrieveCountrySearch = (value = '') => {
    if (this._countrySearchTimeout) {
      clearTimeout(this._countrySearchTimeout);
    }
    this._countrySearchTimeout = setTimeout( () => {
      getAllCountries({page: 1, per_page: 10, name: value}).success(res => {
        this.setState({
          countries: res.countries
        })
      })
    }, 500);
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { user, progress, validationErrors, countries } = this.state;
    const {role} = this.props.params;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel>
                <h2>{ user.id ? I18n.t(`${role}.edit`) : I18n.t(`${role}.new`) } </h2>
            </ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href={`#users/${role}`} className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Image
              value={user.avatar}
              onChange={(val) => this.handleChange('avatar', val)}
            />
            <Clearfix/>
            <FormErrorMessage errors={ validationErrors.avatar } />
          </FormGroup>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('user.fields.first_name') }
              fullWidth={true}
              value={user.first_name}
              onChange={(_,val) => this.handleChange('first_name', val)}
              errorText={ (validationErrors.first_name || []).join(', ') }
            />
          </FormGroup>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('user.fields.last_name') }
              fullWidth={true}
              value={user.last_name}
              onChange={(_,val) => this.handleChange('last_name', val)}
              errorText={ (validationErrors.last_name || []).join(', ') }
            />
          </FormGroup>
          <FormGroup>
            <AutoComplete
              searchText={user.country ? user.country.name : ''}
              errorText={ (validationErrors.country || []).join('. ') }
              floatingLabelText={I18n.t('user.fields.country')}
              dataSource={countries}
              fullWidth={true}
              dataSourceConfig={{text: 'name', value: 'id'}}
              filter={AutoComplete.caseInsensitiveFilter}
              openOnFocus={true}
              onNewRequest={(val) => this.handleChange('country', val)}
              onUpdateInput={(val) => this._retrieveCountrySearch(val) }
              maxSearchResults={5}
            />
          </FormGroup>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('user.fields.phone_number') }
              fullWidth={true}
              value={user.phone_number}
              onChange={(_,val) => this.handleChange('phone_number', val)}
              errorText={ (validationErrors.phone_number || [] ).join(', ') }
            />
          </FormGroup>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('user.fields.role') }
              fullWidth={true}
              disabled={true}
              value={role}
              onChange={(_,val) => this.handleChange('role', val)}
              errorText={ (validationErrors.phone_number || [] ).join(', ') }
            />
          </FormGroup>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('user.fields.password') }
              fullWidth={true}
              type="password"
              value={user.password}
              onChange={(_,val) => this.handleChange('password', val)}
              errorText={ (validationErrors.phone_number || [] ).join(', ') }
            />
          </FormGroup>
          <FormGroup>
            <TextField
              floatingLabelText={ I18n.t('user.fields.password_confirmation') }
              fullWidth={true}
              type="password"
              value={user.password_confirmation}
              onChange={(_,val) => this.handleChange('password_confirmation', val)}
              errorText={ (validationErrors.phone_number || [] ).join(', ') }
            />
          </FormGroup>
          <Col sm={4} smOffset={8} className="text-right">
            <br/>
            <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
            <RaisedButton
              type='submit'
              primary={true}
              className='pull-right'
              label={ I18n.t('actions.submit') }
              disabled={isLoading}
            />
          </Col>
          <Clearfix />
        </form>
      </Paper>
    )
  }
}

export default connect(state => state)(UserForm);