import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Avatar,
  Toggle,
  Tabs,
  Tab,
} from 'material-ui';
import {paperStyle} from '../../common/styles';
import {show} from '../../../services/user';
import {all as getAllCountries} from '../../../services/countries';
import PropTypes from "prop-types";
import UsersComponent from '../../common/tabs_components/users.component';
import ClassroomsComponent from '../../common/tabs_components/classrooms.component';

class User extends Component {
  state = {
    user: {
      first_name: '',
      last_name: '',
      avatar: null,
      country_flag: null,
      country: {
        name: '',
        flag: null,
      },
      phone_number: '',
      friends: [],
      followers: [],
      followeds: [],
      i_blocked_users: [],
      me_blocked_users: [],
      i_blocked_users_count: 0,
      me_blocked_users_count: 0
    },
    role: '',
    id: 0,
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveUser();
  };

  componentWillReceiveProps(nextProps) {
    const {role, id} = nextProps.params;
    if (this.state.role === '' || this.state.id === 0) {
      this.setState({
        ...this.state,
        role: role,
        id: id
      })
    }else {
      if (role !== this.state.role || id !== this.state.id) {
        this.setState({
          ...this.state,
          role: role,
          id: id
          }, () => this._retrieveUser()
        );
      }
    }
  }

  _retrieveUser = () => {
    const {id} = this.props.params;
    if (!id) {
      return false
    }
    show(id).success(res => {
      this.setState({
        user: res.user
      })
    })
  };

  handleChange = (key, value) => {
    const {user} = this.state;

    this.setState({
      user: {
        ...user,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    })
  };

  render() {
    const {user} = this.state;
    const { palette } = this.context.muiTheme;
    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{user.first_name} {user.last_name}</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href={`#/users/${user.role}`} className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <Avatar src={user.avatar} size={120}/>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <Avatar src={ user.country.flag } size={120}/>
            <span className="form-control-static"> { user.country.name }</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('user.fields.first_name')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.first_name || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{ I18n.t('user.fields.last_name')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.last_name || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('user.fields.phone_number')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.phone_number || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('user.fields.email')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.email || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('user.fields.latitude')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.latitude || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('user.fields.longitude')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.longitude || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('user.fields.created_at')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.created_at || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('user.fields.role_verified')}</ControlLabel>
            <br/>
            <Toggle toggled={user.enable_starttls_auto || false}/>
          </Col>
        </Row>
        <hr/>
        <FormGroup>
          <ControlLabel>{I18n.t('user.fields.about_me')}</ControlLabel>
          <br/>
          <span className="form-control-static"> {user.about_me || '-'}</span>
          <hr/>
        </FormGroup>
        <FormGroup>
          <Tabs>
            <Tab label={I18n.t('admin_panel.tabs.friends')} >
              <UsersComponent
                users = {user.friends}
                palette = {palette}
              />
            </Tab>
            <Tab label={I18n.t('admin_panel.tabs.followed')} >
              <UsersComponent
                users = {user.followeds}
                palette = {palette}
              />
            </Tab>
            <Tab label={I18n.t('admin_panel.tabs.followers')} >
              <UsersComponent
                users = {user.followers}
                palette = {palette}
              />
            </Tab>
            <Tab label={I18n.t('admin_panel.tabs.classrooms')} >
              <ClassroomsComponent
                classrooms = {user.classrooms}
                palette = {palette}
              />
            </Tab>
            <Tab label={I18n.t('admin_panel.tabs.blocked_list')} >
              <br/>
              <Tabs tabItemContainerStyle={{backgroundColor:'#908e8e'}}>
                <Tab label={I18n.t('admin_panel.tabs.i_blocked_users')}>
                  <UsersComponent
                    users = {user.i_blocked_users}
                    palette = {palette}
                  />
                </Tab>
                <Tab label={I18n.t('admin_panel.tabs.me_blocked_users')}>
                  <UsersComponent
                    users = {user.me_blocked_users}
                    palette = {palette}
                  />
                </Tab>
              </Tabs>
            </Tab>
          </Tabs>
        </FormGroup>
        <Clearfix/>
      </Paper>
    )
  }
}

User.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(User)