import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  TextField,
  CircularProgress,
  Toggle
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/payment_plans';

class PaymentPlanForm extends Component {
    state = {
      payment_plan: {
        name: '',
        description: '',
        price: 0,
        classroom_count: 0,
        active: false,
        apple_id: '',
        android_id: '',
        type_name: '',
        order: 0
      },
      validationErrors: {},
    };

  componentWillMount() {
    this._retrievePaymentPlan();
  }

  _retrievePaymentPlan = () => {
    const { id } = this.props.params;
    if (!id) { return false }
    show(id).success(res => {
      this.setState({
        payment_plan: res.payment_plan
      })
    })
  };

  _handleChange = (key,value) => {
    const { payment_plan } = this.state;
      this.setState({
        payment_plan: {
            ...payment_plan, [key]: value,
        },
        validationErrors: {
          ...this.state.validationErrors,
          [key]: null
        }
      }, () => {
        // after state update
      })
    }


  _handleSubmit = event => {
    event.preventDefault();
    const { payment_plan } = this.state;
    upsert(payment_plan)
      .success(res => {
        location.hash = '#/payment_plans';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
        let errors = {};
        for (var i in res.validation_errors){
          let err = [];
          res.validation_errors[i].map(item => {err.push(item['message'])});
          errors[i] = err;
        };
        this.setState({
          validationErrors: errors
        })
      })
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { payment_plan, progress, validationErrors } = this.state;
    return (
        <Paper style={paperStyle} zDepth={1}>
          <Row>
            <Col sm={6}>
                <ControlLabel><h2>{ payment_plan.id ? I18n.t('payment_plan.edit') : I18n.t('payment_plan.new') }</h2></ControlLabel>
            </Col>
            <Col sm={6}>
              <RaisedButton
                href='#/payment_plans'
                className='pull-right'
                secondary={true}
                label={ I18n.t('actions.back') }
              />
            </Col>
          </Row>

          <br/>
          <form onSubmit={this._handleSubmit}>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.name') }
                fullWidth={true}
                value={payment_plan.name || ''}
                onChange={(_,val) => this._handleChange('name', val)}
                errorText={ (validationErrors.name || '') }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.description') }
                fullWidth={true}
                value={payment_plan.description || ''}
                onChange={(_,val) => this._handleChange('description', val)}
                errorText={ (validationErrors.description || '') }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.type_name') }
                fullWidth={true}
                value={payment_plan.type_name || ''}
                onChange={(_,val) => this._handleChange('type_name', val)}
                errorText={ (validationErrors.type_name || '') }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.price') }
                fullWidth={true}
                multiLine={true}
                value={payment_plan.price}
                onChange={(_,val) => this._handleChange('price', val)}
                errorText={ (validationErrors.price || '') }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.classroom_count') }
                fullWidth={true}
                multiLine={true}
                value={payment_plan.classroom_count}
                onChange={(_,val) => this._handleChange('classroom_count', val)}
                errorText={ (validationErrors.classroom_count || '') }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.apple_id') }
                fullWidth={true}
                multiLine={true}
                value={payment_plan.apple_id}
                onChange={(_,val) => this._handleChange('apple_id', val)}
                errorText={ (validationErrors.apple_id || '') }
              />
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.android_id') }
                fullWidth={true}
                multiLine={true}
                value={payment_plan.android_id}
                onChange={(_,val) => this._handleChange('android_id', val)}
                errorText={ (validationErrors.android_id || '') }
              />
            </FormGroup>
            <FormGroup>
              <Row>
                <Col sm={2}>
                  <ControlLabel>
                    { I18n.t('payment_plan.fields.toggle_active') }:
                  </ControlLabel>
                </Col>
                <Col sm={10}>
                  <Toggle
                    toggled={payment_plan.active}
                    onToggle={() => this._handleChange('active', !payment_plan.active)}
                  />
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <TextField
                floatingLabelText={ I18n.t('payment_plan.fields.order') }
                fullWidth={true}
                value={payment_plan.order || ''}
                onChange={(_,val) => this._handleChange('order', val)}
                errorText={ (validationErrors.order || '') }
              />
            </FormGroup>

            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton
                type='submit'
                primary={true}
                className='pull-right'
                label={ I18n.t('actions.submit') }
                disabled={isLoading}
              />
            </Col>
            <Clearfix />
          </form>
        </Paper>
    )
  }
}

export default connect(state => state)(PaymentPlanForm)
