import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  CircularProgress
} from 'material-ui';
import {
  ActionVisibility,
  ImageEdit,
  ActionDelete
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../common/sorting_th';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import { all, destroy } from '../../services/payment_plans';

class PaymentPlans extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10,
      ads: 'true',
    },
    payment_plans: [],
    count: 0,
    showConfirm: false
  };

  componentWillMount() {
    this._retrievePaymentPlans();
  }

  _retrievePaymentPlans = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        payment_plans: res.payment_plans,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrievePaymentPlans);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrievePaymentPlans);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrievePaymentPlans)
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  handleDelete = () => {
    const { selectedRecord } = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrievePaymentPlans();
      this.closeConfirm();
    });
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { payment_plans, showConfirm, count } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <li>{ I18n.t('payment_plan.header') }</li>
            </ul>
          </Col>
          <Col sm={6} style={{textAlign: 'right'}}>
            <RaisedButton
                href='#/payment_plan/new'
                className='pull-right'
                primary={true}
                label={ I18n.t('payment_plan.new') }
            />
          </Col>
        </Row>

        <Row>
          <Col sm={6}>
            <Pagination
                selectComponentClass={Select}
                onChange={this.handlePageChange}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={['10','20','50']}
                pageSize={per_page}
                onShowSizeChange={this.handleShowSizeChange}
                current={page}
                total={count}
                locale={en_US}
            />
          </Col>
          <Col sm={6}>

          </Col>
        </Row>

        <Filters
          columns={[
            {label: I18n.t('payment_plan.fields.name'), key: 'text', type: 'string' },
          ]}
          update={this.updateFilters}
        />

        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
                <TableHeaderColumn>
                  <SortingTh
                      update={this.updateFilters}
                      column={ I18n.t('payment_plan.fields.name') }
                  >
                    { I18n.t('payment_plan.fields.name') }
                  </SortingTh>
                </TableHeaderColumn>
                <TableHeaderColumn>{ I18n.t('payment_plan.fields.description') }</TableHeaderColumn>
                <TableHeaderColumn>{ I18n.t('payment_plan.fields.price') }</TableHeaderColumn>
                <TableHeaderColumn>{ I18n.t('payment_plan.fields.classroom_count') }</TableHeaderColumn>
                <TableHeaderColumn>{ I18n.t('payment_plan.fields.active') }</TableHeaderColumn>
              <TableHeaderColumn>

              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              payment_plans.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{ item.name  || '-'}</TableRowColumn>
                    <TableRowColumn>{ item.description  || '-'}</TableRowColumn>
                    <TableRowColumn>{ item.price  }</TableRowColumn>
                    <TableRowColumn>{ item.classroom_count  }</TableRowColumn>
                    <TableRowColumn>{ item.active ? <span style={{color: "#009900"}}>Active</span> : <span style={{color: "#c62828"}}>No</span> }</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'right'}}>

                      <IconButton
                        onTouchTap={() => location.hash = `#/payment_plan/${item.id}`}
                      >
                        <ActionVisibility color={palette.primary1Color} />
                      </IconButton>

                      <IconButton
                        onTouchTap={() => location.hash = `#/payment_plan/${item.id}/edit`}
                      >
                        <ImageEdit color={palette.accent1Color} />
                      </IconButton>

                      <IconButton
                        onTouchTap={this.prepareToDestroy.bind(this,item)}
                      >
                        <ActionDelete color="#c62828" />
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
                onTouchTap={this.closeConfirm}
                label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
                secondary={true}
                onTouchTap={this.handleDelete}
                label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('you_are_going_to_remove') } payment plan.
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

PaymentPlans.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(PaymentPlans)
