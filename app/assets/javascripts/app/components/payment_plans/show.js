import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  FormGroup,
  ControlLabel,
  Row,
  Col,
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Toggle
} from 'material-ui';
import { paperStyle } from '../common/styles';
import { show } from '../../services/payment_plans';

const styles = {
    gridList: {
        display: 'flex',
        flexWrap: 'nowrap',
        overflowX: 'auto',
    },
};

class PaymentPlan extends Component {
  state = {
    payment_plan: {
      name: '',
      description: '',
      price: '',
      classroom_count: 0,
      active: false,
      apple_id: '',
      android_id: '',
      type_name: '',
      order: 0
    },
  };

  componentWillMount() {
    this._retrievePaymentPlan();
  }

  _retrievePaymentPlan = () => {
    const { id } = this.props.params;
    show(id).success(res => {
      this.setState({
        payment_plan: res.payment_plan
      })
    })
  };

  render() {
    const { payment_plan } = this.state;

    return (
        <Paper style={paperStyle} zDepth={1}>
          <Row>
            <Col sm={6}>
              <ul className="breadcrumb">
                <li>{ I18n.t('payment_plan.label') }</li>
              </ul>
            </Col>
            <Col sm={6}>
              <RaisedButton
                href='#/payment_plans'
                className='pull-right'
                secondary={true}
                label={ I18n.t('actions.back') }
              />
            </Col>
          </Row>

          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.name') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.name }
            </span>
            <hr/>
          </FormGroup>
          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.description') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.description}
            </span>
            <hr/>
          </FormGroup>
          <br/>
          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.type_name') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.type_name}
            </span>
            <hr/>
          </FormGroup>
          <br/>
          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.price') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.price }
            </span>
            <hr/>
          </FormGroup>
          <br/>
          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.apple_id') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.apple_id }
            </span>
            <hr/>
          </FormGroup>
          <br/>
          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.android_id') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.android_id }
            </span>
            <hr/>
          </FormGroup>
          <br/>
          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.classroom_count') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.classroom_count }
            </span>
            <hr/>
          </FormGroup>
          <FormGroup>
            <Row>
              <Col sm={2}>
                <ControlLabel>
                  { I18n.t('payment_plan.fields.toggle_active') }:
                </ControlLabel>
              </Col>
              <Col sm={10}>
                <Toggle
                  toggled={payment_plan.active}
                />
              </Col>
            </Row>
          </FormGroup>
          <br/>
          <FormGroup>
            <ControlLabel>{ I18n.t('payment_plan.fields.order') }</ControlLabel>
            <br/>
            <span className="form-control-static">
              { payment_plan.order }
            </span>
            <hr/>
          </FormGroup>
        </Paper>
    )
  }
}

export default connect(state => state)(PaymentPlan)
