import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix} from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  RaisedButton,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
} from 'material-ui';
import { ActionVisibility, ActionDelete, ImageEdit, SocialNotificationsActive } from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import Filters from '../common/filters_component';
import { paperStyle } from '../common/styles';
import { all, destroy, remove_selected, send_push } from '../../services/posts';

class Posts extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10,
      ads: false
    },
    posts: [],
    selectedPosts: [],
    selectedPush: [],
    count: 0,
    showConfirm: false,
    showConfirmPush: false
  };

  componentWillMount() {
    this._retrievePosts();
  }

  _retrievePosts = () => {
    const { filters } = this.state;
    all(filters).success(res => {
      this.setState({
        posts: res.posts,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrievePosts);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrievePosts);
  };

  _handleChange = (key, value) => {
    this.setState({	[key]: value })
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  prepareToPush = record => {
    this.setState({
      selectedPush: record,
      showConfirmPush: true
    })
  };



  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrievePosts)
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  closeConfirmPush = () => {
    this.setState({ showConfirmPush: false })
  };

  handleDelete = () => {
    const { selectedRecord } = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrievePosts();
      this.closeConfirm();
    });
  };

  handlePushSend = () => {
    const { selectedPush } = this.state;
    send_push(selectedPush.id).success(res => {
      this._retrievePosts();
      this.closeConfirmPush();
    });
  };

  removeSelected = () => {
    const { selectedPosts } = this.state;
    remove_selected(selectedPosts).success(res => {
      this._retrievePosts();
      this.setState({	selectedPosts: []});
    })
  };

  rowSelection = (rows) => {
    const { posts } = this.state;
    switch (rows) {

      case 'all':
        this._handleChange('selectedPosts', posts.map(i => i.id));
        break;

      case 'none':
        this._handleChange('selectedPosts', []);
        break;

      default:
        if(rows.length !== 0) this._handleChange('selectedPosts', rows.map(i => posts[i].id))
    }
  };

  render() {
    const { isLoading } = this.props.app.main;
    const { posts, showConfirm, showConfirmPush, count } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={6}>
            <ul className="breadcrumb">
              <li>{ I18n.t('post.header') }</li>
            </ul>
          </Col>
          <Col sm={6} style={{textAlign: 'right'}}>
            <RaisedButton
              href='#/post/new'
              className='pull-right'
              primary={true}
              label={ I18n.t('post.new') }
            />
          </Col>
        </Row>

        <Row>
          <Col sm={6}>
            <Pagination
                selectComponentClass={Select}
                onChange={this.handlePageChange}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={['10','20','50']}
                pageSize={per_page}
                onShowSizeChange={this.handleShowSizeChange}
                current={page}
                total={count}
                locale={en_US}
            />
          </Col>
          <Col sm={6}>

          </Col>
        </Row>

        <Filters
          columns={[
            {label: I18n.t('post.fields.text'), key: 'text', type: 'text' },
            {label: I18n.t('post.fields.only_reported'), key: 'reported', type: 'boolean'}
          ]}
          update={this.updateFilters}
        />
        <br/>
        <Row>
          <Col sm={12} style={{textAlign: 'right'}}>
            <RaisedButton
              onClick={(_) => {this.removeSelected()}}
              className='pull-right'
              primary={true}
              label={ I18n.t('post.fields.remove_selected') }
            />
          </Col>
        </Row>
        <br/>
        <Table
          multiSelectable={true}
          onRowSelection={this.rowSelection}>
          <TableHeader adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn> { I18n.t('post.fields.text') } </TableHeaderColumn>
              <TableHeaderColumn> { I18n.t('user.fields.full_name') } </TableHeaderColumn>
              <TableHeaderColumn>{I18n.t('post.fields.likes')}</TableHeaderColumn>
              <TableHeaderColumn> { I18n.t('post.fields.reports') } </TableHeaderColumn>
              <TableHeaderColumn>

              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            {
              posts.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{ item.is_admin_created ?
                      <span style={{color: "#009900"}}> { item.text }</span> :  item.text
                     }</TableRowColumn>
                    <TableRowColumn>{ item.user.full_name }</TableRowColumn>
                    <TableRowColumn>{ item.likes_count }</TableRowColumn>
                    <TableRowColumn>{
                      item.reports_count >0 ?
                        <span style={{color: "#c62828"}}> { item.reports_count }</span> : <span style={{color: "#009900"}}>No</span>
                    }
                    </TableRowColumn>
                    <TableRowColumn style={{textAlign: 'right'}}>
                      <IconButton onTouchTap={() => location.hash = `#/post/${item.id}`}>
                        <ActionVisibility color={palette.primary1Color} />
                      </IconButton>

                      <IconButton disabled={!item.user.is_owner } onTouchTap={() => location.hash = `#/post/${item.id}/edit`}>
                        <ImageEdit  color={palette.accent1Color} />
                      </IconButton>
                      <IconButton disabled={!item.is_mentor } onTouchTap={this.prepareToPush.bind(this,item)}>
                        <SocialNotificationsActive  color="#388e3c" />
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this,item)}>
                        <ActionDelete color="#c62828" />
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
                onTouchTap={this.closeConfirm}
                label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
                secondary={true}
                onTouchTap={this.handleDelete}
                label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('you_are_going_to_remove') } post.
        </Dialog>
        <Clearfix/>
        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirmPush}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handlePushSend}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirmPush}
          onRequestClose={this.closeConfirmPush}
        >
          { I18n.t('you_are_going_to_send_push') } post.
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

Posts.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Posts)
