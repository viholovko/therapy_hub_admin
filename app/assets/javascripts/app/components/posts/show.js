import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  FormGroup,
  ControlLabel,
  Row,
  Col,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  Avatar,
  IconButton,
  Tabs,
  Tab,
  FlatButton,
  Dialog,
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import { ActionVisibility, ActionDelete, SocialNotificationsActive } from 'material-ui/svg-icons';
import { paperStyle } from '../common/styles';
import {destroy, send_push, show} from '../../services/posts';
import PropTypes from "prop-types";
import GeneralTabComponent from './general_tab.component';
import TabComponent from "../common/tabs_components/tab_table.component";
import MediaComponent from '../common/media.component';

class Post extends Component {
  state = {
    post: {
      is_admin_created: false,
      attachments: [],
      reports: [],
      likes: [],
      comments: [],
      attachments: [],
      cms_notifications: [],
      images: [],
      link: {},
      user: {
        id: 0,
        first_name: '',
        last_name:  '',
        avatar: '',
        role: '',
      },
    },
    showConfirm: false,
    showConfirmPush: false
  };

  componentWillMount() {
    this._retrievePost();
  }

  _retrievePost = () => {
    const { id } = this.props.params;
    show(id).success(res => {
      this.setState({
        post: res.post
      })
    })
  };

  prepareToDestroy = record => {
    this.setState({
      showConfirm: true
    })
  };

  prepareToPush = record => {
    this.setState({
      showConfirmPush: true
    })
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  closeConfirmPush = () => {
    this.setState({ showConfirmPush: false })
  };


  handleDelete = () => {
    const { id } = this.props.params;
    destroy(id).success(res => {
      this.closeConfirm();
      this.props.history.push('/posts')
    });
  };

  handlePushSend = () => {
    const { id } = this.props.params;
    send_push(id).success(res => {
      this.closeConfirmPush();
      this.props.history.push('/posts')
    });
  };

  render() {
    const { post, showConfirm, showConfirmPush } = this.state;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={9}>
            <ul className="breadcrumb">
              <li>{ I18n.t('post.label') }</li>
            </ul>
          </Col>
          <Col sm={3}>
            <IconButton onTouchTap={this.prepareToDestroy.bind(post.id)}>
              <ActionDelete color="#c62828" />
            </IconButton>
            <IconButton disabled={!post.is_mentor }  onTouchTap={this.prepareToPush.bind(post.id)}>
              <SocialNotificationsActive  color="#388e3c" />
            </IconButton>
            <RaisedButton
              href='#/posts'
              className='pull-right'
              secondary={true}
              label={ I18n.t('actions.back') }
            />
          </Col>
        </Row>
        <Card>
          <CardHeader
            title={ I18n.t('admin_panel.cards.user') }
            actAsExpander={true}
            showExpandableButton={true}
          />
          <CardText>
            <FormGroup>
              <Row>
                <Col sm={12}>
                  <Col xs={6} md={2}>
                    <Avatar src={post.user.avatar} size={50}/>
                  </Col>
                  <Col sm={8}>
                    <Row>
                      <Col sm={12}>
                        <Col sm={6}>
                          <Row>
                            <Col sm={5}>{I18n.t('user.fields.first_name')}</Col>
                            <Col sm={6}><ControlLabel>{post.user.first_name || '-'}</ControlLabel></Col>
                          </Row>
                        </Col>
                        <Col sm={6}>
                          <Row>
                            <Col sm={5}>{I18n.t('user.fields.last_name')}</Col>
                            <Col sm={6}><ControlLabel>{post.user.last_name || '-'}</ControlLabel></Col>
                          </Row>
                        </Col>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={6} md={2}>
                    <IconButton onTouchTap={() => location.hash = `#/users/${post.user.role}/${post.user.id}`}>
                      <ActionVisibility color={palette.primary1Color}/>
                    </IconButton>
                  </Col>
                </Col>
              </Row>
            </FormGroup>
          </CardText>
        </Card>
        <br/>
        <Tabs>
          <Tab label={I18n.t('post.fields.general')}>
            <GeneralTabComponent
              post = {post}
            />
          </Tab>
          <Tab label={I18n.t('post.fields.comments')}>
            <TabComponent
              data={post.comments}
              labels={['Avatar', 'Full name', 'Message', 'Created at' ]}
              keys={  ['avatar', 'full_name', 'message', 'created_at']}
              not_include_to_sub_array={['created_at', 'message']}
              sub_array_name='user'
              palette={palette}
            />
          </Tab>
          <Tab label={I18n.t('post.fields.likes')}>
            <TabComponent
              data={post.likes}
              labels={['Avatar', 'Full name', 'Created at']}
              keys={  ['avatar', 'full_name', 'created_at']}
              not_include_to_sub_array={['created_at']}
              sub_array_name='user'
              palette={palette}
            />
          </Tab>
          <Tab label={I18n.t('post.fields.reports')}>
            <TabComponent
              data={post.reports}
              labels={['Avatar', 'Full name', 'Report type', 'Created at']}
              keys={  ['avatar', 'full_name', 'report_type', 'created_at' ]}
              not_include_to_sub_array={['created_at', 'report_type']}
              sub_array_name='user'
              palette={palette}
            />
          </Tab>
          <Tab label={I18n.t('post.fields.attachments')}>
            <MediaComponent
              value={post.attachments}
            />
          </Tab>
          <Tab label={I18n.t('post.fields.notifications')}>
            <Table style={{ tableLayout: 'auto' }} fixedHeader={false}>
              <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                <TableRow >
                  <TableHeaderColumn>{I18n.t('post.fields.created_at')}</TableHeaderColumn>
                  <TableHeaderColumn>{I18n.t('post.fields.created_at')}</TableHeaderColumn>
                  <TableHeaderColumn>{I18n.t('post.fields.notification_send_by')}</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {
                  post.cms_notifications.map(item => {
                    return (
                      <TableRow key={item.id}>
                        <TableRowColumn>{ post.text }</TableRowColumn>
                        <TableRowColumn>{ post.created_at }</TableRowColumn>
                        <TableRowColumn>{ item.created_at }</TableRowColumn>
                      </TableRow>
                    )

                  })
                }
              </TableBody>
            </Table>
          </Tab>
        </Tabs>
        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('you_are_going_to_remove') } post.
        </Dialog>
        <Clearfix/>

        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirmPush}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handlePushSend}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirmPush}
          onRequestClose={this.closeConfirmPush}
        >
          { I18n.t('you_are_going_to_send_push') } post.
        </Dialog>
      </Paper>
    )
  }
}

Post.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Post)
