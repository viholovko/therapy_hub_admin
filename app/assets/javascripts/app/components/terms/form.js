import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
  CircularProgress, Tabs, Tab
} from 'material-ui';
import FroalaEditor from 'react-froala-wysiwyg'
import { paperStyle } from '../common/styles';
import { show, upsert } from '../../services/terms';

class TermForm extends Component {
    state = {
      term: {
        body: '',
        body_android: ''
      },
      validationErrors: {},
    };

  componentWillMount() {
    this._retrieveTerm();
  }

  _retrieveTerm = () => {
    show().success(res => {
      this.setState({
        term: res.term
      })
    })
  };

  _handleChange = (key,value) => {
    const { term } = this.state;

    this.setState({
      term: {
          ...term,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    }, () => {
      // after state update
    })
  };

  _handleSubmit = event => {
    event.preventDefault();
    const { term } = this.state;
    upsert(term)
      .success(res => {
        location.hash = '#/terms';
      })
      .progress(value => {
        this.setState({ progress: value })
      })
      .error(res => {
          this.setState({
          validationErrors: res.validation_errors
        })
      })
  };


  render() {
    const { isLoading } = this.props.app.main;
    const { term, progress, validationErrors } = this.state;

    return (
        <Paper style={paperStyle} zDepth={1}>
          <Row>
            <Col sm={6}>
              <ul className="breadcrumb">
                <li><a href='#/terms'>{ I18n.t('term.header') }</a></li>
              </ul>
            </Col>
            <Col sm={6}>

            </Col>
          </Row>

          <br/>
          <form onSubmit={this._handleSubmit}>
            <FormGroup>
              <Tabs>
                <Tab label='Body IOS' >
                  <FroalaEditor
                    tag="div"
                    model={term.body}
                    onModelChange={val => this._handleChange('body', val)}
                    config={{imageUploadURL: '/admin/attachments/Term'}}
                  />
                </Tab>
                <Tab label='Body Android' >
                  <FroalaEditor
                    tag="div"
                    model={term.body_android}
                    onModelChange={val => this._handleChange('body_android', val)}
                    config={{imageUploadURL: '/admin/attachments/Term'}}
                  />
                </Tab>
              </Tabs>
            </FormGroup>
            <br/>
            <Col sm={4} smOffset={8} className="text-right">
              <br/>
              <CircularProgress className={isLoading && progress > 0 ? 'loading-spinner' : 'hidden'} mode="determinate" value={progress} size={36} />
              <RaisedButton
                type='submit'
                primary={true}
                className='pull-right'
                label={ I18n.t('actions.submit') }
                disabled={isLoading}
              />
            </Col>
            <Clearfix />
          </form>
        </Paper>
    )
  }
}

export default connect(state => state)(TermForm)
