import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from "react-router";

import BaseLayout from "./components/layouts/Base";
import DashboardLayout from "./components/layouts";
import injectTapEventPlugin from 'react-tap-event-plugin';
import DashboardOverviewPage from "./components/pages/Dashboard/Overview";
import LoginPage from "./components/pages/Login";
import EmailSender from './components/pages/EmailSender';
import SystemSettingsForm from './components/pages/SystemSettings';
import { store, history } from './create_store';
import { check } from './services/sessions';

// generated components paths
import Classrooms from './components/classrooms';
import Classroom from './components/classrooms/show';
import ClassroomForm from './components/classrooms/form';
import Posts from './components/posts';
import Post from './components/posts/show';
import PostForm from './components/posts/form';
import Ads from './components/ads';
import Ad from './components/ads/show';
import AdForm from './components/ads/form';
import PaymentPlans from './components/payment_plans';
import PaymenPlan from './components/payment_plans/show';
import PaymentPlanForm from './components/payment_plans/form';
import TermForm from './components/terms/form';
import PrivacyPolicyForm from './components/privacy_policy/form';
import AboutForm from './components/about/form';
import Countries from './components/countries';
import CountryForm from './components/countries/form';
import Users from './components/pages/users/index';
import UserForm from './components/pages/users/form';
import User from './components/pages/users/show';
import FrequentlyAskedQuestions from './components/frequently_asked_questions';
import FrequentlyAskedQuestion from './components/frequently_asked_questions/show';
import FrequentlyAskedQuestionForm from './components/frequently_asked_questions/form';



window.onload = function () {
  injectTapEventPlugin();
  ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route name="base" path="/" component={BaseLayout}>
        <Route name="dashboard" path='' component={DashboardLayout} onEnter={check}>
          <IndexRoute name="dashboard.overview" component={DashboardOverviewPage} />
          <Route name='email_sender' path='email_sender' component={EmailSender} />
          <Route name='system_settings' path='system_settings' component={SystemSettingsForm} />
          <Route name='classrooms' path='classrooms' component={Classrooms} />
          <Route name='new_classroom' path='classroom/new' component={ClassroomForm} />
          <Route name='edit_classroom' path='classroom/:id/edit' component={ClassroomForm} />
          <Route name='show_classroom' path='classroom/:id' component={Classroom} />
          <Route name='posts' path='posts' component={Posts} />
          <Route name='new_post' path='post/new' component={PostForm} />
          <Route name='edit_post' path='post/:id/edit' component={PostForm} />
          <Route name='show_post' path='post/:id' component={Post} />
          <Route name='ads' path='ads' component={Ads} />
          <Route name='new_ad' path='ad/new' component={AdForm} />
          <Route name='edit_ad' path='ad/:id/edit' component={AdForm} />
          <Route name='show_ad' path='ad/:id' component={Ad} />
          <Route name='payment_plans' path='payment_plans' component={PaymentPlans} />
          <Route name='new_payment_plan' path='payment_plan/new' component={PaymentPlanForm} />
          <Route name='edit_payment_plan' path='payment_plan/:id/edit' component={PaymentPlanForm} />
          <Route name='show_payment_plan' path='payment_plan/:id' component={PaymenPlan} />
          <Route name='terms' path='terms' component={TermForm} />
          <Route name='privacy_policy' path='privacy_policy' component={PrivacyPolicyForm} />
          <Route name='about' path='about' component={AboutForm} />
          <Route name='countries' path='countries' component={Countries} />
          <Route name='new_country' path='country/new' component={CountryForm} />
          <Route name='edit_country' path='country/:id/edit' component={CountryForm} />
          <Route name='users' path='users/:role' component={Users} />
          <Route name='new_user' path='users/:role/new' component={UserForm} />
          <Route name='edit_user' path='users/:role/:id/edit' component={UserForm} />
          <Route name='show_user' path='users/:role/:id' component={User} />
          <Route name='frequently_asked_questions' path='frequently_asked_questions' component={FrequentlyAskedQuestions} />
          <Route name='new_frequently_asked_question' path='frequently_asked_question/new' component={FrequentlyAskedQuestionForm} />
          <Route name='edit_frequently_asked_question' path='frequently_asked_question/:id/edit' component={FrequentlyAskedQuestionForm} />
          <Route name='show_frequently_asked_question' path='frequently_asked_question/:id' component={FrequentlyAskedQuestion} />
        </Route>
        <Route name="login" path='login' component={LoginPage} />
      </Route>
      <Route path="*" component={LoginPage} />
    </Router>
  </Provider>,
  document.getElementById('content')
  );

};
