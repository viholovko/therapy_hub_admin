import http from './http';

export function show() {
  let url = '/admin/about.json';
  return http.get({url})
}
export function upsert(model){
  let body = new FormData();

  body.append('about[body]', model.body || '' );
  body.append('about[body_android]', model.body_android || '' );

  if(model.id){
    return http.put({ url:`/admin/about/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/about', body })
  }
}