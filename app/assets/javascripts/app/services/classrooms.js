import http from './http';

export function all(filters) {
  let url = '/admin/classrooms.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function show(id){
  return http.get({url:`/admin/classrooms/${id}.json`})
}

export function remove_user_from_classroom(params){
  let body = new FormData();

  console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!');
  console.log(params);
  console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!');

  if (params.role_classroom === 'parent') {
    body.append('parent_id', params.id ? params.id : 0);
  } else if (params.role_classroom === 'student'){
    body.append('student_id', params.id ? params.id : 0);
  } else if (params.role_classroom === 'teacher'){
    body.append('teacher_id', params.id ? params.id : 0);
  } else if (params.role_classroom === 'caseworker'){
    body.append('caseworker_id', params.id ? params.id : 0);
  }

  return http.put({ url:`/admin/classrooms/${params.classroom_id}/remove_user`, body })
}