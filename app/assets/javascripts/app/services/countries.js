import http from './http';

export function all(filters) {
  let url = '/admin/countries.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model) {
  let body = new FormData();

  body.append('country[name]', model.name || '')
  body.append('country[alpha3_code]', model.alpha3_code || '')
  body.append('country[alpha2_code]', model.alpha2_code || '')
  body.append('country[phone_code]', model.phone_code || '')
  body.append('country[numeric_code]', model.numeric_code || '')

  if(model.flag && model.flag.file) {
    body.append('country[flag]', model.flag.file );
  }

  if (model.selected !== undefined) {
    body.append('country[selected]', model.selected)
  }

  if(model.id){
    return http.put({ url:`/admin/countries/${model.id}`, body });
  }else{
    return http.post({ url:`/admin/countries`, body });
  }
}

export function show(id){
  return http.get({url:`/admin/countries/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/countries/${id}`})
}

export function activate(model) {
  let body = new FormData();

  if (model.selected !== undefined) {
    body.append('country[selected]', model.selected)
  }

  return http.put({ url:`/admin/countries/${model.id}`, body });
}