import http from './http';

export function all(filters) {
  let url = '/admin/events.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function show(id){
  return http.get({url:`/admin/events/${id}.json`})
}