import http from './http';

export function all(filters) {
  let url = '/admin/frequently_asked_questions.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function upsert(model){
  let body = new FormData();

  body.append('title', model.title || '' );
  body.append('description', model.description || '' );
  body.append('content', model.content || '' );


  if(model.id){
    return http.put({ url:`/admin/frequently_asked_questions/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/frequently_asked_questions', body })
  }
}

export function show(id){
  return http.get({url:`/admin/frequently_asked_questions/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/frequently_asked_questions/${id}`})
}
