import http from './http';

export function all(filters) {
  let url = '/admin/payment_plans.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}
export function upsert(model){
  let body = new FormData();

  body.append('payment_plan[name]', model.name || '' );
  body.append('payment_plan[description]', model.description || '' );
  body.append('payment_plan[type_name]', model.type_name || '' );
  body.append('payment_plan[price]', model.price || '0' );
  body.append('payment_plan[classroom_count]', model.classroom_count || 0 );
  body.append('payment_plan[active]', model.active || false);
  body.append('payment_plan[apple_id]', model.apple_id || '');
  body.append('payment_plan[android_id]', model.android_id || '');
  body.append('payment_plan[order]', model.order || 0);

  if(model.id){
    return http.put({ url:`/admin/payment_plans/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/payment_plans', body })
  }
}

export function show(id){
  return http.get({url:`/admin/payment_plans/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/payment_plans/${id}`})
}
