import http from './http';

export function all(filters) {
    let url = '/admin/piechart_data.json?';
    Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
    return http.get({url})
}
