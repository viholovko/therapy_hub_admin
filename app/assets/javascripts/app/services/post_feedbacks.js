import http from './http';

export function all(filters) {
  let url = '/post_feedbacks.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}
export function upsert(model){
  let body = new FormData();


  body.append('post_feedback[post_id]', model.post_id || null );
  body.append('post_feedback[type]', model.type || '' );

  if(model.id){
    return http.put({ url:`/post_feedbacks/${model.id}`, body })
  }else{
    return http.post({ url:'/post_feedbacks', body })
  }
}

export function show(id){
  return http.get({url:`/post_feedbacks/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/post_feedbacks/${id}`})
}
