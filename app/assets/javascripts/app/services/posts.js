import http from './http';

export function all(filters) {
  let url = '/admin/posts.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}
export function upsert(model){
  let body = new FormData();

  if (model.type === 'ads') {
    body.append('post[ads]', model.type || false );
    body.append('post[is_admin_created]', true );
  }

  body.append('post[text]', model.text || '' );
  body.append('post[is_admin_created]', true );
  if(model.link && model.link.url){
    body.append('post[link_attributes][url]', model.link.url);
    body.append('post[link_attributes][id]', model.link.id ? model.link.id : '');
  }
  let index = 0;
  if(model.photos){
      model.photos.forEach(item => {
        if (item && !item.id) {
          body.append(`post[attachments_attributes][${index}][file]`, item );
          body.append(`post[attachments_attributes][${index}][attachable_type]`, 'Post' );
          body.append(`post[attachments_attributes][${index}][attachment_type]`, 'photo' );
          body.append(`post[attachments_attributes][${index}][user_id]`, '' );
        }else {
          body.append(`post[attachments_attributes][${index}][id]`, item.id);
          body.append(`post[attachments_attributes][${index}][_destroy]`, item._destroy || false);
        }
        index += 1;
      })
  }
  if (model.videos){
    model.videos.forEach(item => {
      if (item && !item.id) {
        body.append(`post[attachments_attributes][${index}][file]`, item );
        body.append(`post[attachments_attributes][${index}][attachable_type]`, 'Post' );
        body.append(`post[attachments_attributes][${index}][attachment_type]`, 'video' );
        body.append(`post[attachments_attributes][${index}][user_id]`, '' );
      }else {
        body.append(`post[attachments_attributes][${index}][id]`, item.id);
        body.append(`post[attachments_attributes][${index}][_destroy]`, item._destroy || false);
      }
      index += 1;
    })
  }

  if(model.id){
    return http.put({ url:`/admin/posts/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/posts', body })
  }
}

export function show(id){
  return http.get({url:`/admin/posts/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/posts/${id}`})
}

export function send_push(id){
  return http.post({url:`/admin/posts/${id}/send_push`});
}

export function remove_selected(ids) {
  let body = new FormData();
  body.append('selected_ids', ids || '' );
  return http.delete({url: '/admin/posts/remove_selected', body})
}
