import http from './http';

export function show() {
  let url = '/admin/privacy_policy.json';
  return http.get({url})
}
export function upsert(model){
  let body = new FormData();

  body.append('privacy_policy[body]', model.body || '' );
  body.append('privacy_policy[body_android]', model.body_android || '' );

  if(model.id){
    return http.put({ url:`/admin/privacy_policy/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/privacy_policy', body })
  }
}