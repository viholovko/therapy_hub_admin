import http from './http';

export function all(filters) {
  let url = '/admin/tasks.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}

export function show(id){
  return http.get({url:`/admin/tasks/${id}.json`})
}