import http from './http';

export function show() {
  let url = '/admin/terms.json';
  return http.get({url})
}
export function upsert(model){
  let body = new FormData();

  body.append('terms[body]', model.body || '' );
  body.append('terms[body_android]', model.body_android || '' );

  if(model.id){
    return http.put({ url:`/admin/terms/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/terms', body })
  }
}