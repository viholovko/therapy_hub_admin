class Admin::AboutController < Admin::BaseController
  def create
    @about = About.first_or_create

    if @about.update_attributes about_params
      render json: { message: I18n.t('about.messages.success_upsert') }
    else
      render json: {validation_errors: @about.errors }, status: :unprocessable_entity
    end
  end

  def index
    @about = About.first_or_create
  end

  private

  def about_params
    params.require(:about).permit :body, :body_android
  end
end
