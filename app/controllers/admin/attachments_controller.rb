class Admin::AttachmentsController < Admin::BaseController

  load_and_authorize_resource :attachment
  skip_before_action :verify_authenticity_token

  def create
    @attachment = Attachment.create attachment_params
    render json: { link: paperclip_url(@attachment.file) }
  end

  def destroy
    @attachment.destroy
    render json: { ok: true }
  end

  private

  def attachment_params
    allowed_params = params.permit(:file, :entity_type)
    {
        file: allowed_params[:file],
        attachable_type: allowed_params[:entity_type]
    }
  end
end
