class Admin::ClassroomsController < Admin::BaseController

  load_and_authorize_resource :classroom

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Classroom.search_query_admin params

    count_query = query.clone.project('COUNT(*)')

    @classrooms = Classroom.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Classroom.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def remove_user
    if @classroom.remove_users_from_admin(classroom_params, user_params)
      render json: {message: 'Users have been removed from this classroom'}
    else
      render json: {errors: @classroom.errors.map{|k,v| v}}, status: :bad_request
    end
  end

  private

  def classroom_params
    params.permit :student_id, :parent_id, :teacher_id, :caseworker_id
  end

  def user_params
    params.permit :student_id, :parent_id, :teacher_id, :caseworker_id
    if params[:student_id].present?
      params[:student_id]
    elsif params[:parent_id].present?
      params[:parent_id]
    elsif params[:teacher_id].present?
      params[:teacher_id]
    elsif params[:caseworker_id].present?
      params[:caseworker_id]
    end

  end



end
