class Admin::CountriesController < Admin::BaseController

  load_and_authorize_resource :country

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Country.search_query params
    count_query = Country.search_query params.merge(count: true)

    @countries = Country.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Country.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @country = Country.new country_params

    if @country.save
      render json: { message: I18n.t('country.messages.success_upsert') }
    else
      render json: {validation_errors: @country.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @country.update_attributes country_params
      render json: { message: I18n.t('country.messages.success_upsert') }
    else
      render json: { validation_errors: @country.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @country.destroy
    render json: {ok: true}
  end

  def show

  end

  private

  def country_params
    params.require(:country).permit :name, :alpha3_code, :alpha2_code, :phone_code, :numeric_code, :flag, :selected
  end

end
