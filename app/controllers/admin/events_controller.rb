class Admin::EventsController < Admin::BaseController

  load_and_authorize_resource :event

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Event.search_query params
    count_query = Event.search_query params.merge count: true

    @events = Event.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Event.find_by_sql(count_query.to_sql).first['count']
  end
end
