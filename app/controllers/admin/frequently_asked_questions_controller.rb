class Admin::FrequentlyAskedQuestionsController < Api::V1::BaseController
  load_and_authorize_resource :frequently_asked_question

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = FrequentlyAskedQuestion.search_query params
    count_query = FrequentlyAskedQuestion.search_query params.merge count: true

    @frequently_asked_questions = FrequentlyAskedQuestion.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = FrequentlyAskedQuestion.find_by_sql(count_query.to_sql).first['count']
  end

  def show; end

  def create
    if @frequently_asked_question.save
      render json: {message: 'Frequently Asked Question successfully saved.'}
    else
      render json: {errors: @frequently_asked_question.errors.map{|k, v| v}}, status: :bad_request
    end
  end

  def destroy
    @frequently_asked_question.destroy
    render json: { message: 'Frequently Asked Question successfully removed.' }
  end

  def update
    if @frequently_asked_question.update_attributes frequently_asked_question_params
      render json: { message: 'Frequently Asked Question successfully updated.' }
    else
      render json: { errors: @frequently_asked_question.errors.map{|k, v| v} }, status: :bad_request
    end
  end


  def frequently_asked_question_params
    allowed_params = params.permit(:title, :description, :content)
    allowed_params[:user_id] = current_user.id

    allowed_params
  end

end