class Admin::PaymentPlansController < Admin::BaseController
  load_and_authorize_resource :payment_plan

  def index
    query = PaymentPlan.query
    @payment_plans = PaymentPlan.find_by_sql(query.to_sql)
  end

  def create
    if @payment_plan.save
      render json: {message: 'Payment Plan is created'}
    else
      render json: {validation_errors: @payment_plan.errors}, status: :unprocessable_entity
    end
  end

  def update
    if  @payment_plan.update_attributes payment_plan_params
      render json: {message: 'Payment Plan is updated'}
    else
      render json: { validation_errors: @payment_plan.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @payment_plan.destroy
    render json: { message: 'Payment Plan is destroyed' }
  end

  def show
  end

  private

  def payment_plan_params
    allowed_params = params.require(:payment_plan).permit :name, :description, :type_name,
                                                          :price, :classroom_count, :active,
                                                          :apple_id, :android_id, :order
    allowed_params
  end

end
