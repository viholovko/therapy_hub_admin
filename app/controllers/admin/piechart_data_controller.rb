class Admin::PiechartDataController < Admin::BaseController
  def index
      count_user_type = User.find_by_sql("SELECT count(case when roles.name = 'admin' then 1 else null end) as admins,
                                       count(case when roles.name = 'student' then 1 else null end) as students,
                                       count(case when roles.name = 'teacher' then 1 else null end) as teachers,
                                       count(case when roles.name = 'parent' then 1 else null end) as parents,
                                       count(case when roles.name = 'mentor' then 1 else null end) as mentors,
                                       count(case when roles.name = 'not_upgraded' then 1 else null end) as not_upgradeds
                               FROM users JOIN roles ON users.role_id = roles.id")
      count_post_type = Post.find_by_sql("SELECT count(CASE WHEN ads = FALSE
                                                        THEN 1
                                                        ELSE NULL END) AS posts,
                                                 count(CASE WHEN ads = TRUE
                                                        THEN 1
                                                        ELSE NULL END) AS adss
                                          FROM posts")
      @data = {}
      @data[:post] =         count_post_type[0]['posts']
      @data[:ads] =          count_post_type[0]['adss']
      @data[:admin] =        count_user_type[0]['admins']
      @data[:student] =      count_user_type[0]['students']
      @data[:teacher] =      count_user_type[0]['teachers']
      @data[:parent] =       count_user_type[0]['parents']
      @data[:mentor] =       count_user_type[0]['mentors']
      @data[:not_upgraded] = count_user_type[0]['not_upgradeds']

    if params[:value] == '1'
      to_date = DateTime.now.strftime("%Y-%m-%d")
      from_date =(DateTime.now-5.day).strftime("%Y-%m-%d")
    elsif params[:value] == '2'
      to_date = DateTime.now.strftime("%Y-%m-%d")
      from_date =(DateTime.now-7.day).strftime("%Y-%m-%d")
    elsif params[:value] == '3'
      to_date = DateTime.now.strftime("%Y-%m-%d")
      from_date =(DateTime.now-1.month).strftime("%Y-%m-%d")
    elsif params[:value] == '4'
      f_date = params[:from_date].to_date
      t_date = params[:to_date].to_date

      if f_date<t_date
        to_date = t_date.strftime("%Y-%m-%d")
        from_date =f_date.strftime("%Y-%m-%d")
      elsif f_date>t_date
        to_date = f_date.strftime("%Y-%m-%d")
        from_date =t_date.strftime("%Y-%m-%d")
      else
        to_date = t_date.strftime("%Y-%m-%d")
        from_date =f_date.strftime("%Y-%m-%d")
      end
    end
      statisticDatas = ActiveRecord::Base.connection.execute("SELECT name ,(SELECT count(DISTINCT (user_id)) FROM statistics WHERE created_at BETWEEN g.name AND g.name+1 AND activity_type=0) AS login,
                                                      (SELECT count(DISTINCT (user_id)) FROM statistics WHERE created_at BETWEEN g.name AND g.name+1 AND activity_type=1 ) AS registration,
                                                      (SELECT count(DISTINCT (user_id, post_id)) FROM statistics WHERE created_at BETWEEN g.name AND g.name+1 AND activity_type=2) AS advertisement,
                                                      (SELECT count(DISTINCT (user_id, post_id)) FROM statistics WHERE created_at BETWEEN g.name AND g.name+1 AND activity_type=3) AS post
                                       FROM  (SELECT generate_series('#{from_date}'::date, '#{to_date}'::date, '1d')::date) AS g(name)")
    @statisticDatas = []
    statisticDatas.each do |ex|
      @statisticDatas<< ex
    end
  end
end
