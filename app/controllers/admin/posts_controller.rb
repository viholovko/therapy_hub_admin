class Admin::PostsController < Admin::BaseController
  load_and_authorize_resource :post

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1
    query = ''
    count_query = ''

    if params[:reported] == 'true'
      query = Post.reported_posts_query params.merge(current_user: current_user)
      count_query = Post.reported_posts_query params.merge(count: true, current_user: current_user)
    else
      query = Post.search_query params.merge(current_user: current_user)
      count_query = Post.search_query params.merge(count: true, current_user: current_user)
    end
    @posts = Post.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Post.find_by_sql(count_query.to_sql).first['count']
  end

  def create
    if @post.save post_params
      if @post.ads
        render json: {message: 'Advertisement is created'}
      else
        render json: {message: 'Post is created'}
      end
    else
      render json: {validation_errors: @post.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @post.update_attributes update_params
      if @post.ads
        render json: {message: 'Advertisement is updated'}
      else
        render json: {message: 'Post is updated'}
      end
    else
      render json: { validation_errors: @post.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @post.destroy
    if @post.ads
      render json: {message: 'Advertisement is destroyed'}
    else
      render json: {message: 'Post is destroyed'}
    end
  end

  def show
    content_query = Post.comments_likes_query params
    @content = Post.find_by_sql(content_query.to_sql)
    @cms_notifications = CmsNotification.where(post_id: @post.id)
  end

  def remove_selected
    if params[:selected_ids].present?
      Post.where(id: params[:selected_ids].split(',')).destroy_all
      render json: { message: 'Posts were removed' }
    else
      render json: {errors: ['No selected posts']}, status: :unprocessable_entity
    end
  end

  def send_push
    cms_notification = CmsNotification.new post_id: params[:id]
    if cms_notification.save
      SendPushThumbnailerWorker.perform_async params[:id]

      render json: { message: 'Push prepared and will be send' }
    else
      render json: {validation_errors: cms_notification.errors}, status: :unprocessable_entity
    end



  end

  private

  def post_params
    allowed_params = params.require(:post).permit :text, :ads, :is_admin_created,
                                                  attachments_attributes: [:id,
                                                                           :file, :attachable_type,
                                                                           :attachment_type, :user_id
                                                  ],
                                                  link_attributes: [:id, :url]
    allowed_params[:user_id] = current_user.id
    allowed_params[:attachments_attributes]&.each {|k, v| v[:user_id] = current_user.id}
    if allowed_params[:attachments_attributes].present?  && allowed_params[:link_attributes].present?
      allowed_params.delete :link_attributes
    end

    allowed_params
  end

  def update_params
    allowed_params = params.require(:post).permit :text, :link, :ads, tagged_user_ids: [],
                                                  attachments_attributes: [:id, :_destroy, :file,
                                                                           :attachment_type, :user_id,
                                                                           :attachable_type
                                                  ],
                                                  link_attributes: [:id, :url]
    allowed_params[:attachments_attributes]&.each {|k, v| v[:user_id] = current_user.id }

    if allowed_params[:attachments_attributes].present?  && allowed_params[:link_attributes].present?
      allowed_params.delete :link_attributes
    end

    allowed_params
  end

end
