class Admin::PrivacyPolicyController < Admin::BaseController
  def create
    @privacy_policy = PrivacyPolicy.first_or_create

    if @privacy_policy.update_attributes terms_params
      render json: { message: I18n.t('privacy_policy.messages.success_upsert') }
    else
      render json: {validation_errors: @privacy_policy.errors }, status: :unprocessable_entity
    end
  end

  def index
    @privacy_policy = PrivacyPolicy.first_or_create
  end

  private

  def terms_params
    params.require(:privacy_policy).permit :body, :body_android
  end
end
