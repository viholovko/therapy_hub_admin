class Admin::SystemSettingsController < Admin::BaseController

  def index
    @system_settings = SystemSetting.instance
  end

  def create
    @system_settings = SystemSetting.instance

    @system_settings.assign_attributes system_settings_params
    if @system_settings.save
      render json: {message: I18n.t('settings.messages.success_upsert') }
    else
      render json: {validation_errors: @system_settings.errors }, status: :unprocessable_entity
    end
  end

  private

  def system_settings_params
    params.require(:system_setting).permit :ios_push_environment, :ios_push_password, :ios_push_certificate,
                                           :ios_push_apns_host, :android_push_token, :twilio_account_sid,
                                           :twilio_auth_token, :twilio_phone_number, :s3_bucket, :aws_access_key_id,
                                           :aws_secret_access_key, :s3_region
  end

end
