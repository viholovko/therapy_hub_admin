class Admin::TasksController < Admin::BaseController

  load_and_authorize_resource :task

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Task.search_query params
    count_query = Task.search_query params.merge count: true

    @tasks = Task.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Task.find_by_sql(count_query.to_sql).first['count']
  end
end
