class Admin::TermsController < Admin::BaseController
  def create
    @terms = Term.first_or_create

    if @terms.update_attributes terms_params
      render json: { message: I18n.t('term.messages.success_upsert') }
    else
      render json: {validation_errors: @term.errors }, status: :unprocessable_entity
    end
  end

  def index
    @term = Term.first_or_create
  end

  private

  def terms_params
    params.require(:terms).permit :body, :body_android
  end
end
