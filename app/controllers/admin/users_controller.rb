class Admin::UsersController < Admin::BaseController

  load_and_authorize_resource :user

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = User.search_query params.merge(role_ids:[Role.send("get_#{ params[:role] }").id], current_user: current_user, show_admin: true)
    count_query = User.search_query params.merge(role_ids:[Role.send("get_#{ params[:role] }").id], count: true, current_user: current_user, show_admin: true)

    @users = User.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = User.find_by_sql(count_query.to_sql).first['count']
  end

  def create
    @user = User.new user_params

    if @user.save
      render json: { message: I18n.t('user.messages.success_upsert') }
    else
      render json: { validation_errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @user.update_attributes user_params
      render json: { message: I18n.t('user.messages.success_upsert') }
    else
      render json: { validation_errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      render json: {message: I18n.t('user.messages.success_destroy')}
    else
      render json: {errors: @user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    query = User.query_user params
    @user = User.find_by_sql(query.to_sql).first
    classroom_query = Classroom.search_query classroom_params
    @classrooms = Classroom.find_by_sql(classroom_query.to_sql)

  end

  private

  def user_params
    allowed_params = params.permit :email, :password, :password_confirmation, :first_name, :last_name, :avatar, :phone_number, :country_id
    allowed_params[:role_id] = Role.send("get_#{ params[:role] }").id
    allowed_params
  end

  def classroom_params
    allowed_params = params.permit
    allowed_params[:current_user] = @user
    allowed_params[:accepted] = true
    allowed_params


  end

end