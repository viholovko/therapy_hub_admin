class AdsController < ApplicationController

  load_and_authorize_resource :ad

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Ad.search_query params

    count_query = query.clone.project('COUNT(*)')

    @ads = Ad.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Ad.find_by_sql(count_query.to_sql).count
  end

  def create
    @ad = Ad.new ad_params

    if @ad.save
      render json: { message: I18n.t('ad.messages.success_upsert') }
    else
      render json: {validation_errors: @ad.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @ad.update_attributes ad_params
      render json: { message: I18n.t('ad.messages.success_upsert') }
    else
      render json: { validation_errors: @ad.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @ad.destroy
    render json: {ok: true}
  end

  def show

  end

  # related models actions

  private

  def ad_params
    params.require(:ad).permit!
  end

end
