module ApplicationHelper
  def paperclip_url(paperclip_attachment, style = nil)
    return nil unless paperclip_attachment
    if Paperclip::Attachment.default_options[:storage] == :s3
      paperclip_attachment&.instance["#{ paperclip_attachment.name }_file_name"] ? 'http:' + paperclip_attachment.try(:url, style) : nil
    else
      paperclip_attachment&.instance["#{ paperclip_attachment.name }_file_name"] ? "#{ ENV['HOST'] }#{ paperclip_attachment.try(:url, style)}" : nil
    end
  end
end
