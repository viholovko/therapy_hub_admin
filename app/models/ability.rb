class Ability
  include CanCan::Ability

  def initialize(user)

      can :manage, :all   if user.admin?

      not_upgraded_abilities(user)       if user.not_upgraded?

      parent_abilities(user)             if user.parent?

      student_abilities(user)            if user.student?

      teacher_abilities(user)            if user.teacher?

      caseworker_abilities(user)         if user.caseworker?

      mentor_abilities(user)             if user.mentor?
  end


  def not_upgraded_abilities(user)
    can :index, User
    can :show, User

    can :create_friend_invite, User do |other_user|
      !other_user.mentor?
    end
    can [:accept_friend_invite, :reject_friend_invite], Friendship do |friendship|
      friendship.user_id == user.id && friendship.status == 'not_accepted'
    end

    can [:manage], Chat do |chat|
      chat.new_record? || chat.users.include?(user)
    end

    can :create, ChatMessage do |chat_message|
      chat_message.chat.users.include?(user)
    end
    can [:update, :destroy, :read], ChatMessage do |chat_message|
      chat_message.user == user
    end

    can :manage, Attachment do |att|
      att.new_record? || att.user_id == user.id
    end

    can :read, Post
    can :share_post, Post
    can :manage, Post do |post|
      post.new_record? || post.user_id == user.id
    end

    can :read, Comment
    can [:destroy, :update], Comment do |comment|
      comment.user == user
    end
    can :manage, Comment do |comment|
      comment.new_record? || comment.user_id == user.id
    end

    can :manage, Like do |like|
      like.new_record? || like.user_id == user.id
    end

    can :manage, Silent do |silent|
      silent.new_record? || silent.user_id == user.id
    end

    cannot :create, Classroom
    can :index, Classroom
    can [:show, :tasks_and_events,
         :days_for_tasks_and_events], Classroom do |classroom|
      classroom.student_id == user.id && classroom.student_accepted ||
          classroom.parent_id == user.id && classroom.parent_accepted ||
          classroom.teacher_id == user.id && classroom.teacher_accepted ||
          classroom.caseworker_id == user.id && classroom.caseworker_accepted
    end
    can [:accept_invitation_to_classroom,
         :reject_invitation_to_classroom], Classroom do |classroom|
      classroom.student_id == user.id ||
          classroom.parent_id == user.id ||
          classroom.teacher_id == user.id ||
          classroom.caseworker_id == user.id
    end

    can :my_classroom, Classroom do |classroom|
      classroom.owner_id == user.id
    end

    can :read, Task

    can :read, Event

    can :manage, Notification, user_id: user.id

    can :manage, Report do |report|
      report.new_record? || report.user_id == user.id
    end
  end


  def student_abilities(user)
    can :index, User
    can :show, User

    can :create_friend_invite, User do |other_user|
      !other_user.mentor?
    end
    can [:accept_friend_invite, :reject_friend_invite], Friendship do |friendship|
      friendship.user_id == user.id && friendship.status == 'not_accepted'
    end

    can [:follow, :unfollow], User do |other_user|
      !other_user.not_upgraded?
    end

    can :manage, Chat do |chat|
      chat.new_record? || chat.users.include?(user)
    end

    can :create, ChatMessage do |chat_message|
      chat_message.chat.users.include?(user)
    end
    can [:update, :destroy, :read], ChatMessage do |chat_message|
      chat_message.user == user
    end

    can :manage, Attachment do |att|
      att.new_record? || att.user_id == user.id
    end

    can :read, Post
    can :share_post, Post
    can :manage, Post do |post|
      post.new_record? || post.user_id == user.id
    end

    can :read, Comment
    can [:destroy, :update], Comment do |comment|
      comment.user == user
    end
    can :manage, Comment do |comment|
      comment.new_record? || comment.user_id == user.id
    end

    can :manage, Like do |like|
      like.new_record? || like.user_id == user.id
    end

    can :manage, Silent do |silent|
      silent.new_record? || silent.user_id == user.id
    end

    can :create, Classroom
    can :index, Classroom
    can [:update, :destroy], Classroom, owner_id: user.id
    can [:show, :tasks_and_events, :days_for_tasks_and_events], Classroom do |classroom|
      classroom.student_id == user.id && classroom.student_accepted ||
          classroom.parent_id == user.id && classroom.parent_accepted ||
          classroom.teacher_id == user.id && classroom.teacher_accepted ||
          classroom.caseworker_id == user.id && classroom.caseworker_accepted
    end
    can [:accept_invitation_to_classroom,
         :reject_invitation_to_classroom], Classroom do |classroom|
      classroom.student_id == user.id ||
          classroom.parent_id == user.id ||
          classroom.teacher_id == user.id ||
          classroom.caseworker_id == user.id
    end

    can :my_classroom, Classroom do |classroom|
      classroom.owner_id == user.id
    end

    can :read, Task
    can :mark_complete, Task

    can :read, Event
    can :manage, Event do |event|
      event.new_record? || event.owner_id == user.id
    end
    can :manage, Notification do |notification|
      notification.user_id == user.id
    end

    can :manage, Report do |report|
      report.new_record? || report.user_id == user.id
    end
  end


  def parent_abilities(user)
    can :index, User
    can :show, User

    can :create_friend_invite, User do |other_user|
      !other_user.mentor?
    end
    can [:accept_friend_invite, :reject_friend_invite], Friendship do |friendship|
      friendship.user_id == user.id && friendship.status == 'not_accepted'
    end

    can [:follow, :unfollow], User do |other_user|
      !other_user.not_upgraded?
    end

    can :manage, Chat do |chat|
      chat.new_record? || chat.users.include?(user)
    end

    can :create, ChatMessage do |chat_message|
      chat_message.chat.users.include?(user)
    end
    can [:update, :destroy, :read], ChatMessage do |chat_message|
      chat_message.user == user
    end

    can :manage, Attachment do |att|
      att.new_record? || att.user_id == user.id
    end

    can :read, Post
    can :share_post, Post
    can :manage, Post do |post|
      post.new_record? || post.user_id == user.id
    end

    can :read, Comment
    can [:destroy, :update], Comment do |comment|
      comment.user == user
    end
    can :manage, Comment do |comment|
      comment.new_record? || comment.user_id == user.id
    end

    can :manage, Like do |like|
      like.new_record? || like.user_id == user.id
    end

    can :manage, Silent do |silent|
      silent.new_record? || silent.user_id == user.id
    end

    can :create, Classroom
    can :index, Classroom
    can [:update, :destroy], Classroom, owner_id: user.id
    can [:show, :tasks_and_events, :days_for_tasks_and_events], Classroom do |classroom|
      classroom.student_id == user.id && classroom.student_accepted ||
          classroom.parent_id == user.id && classroom.parent_accepted ||
          classroom.teacher_id == user.id && classroom.teacher_accepted ||
          classroom.caseworker_id == user.id && classroom.caseworker_accepted
    end
    can [:accept_invitation_to_classroom,
         :reject_invitation_to_classroom], Classroom do |classroom|
      classroom.new_record? ||
          classroom.student_id == user.id ||
          classroom.parent_id == user.id ||
          classroom.teacher_id == user.id ||
          classroom.caseworker_id == user.id
    end
    can :assessments, Classroom do |classroom|
      (classroom.parent_id == user.id || classroom.caseworker_id == user.id) &&
          (classroom.assessment_owner_id.blank? || classroom.assessment_owner_id == user.id)
    end
    can :delete_assessments, Classroom do |classroom|
      (classroom.parent_id == user.id || classroom.caseworker_id == user.id) &&
          classroom.assessment_owner_id == user.id
    end

    can :my_classroom, Classroom do |classroom|
      classroom.owner_id == user.id
    end

    can :read, Task
    can :mark_complete, Task

    can :read, Event
    can :manage, Event do |event|
      event.new_record? || event.owner_id == user.id
    end

    can :manage, Notification, user_id: user.id

    can :manage, Report do |report|
      report.new_record? || report.user_id == user.id
    end
  end


  def teacher_abilities(user)
    can :index, User
    can :show, User

    can :create_friend_invite, User do |other_user|
      !other_user.mentor?
    end
    can [:accept_friend_invite, :reject_friend_invite], Friendship do |friendship|
      friendship.user_id == user.id && friendship.status == 'not_accepted'
    end

    can [:follow, :unfollow], User do |other_user|
      !other_user.not_upgraded?
    end

    can :manage, Chat do |chat|
      chat.new_record? || chat.users.include?(user)
    end

    can :create, ChatMessage do |chat_message|
      chat_message.chat.users.include?(user)
    end
    can [:update, :destroy, :read], ChatMessage do |chat_message|
      chat_message.user == user
    end

    can :manage, Attachment do |att|
      att.new_record? || att.user_id == user.id
    end

    can :read, Post
    can :share_post, Post
    can :manage, Post do |post|
      post.new_record? || post.user_id == user.id
    end

    can :read, Comment
    can [:destroy, :update], Comment do |comment|
      comment.user == user
    end
    can :manage, Comment do |comment|
      comment.new_record? || comment.user_id == user.id
    end

    can :manage, Like do |like|
      like.new_record? || like.user_id == user.id
    end

    can :manage, Silent do |silent|
      silent.new_record? || silent.user_id == user.id
    end

    can :create, Classroom
    can :index, Classroom
    can [:update, :destroy], Classroom, owner_id: user.id
    can [:show, :tasks_and_events, :days_for_tasks_and_events], Classroom do |classroom|
      classroom.student_id == user.id && classroom.student_accepted ||
          classroom.parent_id == user.id && classroom.parent_accepted ||
          classroom.teacher_id == user.id && classroom.teacher_accepted ||
          classroom.caseworker_id == user.id && classroom.caseworker_accepted
    end
    can [:accept_invitation_to_classroom,
         :reject_invitation_to_classroom], Classroom do |classroom|
      classroom.student_id == user.id ||
          classroom.parent_id == user.id ||
          classroom.teacher_id == user.id ||
          classroom.caseworker_id == user.id
    end
    can :assessments, Classroom do |classroom|
      (classroom.parent_id == user.id || classroom.caseworker_id == user.id) &&
          (classroom.assessment_owner_id.blank? || classroom.assessment_owner_id == user.id)
    end
    can :delete_assessments, Classroom do |classroom|
      (classroom.parent_id == user.id || classroom.caseworker_id == user.id) &&
          classroom.assessment_owner_id == user.id
    end

    can :my_classroom, Classroom do |classroom|
      classroom.owner_id == user.id
    end

    can :read, Task
    can :manage, Task do |task|
      task.new_record? || user.classroom_ids.include?(task.classroom_id) && task.classroom.teacher_accepted
    end

    can :read, Event
    can :manage, Event do |event|
      event.new_record? || event.owner_id == user.id
    end

    can :manage, Notification do |notification|
      notification.user_id == user.id
    end

    can :manage, Report do |report|
      report.new_record? || report.user_id == user.id
    end
  end


  def caseworker_abilities(user)
    can :index, User
    can :show, User

    can :create_friend_invite, User do |other_user|
      !other_user.mentor?
    end
    can [:accept_friend_invite, :reject_friend_invite], Friendship do |friendship|
      friendship.user_id == user.id && friendship.status == 'not_accepted'
    end

    can [:follow, :unfollow], User do |other_user|
      !other_user.not_upgraded?
    end

    can :manage, Chat do |chat|
      chat.new_record? || chat.users.include?(user)
    end

    can :create, ChatMessage do |chat_message|
      chat_message.chat.users.include?(user)
    end
    can [:update, :destroy, :read], ChatMessage do |chat_message|
      chat_message.user == user
    end

    can :manage, Attachment do |att|
      att.new_record? || att.user_id == user.id
    end

    can :read, Post
    can :share_post, Post
    can :manage, Post do |post|
      post.new_record? || post.user_id == user.id
    end

    can :read, Comment
    can [:destroy, :update], Comment do |comment|
      comment.user == user
    end
    can :manage, Comment do |comment|
      comment.new_record? || comment.user_id == user.id
    end

    can :manage, Like do |like|
      like.new_record? || like.user_id == user.id
    end

    can :manage, Silent do |silent|
      silent.new_record? || silent.user_id == user.id
    end

    can :create, Classroom
    can :index, Classroom
    can [:update, :destroy], Classroom, owner_id: user.id
    can [:show, :tasks_and_events, :days_for_tasks_and_events], Classroom do |classroom|
      classroom.student_id == user.id && classroom.student_accepted ||
          classroom.parent_id == user.id && classroom.parent_accepted ||
          classroom.teacher_id == user.id && classroom.teacher_accepted ||
          classroom.caseworker_id == user.id && classroom.caseworker_accepted
    end
    can [:accept_invitation_to_classroom,
         :reject_invitation_to_classroom], Classroom do |classroom|
      classroom.student_id == user.id ||
          classroom.parent_id == user.id ||
          classroom.teacher_id == user.id ||
          classroom.caseworker_id == user.id
    end
    can :assessments, Classroom do |classroom|
      (classroom.parent_id == user.id || classroom.caseworker_id == user.id) &&
          (classroom.assessment_owner_id.blank? || classroom.assessment_owner_id == user.id)
    end
    can :delete_assessments, Classroom do |classroom|
      (classroom.parent_id == user.id || classroom.caseworker_id == user.id) &&
          classroom.assessment_owner_id == user.id
    end

    can :my_classroom, Classroom do |classroom|
      classroom.owner_id == user.id
    end

    can :read, Task
    can :manage, Task do |task|
      task.new_record? || user.classroom_ids.include?(task.classroom_id) && task.classroom.caseworker_accepted
    end

    can :read, Event
    can :manage, Event do |event|
      event.new_record? || event.owner_id == user.id
    end

    can :manage, Notification do |notification|
      notification.user_id == user.id
    end

    can :manage, Report do |report|
      report.new_record? || report.user_id == user.id
    end
  end


  def mentor_abilities(user)
    can :show, User

    can [:follow, :unfollow], User do |other_user|
      !other_user.not_upgraded?
    end

    can :manage, Chat do |chat|
      chat.new_record? || chat.users.include?(user)
    end

    can :create, ChatMessage do |chat_message|
      chat_message.chat.users.include?(user)
    end
    can [:update, :destroy, :read], ChatMessage do |chat_message|
      chat_message.user == user
    end

    can :read, Post
    can :share_post, Post
    can :manage, Post do |post|
      post.new_record? || post.user_id == user.id
    end

    can :read, Comment
    can [:destroy, :update], Comment do |comment|
      comment.user == user
    end
    can :manage, Comment do |comment|
      comment.new_record? || comment.user_id == user.id
    end

    can :manage, Like do |like|
      like.new_record? || like.user_id == user.id
    end

    can :manage, Silent do |silent|
      silent.new_record? || silent.user_id == user.id
    end

    can :manage, Notification do |notification|
      notification.user_id == user.id
    end

    can :manage, Report do |report|
      report.new_record? || report.user_id == user.id
    end
  end
end
