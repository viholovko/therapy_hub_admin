class Attachment < ApplicationRecord
  include ApplicationHelper

  before_create :extract_dimensions

  enum attachment_type: {
      photo: 0,
      video: 1,
      audio: 2,
      other: 3
  }

  belongs_to :attachable, polymorphic: true, optional: true
  belongs_to :user
  validates :file, presence: {message: {code: "E00500", message: "File can't be blank"}}
  validates :attachment_type, presence: {message: {code: "E00501", message: "Attachment type can't be blank"}}

  has_attached_file :file, :default_url => "/assets/missing.png",
                    styles: ->(i) { i.content_type.match?(/\Aimage\/.*\z/) ? { preview: '680x>' } : i.content_type.match?(/\Avideo\/.*\z/) ? {preview: { geometry: Proc.new { |instance| instance.resize }, :format => 'jpg', :time => 2 }} : {}},
                    processors: ->(i) {i.file_content_type.match?(/\Avideo\/.*\z/) ? [:transcoder] : [:thumbnail]},
                    url: "/system/attachments/:id/files/:style/:filename"
  validates_attachment :file, size: {less_than: 25.megabytes}, content_type: {content_type: /\Aimage\/.*\z/},
                       if: Proc.new { |i| i.attachment_type == 'photo' }


  def to_json
    result = {
        id: id,
        name: file_file_name,
        type: attachment_type,
        size: file_file_size,
        url: paperclip_url(file),
        url_preview: (file_content_type.match?(/\Aimage\/.*\z/) || file_content_type.match?(/\Avideo\/.*\z/)) ? paperclip_url(file, :preview) : nil,
        width: width,
        height: height
    }

    result
  end

  def to_json_socket
    result = {
        id: id,
        name: file_file_name,
        type: attachment_type,
        size: file_file_size,
        url: paperclip_url(file),
        url_preview: (file_content_type.match?(/\Aimage\/.*\z/) || file_content_type.match?(/\Avideo\/.*\z/)) ? paperclip_url(file, :preview) : nil,
        width: width,
        height: height
    }

    result
  end

  def self.set_atachments_order(positions)
    positions&.each_with_index do |value, index |
      Attachment.where(id: value).update(order_index: index)
    end
  end

  def resize
    file_meta = JSON.parse(`ffprobe -v quiet -print_format json -show_format -show_streams #{Paperclip.io_adapters.for(file).path} 2>&1`)

    width = file_meta['streams'][0]['width'].to_i
    height = file_meta['streams'][0]['height'].to_i

    min_width = 800
    min_height = 600
    ratio = width.fdiv(height)

    if ratio>1
      # Horizontal Image
      final_height = min_height
      final_width  = final_height * ratio
      "#{final_width.round}x#{final_height.round}!"
    else
      # Vertical Image
      final_width  = min_width
      final_height = final_width * ratio
      "#{final_height.round}x#{final_width.round}!"
    end
  end

  private

  def image?
    file_content_type =~ %r{^(image|(x-)?application)/(bmp|gif|jpeg|jpg|pjpeg|png|x-png)$}
  end

  def video?
    file_content_type =~ /\Avideo\/.*\z/
  end

  def extract_dimensions
    return unless image? || video?
    tempfile = file.queued_for_write[:original] if image?
    tempfile = file.queued_for_write[:preview] if video?
    unless tempfile.nil?
      geometry = Paperclip::Geometry.from_file(tempfile)
      self.width = geometry.width.to_i
      self.height = geometry.height.to_i
    end
  end
end
