class Classroom < ApplicationRecord


  # has_many    :attachments, as: :attachable,  dependent: :destroy
  has_many    :tasks, dependent: :destroy
  belongs_to  :owner,       class_name: 'User', optional: true
  belongs_to  :student,     class_name: 'User', optional: true
  belongs_to  :parent,      class_name: 'User', optional: true
  belongs_to  :teacher,     class_name: 'User', optional: true
  belongs_to  :caseworker,  class_name: 'User', optional: true
  has_many  :chats
  accepts_nested_attributes_for :chats

  belongs_to :silent, dependent: :destroy, required: false
  has_many :silents, as: :silentable, dependent: :destroy

  has_attached_file :avatar,
                    styles: { medium: '1000x1000>', thumb: '120x250>' },
                    default_url: '/images/missing.png',
                    path: ":rails_root/public/system/classrooms/:id/avatars/:style/:filename",
                    url: "/system/classrooms/:id/avatars/:style/:filename"
  has_attached_file :assessment_file,
                    url: "/system/classrooms/:id/assessment_files/:style/:filename"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates_attachment :assessment_file, size: {less_than: 25.megabytes}
  # TODO "Need to resolve problems with error. In base all object"
  validates_attachment_content_type :assessment_file, :content_type => ["application/pdf","application/vnd.ms-excel",
                                                                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                                                        "application/msword", "text/plain",
                                                                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                                                        "application/vnd.ms-excel",
                                                                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                                                        "image/vnd.djvu"], message: {code: 'E00711', message: 'You can\'t attache this file format.' }


  after_validation {
    self.errors.delete(:assessment_file_content_type)
  }

  validates :name, presence: { message: {code: 'E00708', message: 'Classroom name can\'t be blank.' }}
  validate :validate_members
  validate :validate_assessments

  after_save :verify_role
  after_update_commit :create_notification
  after_create_commit :create_notification
  after_create :update_classroom_available_count
  after_update :update_classroom_available_count
  after_destroy :update_classroom_available_count

  def self.search_query(params)
    classrooms = Classroom.arel_table
    users = User.arel_table

    if params[:count]
      q = classrooms.project('COUNT(*)')
    else
      q = classrooms
              .project('classrooms.*',
                       # "
            # (
            #     SELECT json_agg(att)
            #     FROM (
            #       SELECT *
            #       FROM attachments
            #       WHERE attachments.attachable_id = classrooms.id AND attachments.attachable_type = 'Classroom'
            #     ) att
            # ) AS attachments",
            "(
              SELECT json_agg(att)
              FROM (
                SELECT *
                FROM chats
                WHERE chats.classroom_id = classrooms.id) att
              ) AS chats",
                       "(SELECT json_agg(us) FROM (
              SELECT users.id, users.first_name, users.last_name,
                users.avatar_file_name, users.avatar_file_size,
                users.avatar_content_type
              FROM users WHERE classrooms.student_id = users.id
                               OR classrooms.parent_id = users.id
                               OR classrooms.teacher_id = users.id
                               OR classrooms.caseworker_id = users.id
            ) us )AS users",
                       "CASE
                          WHEN classrooms.student_id = #{params[:current_user].id.to_i}
                            THEN json_build_object('role', 'student',    'status', classrooms.student_accepted)
                          WHEN classrooms.parent_id = #{params[:current_user].id.to_i}
                            THEN json_build_object('role', 'parent',     'status', classrooms.parent_accepted)
                          WHEN classrooms.teacher_id = #{params[:current_user].id.to_i}
                            THEN json_build_object('role', 'teacher',    'status', classrooms.teacher_accepted)
                          WHEN classrooms.caseworker_id = #{params[:current_user].id.to_i}
                            THEN json_build_object('role', 'caseworker', 'status', classrooms.caseworker_accepted)
                          END
                       AS my_role_in_classroom"
      )

      q.join(users).on(classrooms[:student_id].eq(users[:id])
                           .or(classrooms[:parent_id].eq(users[:id]))
                           .or(classrooms[:teacher_id].eq(users[:id]))
                           .or(classrooms[:caseworker_id].eq(users[:id]))
      )
    end


    if params[:count]
    else
      if Classroom.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(classrooms[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
        q.group(classrooms[:id])
      else
        q.group(classrooms[:id])
        q.order(classrooms[:id].desc)
      end
    end

    q.where(classrooms[:id].eq(params[:id])) if params[:id].present?
    q.where(classrooms[:name].matches("%#{params[:name]}%")) if params[:name].present?
    q.where(classrooms[:assessment_link].matches("%#{params[:assessment_link]}%")) if params[:assessment_link].present?
    q.where(classrooms[:created_at].in(Date.parse(params[:created_at]).beginning_of_day..Date.parse(params[:created_at]).end_of_day)) if params[:created_at].present?
    q.where(classrooms[:updated_at].in(Date.parse(params[:updated_at]).beginning_of_day..Date.parse(params[:updated_at]).end_of_day)) if params[:updated_at].present?
    q.where(classrooms.grouping(classrooms[:student_id].eq(params[:current_user].id).and(classrooms[:student_accepted].eq(params[:accepted])))
        .or(classrooms.grouping(classrooms[:parent_id].eq(params[:current_user].id).and(classrooms[:parent_accepted].eq(params[:accepted]))))
        .or(classrooms.grouping(classrooms[:teacher_id].eq(params[:current_user].id).and(classrooms[:teacher_accepted].eq(params[:accepted]))))
        .or(classrooms.grouping(classrooms[:caseworker_id].eq(params[:current_user].id).and(classrooms[:caseworker_accepted].eq(params[:accepted]))))
    )
    q.where(classrooms[:owner_id].not_eq(params[:current_user].id)) if params[:not_owner]=='true'

    q
  end

  def self.search_query_admin(params)
    classrooms = Classroom.arel_table

    q = classrooms
            .project(Arel.star)
            .group(classrooms[:id])

    if Classroom.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
      q.order(classrooms[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
    else
      q.order(classrooms[:id].desc)
    end

    q.where(classrooms[:id].eq(params[:id])) if params[:id].present?
    q.where(classrooms[:name].matches("%#{params[:name]}%")) if params[:name].present?
    q.where(classrooms[:assessment_link].matches("%#{params[:assessment_link]}%")) if params[:assessment_link].present?
    q.where(classrooms[:created_at].in(Date.parse(params[:created_at]).beginning_of_day..Date.parse(params[:created_at]).end_of_day)) if params[:created_at].present?
    q.where(classrooms[:updated_at].in(Date.parse(params[:updated_at]).beginning_of_day..Date.parse(params[:updated_at]).end_of_day)) if params[:updated_at].present?

    q
  end

  def self.users_query(params)
    student = User.arel_table.alias('student')
    parent = User.arel_table.alias('parent')
    teacher = User.arel_table.alias('teacher')
    caseworker = User.arel_table.alias('caseworker')
    classrooms = Classroom.arel_table

    q = classrooms.project("classrooms.id",
                            "(SELECT json_agg(us)
                              FROM (
                                SELECT users.id, users.first_name, users.last_name,
                                  users.avatar_content_type, users.avatar_file_size,
                                  users.avatar_file_name, roles.name AS role, users.role_verified,
                                  CASE
                                      WHEN classrooms.student_id = users.id
                                               THEN json_build_object('role', 'student',    'status', classrooms.student_accepted)
                                      WHEN classrooms.parent_id = users.id
                                               THEN json_build_object('role', 'parent',     'status', classrooms.parent_accepted)
                                      WHEN classrooms.teacher_id = users.id
                                               THEN json_build_object('role', 'teacher',    'status', classrooms.teacher_accepted)
                                      WHEN classrooms.caseworker_id = users.id
                                               THEN json_build_object('role', 'caseworker', 'status', classrooms.caseworker_accepted)
                                      END
                                        AS my_role_in_classroom
                                FROM users
                                LEFT OUTER JOIN roles ON (roles.id = users.role_id)
                            WHERE users.id = student.id OR
                                  users.id = parent.id  OR
                                  users.id = teacher.id OR
                                  users.id = caseworker.id
                            )us ) AS users"
                        )
    q.join(student, Arel::Nodes::OuterJoin).on(classrooms[:student_id].eq(student[:id]))
    q.join(parent, Arel::Nodes::OuterJoin).on(classrooms[:parent_id].eq(parent[:id]))
    q.join(teacher, Arel::Nodes::OuterJoin).on(classrooms[:teacher_id].eq(teacher[:id]))
    q.join(caseworker, Arel::Nodes::OuterJoin).on(classrooms[:caseworker_id].eq(caseworker[:id]))

    q.where(classrooms[:id].eq(params[:id]))

    q
  end

  def self.invites_count_query(params)
    classrooms = Classroom.arel_table

    q = classrooms.project("COUNT(*)")

    q.where(classrooms.grouping(classrooms[:student_id].eq(params[:current_user].id).and(classrooms[:student_accepted].eq(false)))
                .or(classrooms.grouping(classrooms[:parent_id].eq(params[:current_user].id).and(classrooms[:parent_accepted].eq(false))))
                .or(classrooms.grouping(classrooms[:teacher_id].eq(params[:current_user].id).and(classrooms[:teacher_accepted].eq(false))))
                .or(classrooms.grouping(classrooms[:caseworker_id].eq(params[:current_user].id).and(classrooms[:caseworker_accepted].eq(false))))
    )

    q
  end

  def accept_invitation(current_user_id)
    case current_user_id
      when student_id
        update_attribute :student_accepted, true
      when parent_id
        update_attribute :parent_accepted, true
      when teacher_id
        update_attribute :teacher_accepted, true
      when caseworker_id
        update_attribute :caseworker_accepted, true
      else
        errors.add(:base, {code: 'E00709', message: 'Classroom with given user not found.'})
    end
  end

  def leave_from_classroom(role)
    case role
      when 'student'
        transaction do
          Notification.create({
                                  user_id: owner_id,
                                  from_user_id: student_id,
                                  classroom_id: id,
                                  classroom_title: name,
                                  message_type: :classroom_leave_user,
                                  action_id: id,
                                  notification_type: :classroom
                              }) if student_accepted
          assign_attributes student_id: nil, student_accepted: false
          save validate: false
        end
      when 'parent'
        transaction do
          Notification.create({
                                  user_id: owner_id,
                                  from_user_id: parent_id,
                                  classroom_id: id,
                                  classroom_title: name,
                                  action_id: id,
                                  message_type: :classroom_leave_user,
                                  notification_type: :classroom
                              }) if parent_accepted
          assign_attributes parent_id: nil, parent_accepted: false
          save validate: false
        end
      when 'teacher'
        transaction do
          Notification.create({
                                  user_id: owner_id,
                                  from_user_id: teacher_id,
                                  classroom_id: id,
                                  classroom_title: name,
                                  action_id: id,
                                  message_type: :classroom_leave_user,
                                  notification_type: :classroom
                              }) if teacher_accepted
          assign_attributes teacher_id: nil, teacher_accepted: false
          save validate: false
        end
      when 'caseworker'
        transaction do
          Notification.create({
                                  user_id: owner_id,
                                  from_user_id: caseworker_id,
                                  classroom_id: id,
                                  classroom_title: name,
                                  action_id: id,
                                  message_type: :classroom_leave_user,
                                  notification_type: :classroom
                              }) if caseworker_accepted
          assign_attributes caseworker_id: nil, caseworker_accepted: false
          save validate: false
        end
      else
        errors.add(:base, {code: 'E00709', message: 'Classroom with given user not found.'})
    end
  end

  def role_in_classroom(current_user_id)
    case current_user_id
      when student_id
        'student'
      when parent_id
        'parent'
      when teacher_id
        'teacher'
      when caseworker_id
        'caseworker'
      else
        errors.add(:base, {code: 'E00709', message: 'Classroom with given user not found.'})
    end
  end

  def invite_users(params)
    params.each do |k, v|
      unless self[k].nil?
        errors.add :base, {
            code: 'E00710', message: "#{k} is already exist."
        } and return false
      end
    end
    if update_attributes params
      chat_user_ids = chats.first.user_ids
      params.each do |k, v|
        Notification.create user_id: v.to_i,
                            from_user_id: owner_id,
                            classroom_id: id,
                            classroom_title: name,
                            message_type: :classroom_invite_user,
                            action_id: id,
                            notification_type: :classroom_invites

        chat_user_ids << v.to_i
      end
      chats.first.update_attributes user_ids: chat_user_ids
    end
  end

  def remove_users(params, current_user)
    chat_user_ids = chats.first.user_ids
    params.each do |k, v|
      if v.to_i != current_user.id
        if self[k] == v.to_i
          self[k] = nil
          key = k.sub('id', 'accepted')
          self[key] = false
          Notification.create user_id: v.to_i,
                              from_user_id: owner_id,
                              classroom_id: id,
                              classroom_title: name,
                              message_type: :classroom_remove_user,
                              notification_type: :classroom
          chat_user_ids.delete v.to_i
        else
          errors.add :base, {
              code: 'E00712', message: "#{k} is wrong."
          } and return false
        end
      else
        errors.add :base, {
            code: 'E00711', message: 'You can\'t to remove yourself.'
        } and return false
      end
    end
    save validate: false
    chats.first.update_attributes user_ids: chat_user_ids
  end

  def remove_users_from_admin(params, user_id)
    chat_user_ids = chats.first.user_ids
    params.each do |k, v|
      if v.to_i != user_id
        if self[k] == v.to_i
          self[k] = nil
          key = k.sub('id', 'accepted')
          self[key] = false
          chat_user_ids.delete v.to_i
        else
          errors.add :base, {
            code: 'E00712', message: "#{k} is wrong."
          } and return false
        end
      else
        errors.add :base, {
          code: 'E00711', message: 'You can\'t to remove yourself.'
        } and return false
      end
    end
    save validate: false
    chats.first.update_attributes user_ids: chat_user_ids
  end

  def add_assessment(params)
    transaction do
      assign_attributes params
      create_notification(true)
      save
    end
  end


  private

  def validate_members
    validate_student      unless student.nil?
    validate_parent       unless parent.nil?
    validate_teacher      unless teacher.nil?
    validate_caseworker   unless caseworker.nil?
  end

  def validate_assessments
    errors.add :base, {
      code: 'E00710', message: 'Assessment can\'t contain link and file at the same time.'
    }  unless (!assessment_link.present? && assessment_file.present?) ||
      (assessment_link.present? && !assessment_file.present?) ||
      (!assessment_link.present? && !assessment_file.present?)
  end


  def validate_student
    errors.add :base, {
        code: 'E00704', message: 'Given user is not student.'
    }  unless student.student?       || student.not_upgraded?

    unless owner.id == student.id
      errors.add :base, {
          code: 'E00700', message: 'Student is not your friend.'
      } unless Friendship.exists_accepted_friend?(owner.id, student.id)
    end
  end

  def validate_parent
    errors.add :base, {
        code: 'E00705', message: 'Given user is not parent.'
    }  unless parent.parent?          || parent.not_upgraded?

    unless owner.id == parent.id
      errors.add :base, {
          code: 'E00701', message: 'Parent is not your friend.'
      } unless Friendship.exists_accepted_friend?(owner.id, parent.id) || owner.id == parent.id
    end
  end

  def validate_teacher
    errors.add :base, {
        code: 'E00706', message: 'Given user is not teacher.'
    }  unless teacher.teacher? || teacher.not_upgraded?
    errors.add :base, {
        code: 'E00702', message: 'Teacher is not your friend.'
    } unless Friendship.exists_accepted_friend?(owner.id, teacher.id) || owner.id == teacher.id
  end

  def validate_caseworker
    errors.add :base, {
        code: 'E00707', message: 'Given user is not caseworker.'
    }  unless caseworker.caseworker?  || caseworker.not_upgraded?
    errors.add :base, {
        code: 'E00703', message: 'Caseworker is not your friend.'
    } unless Friendship.exists_accepted_friend?(owner.id, caseworker.id) || owner.id == caseworker.id
  end


  def verify_role
    student&.verify_role      if saved_change_to_student_id?      || new_record?
    parent&.verify_role       if saved_change_to_parent_id?       || new_record?
    teacher&.verify_role      if saved_change_to_teacher_id?      || new_record?
    caseworker&.verify_role   if saved_change_to_caseworker_id?   || new_record?
  end

  def create_notification(assessment = false)
     Notification.create({
                             user_id: student_id,
                             from_user_id: assessment ? assessment_owner_id : owner_id,
                             classroom_id: id,
                             classroom_title: name,
                             action_id: id,
                             message_type: assessment ? :classroom_create_assessment : :classroom_invite_user,
                             notification_type: assessment ? :classroom : :classroom_invites,
                         }) if student_id && (student_id != owner_id && !assessment) || (assessment && student_accepted)
     Notification.create({
                             user_id: parent_id,
                             from_user_id: assessment ? assessment_owner_id : owner_id,
                             classroom_id: id,
                             classroom_title: name,
                             action_id: id,
                             message_type: assessment ? :classroom_create_assessment : :classroom_invite_user,
                             notification_type: assessment ? :classroom : :classroom_invites,
                         }) if parent_id && (parent_id != owner_id && !assessment) || (assessment && parent_accepted)

     Notification.create({
                             user_id: teacher_id,
                             from_user_id: assessment ? assessment_owner_id : owner_id,
                             classroom_id: id,
                             classroom_title: name,
                             action_id: id,
                             message_type: assessment ? :classroom_create_assessment : :classroom_invite_user,
                             notification_type: assessment ? :classroom : :classroom_invites,
                         }) if teacher_id && !assessment || (teacher_id != assessment_owner_id && teacher_accepted)

     Notification.create({
                             user_id: caseworker_id,
                             from_user_id: assessment ? assessment_owner_id : owner_id,
                             classroom_id: id,
                             classroom_title: name,
                             action_id: id,
                             message_type: assessment ? :classroom_create_assessment : :classroom_invite_user,
                             notification_type: assessment ? :classroom : :classroom_invites,
                         }) if caseworker_id && !assessment || (caseworker_id != assessment_owner_id && caseworker_accepted)
  end

  def update_classroom_available_count
    @user = User.find(self.owner_id)
    classroom_count = 0
    if @user.payment_plan.present?
      if @user.payment.payment_status == 'expired'
        @user.available_classrooms = 0
        if @user.save
          Classroom.where(owner_id: @user.id).update_all(blocked: true, system_blocked: true)
        end
      else
        count = Classroom.where(blocked: false, owner_id: @user.id).count
        if count>=0
          if (@user.payment_plan.classroom_count-count) >= 0
            classroom_count = @user.payment_plan.classroom_count-count
          else
            Classroom.where(owner_id: @user.id).update_all(blocked: true, system_blocked: true)
            classroom_count = @user.payment_plan.classroom_count
          end
        else
          Classroom.where(owner_id: self.user.id).update_all(blocked: true, system_blocked: true)
          classroom_count = @user.payment_plan.classroom_count
        end

      @user.update_attributes available_classrooms: classroom_count
    end

    end
  end
end
