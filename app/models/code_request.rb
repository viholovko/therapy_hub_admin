class CodeRequest < ApplicationRecord

  belongs_to :country

  validate :attempt_timeout

  after_save :remove_outdated
  before_save :generate_code

  def user
    User.find_by country_id: country_id, phone_number: phone_number
  end

  def retry_time_remaining
    if attempts_count == 1 && Time.now - updated_at < 1.minute
      ((updated_at + 1.minute) - Time.now).to_i
    elsif attempts_count == 2 && Time.now - updated_at < 3.minute
      ((updated_at + 3.minute) - Time.now).to_i
    elsif attempts_count >= 3 && Time.now - updated_at < 10.minute
      ((updated_at + 10.minute) - Time.now).to_i
    else
      0
    end
  end

  def self.remove_outdated
    where("updated_at < ?", Time.now - 1.hour).destroy_all
  end

  def send_code
    client = Twilio::REST::Client.new
    client.messages.create({ from: SystemSetting.twilio_phone_number, to: "#{ country.phone_code }#{ phone_number }", body: code })
  rescue Exception => e
    # self.errors.add :base, {code: "E00112", message: e.message}
  end

  private

  def attempt_timeout
    if updated_at_was && attempts_count_was
      if attempts_count_was == 1 && Time.now - updated_at_was < 1.minute
        self.errors.add :base, {code: "E00100", message: "Next attempt allowed in #{ ((updated_at_was + 1.minute - Time.now)/60).round(2) } minutes"}
      elsif attempts_count_was == 2 && Time.now - updated_at_was < 3.minute
        self.errors.add :base, {code: "E00100", message: "Next attempt allowed in #{ ((updated_at_was + 3.minute - Time.now)/60).round(2) } minutes"}
      elsif attempts_count_was >= 3 && Time.now - updated_at_was < 10.minute
        self.errors.add :base, {code: "E00100", message: "Next attempt allowed in #{ ((updated_at_was + 10.minutes - Time.now)/60).round(2) } minutes"}
      end
    end
  end

  def generate_code
    if self.phone_number == '22242426' || self.phone_number == '+3122242426' || self.phone_number == '3122242426'
      self.code = 1111
    else
      self.code = 1000 + rand(9999 - 1000)
    end
  end

  def remove_outdated
    CodeRequest.remove_outdated
  end
end