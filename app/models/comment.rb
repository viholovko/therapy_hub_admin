class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true, required: false
  belongs_to :post, optional: true
  has_many :comments, as: :commentable

  validates :message, presence: {message: {code: 'E01000', message: 'Message can\'t be blank'}}
  validates :commentable_id, presence: {message: {code: 'E01001', message: 'Commentable id can\'t be blank'}}
  validates :commentable_type, presence: {message: {code: 'E01002', message: 'Commentable type can\'t be blank'}}

  after_commit :create_notification, on: [:create, :update]

  def self.search_query(params)
    comments = Comment.arel_table
    users = User.arel_table
    comment_members = users.alias

    if params[:count]
      q = comments.project("COUNT(*)")
    else
      q = comments
              .project(Arel.star)
              .group(comments[:id], users[:id], comment_members[:id])
              .outer_join(users).on(comments[:user_id].eq(users[:id]))
              .outer_join(comment_members).on(comments[:comment_member_id].eq(comment_members[:id]))
      q.project(
          comments[:message],
          comments[:commentable_id],
          comments[:commentable_type],
          comments[:comment_member_id],
          comments[:user_id],
          comments[:created_at],
          users[:id],
          users[:first_name],
          users[:last_name],
          users[:avatar_file_name],
          users[:avatar_file_size],
          users[:avatar_content_type],
          comment_members[:first_name].as("member_first_name"),
          comment_members[:last_name].as("member_last_name"),
          comments[:id]
          )
      if %w(asc desc).include?(params[:sort_type])
        q.order(comments[:created_at].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.order(comments[:created_at].desc)
      end

      q.where(comments[:id].gt(params[:from_id]))                        if params[:from_id].present?
      q.where(comments[:id].lt(params[:to_id]))                        if params[:to_id].present?
    end

    q.where(comments[:message].matches("%#{params[:message]}%"))       if params[:message].present?
    q.where(comments[:commentable_type].eq(params[:commentable_type])) if params[:commentable_type].present?
    q.where(comments[:commentable_id].eq(params[:commentable_id]))     if params[:commentable_id].present?

    q
  end

  private

  def create_notification
    message_type = commentable.class.name == 'Post' ? :post_create_comment : :post_comment_create_reply
    Notification.create({
        user_id: commentable.user_id,
        from_user_id: user_id,
        post_id: post_id,
        post_title: post&.text,
        message_type: message_type,
        action_id: post_id,
        notification_type: :posts
                        }) unless commentable.user_id == user_id
  end
end