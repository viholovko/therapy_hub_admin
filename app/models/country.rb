class Country < ApplicationRecord
  has_attached_file :flag,
                    styles: { medium: '1024x768', small: '100x100>'},
                    default_url: '/images/missing.png',
                    path: ":rails_root/public/system/images/countries/:style/:filename",
                    url: "/system/images/countries/:style/:filename"

  validates_attachment_size :flag, less_than: 20.megabytes
  validates_attachment_content_type :flag, content_type: /\Aimage\/.*\Z/

  validates :name, presence: true
  validates :phone_code, presence: true
  validates :alpha2_code, presence: true
  validates :alpha3_code, presence: true
  validates :numeric_code, presence: true


  def self.search_query(params)
    countries = Country.arel_table

    q = nil

    if params[:count]
      q = countries.project('COUNT(*)')
    else
      q = countries.project(Arel.star).group(countries[:id]).order(countries[:name])
    end

    q.where(countries[:name].matches("%#{params[:name]}%")) if params[:name].present?
    q.where(countries[:phone_code].matches("%#{params[:phone_code]}%")) if params[:phone_code].present?
    q.where(countries[:alpha3_code].matches("%#{params[:alpha3_code]}%")) if params[:alpha3_code].present?
    q.where(countries[:alpha2_code].matches("%#{params[:alpha2_code]}%")) if params[:alpha2_code].present?
    q.where(countries[:numeric_code].matches("%#{params[:numeric_code]}%")) if params[:numeric_code].present?
    q.where(countries[:selected].eq(true)) if params[:selected] == 'true' || params[:selected].kind_of?(TrueClass)
    q.where(countries[:selected].eq(false)) if params[:not_selected] == 'true'

    q
  end

  private

end
