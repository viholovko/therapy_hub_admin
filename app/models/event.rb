class Event < ApplicationRecord
  belongs_to :classroom, required: false
  belongs_to :owner, class_name: 'User'

  after_commit  :create_notification
  after_update  :update_notification
  after_destroy :destroy_notification


  validates :title, presence: {message: {code: 'E01300', message: 'Title can\'t be blank.'}}
  validates :description, presence: {message: {code: 'E01301', message: 'Description can\'t be blank.'}}
  validates :time_from, presence: {message: {code: 'E01302', message: 'From date can\'t be blank.'}}
  validates :time_to, presence: {message: {code: 'E01303', message: 'To date can\'t be blank.'}}
  validates :classroom, presence: {message: {code: 'E01304', message: 'Classroom can\'t be blank.'}}

  def self.search_query(params)
    events = Event.arel_table

    q = events.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Event.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(events[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.group(events[:id])
        q.order(events[:id].desc)
      end
    end

    q.where(events[:classroom_id].eq(params[:classroom_id])) if params[:classroom_id].present?
    q.where(
        events[:time_from].between(DateTime.parse(params[:date_from]).beginning_of_day..(DateTime.parse(params[:date_to]).end_of_day))
               .or(events[:time_to].between(DateTime.parse(params[:date_from]).beginning_of_day..DateTime.parse(params[:date_to]).end_of_day))
               .or(events[:time_from].gteq(DateTime.parse(params[:date_from]).beginning_of_day).and(events[:time_to].lteq(DateTime.parse(params[:date_to]).end_of_day)))
    ) if params[:date_from].present? && params[:date_to].present?

    q
  end

  private

    def create_notification
      Notification.create({user_id: classroom.student_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.time_to,
                           message_type: :classroom_create_event,
                           notification_type: :classroom
                          }
      ) if classroom.student_id != self.owner_id && classroom.student_id && classroom.student_accepted

      Notification.create({user_id: classroom.parent_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.time_to,
                           message_type: :classroom_create_event,
                           notification_type: :classroom
                          }
      ) if classroom.parent_id != self.owner_id && classroom.parent_id && classroom.parent_accepted

      Notification.create({user_id: classroom.teacher_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.time_to,
                           message_type: :classroom_create_event,
                           notification_type: :classroom
                          }
      ) if classroom.teacher_id != self.owner_id && classroom.teacher_id && classroom.teacher_accepted

      Notification.create({user_id: classroom.caseworker_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.time_to,
                           message_type: :classroom_create_event,
                           notification_type: :classroom
                          }
      ) if classroom.caseworker_id != self.owner_id && classroom.caseworker_id && classroom.caseworker_accepted

      # Notification.create({user_id: classroom.student_id,
      #                      from_user_id: self.owner_id,
      #                      classroom_id: self.classroom_id,
      #                      classroom_title: classroom.name,
      #                      action_id: self.classroom_id,
      #                      sub_resource_id: self.id,
      #                      sub_resource_title: self.title,
      #                      sub_resource_time_from: self.time_from,
      #                      sub_resource_time_to: self.time_to,
      #                      message_type: :classroom_coming_event,
      #                      notification_type: :classroom,
      #                      scheduled: true
      #                     }
      # ) if classroom.student_id != self.owner_id && classroom.student_id && classroom.student_accepted
      #
      # Notification.create({user_id: classroom.parent_id,
      #                      from_user_id: self.owner_id,
      #                      classroom_id: self.classroom_id,
      #                      classroom_title: classroom.name,
      #                      action_id: self.classroom_id,
      #                      sub_resource_id: self.id,
      #                      sub_resource_title: self.title,
      #                      sub_resource_time_from: self.time_from,
      #                      sub_resource_time_to: self.time_to,
      #                      message_type: :classroom_coming_event,
      #                      notification_type: :classroom,
      #                      scheduled: true
      #                     }
      # ) if classroom.parent_id != self.owner_id && classroom.parent_id && classroom.parent_accepted

      #
      #   period 1
      #

      Notification.create({user_id: classroom.student_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_1,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_1 != 'none' && classroom.student_id != owner_id && classroom.student_accepted && self.notification_period_1.present?

      Notification.create({user_id: classroom.parent_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_1,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_1 != 'none' && classroom.parent_id != owner_id && classroom.parent_accepted && self.notification_period_1.present?

      Notification.create({user_id: classroom.caseworker_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_1,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_1 != 'none' && classroom.caseworker_id != owner_id && classroom.caseworker_accepted && self.notification_period_1.present?

      Notification.create({user_id: classroom.teacher_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_1,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_1 != 'none' && classroom.teacher_id != owner_id && classroom.teacher_accepted && self.notification_period_1.present?

      #
      #   period 2
      #

      Notification.create({user_id: classroom.student_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_2,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_2 != 'none' && classroom.student_id != owner_id && classroom.student_accepted && self.notification_period_1.present?

      Notification.create({user_id: classroom.parent_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_2,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_2 != 'none' && classroom.parent_id != owner_id && classroom.parent_accepted && self.notification_period_1.present?

      Notification.create({user_id: classroom.caseworker_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_2,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_2 != 'none' && classroom.caseworker_id != owner_id && classroom.caseworker_accepted && self.notification_period_1.present?

      Notification.create({user_id: classroom.teacher_id,
                           from_user_id: self.owner_id,
                           classroom_id: self.classroom_id,
                           classroom_title: classroom.name,
                           action_id: self.classroom_id,
                           sub_resource_id: self.id,
                           sub_resource_title: self.title,
                           sub_resource_time_from: self.time_from,
                           sub_resource_time_to: self.notification_period_2,
                           message_type: :classroom_reminder1_event,
                           notification_type: :classroom,
                           scheduled: true
                          }
      ) if self.period_2 != 'none' && classroom.teacher_id != owner_id && classroom.teacher_accepted && self.notification_period_1.present?

    end

  def destroy_notification
    Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true ).destroy_all
  end

  def update_notification
    Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true).update_all(sub_resource_title: self.title, sub_resource_time_from: self.time_from, sub_resource_time_to: self.time_to)
    Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true, message_type: :classroom_reminder1_event).update_all(sub_resource_title: self.title, sub_resource_time_to: self.notification_period_1)
    Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true, message_type: :classroom_reminder2_event).update_all(sub_resource_title: self.title, sub_resource_time_to: self.notification_period_2)
  end
end
