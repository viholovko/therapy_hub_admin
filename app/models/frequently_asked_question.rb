class FrequentlyAskedQuestion < ApplicationRecord

  def self.search_query(params)
    frequently_asked_questions = FrequentlyAskedQuestion.arel_table

    q = frequently_asked_questions.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if FrequentlyAskedQuestion.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(frequently_asked_questions[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.group(frequently_asked_questions[:id])
        q.order(frequently_asked_questions[:id].desc)
      end
    end

    q.where(frequently_asked_questions[:title].matches("%#{params[:title]}%")) if params[:title].present?
    q.where(frequently_asked_questions[:description].matches("%#{params[:description]}%")) if params[:description].present?
    q.where(frequently_asked_questions[:content].matches("%#{params[:content]}%")) if params[:content].present?

    q
  end
end
