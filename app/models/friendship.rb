class Friendship < ApplicationRecord

  enum status: {not_accepted: 0, requested: 1, accepted: 2}

  belongs_to :user
  belongs_to :friend, class_name: 'User', foreign_key: :friend_id

  validates_presence_of :user_id, :friend_id

  def self.between(user_id, friend_id)
    find_by(user_id: user_id, friend_id: friend_id)
  end

  def self.exists_friend?(user_id, friend_id)
    not between(user_id, friend_id).nil?
  end

  def self.exists_accepted_friend?(user_id, friend_id)
    not find_by(user_id: user_id, friend_id: friend_id, status: :accepted).nil?
  end

  def self.request_to_invite(user_id, friend_id)
    unless user_id == friend_id || Friendship.exists_friend?(user_id, friend_id)
      transaction do
        create(user_id: user_id, friend_id: friend_id, status: 'requested')
        create(user_id: friend_id, friend_id: user_id, status: 'not_accepted')
        Notification.create user_id: friend_id,
                            from_user_id: user_id,
                            message_type: :friend_request_invite,
                            notification_type: :friend_requests,
                            action_id: user_id
      end
    end
  end

  def accept_request
    transaction do
      accepted_at = Time.now
      friendship = Friendship.find_by(user_id: friend_id, friend_id: user_id)
      accept_one_side(friendship, accepted_at)
      Notification.create user_id: friend_id,
                          from_user_id: user_id,
                          message_type: :friend_request_accept,
                          notification_type: :friend_requests,
                          action_id: user_id
    end
  end

  def reject_request
    transaction do
      friendship = Friendship.find_by(user_id: friend_id, friend_id: user_id)
      self.destroy
      friendship.destroy
    end
  end

  def self.remove_from_friend(user_id, friend_id)
    unless user_id == friend_id || !Friendship.exists_friend?(user_id, friend_id)
      transaction do
        Friendship.find_by(user_id: user_id, friend_id: friend_id).destroy
        Friendship.find_by(user_id: friend_id, friend_id: user_id).destroy
      end
    end
  end

  private

  def accept_one_side(friendship, accepted_at)
    transaction do
      self.status = 'accepted'
      self.accepted_at = accepted_at
      self.save
      friendship.status = 'accepted'
      friendship.accepted_at = accepted_at
      friendship.save
    end
  end
end
