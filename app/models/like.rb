class Like < ApplicationRecord

  belongs_to :user, required: false
  belongs_to :likeable, polymorphic: true, required: false

  after_commit :create_notification, on: [:create]

  validates :likeable_id, presence: {message: {code: 'E01100', message: 'Likeable id can\'t be blank.'}}
  validates :likeable_type, presence: {message: {code: 'E01101', message: 'Likeable type can\'t be blank.'}}
  validates :likeable_id, uniqueness: {scope:[:user_id, :likeable_type], message: {code: 'E01102', message: 'Likeable is already exist.'}}

  private

  def create_notification
    Notification.create({
        user_id: likeable.user_id,
        from_user_id: self.user_id,
        post_id: likeable.id,
        post_title: likeable.text,
        message_type: :post_create_like,
        action_id: likeable.id,
        notification_type: :posts
                        }) unless likeable.user_id == user_id
  end

end
