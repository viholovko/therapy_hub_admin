class Post < ApplicationRecord

  belongs_to :user
  belongs_to :link, dependent: :destroy, required: false
  belongs_to :classroom, optional: true
  has_many :attachments, as: :attachable, dependent: :destroy
  has_and_belongs_to_many :tagged_users,
                          class_name: 'User',
                          join_table: :posts_tagged_users,
                          foreign_key: :post_id,
                          association_foreign_key: :user_id
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :likes, as: :likeable, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :cms_notifications, dependent: :destroy
  accepts_nested_attributes_for :attachments, allow_destroy: true
  accepts_nested_attributes_for :link, allow_destroy: true
  validates :user, presence: true
  validates :text, presence: {message: {code: 'E00500', message: 'Text can\'t be blank'}}, if: -> {!link.present? && !attachments.present?}

  before_create :classroom_exist?
  after_commit :create_notification, on: [:create, :update]


  def self.query(params)
    posts = Post.arel_table
    attachments = Attachment.arel_table
    users = User.arel_table
    roles = Role.arel_table
    block_lists = Arel::Table.new(:block_lists)
    reports = Arel::Table.new(:reports)

      q = posts
              .project(
                  'posts.*', 'roles.name',
                  "json_build_object(
                    'first_name', users.first_name,
                    'last_name', users.last_name,
                    'id', users.id,
                    'avatar_file_name', users.avatar_file_name,
                    'avatar_content_type', users.avatar_content_type,
                    'avatar_file_size', users.avatar_file_size
                  ) AS user",
                  'roles.name AS role_name',
                  "(
                  SELECT json_agg(att)
                  FROM (
                    SELECT *
                    FROM attachments
                    WHERE attachments.attachable_id = posts.id AND attachments.attachable_type = 'Post'
                    ORDER BY attachments.order_index ASC
                  ) att ) AS attachments",
                  "(
                  SELECT json_agg(who_liked)
                  FROM (
                    SELECT users.id AS id, users.first_name, users.last_name,
                    users.avatar_file_name, users.avatar_content_type, users.avatar_file_size,
                    roles.name AS role_name
                    FROM users
                  LEFT OUTER JOIN likes ON (users.id = likes.user_id)
                  LEFT JOIN roles ON roles.id = users.role_id
                  WHERE likes.likeable_id = posts.id AND likes.likeable_type = 'Post'
                  ORDER BY likes.id DESC
                  LIMIT 3
                  ) who_liked) AS who_liked",
                  "(SELECT COUNT(*)
                    FROM posts_tagged_users
                    WHERE post_id = posts.id
                    ) AS tagged_users_count",
                  "(SELECT COUNT(*)
                  FROM comments
                  WHERE comments.commentable_id = posts.id
                        AND comments.commentable_type = 'Post'
                  ) AS comments_count",
                  "(SELECT COUNT(*)
                  FROM likes
                  WHERE likes.likeable_id = posts.id
                        AND likes.likeable_type = 'Post'
                  ) AS likes_count",
                  "(SELECT CASE
                   WHEN #{params[:current_user].id.to_i} IN
                        (SELECT user_id FROM likes WHERE likeable_id = posts.id AND likeable_type = 'Post')
                     THEN TRUE
                     ELSE FALSE
                   END
                   ) AS is_liked",
                  "(SELECT COUNT(*)
                  FROM reports
                  WHERE reports.post_id = posts.id
                  ) AS reports_count",
                  "(
                  SELECT json_agg(link)
                  FROM (
                    SELECT *
                    FROM links
                    WHERE posts.link_id = links.id
                  ) link ) AS link"
              )
              .group(posts[:id], users[:id], roles[:id])

    q.join(users, Arel::Nodes::OuterJoin).on(users[:id].eq(posts[:user_id]))
    q.join(roles, Arel::Nodes::OuterJoin).on(users[:role_id].eq(roles[:id]))
    q.join(attachments, Arel::Nodes::OuterJoin)
        .on(posts[:id].eq(attachments[:attachable_id])
                .and(attachments[:attachable_type].eq('Post'))
        )
    q.join(block_lists, Arel::Nodes::OuterJoin)
        .on(
            (
            block_lists[:user_id].eq(posts[:user_id])
                .and(block_lists[:blocked_user_id].eq(params[:current_user]&.id.to_i))
            )
                .or(
                    block_lists[:user_id].eq(params[:current_user]&.id.to_i)
                        .and(block_lists[:blocked_user_id].eq(posts[:user_id]))
                )
        )
    q.join(reports, Arel::Nodes::OuterJoin)
        .on(posts[:id].eq(reports[:post_id])
                .and(reports[:user_id].eq(params[:current_user]&.id.to_i))
        )


    q.where(posts[:id].eq(params[:id])) if params[:id].present?
    q.where(posts[:text].matches("%#{params[:text]}%").or(users[:first_name].matches("%#{params[:text]}%").or(users[:last_name].matches("%#{params[:text]}%")))) if params[:text].present?
    q.where(posts[:ads].eq(params[:ads])) if params[:ads].present?
    q.where(posts[:created_at].in(Date.parse(params[:created_at]).beginning_of_day..Date.parse(params[:created_at]).end_of_day)) if params[:created_at].present?
    q.where(posts[:updated_at].in(Date.parse(params[:updated_at]).beginning_of_day..Date.parse(params[:updated_at]).end_of_day)) if params[:updated_at].present?
    q.where(block_lists[:id].eq(nil)) unless params[:current_user].admin?
    q.where(reports[:id].eq(nil)) unless params[:current_user].admin?
    q.where(posts[:classroom_id].eq(nil)) unless params[:classroom_id].present?
    q.where(posts[:classroom_id].eq(params[:classroom_id])) if params[:classroom_id].present?
    q.where(posts[:created_at].gteq(params[:from_date])) if params[:from_date].present?
    q.where(posts[:created_at].lteq(params[:to_date])) if params[:to_date].present?
    q.where(posts[:user_id].eq(params[:user_id])) if params[:user_id].present?
    # q.where() if params[:text].present?

    q
  end

  def self.search_query(params)
    posts = Post.arel_table
    q = query(params).as('t')

    result = posts.project(params[:count] ? "COUNT(*)" : "t.*").from(q)

    if params[:count]
    else
      if Post.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        result.order(q[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        result.order(q[:id].desc)
      end
    end

    result
  end

  def self.reported_posts_query(params)
    posts = Post.arel_table
    reports = Report.arel_table
    q = query(params).as('t')

    result = posts.project(params[:count] ? "COUNT(*)" : "t.*").from(q)

    result.join(reports, Arel::Nodes::OuterJoin).on(q[:id].eq(reports[:post_id]))

    if params[:count]
    else
      if Post.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        result.order(q[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        result.order(q[:id].desc)
      end
    end

    result.where(q[:id].eq(reports[:post_id]))

  end

  def self.comments_likes_query(params)
    posts = Post.arel_table

    q = posts.project("posts.id, (SELECT json_agg(comm)
                        FROM (
                          SELECT comments.id, comments.message, comments.user_id,
                                 comments.commentable_id, comments.commentable_type, comments.created_at,
                                 comments.updated_at, users.first_name, users.last_name,
                                 users.avatar_file_size, users.avatar_content_type, users.avatar_file_name,
                                 users.email, users.phone_number, users.created_at, roles.name AS role
                          FROM comments
                            LEFT OUTER JOIN users ON (users.id = comments.user_id)
                            LEFT JOIN roles ON roles.id = users.role_id
                          WHERE comments.commentable_id = #{params[:id].to_i} AND comments.commentable_type = 'Post'
                      ) comm ) AS comments",
                      "(SELECT json_agg(users)
                        FROM (
                          SELECT likes.id, likes.created_at, likes.user_id, users.first_name, users.last_name,
                                 users.avatar_file_size, users.avatar_content_type,
                                 users.avatar_file_name, users.email, users.phone_number,
                                 users.created_at, roles.name AS role
                          FROM users
                            LEFT OUTER JOIN likes ON (likes.likeable_id = #{params[:id].to_i} AND likes.likeable_type = 'Post')
                            LEFT JOIN roles ON roles.id = users.role_id
                          WHERE (likes.user_id = users.id)
                      ) users ) AS likes",
                      "(SELECT json_agg(users)
                        FROM (
                          SELECT reports.id, reports.created_at, reports.report_type, reports.user_id, users.first_name,
                                 users.last_name, users.avatar_file_size, users.avatar_content_type,
                                 users.avatar_file_name, users.email, users.phone_number,
                                 users.created_at, roles.name AS role
                          FROM users
                            LEFT OUTER JOIN reports ON (reports.post_id = #{params[:id].to_i} )
                            LEFT JOIN roles ON roles.id = users.role_id
                          WHERE (reports.user_id = users.id)
                      ) users ) AS reports"
    )

    q.where(posts[:id].eq(params[:id].to_i))

    q
  end


  def to_json_chat_message
    new_attachments = []
    attachments&.each do |item|
      new_attachments<<item.to_json_socket
    end

    {
        id: id,
        text: text,
        link: link.to_json,
        attachments: new_attachments,
        ads: ads,
        created_at: created_at,
        updated_at: updated_at,
        attachments_count: attachments.size,
        user: user.to_json_for_broadcast_user
    }
  end

  private

    def classroom_exist?
      Classroom.find classroom_id if classroom_id
    end

  def create_notification
    if self.user.mentor?
      self.user.followers.find_each do |follower|
        Notification.create user_id: follower.id,
                            from_user_id: self.user_id,
                            post_id: self.id,
                            post_title: text,
                            message_type: :post_mentor_create_post,
                            action_id: self.user_id,
                            notification_type: :mentor_posts
      end
    end
    tagged_users.find_each do |user|
      Notification.create user_id: user.id,
                          from_user_id: self.user_id,
                          post_id: self.id,
                          classroom_id: self.classroom_id,
                          post_title: text,
                          classroom_title: classroom&.name,
                          message_type: :post_tagged_users,
                          action_id: self.id,
                          notification_type: :posts
    end
    if classroom_id
      Notification.create({
                              user_id: classroom.student_id,
                              from_user_id: self.user_id,
                              post_id: self.id,
                              post_title: self.text,
                              classroom_id: classroom.id,
                              classroom_title: classroom.name,
                              action_id: classroom.id,
                              message_type: :classroom_create_post,
                              notification_type: :classroom
                          }) if classroom&.student_id && classroom&.student_id != user_id && classroom&.student_accepted

      Notification.create({
                              user_id: classroom.parent_id,
                              from_user_id: self.user_id,
                              post_id: self.id,
                              post_title: self.text,
                              classroom_id: classroom.id,
                              classroom_title: classroom.name,
                              action_id: classroom.id,
                              message_type: :classroom_create_post,
                              notification_type: :classroom
                          }) if classroom&.parent_id && classroom&.parent_id != user_id && classroom&.parent_accepted

      Notification.create({
                              user_id: classroom.teacher_id,
                              from_user_id: self.user_id,
                              post_id: self.id,
                              post_title: self.text,
                              classroom_id: classroom.id,
                              classroom_title: classroom.name,
                              action_id: classroom.id,
                              message_type: :classroom_create_post,
                              notification_type: :classroom
                          }) if classroom&.teacher_id && classroom&.teacher_accepted

      Notification.create({
                              user_id: classroom.caseworker_id,
                              from_user_id: self.user_id,
                              post_id: self.id,
                              post_title: self.text,
                              classroom_id: classroom.id,
                              classroom_title: classroom.name,
                              action_id: classroom.id,
                              message_type: :classroom_create_post,
                              notification_type: :classroom
                          }) if classroom&.caseworker_id && classroom&.caseworker_accepted
    end
  end
end
