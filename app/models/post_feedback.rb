class PostFeedback < ApplicationRecord
  belongs_to :post



  validates :post, presence: true
  validates :type, presence: true

  def self.search_query(params)
    post_feedbacks = PostFeedback.arel_table

    q = post_feedbacks
            .project(Arel.star)
            .group(post_feedbacks[:id])

    if PostFeedback.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(post_feedbacks[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
    else
        q.order(post_feedbacks[:id].desc)
    end

    q.where(post_feedbacks[:id].eq(params[:id])) if params[:id].present?
    q.where(post_feedbacks[:type].matches("%#{params[:type]}%")) if params[:type].present?
    q.where(post_feedbacks[:created_at].in(Date.parse(params[:created_at]).beginning_of_day..Date.parse(params[:created_at]).end_of_day)) if params[:created_at].present?
    q.where(post_feedbacks[:updated_at].in(Date.parse(params[:updated_at]).beginning_of_day..Date.parse(params[:updated_at]).end_of_day)) if params[:updated_at].present?

    q
  end

  private

end
