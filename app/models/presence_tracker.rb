class PresenceTracker

  def self.redis
    @redis ||= ::Redis.new(url: "redis://localhost:6379/1")
  end

  def self.page_users(page)
    redis.smembers(page)
  end

  def self.track(page, user_id)
    redis.sadd(page, user_id)
  end

  def self.untrack(page, user_id)
    redis.srem(page, user_id)
  end
end
