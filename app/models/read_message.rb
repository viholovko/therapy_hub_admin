class ReadMessage < ApplicationRecord

  belongs_to :user, required: false
  belongs_to :readable, polymorphic: true, required: false

  validates :readable_type, presence: {message: {code: 'E00607', message: 'Readable type can\'t be blank.'}}
  validates :readable_id, uniqueness: {scope:[:user_id, :readable_type], message: {code: 'E00608', message: 'Read is already exist.'}}


  def self.unread_message_count(user_id)
    read_messages = ReadMessage.arel_table

    q = read_messages.project('COUNT(*)')
    q.where(read_messages[:user_id].eq(user_id))

    q
  end
end
