class Report < ApplicationRecord

  enum report_type: {
    spam: 0,
    inappropriate: 1
  }

  belongs_to :user, required: false
  belongs_to :post, required: false

  validates :user, presence: {message: {code: 'E01200', message: 'User can\'t be blank.'}}
  validates :user, uniqueness: {scope: :post, message: {code: 'E01203', message: 'Report is already exist.'}}
  validates :post, presence: {message: {code: 'E01201', message: 'Post can\'t be blank.'}}
  validates :report_type, presence: {message: {code: 'E01202', message: 'Report type can\'t be blank.'}}
  validates :report_type, inclusion: {in: %w(spam inappropriate), message: {code: 'E01204', message: "Report type is not valid."}}

  after_commit :create_notification

  private

  def create_notification
    Notification.create({user_id: self.post.user_id, from_user_id: self.user_id, post_id: self.post.id, action_id: self.post.id, post_title: self.post.text, notification_type: :posts, message_type: :post_report})
  end

end