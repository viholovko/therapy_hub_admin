class Setting < ApplicationRecord

  enum geo_search: {everyone: 0, friends: 1, no_one: 2}
  enum system_of_units: {metric: 0, imperial: 1}

  belongs_to :user



end

