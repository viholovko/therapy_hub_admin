class Silent < ApplicationRecord

  belongs_to :user, required: false
  belongs_to :silentable, polymorphic: true, required: false

  validates :silentable_id, presence: {message: {code: 'E01400', message: 'Silentable id can\'t be blank.'}}
  validates :silentable_type, presence: {message: {code: 'E01401', message: 'Silentable type can\'t be blank.'}}
  validates :silentable_id, uniqueness: {scope:[:user_id, :silentable_type], message: {code: 'E01402', message: 'Silentable is already exist.'}}

  private

end
