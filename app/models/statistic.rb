class Statistic < ApplicationRecord

  enum activity_type: {
    login: 0,
    registration: 1,
    advertisement: 2,
    post: 3
  }

  belongs_to :user, required: false
  belongs_to :post, required: false

  validates :user, presence: {message: {code: 'E01200', message: 'User can\'t be blank.'}}

  private

  def to_json
    {
      name: name,
      login: login,
      registration: registration,
      advertisement: advertisement,
      post: post
    }
  end

end