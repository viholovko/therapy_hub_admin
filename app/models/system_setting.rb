class SystemSetting < ApplicationRecord

  before_save :configure_twillio
  before_save :configure_s3

  has_attached_file :ios_push_certificate, { validate_media_type: false }
  do_not_validate_attachment_file_type :ios_push_certificate

  class << self
    SystemSetting.column_names.each do |column|
      define_method column do
        instance.send(column)
      end
    end

    def ios_push_certificate
      instance.ios_push_certificate
    end
  end

  def self.instance
    first_or_create do |s|
      s.ios_push_environment = 'sandbox'
      s.ios_push_password = ''
      s.ios_push_apns_host =
      s.ios_push_environment_sandbox = true

      s.android_push_token = ''

      s.twilio_account_sid = ''
      s.twilio_auth_token = ''
      s.twilio_phone_number = ''
    end
  end

  def configure_twillio
    Twilio.configure do |config|
      config.account_sid = twilio_account_sid
      config.auth_token = twilio_auth_token
    end
  end

  def configure_s3
    s3_credentials = {
        bucket: s3_bucket,
        access_key_id: aws_access_key_id,
        secret_access_key: aws_secret_access_key,
        s3_region: s3_region
    }
    Paperclip::Attachment.default_options[:storage] = :s3
    Paperclip::Attachment.default_options[:s3_credentials] = s3_credentials
  end

  def movetos3(classes)
    bucket_name = s3_bucket

    Aws.config.update({
                          region: s3_region,
                          credentials: Aws::Credentials.new(aws_access_key_id, aws_secret_access_key)
                      })

    s3 = Aws::S3::Resource.new(region: s3_region)

    bucket = s3.bucket(s3_bucket)

    classes.each do |class_info|
      begin
        class_name = class_info.split(":")[0]
        attachment_name = class_info.split(":")[1].downcase

        class_def = class_name.capitalize.constantize

        puts "Migrating #{class_name}:#{attachment_name}..."
        if class_def.all.empty?
          puts "#{class_name} is empty"
          next
        end

        styles = class_def.first.send(attachment_name).styles.map{|style| style[0]} + [:original]

        class_def.find_each do |instance|
          begin
            if not instance.send(attachment_name).exists? or instance.send(attachment_name).url.include? "amazonaws"
              next
            end

            styles.each do |style|
              attach = instance.send(attachment_name).path(style.to_sym)
              filename = attach.split("/").last
              path = "#{class_name.downcase.pluralize}/#{instance.id}/#{attachment_name.pluralize}/#{style}/#{filename}"
              puts "Storing #{style} #{filename} in S3..."
              attachment = bucket.object(path).upload_file(attach)
            end
          rescue Exception => e
            puts "Error on id #{ instance.id }"
            puts e.message
          end
        end
      rescue Aws::S3::Errors::NoSuchBucket => e
        puts "Creating the non-existing bucket: #{bucket_name}"
        s3.buckets.create(bucket_name)
        retry
      rescue Exception => e
        puts e.message
        puts "Ignoring #{class_name}"
      end
      puts ""
    end
  end

  # def configure_stripe
  #   if stripe_publishable_key_changed? || stripe_secret_key_changed?
  #     Rails.configuration.stripe = {
  #         :publishable_key => stripe_publishable_key,
  #         :secret_key      => stripe_secret_key
  #     }
  #
  #     Stripe.api_key = Rails.configuration.stripe[:secret_key]
  #   end
  # end
  #
  # def configure_qb
  #   if qb_host_changed? || qb_application_id_changed? || qb_auth_key_changed? || qb_auth_secret_changed?
  #     QuickBlox.configure do |config|
  #       config.host = qb_host || "https://api.quickblox.com"
  #       config.application_id = qb_application_id.to_s
  #       config.auth_key = qb_auth_key.to_s
  #       config.auth_secret = qb_auth_secret.to_s
  #     end
  #   end
  # end

end
