class Task < ApplicationRecord
  belongs_to :classroom, required: false
  belongs_to :owner, class_name: 'User'
  has_many :links, dependent: :destroy

  before_create :remove_expired_tasks
  after_commit :create_notification
  after_update  :update_notification

  validates :title,       presence: {message: {code: 'E00800', message: 'Title can\'t be blank.'        }}
  validates :description, presence: {message: {code: 'E00801', message: 'Description can\'t be blank.'  }}
  validates :due_date,    presence: {message: {code: 'E00802', message: 'Due date can\'t be blank.'     }}
  validates :classroom,   presence: {message: {code: 'E00803', message: 'Classroom can\'t be blank.'    }}

  has_many :attachments, as: :attachable, dependent: :destroy
  accepts_nested_attributes_for :attachments, allow_destroy: true
  accepts_nested_attributes_for :links, allow_destroy: true

  # validates :attachments, presence: {
  #   message: {code: 'E00804', message: 'Attachment can\'t be blank'}
  # }

  def self.search_query(params)
    tasks = Task.arel_table

    if params[:count]
      q = tasks.project(params[:count] ? 'COUNT(*)' : Arel.star)
    else
      q = tasks.group(tasks[:id])
            .project('tasks.*',
                     "(
                          SELECT json_agg(att)
                          FROM (
                            SELECT *
                            FROM attachments
                            WHERE attachments.attachable_id = tasks.id AND attachments.attachable_type = 'Task'
                        ) att ) AS attachments",
                     "(
                          SELECT json_agg(att)
                          FROM (
                            SELECT *
                            FROM links
                            WHERE links.task_id = tasks.id
                        ) att ) AS links"
            )

      if Task.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(tasks[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.group(tasks[:id])
        q.order(tasks[:completed].desc, tasks[:due_date].asc)
      end
    end

    q.where(tasks[:classroom_id].eq(params[:classroom_id]))                             if params[:classroom_id].present?
    q.where(tasks[:due_date].gteq(DateTime.parse(params[:from_date]).beginning_of_day)) if params[:from_date].present?
    q.where(tasks[:due_date].lteq(DateTime.parse(params[:to_date]).end_of_day))         if params[:to_date].present?
    q.where(tasks[:id].gteq(params[:from_id]))                                          if params[:from_id].present?
    q.where(tasks[:id].lteq(params[:to_id]))                                            if params[:to_id].present?

    q
  end

  def self.not_completed_tasks(params)
    tasks = Task.arel_table

    q = tasks.project( 'COUNT(*)')

    q.where(tasks[:classroom_id].eq(params[:classroom_id]))                 if params[:classroom_id].present?
    q.where(tasks[:completed].eq(false))

    q
  end

  def mark_as_complete(user_id)
    if completed
      errors.add(:base, {code: 'E00803', message: 'The task has already marked as complete'})
      return false
    end
    transaction do
      assign_attributes completed: true, completed_at: DateTime.now
      create_notification(user_id, true)
      save
    end
  end

  private

  def remove_expired_tasks
    Task.where(["updated_at < ? and deleted = ?", Time.now - 2.hour, true]).destroy_all
  end

  def create_notification(user_id = nil, completed = false)
    Notification.create({user_id: classroom.student_id,
                        from_user_id: user_id ? user_id : self.owner_id,
                        classroom_id: self.classroom_id,
                        classroom_title: classroom.name,
                        action_id: self.classroom_id,
                        sub_resource_id: self.id,
                        sub_resource_title: self.title,
                        sub_resource_time_to: self.due_date,
                        message_type: completed ? :classroom_complete_task : :classroom_create_task,
                        notification_type: :classroom,
                        }
    ) if classroom.student_id != user_id && classroom.student_id && classroom.student_accepted

    Notification.create({user_id: classroom.parent_id,
                         from_user_id: user_id ? user_id : self.owner_id,
                         classroom_id: self.classroom_id,
                         classroom_title: classroom.name,
                         action_id: self.classroom_id,
                         sub_resource_id: self.id,
                         sub_resource_title: self.title,
                         sub_resource_time_to: self.due_date,
                         message_type: completed ? :classroom_complete_task : :classroom_create_task,
                         notification_type: :classroom,
                        }
    ) if classroom.parent_id != user_id && classroom.parent_id && classroom.parent_accepted

    Notification.create({user_id: classroom.teacher_id,
                         from_user_id: user_id ? user_id : self.owner_id,
                         classroom_id: self.classroom_id,
                         classroom_title: classroom.name,
                         action_id: self.classroom_id,
                         sub_resource_id: self.id,
                         sub_resource_title: self.title,
                         sub_resource_time_to: self.due_date,
                         message_type: completed ? :classroom_complete_task : :classroom_create_task,
                         notification_type: :classroom,
                        }
    ) if classroom.teacher_id != self.owner_id && classroom.teacher_id && classroom.teacher_accepted

    Notification.create({user_id: classroom.caseworker_id,
                         from_user_id: user_id ? user_id : self.owner_id,
                         classroom_id: self.classroom_id,
                         classroom_title: classroom.name,
                         action_id: self.classroom_id,
                         sub_resource_id: self.id,
                         sub_resource_title: self.title,
                         sub_resource_time_to: self.due_date,
                         message_type: completed ? :classroom_complete_task : :classroom_create_task,
                         notification_type: :classroom,
                        }
    ) if classroom.caseworker_id != self.owner_id && classroom.caseworker_id && classroom.caseworker_accepted

    # Notification.create({user_id: classroom.student_id,
    #                      from_user_id: user_id ? user_id : self.owner_id,
    #                      classroom_id: self.classroom_id,
    #                      classroom_title: classroom.name,
    #                      action_id: self.classroom_id,
    #                      sub_resource_id: self.id,
    #                      sub_resource_title: self.title,
    #                      sub_resource_time_to: self.due_date,
    #                      message_type: :classroom_coming_task,
    #                      notification_type: :classroom,
    #                      scheduled: true
    #                     }
    # ) if classroom.student_id != user_id && classroom.student_id && classroom.student_accepted && !completed
    #
    # Notification.create({user_id: classroom.parent_id,
    #                      from_user_id: user_id ? user_id : self.owner_id,
    #                      classroom_id: self.classroom_id,
    #                      classroom_title: classroom.name,
    #                      action_id: self.classroom_id,
    #                      sub_resource_id: self.id,
    #                      sub_resource_title: self.title,
    #                      sub_resource_time_to: self.due_date,
    #                      message_type: :classroom_coming_task,
    #                      notification_type: :classroom,
    #                      scheduled: true
    #                     }
    # ) if classroom.parent_id != user_id && classroom.parent_id && classroom.parent_accepted && !completed

    #
    #   period 1
    #

    Notification.create({user_id: classroom.student_id,
                         from_user_id: user_id ? user_id : self.owner_id,
                         classroom_id: self.classroom_id,
                         classroom_title: classroom.name,
                         action_id: self.classroom_id,
                         sub_resource_id: self.id,
                         sub_resource_title: self.title,
                         sub_resource_time_from: self.due_date,
                         sub_resource_time_to: self.notification_period_1,
                         message_type: :classroom_reminder1_task,
                         notification_type: :classroom,
                         scheduled: true
                        }
    ) if classroom.student_id != user_id && classroom.student_id && classroom.student_accepted && !completed && self.notification_period_1.present?

    Notification.create({user_id: classroom.parent_id,
                         from_user_id: user_id ? user_id : self.owner_id,
                         classroom_id: self.classroom_id,
                         classroom_title: classroom.name,
                         action_id: self.classroom_id,
                         sub_resource_id: self.id,
                         sub_resource_title: self.title,
                         sub_resource_time_from: self.due_date,
                         sub_resource_time_to: self.notification_period_1,
                         message_type: :classroom_reminder1_task,
                         notification_type: :classroom,
                         scheduled: true
                        }
    ) if classroom.parent_id != user_id && classroom.parent_id && classroom.parent_accepted && !completed && self.notification_period_1.present?

    #
    #   period 2
    #

    Notification.create({user_id: classroom.student_id,
                         from_user_id: user_id ? user_id : self.owner_id,
                         classroom_id: self.classroom_id,
                         classroom_title: classroom.name,
                         action_id: self.classroom_id,
                         sub_resource_id: self.id,
                         sub_resource_title: self.title,
                         sub_resource_time_from: self.due_date,
                         sub_resource_time_to: self.notification_period_2,
                         message_type: :classroom_reminder2_task,
                         notification_type: :classroom,
                         scheduled: true
                        }
    ) if classroom.student_id != user_id && classroom.student_id && classroom.student_accepted && !completed && self.notification_period_2.present?

    Notification.create({user_id: classroom.parent_id,
                         from_user_id: user_id ? user_id : self.owner_id,
                         classroom_id: self.classroom_id,
                         classroom_title: classroom.name,
                         action_id: self.classroom_id,
                         sub_resource_id: self.id,
                         sub_resource_title: self.title,
                         sub_resource_time_from: self.due_date,
                         sub_resource_time_to: self.notification_period_2,
                         message_type: :classroom_reminder2_task,
                         notification_type: :classroom,
                         scheduled: true
                        }
    ) if classroom.parent_id != user_id && classroom.parent_id && classroom.parent_accepted && !completed && self.notification_period_2.present?
  end

  def update_notification
    if self.deleted
      Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true).destroy_all
    else
      Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true, message_type: :classroom_coming_task).update_all(sub_resource_title: self.title, sub_resource_time_to: self.due_date)
      Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true, message_type: :classroom_reminder1_task).update_all(sub_resource_title: self.title, sub_resource_time_to: self.notification_period_1)
      Notification.where(classroom_id: self.classroom_id, sub_resource_id: self.id, scheduled: :true, message_type: :classroom_reminder2_task).update_all(sub_resource_title: self.title, sub_resource_time_to: self.notification_period_2)
    end
  end

end
