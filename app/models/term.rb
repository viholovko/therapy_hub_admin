class Term < ApplicationRecord

  has_many :attachments, as: :attachable, dependent: :destroy

  def text_attachments
    Attachment.where entity_id: self.id, entity_type: 'terms_text'
  end

  after_destroy :destroy_attachments
  after_save :update_attachments

  validates :body, presence: {message: I18n.t('validations.cant_be_blank')}

  private

  def destroy_attachments
    text_attachments.destroy_all
  end

  def update_attachments
    update_text_attachments
  end

  def update_text_attachments
    Attachment.where('created_at <= :day_ago AND attachable_id IS NULL', :day_ago  => 1.day.ago ).destroy_all

    Attachment.where(attachable_id: nil, attachable_type: "Term").each do |attachment|
      if body.include? attachment.file.url
        attachment.update_attributes attachable_id: id, attachable_type: 'Term'
      end
    end

    puts 'OOOOOO'
    puts self.id
    puts attachments.count


    attachments.each do |attachment|
      puts 'here'
      if body.include? attachment.file.url

      else
        puts 'hereQWQWQWQWQW'
        attachment.destroy
      end
    end
  end
end
