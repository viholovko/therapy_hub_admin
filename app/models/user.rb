class User < ApplicationRecord

  include ApplicationHelper

  has_many :sessions, dependent: :destroy
  has_many :chat_messages, dependent: :destroy
  belongs_to :role
  belongs_to :payment_plan, required: false
  belongs_to :country, required: false
  has_many :friendships
  has_one :payment, dependent: :destroy
  has_many :friends, through: :friendships
  has_many :notifications, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :classrooms, ->(user) {
    unscope(where: :user_id).where(
        "student_id = :user_id OR
          parent_id = :user_id OR
          teacher_id = :user_id OR
          caseworker_id = :user_id", user_id: user.id)
  }, dependent: :destroy
  has_one :setting, dependent: :destroy
  has_and_belongs_to_many :followed, class_name: 'User',
                          foreign_key: :followed_id,
                          association_foreign_key: :follower_id,
                          join_table: :followships, dependent: :destroy
  has_and_belongs_to_many :followers, class_name: 'User',
                          foreign_key: :follower_id,
                          association_foreign_key: :followed_id,
                          join_table: :followships, dependent: :destroy
  has_and_belongs_to_many :tagged_posts,
                          class_name: 'Post',
                          join_table: :posts_tagged_users,
                          foreign_key: :user_id,
                          association_foreign_key: :post_id
  has_many :posts, dependent: :destroy
  has_and_belongs_to_many :chats
  has_and_belongs_to_many :blocked_users, class_name: 'User',
                          foreign_key: :user_id, association_foreign_key: :blocked_user_id,
                          join_table: :block_lists, dependent: :destroy
  has_and_belongs_to_many :blocker_users, class_name: 'User',
                          foreign_key: :blocked_user_id, association_foreign_key: :user_id,
                          join_table: :block_lists, dependent: :destroy

  attr_accessor :password, :password_confirmation

  has_attached_file :avatar,
                    styles: { medium: '1000x1000>', thumb: '120x250>' },
                    default_url: '/images/missing.png',
                    path: ":rails_root/public/system/users/:id/avatars/:style/:filename",
                    url: "/system/users/:id/avatars/:style/:filename"

  validates :country, presence: {message: {code: 'E00113', message: "Country can't be blank."}}, if: -> { !admin? }
  validates :first_name,
            presence: { message: {code: "E00103", message: "First name can't be blank."}},
            format: { with: /\A[a-zA-Z]+\z/,
                      message: {code: "E00110", message: "First name must be letters only"}
            }, if: -> { !admin? }
  validates :last_name,
            presence: { message: {code: "E00104", message: "Last name can't be blank."}},
            format: {with: /\A[a-zA-Z]+\z/,
                     message: {code: "E00111", message: "Last name must be letters only"}
            }, if: -> { !admin? }
  validates :phone_number,
            presence: { message: {code: "E00105", message: "Phone number can't be blank."}},
            uniqueness: { message: {code: "E00106", message: "Phone number already registered."}, scope: :country_id},
            numericality: {message: {code: "E00108", message: "Phone number must be numeric"}}, if: -> { !admin? }
  validate :validate_phone_number_length
  validates :password,
            presence: true, confirmation: true, length: {within: 6..40}, if: :validate_password?
  validates :password_confirmation,
            presence: true, if: :validate_password?
  validates :email,
            uniqueness: { case_sensitive: false, message: 'This email address is already registered.'},
            format: { with: /.*\@.*\..*/, message: 'is incorrect'}, allow_blank: true
  validates_attachment_size :avatar, less_than: 20.megabytes, unless: Proc.new {|model| model.avatar }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  before_save       :encrypt_password
  before_validation :downcase_email
  before_destroy    :validate_destroy
  before_save :verify_role
  # before_save :verify_payment_plan
  after_create :create_settings
  after_destroy :destroy_friendships

  Role::NAMES.each do |name_constant|
    define_method("#{name_constant}?") { self.role.try(:name) == name_constant.to_s }
  end

  def authenticate(password)
    self.encrypted_password == encrypt(password)
  end

  class << self
    def query(params)
      users = User.arel_table
      roles = Role.arel_table
      countries = Country.arel_table
      friendships = Friendship.arel_table
      my_friendships = Friendship.arel_table.alias('my_friendships')
      followers = Arel::Table.new(:followships).alias(:followers)
      my_followers = Arel::Table.new(:followships).alias(:my_followers)
      followed = Arel::Table.new(:followships).alias(:followed)
      my_followed = Arel::Table.new(:followships).alias(:my_followed)
      my_block_list = Arel::Table.new(:block_lists).alias(:my_block_list)
      their_block_list = Arel::Table.new(:block_lists).alias(:their_block_list)
      settings = Setting.arel_table


      params[:role_ids] ||= []
      params[:roles] ||= []
      params[:roles].each do |role_name|
        params[:role_ids] << Role.send("get_#{ role_name }").id if Role.respond_to?("get_#{ role_name }")
      end

      q = users
              .project(
                  'users.*',
                  roles[:name].as('role_name'),
                  "json_build_object(
                      'id', countries.id, 'name', countries.name,
                      'flag_file_name', countries.flag_file_name, 'flag_content_type', countries.flag_content_type,
                      'phone_code', countries.phone_code, 'alpha2_code', countries.alpha2_code,
                      'alpha3_code', countries.alpha3_code
                   ) AS country",
                  my_friendships[:status].as('friendship_status'),
                  my_friendships[:created_at].as('friendship_created_at'),
                  my_friendships[:accepted_at].as('friendship_accepted_at'),
                  params[:current_user] && params[:current_user].longitude && params[:current_user].latitude ?
                      "(SELECT gc_dist(#{ params[:current_user].latitude }, #{ params[:current_user].longitude }, users.latitude, users.longitude)) AS distance"
                      :
                      "0 AS distance",
                  "CASE WHEN my_followers.follower_id IS NULL THEN false ELSE true END AS follower_status",
                  "CASE WHEN my_followed.followed_id IS NULL THEN false ELSE true END AS followed_status",
                  "(
                    SELECT count(*)
                      FROM friendships
                      WHERE friendships.user_id = users.id and friendships.status=2
                    ) AS friends_count",
                  "CASE
                    WHEN my_block_list.user_id = #{params[:current_user].id.to_i}
                      THEN 'i_blocked'
                    WHEN their_block_list.user_id = users.id
                      THEN 'me_blocked'
                    ELSE 'none'
                  END AS who_blocked"
              )
              .group(users[:id], roles[:id], countries[:id], friendships[:id], my_friendships[:id], followers[:id], my_followers[:id], followed[:id], my_followed[:id], my_block_list[:id], their_block_list[:id])

      q.join(roles, Arel::Nodes::OuterJoin).on(users[:role_id].eq(roles[:id]))
      q.join(countries, Arel::Nodes::OuterJoin).on(users[:country_id].eq(countries[:id]))
      q.join(friendships, Arel::Nodes::OuterJoin).on(users[:id].eq(friendships[:friend_id]).and(friendships[:user_id].eq(params[:friendship_user_id] || params[:current_user].id)))
      q.join(my_friendships, Arel::Nodes::OuterJoin).on(users[:id].eq(my_friendships[:friend_id]).and(my_friendships[:user_id].eq(params[:current_user].id)))

      q.join(my_block_list, Arel::Nodes::OuterJoin).on(my_block_list[:user_id].eq(params[:current_user].id).and(my_block_list[:blocked_user_id].eq(users[:id])))
      q.join(their_block_list, Arel::Nodes::OuterJoin).on(their_block_list[:user_id].eq(users[:id]).and(their_block_list[:blocked_user_id].eq(params[:current_user].id)))

      q.join(followers, Arel::Nodes::OuterJoin).on(users[:id].eq(followers[:follower_id]).and(followers[:followed_id].eq(params[:followship_user_id] || params[:current_user].id)))
      q.join(followed, Arel::Nodes::OuterJoin).on(users[:id].eq(followed[:followed_id]).and(followed[:follower_id].eq(params[:followship_user_id] || params[:current_user].id)))

      q.join(my_followers, Arel::Nodes::OuterJoin).on(users[:id].eq(my_followers[:follower_id]).and(my_followers[:followed_id].eq(params[:current_user].id)))
      q.join(my_followed, Arel::Nodes::OuterJoin).on(users[:id].eq(my_followed[:followed_id]).and(my_followed[:follower_id].eq(params[:current_user].id)))

      q.join(settings, Arel::Nodes::OuterJoin).on(users[:id].eq(settings[:user_id]))

      q.where(users[:id].eq(params[:id]))                                 if params[:id].present?
      q.where(users[:email].matches("%#{params[:email]}%"))               if params[:email].present?
      q.where(users[:first_name].matches("%#{params[:first_name]}%"))     if params[:first_name].present?
      q.where(users[:last_name].matches("%#{params[:last_name]}%"))       if params[:last_name].present?
      q.where(users[:first_name].matches("%#{params[:name]}%").or(users[:last_name].matches("%#{params[:name]}%"))) if params[:name].present?
      q.where(users[:phone_number].matches("%#{params[:phone_number]}%")) if params[:phone_number].present?
      q.where(roles[:id].in(params[:role_ids]))                           if params[:role_ids].present?
      q.where(users[:id].not_eq(params[:current_user].id))                if params[:except_me] && params[:current_user]
      q.where(users[:id].not_in(params[:ignore_users])) if params[:ignore_users].present?
      q.where(roles[:name].not_eq('admin'))                               if !params[:show_admin].present?

      q.where(
          settings[:geo_search].eq(:everyone).or(users.grouping(
          friendships[:status].eq('accepted').and(settings[:geo_search].eq(:friends))
          ))
      )

      if params[:friendship_status].present? && params[:friendship_status].is_a?(Array) && params[:followship_status].present? && params[:followship_status].is_a?(Array)
        friendship_status_condition = friendships[:status].in(params[:friendship_status].map{|i| Friendship.statuses[i] })

        if params[:friendship_status].include?('not_connected')
          friendship_status_condition = friendship_status_condition.or(friendships[:status].eq(nil))
        end

        friendship_status_condition = friendship_status_condition.or(users[:id].eq(-1))

        if params[:followship_status].include?('followers')
          friendship_status_condition = friendship_status_condition.or(followers[:id].not_eq(nil))
        end

        if params[:followship_status].include?('followed')
          friendship_status_condition = friendship_status_condition.or(followed[:id].not_eq(nil))
        end

        if params[:followship_status].include?('not_connected')
          friendship_status_condition = friendship_status_condition.or(
            followers.grouping(   followers[:id].eq(nil) .and(followed[:id].eq(nil)))
          )
        end

        q.where(friendship_status_condition)

      elsif params[:friendship_status].is_a?(Array) && !params[:followship_status].present?
        friendship_status_condition = friendships[:status].in(params[:friendship_status].map{|i| Friendship.statuses[i] })

        if params[:friendship_status].include?('not_connected')
          friendship_status_condition = friendship_status_condition.or(friendships[:status].eq(nil))
        end

        q.where(friendship_status_condition)
      elsif params[:followship_status].is_a?(Array) && !params[:friendship_status].present?
        followship_status_condition = users[:id].eq(-1)

        if params[:followship_status].include?('followers')
          followship_status_condition = followship_status_condition.or(followers[:id].not_eq(nil))
        end

        if params[:followship_status].include?('followed')
          followship_status_condition = followship_status_condition.or(followed[:id].not_eq(nil))
        end

        if params[:followship_status].include?('not_connected')
          followship_status_condition = followship_status_condition.or(
              followers.grouping(   followers[:id].eq(nil) .and(followed[:id].eq(nil)))
          )
        end

        q.where(followship_status_condition)
      end

      q
    end

    def search_query(params)
      users = User.arel_table

      q = query(params).as('t')

      result = users.project(params[:count] ? "COUNT(*)" : "t.*").from(q)

      if params[:count]

      else
        if [*User.column_names, 'distance'].include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          result.order(q[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
        else
          result.order(q[:id].desc)
        end
      end

      result
    end

    def tagged_users_query(params)
      users = User.arel_table
      tagged_users = Arel::Table.new(:posts_tagged_users)
      q = query(params).as('t')

      result = users.project(params[:count] ? "COUNT(*)" : "t.*").from(q)

      result.join(tagged_users, Arel::Nodes::OuterJoin)
          .on(tagged_users[:post_id].eq(params[:post_id]))
      result.where(q[:id].eq(tagged_users[:user_id]))

      result
    end

    def recommendations_query(params)
      users = User.arel_table
      friendships = Friendship.arel_table.alias('friendships1')
      friendships2 = Friendship.arel_table.alias('friendships2')

      users = User.arel_table

      q = query(params).as('t')

      result = users.project(params[:count] ? "COUNT(*)" : "t.*").from(q)

      result.where(Arel::Nodes::SqlLiteral.new("
        (
          SELECT COUNT(*) from friendships f1
            JOIN friendships f2 ON f2.user_id = f1.friend_id
          WHERE f1.user_id = t.id
            AND f2.friend_id = #{params[:current_user].id}
            AND f1.friend_id != #{params[:current_user].id}
        ) > 0"
      ))

      if params[:count]

      else
        if [*User.column_names, 'distance'].include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          result.order(q[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
        else
          result.order(q[:id].desc)
        end
      end

      result
    end

    def likes_query(params)
      users = User.arel_table
      likes = Like.arel_table
      allowed_types = ['Post']
      q = query(params).as('t')

      result = users.project(params[:count] ? "COUNT(*)" : "t.*").from(q)

      result.join(likes, Arel::Nodes::OuterJoin)
          .on(
              likes[:likeable_id].eq(params[:likeable_id])
                  .and(
                      likes[:likeable_type].eq(params[:likeable_type])
                  )
          )
      result.where(q[:id].eq(likes[:user_id]))

      result
    end

    def query_user(params)
      users = User.arel_table
      countries = Country.arel_table

      q = users.project(
                  'users.*',
                  "(
                    SELECT json_agg(t) FROM (
                      SELECT * FROM users AS local WHERE local.id IN
                          (SELECT friend_id FROM friendships WHERE friendships.user_id = users.id AND friendships.status = 2)
                    ) t
                  ) AS friends_users",
                  "(
                    SELECT json_agg(t) FROM (
                      SELECT * FROM users AS local WHERE local.id IN
                          (SELECT follower_id FROM followships WHERE followships.followed_id = users.id)
                    ) t
                  ) AS followers",
                  "(
                    SELECT json_agg(t) FROM (
                      SELECT * FROM users AS local WHERE local.id IN
                          (SELECT followed_id FROM followships WHERE followships.follower_id = users.id)
                    ) t
                  ) AS followeds",
                  "json_build_object(
                      'id', countries.id, 'name', countries.name,
                      'flag_file_name', countries.flag_file_name, 'flag_content_type', countries.flag_content_type,
                      'phone_code', countries.phone_code, 'alpha2_code', countries.alpha2_code,
                      'alpha3_code', countries.alpha3_code
                   ) AS country",
                  "(SELECT json_agg(bu)
                    FROM (
                      SELECT *
                      FROM users AS local
                      WHERE local.id IN
                        (SELECT blocked_user_id
                          FROM block_lists
                          WHERE block_lists.user_id = users.id)
                    ) bu
                  ) AS i_blocked_users",
                  "(SELECT COUNT(*)
                    FROM block_lists
                    WHERE users.id = block_lists.user_id
                  ) AS i_blocked_users_count",
                  "(SELECT json_agg(bu)
                    FROM (
                      SELECT *
                      FROM users AS local
                      WHERE local.id IN
                        (SELECT user_id
                          FROM block_lists
                          WHERE block_lists.blocked_user_id = users.id)
                    ) bu
                  ) AS me_blocked_users",
                  "(SELECT COUNT(*)
                    FROM block_lists
                    WHERE users.id = block_lists.blocked_user_id
                  ) AS me_blocked_users_count"
              )

      q.join(countries, Arel::Nodes::OuterJoin).on(users[:country_id].eq(countries[:id]))

      q.where(users[:id].eq(params[:id])) if params[:id].present?

      q
    end

  end

  def to_json_for_broadcast_user
    result = {
        id: id,
        first_name: first_name,
        last_name: last_name,
        avatar: paperclip_url(avatar),
        avatar_preview: paperclip_url(avatar, :medium),
        online: chat_active,
        last_chat_login: last_chat_login
    }

    result.to_hash
  end

  def to_json_for_chat_message
    result = {
        id: id,
        phone_number: phone_number,
        first_name: first_name,
        last_name: last_name,
        role: role.name,
        role_verified: role_verified,
        avatar: paperclip_url(avatar),
        about_me: about_me,
        online: chat_active,
        last_chat_login: last_chat_login
    }

    result.to_hash
  end

  def validate_destroy
    if self.admin? && User.where(role_id: Role.get_admin.id).count == 1
      self.errors.add :base, 'Can not remove last admin.'
      throw :abort
      false
    elsif User.count == 1

      self.errors.add :base, 'Can not remove last user.'
      throw :abort
      false
    end
  end

  def to_json
    {
        id: id,
        phone_number: phone_number,
        first_name: first_name,
        last_name: last_name,
        country: {
            id: country_id,
            name: country&.name
        },
        role: role.name,
        role_verified: role_verified,
        avatar: paperclip_url(avatar),
        avatar_preview: paperclip_url(avatar, :medium),
        about_me: about_me,
        followers_count: followers.count,
        followed_count: followed.count,
        system_of_units: setting.system_of_units
    }
  end

  def to_json_chat_message
    {
        id: id,
        phone_number: phone_number,
        first_name: first_name,
        last_name: last_name,
        country: {
            id: country_id,
            name: country&.name
        },
        role: role.name,
        role_verified: role_verified,
        avatar: paperclip_url(avatar),
        avatar_preview: paperclip_url(avatar, :medium),
        about_me: about_me,
        followers_count: followers.count,
        followed_count: followed.count,
        online: chat_active,
        last_chat_login: last_chat_login
    }
  end

  def follow(other_user)
    if self.id != other_user.id
      if followed?(other_user)
      followed<<other_user
      Notification.create user_id: other_user.id,
                          from_user_id: id,
                          message_type: :friend_request_follow,
                          notification_type: :friend_requests,
                          action_id: id
      else
        errors.add :base, {code: 'E00302', message: "You already following to #{other_user.first_name} #{other_user.last_name}."}
        return false
      end
    else
      errors.add :base, {code: 'E00301', message: 'You can\'t follow on to yourself.'}
      return false
    end
  end

  def unfollow(other_user)
    unless followed?(other_user)
      followed.delete other_user
      Notification.create user_id: other_user.id,
                          from_user_id: id,
                          message_type: :friend_request_unfollow,
                          notification_type: :friend_requests,
                          action_id: id
    else
      errors.add :base, {code: 'E00303', message: "You have not been followed to #{other_user.first_name} #{other_user.last_name} before."}
      return false
    end

  end

  def followed?(other_user)
    not followed.include? other_user
  end

  def block_user(other_user)
    if self.id != other_user.id
      if blocked?(other_user)
        Friendship.remove_from_friend(self.id, other_user.id)

        block_user_in_classroom(self.id, other_user.id)

        blocked_users<<other_user
        Notification.create user_id: other_user.id,
                            from_user_id: id,
                            message_type: :friend_request_add_to_block_list,
                            notification_type: :friend_requests
      else
        errors.add :base, {code: 'E00900', message: "You already blocking #{other_user.first_name} #{other_user.last_name}."}
        return false
      end
    else
      errors.add :base, {code: 'E00901', message: 'You can\'t block to yourself.'}
      return false
    end
  end

  def unblock_user(other_user)
    unless blocked?(other_user)
      blocked_users.delete other_user
    else
      errors.add :base, {code: 'E00902', message: "You have not been blocked #{other_user.first_name} #{other_user.last_name} before."}
      return false
    end
  end

  def block_user_in_classroom(user, other_user)
    classrooms = Classroom.find_by_sql("SELECT * FROM classrooms WHERE (owner_id=#{user} AND (student_id=#{other_user} OR parent_id=#{other_user} OR teacher_id=#{other_user} OR caseworker_id=#{other_user})) OR (owner_id=#{other_user} AND (student_id=#{user} OR parent_id=#{user} OR teacher_id=#{user} OR caseworker_id=#{user}))")

    classrooms.each do |classroom|
      chat = Chat.find_by(classroom_id: classroom.id)

      if classroom.owner_id == user
        if classroom.student_id == other_user
          classroom.update_attributes student_id: nil, student_accepted: false
        elsif classroom.parent_id == other_user
          classroom.update_attributes parent_id: nil, parent_accepted: false
        elsif classroom.teacher_id == other_user
          classroom.update_attributes teacher_id: nil, teacher_accepted: false
        elsif classroom.caseworker_id == other_user
          classroom.update_attributes caseworker_id: nil, caseworker_accepted: false
        end

        Chat.find_by_sql("DELETE FROM chats_users WHERE chat_id=#{chat.id} AND user_id=#{other_user}" )
      elsif classroom.owner_id == other_user
        if classroom.student_id == user
          classroom.update_attributes student_id: nil, student_accepted: false
        elsif classroom.parent_id == user
          classroom.update_attributes parent_id: nil, parent_accepted: false
        elsif classroom.teacher_id == user
          classroom.update_attributes teacher_id: nil, teacher_accepted: false
        elsif classroom.caseworker_id == user
          classroom.update_attributes caseworker_id: nil, caseworker_accepted: false
        end

        Chat.find_by_sql("DELETE FROM chats_users WHERE chat_id=#{chat.id} AND user_id=#{user}" )
      end
    end
  end

  def blocked?(other_user)
    not blocked_users.include? other_user
  end

  def verify_role
    return if role.name == 'not_upgraded' || role.name == 'admin' || role.name == 'mentor'
    role_id = role.name + '_id'
    if Classroom.where("#{role_id} = ?", id).count > 0 && !role_verified
      update_attributes role_verified: true
      remove_user_from_classroom_after_update_role
    end
  end

  # def verify_payment_plan
  #   return if role.name == 'not_upgraded' || role.name == 'admin' || role.name == 'mentor'
  #   payment_plan = PaymentPlan.find_by(name: 'Not upgrated')
  # end

  def notify(message, data = {}, action = {}, notification = {}, data_android = {})
    self.sessions.uniq{ |s| [s.device_type, s.push_token] }.each do |session|
      case session.device_type
        when 'ios'
          if session.push_token.present? && session.push_token.length>60
            begin
            tokens = [ session.push_token ]
            password = nil
            host = SystemSetting.ios_push_apns_host
            envirement_mode = SystemSetting.ios_push_environment == 'sandbox'
            notification = RubyPushNotifications::APNS::APNSNotification.new tokens, { aps: { data: data, action: action, alert: message, sound: 'true', 'content-available': 1} }
            pusher = RubyPushNotifications::APNS::APNSPusher.new(File.read(SystemSetting.ios_push_certificate.path),envirement_mode, password, { host: host })

            pusher.push [notification]
            rescue Exception => e
              puts "==================Push IOS Error============="
              puts e.message
              puts e.backtrace.join("\n")
            end
          end
        when 'android'
          if session.push_token.present?
            begin
              RestClient::Request.execute(
                method: :post,
                url: 'https://fcm.googleapis.com/fcm/send',
                payload: {
                  to: session.push_token,
                  notification: notification,
                  data: data_android
                }.to_json,
                headers: {'Content-Type': 'application/json', 'Authorization': "key=#{ SystemSetting.android_push_token }"}
              ){ |response, request, result|
                response
              }
            rescue Exception => e
              puts "==================Push Android Error============="
              puts e.message
              puts e.backtrace.join("\n")
            end
          end
      end
    end
  end

  def online
    isOnline = self.sessions.order(updated_at: :desc).first
    return true if isOnline && isOnline.updated_at > Time.now - 15.minutes
    false
  end

  def in_chat?(chat_id)
    PresenceTracker.page_users("chatrooms_channel#{chat_id}").include?(id.to_s)
  end

  private

  def validate_password?
    admin? && (new_record? || !password.nil? || !password_confirmation.nil?)
  end

  def downcase_email
    self.email = self.email.downcase if self.email
  end

  def encrypt_password
    self.salt = make_salt if salt.blank?
    self.encrypted_password = encrypt(self.password) if self.password
  end

  def encrypt(string)
    secure_hash("#{string}--#{self.salt}")
  end

  def make_salt
    secure_hash("#{Time.now.utc}--#{self.password}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

  def validate_phone_number_length
    errors.add(:phone_number,
               {code: "E00109",
                message: "Phone number must be in range of 7 to 15 numeric"
               }) unless phone_number.length.between?(7, 15)
  end

  def remove_user_from_classroom_after_update_role
    Classroom.where("student_id     = ?", self.id).update_all(student_id: nil,
                                                              student_accepted: false)      unless self.role.name == 'student'
    Classroom.where("parent_id      = ?", self.id).update_all(parent_id: nil,
                                                              parent_accepted: false)       unless self.role.name == 'parent'
    Classroom.where("teacher_id     = ?", self.id).update_all(teacher_id: nil,
                                                              teacher_accepted: false)      unless self.role.name == 'teacher'
    Classroom.where("caseworker_id  = ?", self.id).update_all(caseworker_id: nil,
                                                              caseworker_accepted: false)   unless self.role.name == 'caseworker'
  end

  def create_settings
    Setting.create user: self
  end

  def destroy_friendships
    Friendship.where(user_id: id).destroy_all
    Friendship.where(friend_id: id).destroy_all
  end
end
