json.post do
  json.id @post.id
  json.user do
    json.id         @post.user&.id
    json.first_name @post.user&.first_name
    json.last_name  @post.user&.last_name
    json.avatar     @post.user&.avatar.url
    json.role       @post.user&.role.name
  end
  json.text @post.text
  json.link @post.link
  json.toggled_users @post.toggled_users.map{|user| {id: user.id,
                                                     first_name: user.first_name,
                                                     last_name: user.last_name,
                                                     avatar: paperclip_url(user.avatar)}}
  json.attachments @post.attachments.map{|a| { id: a.id, url: paperclip_url(a.file) }}
  json.ads @post.ads
  json.created_at @post.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at @post.updated_at.strftime("%d-%m-%Y %H:%M")
end
