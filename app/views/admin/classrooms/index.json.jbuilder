json.classrooms @classrooms.each do |classroom|
  json.id classroom.id
  json.name classroom.name
  json.assessment_link classroom.assessment_link
  json.created_at classroom.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at classroom.updated_at.strftime("%d-%m-%Y %H:%M")
end
json.count @count