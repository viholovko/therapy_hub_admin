json.classroom do
  json.id @classroom.id
  json.name @classroom.name
  json.assessment_link @classroom.assessment_link
  json.created_at @classroom.created_at.try :strftime, '%d-%m-%Y %H:%M'
  json.updated_at @classroom.updated_at.try :strftime, '%d-%m-%Y %H:%M'

  student = User.find_by(id: @classroom&.student_id)
  json.student do
    json.id           student&.id
    json.first_name   student&.first_name
    json.last_name    student&.last_name
    json.phone_number student&.phone_number
    json.email        student&.email
    json.avatar       paperclip_url student&.avatar
    json.role         student&.role&.name
    json.accepted @classroom.student_accepted
    json.role_classroom 'student'
    json.owner_id @classroom.owner_id
    json.classroom_id @classroom.id
  end

  teacher = User.find_by(id: @classroom&.teacher_id)
  json.teacher do
    json.id           teacher&.id
    json.first_name   teacher&.first_name
    json.last_name    teacher&.last_name
    json.phone_number teacher&.phone_number
    json.email        teacher&.email
    json.avatar       paperclip_url teacher&.avatar
    json.role         teacher&.role&.name
    json.accepted @classroom.teacher_accepted
    json.role_classroom 'teacher'
    json.owner_id @classroom.owner_id
    json.classroom_id @classroom.id
  end

  parent = User.find_by(id: @classroom&.parent_id)
  json.parent do
    json.id           parent&.id
    json.first_name   parent&.first_name
    json.last_name    parent&.last_name
    json.phone_number parent&.phone_number
    json.email        parent&.email
    json.avatar       paperclip_url parent&.avatar
    json.role         parent&.role&.name
    json.accepted @classroom.parent_accepted
    json.role_classroom 'parent'
    json.owner_id @classroom.owner_id
    json.classroom_id @classroom.id
  end

  caseworker = User.find_by(id: @classroom&.caseworker_id)
  json.caseworker do
    json.id           caseworker&.id
    json.first_name   caseworker&.first_name
    json.last_name    caseworker&.last_name
    json.phone_number caseworker&.phone_number
    json.email        caseworker&.email
    json.avatar       paperclip_url caseworker&.avatar
    json.role         caseworker&.role&.name
    json.accepted @classroom.caseworker_accepted
    json.role_classroom 'caseworker'
    json.owner_id @classroom.owner_id
    json.classroom_id @classroom.id
  end
  json.caseworker_accepted @classroom.caseworker_accepted
  json.student_accepted @classroom.student_accepted
  json.teacher_accepted @classroom.teacher_accepted
  json.parent_accepted @classroom.parent_accepted
end
