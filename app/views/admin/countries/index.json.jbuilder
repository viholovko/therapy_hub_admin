json.countries @countries.each do |c|
  json.id c.id
  json.name c.name
  json.phone_code c.phone_code
  json.selected c.selected
  json.alpha3_code c.alpha3_code
  json.alpha2_code c.alpha2_code
  json.phone_code c.phone_code
  json.numeric_code c.numeric_code
  json.flag paperclip_url c.flag
end
json.count @count