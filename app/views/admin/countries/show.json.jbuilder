json.country do
  json.id @country.id
  json.phone_code @country.phone_code
  json.name @country.name
  json.alpha3_code @country.alpha3_code
  json.alpha2_code @country.alpha2_code
  json.numeric_code @country.numeric_code
  json.flag do
    json.url paperclip_url @country.flag
  end
end