json.events @events.each do |event|
  json.id event.id
  json.title event.title
  json.description event.description
  json.time_from event.time_from
  json.time_to event.time_to
  json.created_at event.created_at
end
json.count @count