json.frequently_asked_questions @frequently_asked_questions.each do |faq|
  json.id           faq.id
  json.title        faq.title
  json.description  faq.description
  json.content      faq.content
  json.created_at   faq.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at   faq.updated_at.strftime("%d-%m-%Y %H:%M")
end
json.count @count