json.frequently_asked_question do
  json.id           @frequently_asked_question.id
  json.title        @frequently_asked_question.title
  json.description  @frequently_asked_question.description
  json.content      @frequently_asked_question.content
  json.created_at   @frequently_asked_question.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at   @frequently_asked_question.updated_at.strftime("%d-%m-%Y %H:%M")
end