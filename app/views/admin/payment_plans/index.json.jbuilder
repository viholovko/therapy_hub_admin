json.payment_plans @payment_plans.each do |plan|
  json.id                plan.id
  json.name              plan.name
  json.type_name         plan.type_name
  json.description       plan.description
  json.price             plan.price
  json.classroom_count   plan.classroom_count
  json.apple_id          plan.apple_id
  json.android_id        plan.android_id
  json.active            plan.active
  json.order             plan.order
end