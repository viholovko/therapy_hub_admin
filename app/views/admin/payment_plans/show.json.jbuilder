json.payment_plan do
  json.id                @payment_plan.id
  json.name              @payment_plan.name
  json.type_name         @payment_plan.type_name
  json.description       @payment_plan.description
  json.price             @payment_plan.price
  json.classroom_count   @payment_plan.classroom_count
  json.apple_id          @payment_plan.apple_id
  json.android_id        @payment_plan.android_id
  json.active            @payment_plan.active
  json.order             @payment_plan.order
end
