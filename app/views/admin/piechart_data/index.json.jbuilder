json.data do
  json.post           @data[:post]
  json.ads            @data[:ads]
  json.admin          @data[:admin]
  json.student        @data[:student]
  json.teacher        @data[:teacher]
  json.parent         @data[:parent]
  json.mentor         @data[:mentor]
  json.not_upgraded   @data[:not_upgraded]
end
json.statisticData  @statisticDatas
