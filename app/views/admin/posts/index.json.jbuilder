json.posts @posts.each do |post|
  json.id post.id
  json.created_at post.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at post.updated_at.strftime("%d-%m-%Y %H:%M")
  json.link       post.link
  json.user do
    user = User.new({
                        id: post['user']['id'],
                        first_name: post['user']['first_name'],
                        last_name: post['user']['last_name'],
                        avatar_file_name: post['user']['avatar_file_name'],
                        avatar_content_type: post['user']['avatar_content_type'],
                        avatar_file_size: post['user']['avatar_file_size']
                    })
    json.first_name user.first_name
    json.last_name  user.last_name
    json.id         user.id
    json.avatar     user.avatar.url
    json.full_name "#{user.first_name} #{user.last_name}"
    json.is_owner   user.id == current_user.id
  end
  json.is_mentor     post.role_name == 'mentor'
  json.text post.text
  json.ads post.ads
  json.attachments post['attachments']&.each do |att|
    attachment = Attachment.new ({
      id:                  att['id'],
      file_file_name:      att['file_file_name'],
      file_content_type:   att['file_content_type'],
      file_file_size:      att['file_file_size'],
      attachment_type:     att['attachment_type'],
      user_id:             att['user_id'],
      width:               att['width'],
      height:              att['height']
    })
    json.id               attachment.id
    json.name             attachment.file_file_name
    json.type             attachment.attachment_type
    json.file             attachment.file.url
    json.url_preview      (attachment.file_content_type.match?(/\Aimage\/.*\z/) || attachment.file_content_type.match?(/\Avideo\/.*\z/)) ? paperclip_url(attachment.file, :preview) : nil
    json.width            attachment.width
    json.height           attachment.height
  end
  json.reports_count post['reports_count']
  json.likes_count post['likes_count']
  json.comments_count post['comments_count']
  json.is_admin_created post.is_admin_created
end
json.count @count