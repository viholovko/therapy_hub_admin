json.post do
  json.id @post.id
  json.user do
    json.id         @post.user&.id
    json.first_name @post.user&.first_name
    json.last_name  @post.user&.last_name
    json.avatar     @post.user&.avatar.url
    json.role       @post.user&.role.name
  end
  json.is_mentor     @post.user&.role.name == 'mentor'
  json.text @post.text
  json.link @post.link
  json.toggled_users @post.tagged_users.map{|user| {id: user.id,
                                                     first_name: user.first_name,
                                                     last_name: user.last_name,
                                                     avatar: paperclip_url(user.avatar)}}
  json.ads @post.ads
  json.is_admin_created @post.is_admin_created
  json.created_at @post.created_at.strftime("%d-%m-%Y %H:%M")
  json.updated_at @post.updated_at.strftime("%d-%m-%Y %H:%M")
  json.comments @content[0]['comments']&.each do |item|
    user = User.new id: item['user_id'], first_name: item['first_name'], last_name: item['last_name'],
                    avatar_file_size: item['avatar_file_size'], avatar_content_type: item['avatar_content_type'],
                    avatar_file_name: item['avatar_file_name'], email: item['email'],
                    phone_number: item['phone_number'], created_at: item['created_at']
    json.id item['id']
    json.message item['message']
    json.created_at item['created_at'].to_datetime.try :strftime, '%d-%m-%Y %H:%M'
    json.user do
      json.id user.id
      json.first_name user.first_name
      json.last_name user.last_name
      json.avatar paperclip_url user.avatar
      json.email user.email
      json.phone_number user.phone_number
      json.created_at user.created_at
      json.role item['role']
      json.full_name "#{user.first_name} #{user.last_name}"
    end
  end
  json.likes @content[0]['likes']&.each do |item|
    user = User.new id: item['user_id'], first_name: item['first_name'], last_name: item['last_name'],
                    avatar_file_size: item['avatar_file_size'], avatar_content_type: item['avatar_content_type'],
                    avatar_file_name: item['avatar_file_name'], email: item['email'],
                    phone_number: item['phone_number']
    json.id item['id']
    json.created_at item['created_at'].to_datetime.try :strftime, '%d-%m-%Y %H:%M'
    json.user do
      json.id user.id
      json.first_name user.first_name
      json.last_name user.last_name
      json.avatar paperclip_url user.avatar
      json.email user.email
      json.phone_number user.phone_number
      json.created_at user.created_at
      json.role item['role']
      json.full_name "#{user.first_name} #{user.last_name}"
    end
  end
  json.reports @content[0]['reports']&.each do |item|
    user = User.new id: item['user_id'], first_name: item['first_name'], last_name: item['last_name'],
                    avatar_file_size: item['avatar_file_size'], avatar_content_type: item['avatar_content_type'],
                    avatar_file_name: item['avatar_file_name'], email: item['email'],
                    phone_number: item['phone_number']
    json.id item['id']
    json.created_at item['created_at'].to_datetime.try :strftime, '%d-%m-%Y %H:%M'
    json.report_type Report.report_types.key item['report_type']
    json.user do
      json.id user.id
      json.first_name user.first_name
      json.last_name user.last_name
      json.avatar paperclip_url user.avatar
      json.email user.email
      json.phone_number user.phone_number
      json.created_at user.created_at
      json.role item['role']
      json.full_name "#{user.first_name} #{user.last_name}"
    end
  end
  json.attachments        @post.attachments&.sort_by{ |attachment| attachment.order_index }.each do |attachment|
    json.id               attachment.id
    json.url              attachment.file.url
    json.type        attachment.attachment_type
  end
  json.images        @post.attachments&.sort_by{ |attachment| attachment.order_index }.each do |attachment|
    json.src         paperclip_url attachment.file
    json.url         paperclip_url attachment.file
    json.type        attachment.attachment_type
    json.original         paperclip_url attachment.file
    json.thumbnail        paperclip_url attachment.file
  end
  json.cms_notifications @cms_notifications.each do |cms_notification|
    json.id cms_notification.id
    json.created_at cms_notification.created_at.to_datetime.try :strftime, '%d-%m-%Y %H:%M'
  end
end
