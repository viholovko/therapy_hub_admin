json.system_settings do
  json.ios_push_environment       @system_settings.ios_push_environment
  json.ios_push_apns_host         @system_settings.ios_push_apns_host
  json.ios_push_password          @system_settings.ios_push_password
  json.ios_push_certificate       @system_settings.ios_push_certificate

  json.android_push_token         @system_settings.android_push_token

  json.twilio_account_sid         @system_settings.twilio_account_sid
  json.twilio_auth_token          @system_settings.twilio_auth_token
  json.twilio_phone_number        @system_settings.twilio_phone_number

  json.s3_bucket                  @system_settings.s3_bucket
  json.aws_access_key_id          @system_settings.aws_access_key_id
  json.aws_secret_access_key      @system_settings.aws_secret_access_key
  json.s3_region                  @system_settings.s3_region
end