json.tasks @tasks.each do |task|
  json.id task.id
  json.title task.title
  json.description task.description
  json.due_date task.due_date.try :strftime, '%d-%m-%Y %H:%M'
  json.completed_at task.completed_at.try :strftime, '%d-%m-%Y %H:%M'
  json.completed task.completed
  json.completed_at task.completed_at.try :strftime, '%d-%m-%Y %H:%M'
  json.created_at task.created_at.try :strftime, '%d-%m-%Y %H:%M'
  json.updated_at task.updated_at.try :strftime, '%d-%m-%Y %H:%M'
end
json.count @count