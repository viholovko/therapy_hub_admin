json.users @users.each do |user|
  json.id user.id
  json.email user.email
  json.first_name user.first_name
  json.last_name user.last_name
  json.phone_number user.phone_number
  json.avatar paperclip_url user.avatar

  country = Country.new(user['country'])
  json.country do
    json.flag paperclip_url(country.flag)
  end

  json.friends_count user.friends_count
  json.created_at user.created_at.strftime("%d-%m-%Y")
end
json.count @count