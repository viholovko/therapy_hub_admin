json.user do
  json.id              @user.id
  json.email           @user.email

  json.first_name      @user.first_name
  json.last_name       @user.last_name
  json.phone_number    @user.phone_number
  json.avatar          @user.avatar.url

  json.latitude        @user.latitude
  json.longitude       @user.longitude
  json.about_me        @user.about_me
  json.role_verified   @user.role_verified
  json.role            @user.role.name
  json.created_at      @user.created_at.strftime("%d-%m-%Y")

  country = Country.new(@user['country'])
  json.country do
    json.id           country.id
    json.name         country.name
    json.flag         country.flag.url
  end

  json.friends        @user.friends_users do |friend|
    user = User.new(friend)

    json.id           user.id
    json.first_name   user.first_name
    json.last_name    user.last_name
    json.email        user.email
    json.phone_number user.phone_number
    json.avatar       user.avatar.url
    json.created_at   user.created_at.strftime("%d-%m-%Y")
    json.role         user.role.name
  end

  json.followers        @user['followers'] do |follower|
    user = User.new(follower)
    json.id           user.id
    json.first_name   user.first_name
    json.last_name    user.last_name
    json.email        user.email
    json.phone_number user.phone_number
    json.avatar       user.avatar.url
    json.created_at   user.created_at.strftime("%d-%m-%Y")
    json.role         user.role.name
  end

  json.followeds        @user['followeds'] do |followed|
    user = User.new(followed)

    json.id           user.id
    json.first_name   user.first_name
    json.last_name    user.last_name
    json.email        user.email
    json.phone_number user.phone_number
    json.avatar       user.avatar.url
    json.created_at   user.created_at.strftime("%d-%m-%Y")
    json.role         user.role.name
  end

  json.i_blocked_users @user['i_blocked_users']&.each do |block_user|
    user = User.new(block_user)

    json.id           user.id
    json.first_name   user.first_name
    json.last_name    user.last_name
    json.email        user.email
    json.phone_number user.phone_number
    json.avatar       user.avatar.url
    json.created_at   user.created_at.strftime("%d-%m-%Y")
    json.role         user.role.name
  end
  json.i_blocked_users_count @user['i_blocked_users_count']

  json.me_blocked_users @user['me_blocked_users']&.each do |block_user|
    user = User.new(block_user)

    json.id           user.id
    json.first_name   user.first_name
    json.last_name    user.last_name
    json.email        user.email
    json.phone_number user.phone_number
    json.avatar       user.avatar.url
    json.created_at   user.created_at.strftime("%d-%m-%Y")
    json.role         user.role.name
  end
  json.me_blocked_users_count @user['me_blocked_users_count']

  json.classrooms        @classrooms do |classroom|
    json.id              classroom&.id
    json.name            classroom&.name
    json.assessment_link classroom&.assessment_link
    json.created_at      classroom&.created_at.strftime("%d-%m-%Y")
  end
end