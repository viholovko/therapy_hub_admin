Rails.application.routes.draw do

  root to: 'pages#index'

  mount TryApi::Engine => '/developers'


  scope '(:locale)' do
    namespace :admin do
      resources :countries,        only: [:index, :create, :update, :destroy, :show]
      resources :users,            only: [:index, :create, :update, :destroy, :show]
      resources :terms,            only: [:create, :index]
      resources :privacy_policy,   only: [:create, :index]
      resources :about,            only: [:create, :index]
      resources :posts,            only: [:index, :create, :update, :destroy, :show] do
        collection do
          delete :'remove_selected'
        end
        member do
          post :send_push
        end
      end
      resources :advertisements,   only: [:index, :create, :update, :destroy, :show]
      resources :attachments,      only: [:destroy] do
        collection do
          post '/:entity_type ',    to: 'attachments#create'
        end
      end
      resources :email_sender,     only: [:index, :create]
      resources :system_settings,  only: [:index, :create]
      resources :classrooms,       only: [:index, :show] do
        member do
          put :remove_user
        end
      end
      resources :piechart_data,    only: :index
      resources :tasks,            only: :index
      resources :events,           only: :index
      resources :frequently_asked_questions, only: %i[index create update destroy show]
    end



    resources :sessions, only: [:create] do
      collection do
        delete :destroy
        get :check
      end
    end

    resources :frequently_asked_questions, only: %i[index show]
  end
end
