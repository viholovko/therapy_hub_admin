class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :encrypted_password
      t.string :salt
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.references :country
      t.references :role
      t.datetime :last_logged_in
      t.text :about_me

      t.timestamps
    end
    add_attachment :users, :avatar
  end
end