class CreateCountries < ActiveRecord::Migration[5.1]
  def change
    create_table :countries do |t|
      t.string :name
      t.string :phone_code
      t.string :alpha2_code
      t.string :alpha3_code
      t.string :numeric_code
      t.attachment :flag
      t.boolean :selected
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
