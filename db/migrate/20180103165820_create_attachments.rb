class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments do |t|
      t.references :entity
      t.string :entity_type
      t.timestamps null: false
    end
  end
end
