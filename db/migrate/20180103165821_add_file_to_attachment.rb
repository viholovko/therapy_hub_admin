class AddFileToAttachment < ActiveRecord::Migration[5.0]
  def up
    add_attachment :attachments, :file
  end

  def down
    remove_attachment :attachments, :file
  end
end
