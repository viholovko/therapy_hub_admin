class CreateClassrooms < ActiveRecord::Migration[5.1]
  def change
    create_table :classrooms do |t|
      t.string :name
      t.integer :assessment_file_id
      t.string :assessment_link
      t.datetime :created_at
      t.datetime :updated_at
    end
    add_index :classrooms, :assessment_file_id
  end
end
