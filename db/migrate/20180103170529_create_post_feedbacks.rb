class CreatePostFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :post_feedbacks do |t|
      t.references :post, foreign_key: true
      t.string :type
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
