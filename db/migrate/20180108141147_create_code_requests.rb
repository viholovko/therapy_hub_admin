class CreateCodeRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :code_requests do |t|
      t.string :code
      t.references :country
      t.string :phone_number
      t.integer :attempts_count
      t.timestamps
    end
  end
end
