class CreateFriendships < ActiveRecord::Migration[5.1]
  def change
    create_table :friendships do |t|
      t.bigint :user_id
      t.bigint :friend_id
      t.integer :status
      t.datetime :accepted_at
      t.timestamps
    end
  end

end
