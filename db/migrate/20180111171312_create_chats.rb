class CreateChats < ActiveRecord::Migration[5.0]
  def change
    create_table :chats do |t|
      t.string  :title
      t.integer :chat_type
      t.integer :owner_id

      t.timestamps
    end
    add_attachment :chats, :avatar

    create_table :chats_users, id: false do |t|
      t.belongs_to :chat, index: true
      t.belongs_to :user, index: true
    end
  end
end