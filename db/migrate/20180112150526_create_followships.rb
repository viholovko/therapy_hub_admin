class CreateFollowships < ActiveRecord::Migration[5.1]
  def change
    create_table :followships do |t|
      t.bigint :follower_id
      t.bigint :followed_id
      t.timestamps
    end
    add_index :followships, :follower_id
    add_index :followships, :followed_id
    add_index :followships, [:follower_id, :followed_id], unique: true
  end
end
