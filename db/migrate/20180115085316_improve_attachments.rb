class ImproveAttachments < ActiveRecord::Migration[5.1]
  def change
    rename_column :attachments, :entity_type, :attachable_type
    rename_column :attachments, :entity_id, :attachable_id
  end
end
