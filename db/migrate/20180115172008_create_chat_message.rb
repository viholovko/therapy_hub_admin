class CreateChatMessage < ActiveRecord::Migration[5.1]
  def change
    create_table :chat_messages do |t|
      t.string :message
      t.integer :chat_id
      t.integer :user_id

      t.timestamps
    end
  end
end