class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.references :user
      t.integer :device_type
      t.text :token
      t.boolean :notified
      t.string :role
      t.integer :payment_type

      t.timestamps
    end

    add_column :users, :role_verified, :boolean, default: true
  end
end