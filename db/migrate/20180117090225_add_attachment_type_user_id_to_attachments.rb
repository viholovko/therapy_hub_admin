class AddAttachmentTypeUserIdToAttachments < ActiveRecord::Migration[5.1]
  def change
    change_table :attachments do |t|
      t.references :user
      t.integer :attachment_type
    end
  end
end
