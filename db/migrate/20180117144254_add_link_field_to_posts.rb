class AddLinkFieldToPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :link, :string
    rename_column :posts, :type, :post_type
  end
end
