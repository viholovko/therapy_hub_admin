class CreatePostsToggledUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :posts_toggled_users do |t|
      t.references :post
      t.references :user
    end
  end
end
