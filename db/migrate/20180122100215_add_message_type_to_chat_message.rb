class AddMessageTypeToChatMessage < ActiveRecord::Migration[5.1]
  def change
    change_table :chat_messages do |t|
      t.integer :message_type
    end
  end
end
