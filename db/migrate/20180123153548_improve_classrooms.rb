class ImproveClassrooms < ActiveRecord::Migration[5.1]
  def change
    change_table :classrooms do |t|
      t.bigint :owner_id, belongs_to: :user
      t.bigint :student_id, belongs_to: :user
      t.boolean :student_accepted, default: false
      t.bigint :parent_id, belongs_to: :user
      t.boolean :parent_accepted, default: false
      t.bigint :teacher_id, belongs_to: :user
      t.boolean :teacher_accepted, default: false
      t.bigint :caseworker_id, belongs_to: :user
      t.boolean :caseworker_accepted, default: false
    end
    remove_column :classrooms, :assessment_file_id, :integer
  end
end
