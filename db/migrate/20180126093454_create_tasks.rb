class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.references :owner
      t.timestamp :due_date
      t.references :classroom, index: true
      t.boolean :completed, default: false
      t.timestamp :completed_at
      t.timestamps null: false
    end
  end
end
