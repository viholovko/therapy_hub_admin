class CreateBlockLists < ActiveRecord::Migration[5.1]
  def change
    create_table :block_lists do |t|
      t.integer :user_id
      t.integer :blocked_user_id
      t.timestamps null: false
    end
  end
end
