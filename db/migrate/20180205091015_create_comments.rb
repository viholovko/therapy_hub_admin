class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.string :message
      t.references :user
      t.references :commentable
      t.string :commentable_type

      t.timestamps
    end
  end
end
