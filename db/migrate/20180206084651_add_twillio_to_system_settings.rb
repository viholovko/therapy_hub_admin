class AddTwillioToSystemSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :system_settings, :twilio_account_sid, :string
    add_column :system_settings, :twilio_auth_token, :string
    add_column :system_settings, :twilio_phone_number, :string
  end
end
