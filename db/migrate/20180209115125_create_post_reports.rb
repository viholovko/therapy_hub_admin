class CreatePostReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.references :user
      t.references :post
      t.integer :report_type
      t.timestamps
    end
  end
end
