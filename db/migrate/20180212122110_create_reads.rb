class CreateReads < ActiveRecord::Migration[5.1]
  def change
    create_table :read_messages do |t|
      t.references :readable
      t.string :readable_type
      t.references :user
      t.integer :chat_id
      t.timestamps
    end
  end
end
