class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.references :user
      t.references :classroom
      t.string :title
      t.text :description
      t.datetime :time_from
      t.datetime :time_to
      t.timestamps
    end
  end
end
