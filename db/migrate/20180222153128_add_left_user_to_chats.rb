class AddLeftUserToChats < ActiveRecord::Migration[5.1]
  def change
    add_column :chats, :leave_user, :integer
  end
end
