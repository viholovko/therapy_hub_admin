class AddUiidToChatMessages < ActiveRecord::Migration[5.1]
  def change
    add_column :chat_messages, :ui_id, :string
  end
end
