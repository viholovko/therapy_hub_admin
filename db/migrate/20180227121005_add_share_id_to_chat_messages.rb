class AddShareIdToChatMessages < ActiveRecord::Migration[5.1]
  def change
    add_column :chat_messages, :share_id, :integer
  end
end
