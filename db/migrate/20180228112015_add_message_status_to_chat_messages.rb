class AddMessageStatusToChatMessages < ActiveRecord::Migration[5.1]
  def change
    change_table :chat_messages do |t|
      t.integer :message_status, default: 0
    end
  end
end
