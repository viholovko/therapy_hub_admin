class AddShareContentToChatMessages < ActiveRecord::Migration[5.1]
  def change
    change_table :chat_messages do |t|
      t.string :share_content, default: ''
    end
  end
end
