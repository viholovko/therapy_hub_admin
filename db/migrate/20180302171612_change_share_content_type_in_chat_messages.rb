class ChangeShareContentTypeInChatMessages < ActiveRecord::Migration[5.1]
  def change
    remove_column :chat_messages, :share_content, :string
    change_table :chat_messages do |t|
      t.jsonb :share_content, null: false, default: '{}'
    end
  end
end
