class AddAvatarToClassroom < ActiveRecord::Migration[5.1]
  def change
    add_attachment :classrooms, :avatar
  end
end
