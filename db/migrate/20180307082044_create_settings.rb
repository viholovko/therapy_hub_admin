class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.bigint :user_id

      t.boolean :messages_notification, default: true
      t.boolean :friend_requests_notification, default: true
      t.boolean :comments_notification, default: true
      t.boolean :likes_notification, default: true
      t.boolean :taggs_notification, default: true
      t.boolean :system_notification, default: true
      t.boolean :classroom_notification, default: true
      t.boolean :calendar_notification, default: true
      t.integer :geo_search, default: 0
      t.integer :system_of_units, default: 0

      t.timestamps
    end
    add_index :settings, :user_id, unique: true
  end
end
