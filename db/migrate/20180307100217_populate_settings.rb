class PopulateSettings < ActiveRecord::Migration[5.1]
  def up
    User.find_each do |user|
      Setting.create user: user
    end
  end

  def down

  end
end
