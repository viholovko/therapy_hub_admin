class AddedChatIdToClassrooms < ActiveRecord::Migration[5.1]
  def change
    add_column :chats, :classroom_id, :bigint
  end
end
