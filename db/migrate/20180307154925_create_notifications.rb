class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.string :title
      t.string :message
      t.references :user
      t.boolean :read, default: false
      t.integer :notification_type
      t.attachment :avatar
      t.timestamps
    end
  end
end
