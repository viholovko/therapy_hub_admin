class AddChatStatusAndActivityToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :chat_active, :boolean, default: false
    add_column :users, :last_chat_login, :datetime, default: -> { 'CURRENT_TIMESTAMP' }
  end
end
