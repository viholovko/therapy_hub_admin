class AddDeleteToTasks < ActiveRecord::Migration[5.1]
  def change
    add_column :tasks, :deleted, :boolean, default: false
  end
end
