class ImprovePosts < ActiveRecord::Migration[5.1]
  def change
    remove_column :posts, :post_type, :integer
    add_column :posts, :ads, :boolean, default: false
    add_column :posts, :classroom_id, :integer
  end
end
