class AddDimensionToAttachments < ActiveRecord::Migration[5.1]
  def change
    add_column :attachments, :width, :integer
    add_column :attachments, :height, :integer
  end
end
