class CreateLinkAndImproveChatMessagesAndPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.string :title
      t.string :favicon
      t.string :description
      t.string :image
      t.string :url
      t.string :canonical_url
    end
    add_column :posts, :link_id, :integer
    remove_column :posts, :link, :string
    add_column :chat_messages, :link_id, :integer
  end
end
