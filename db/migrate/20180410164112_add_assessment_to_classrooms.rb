class AddAssessmentToClassrooms < ActiveRecord::Migration[5.1]
  def change
    add_attachment :classrooms, :assessment_file
    add_column :classrooms, :assessment_owner_id, :bigint
    add_column :classrooms, :assessment_title, :string

  end
end
