class ChangeToggledUsersToTaggedUsers < ActiveRecord::Migration[5.1]
  def change
    rename_table :posts_toggled_users, :posts_tagged_users
  end
end
