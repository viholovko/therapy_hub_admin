class AddCommentMemberToComments < ActiveRecord::Migration[5.1]
  def change
    add_column :comments, :comment_member_id, :bigint
  end
end
