class AddOrderIndexToAttachments < ActiveRecord::Migration[5.1]
  def change
    add_column :attachments, :order_index, :bigint
  end
end
