class AddDefaultOrderToAttachment < ActiveRecord::Migration[5.1]
  def change
    change_column :attachments, :order_index, :bigint, default: 0
  end
end
