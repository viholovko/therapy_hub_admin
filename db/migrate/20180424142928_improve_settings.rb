class ImproveSettings < ActiveRecord::Migration[5.1]
  def change
    remove_column :settings, :comments_notification, :boolean
    remove_column :settings, :likes_notification, :boolean
    remove_column :settings, :taggs_notification, :boolean
    remove_column :settings, :system_notification, :boolean
    remove_column :settings, :calendar_notification, :boolean

    add_column :settings, :posts_notification, :boolean, default: true
    add_column :settings, :mentor_posts_notification, :boolean, default: true
    add_column :settings, :classroom_invites_notification, :boolean, default: true
  end
end
