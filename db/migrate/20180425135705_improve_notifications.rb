class ImproveNotifications < ActiveRecord::Migration[5.1]
  def change
    remove_column :notifications, :title,                   :string
    remove_column :notifications, :message,                 :string
    add_column    :notifications, :message_type,            :integer
    add_column    :notifications, :from_user_id,            :integer
    add_column    :notifications, :classroom_id,            :integer
    add_column    :notifications, :post_id,                 :integer
    add_column    :notifications, :post_title,              :string
    add_column    :notifications, :classroom_title,         :string
    add_column    :notifications, :sub_resource_id,         :integer
    add_column    :notifications, :sub_resource_title,      :string
    add_column    :notifications, :sub_resource_time_from,  :datetime
    add_column    :notifications, :sub_resource_time_to,    :datetime
    add_column    :comments,      :post_id,                 :integer
  end
end
