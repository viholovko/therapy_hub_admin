class AddIosCertificateFileToSettings < ActiveRecord::Migration[5.0]
  def change
    rename_column :system_settings, :apple_purchase_password, :ios_push_password
    rename_column :system_settings, :apple_purchase_environment, :ios_push_environment

    add_attachment :system_settings, :ios_push_certificate
  end
end