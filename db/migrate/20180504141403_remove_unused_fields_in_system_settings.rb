class RemoveUnusedFieldsInSystemSettings < ActiveRecord::Migration[5.0]
  def change
    rename_column :system_settings, :android_purchase_package_name, :android_push_token
    remove_column :system_settings, :android_purchase_product_id
    remove_column :system_settings, :android_purchase_refresh_token
    remove_column :system_settings, :android_purchase_client_id
    remove_column :system_settings, :android_purchase_client_secret
    remove_column :system_settings, :android_purchase_redirect_uri
  end
end