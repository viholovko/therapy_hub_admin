class AddS3SettingsToSystemSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :system_settings, :s3_bucket, :string
    add_column :system_settings, :aws_access_key_id, :string
    add_column :system_settings, :aws_secret_access_key, :string
    add_column :system_settings, :s3_region, :string
  end
end
