class AddScheduledToNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications, :scheduled, :boolean, default: false
  end
end
