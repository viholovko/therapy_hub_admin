class AddHostToSystemSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :system_settings, :ios_push_apns_host, :string
  end
end
