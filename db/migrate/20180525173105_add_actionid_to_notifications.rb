class AddActionidToNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column    :notifications, :action_id,  :integer, default: 0
    add_column    :notifications, :icon, :string
  end
end
