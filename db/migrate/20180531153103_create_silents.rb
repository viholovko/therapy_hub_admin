class CreateSilents < ActiveRecord::Migration[5.1]
  def change
    create_table :silents do |t|
      t.references :silentable
      t.string :silentable_type
      t.references :user
      t.timestamps
    end
  end
end
