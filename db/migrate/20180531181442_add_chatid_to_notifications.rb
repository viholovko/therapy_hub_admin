class AddChatidToNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column    :notifications, :chat_id, :integer
  end
end
