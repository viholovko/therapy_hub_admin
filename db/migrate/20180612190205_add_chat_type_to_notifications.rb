class AddChatTypeToNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column    :notifications, :chat_type, :string
  end
end
