class AddRoleInClassroomToNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column    :notifications, :role_in_classroom, :string
  end
end
