class DeleteRoleInClassroomToNotifications < ActiveRecord::Migration[5.1]
  def change
    remove_column    :notifications, :role_in_classroom
  end
end
