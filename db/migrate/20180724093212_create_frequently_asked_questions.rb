class CreateFrequentlyAskedQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :frequently_asked_questions do |t|
      t.string :title
      t.string :description
      t.text :content
      t.integer :user_id
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
