class CreateTaskNotificationPeriod < ActiveRecord::Migration[5.1]
  def change
    add_column :tasks, :notification_period_1, :timestamp
    add_column :tasks, :notification_period_2, :timestamp
  end
end
