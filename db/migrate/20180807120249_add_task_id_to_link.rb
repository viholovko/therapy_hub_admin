class AddTaskIdToLink < ActiveRecord::Migration[5.1]
  def change
    add_column :links, :task_id, :integer
    remove_column :tasks, :link_id, :integer
  end
end
