class AddPeriodsKeyToTask < ActiveRecord::Migration[5.1]
  def change
    add_column :tasks, :period_1, :string, default: 'none'
    add_column :tasks, :period_2, :string, default: 'none'
  end
end
