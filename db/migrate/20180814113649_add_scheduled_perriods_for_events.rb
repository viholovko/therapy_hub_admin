class AddScheduledPerriodsForEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :period_1, :string, default: 'none'
    add_column :events, :period_2, :string, default: 'none'
    add_column :events, :notification_period_1, :timestamp
    add_column :events, :notification_period_2, :timestamp
  end
end
