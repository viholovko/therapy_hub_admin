class CreateCmsNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :cms_notifications do |t|
        t.references :post
        t.timestamps
    end
  end
end
