class AddMentorPostsToUserSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :mentor_posts_cms, :boolean, default: true
  end
end
