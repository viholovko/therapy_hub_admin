class CreateStatisticForActivity < ActiveRecord::Migration[5.1]
  def change
    create_table :statistics do |t|
      t.references :user
      t.references :post
      t.integer    :activity_type
      t.timestamps
    end
  end
end
