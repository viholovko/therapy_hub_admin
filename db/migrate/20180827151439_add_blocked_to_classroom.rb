class AddBlockedToClassroom < ActiveRecord::Migration[5.1]
  def change
    add_column :classrooms, :blocked, :boolean, default: false
  end
end
