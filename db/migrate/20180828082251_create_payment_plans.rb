class CreatePaymentPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_plans do |t|
      t.string :name, null: false
      t.string :description
      t.float :price, default: 0, null: false
      t.integer :classroom_count, default: 0, null: false
      t.boolean :active, default: false
    end
  end
end
