class AddReferenceUserToPaymentPlan < ActiveRecord::Migration[5.1]
  def change
    change_table :users do |t|
      t.references :payment_plan
      t.integer :available_classrooms, default: 0
    end

    change_table :system_settings do |t|
      t.string :apple_purchase_password

      t.string :android_purchase_package_name
      t.string :android_purchase_product_id
      t.string :android_purchase_refresh_token
      t.string :android_purchase_client_id
      t.string :android_purchase_client_secret
      t.string :android_purchase_redirect_uri
    end
  end
end
