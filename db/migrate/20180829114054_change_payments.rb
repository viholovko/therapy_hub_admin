class ChangePayments < ActiveRecord::Migration[5.1]
  def change
    remove_column :payments, :role
    add_column :payments, :payment_plan_id, :integer
    add_column :payments, :trial_to, :timestamp
  end
end
