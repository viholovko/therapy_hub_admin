class AddProductIdToPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_plans, :apple_id, :string
    add_column :payment_plans, :android_id, :string
  end
end
