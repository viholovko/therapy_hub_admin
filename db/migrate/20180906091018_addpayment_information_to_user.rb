class AddpaymentInformationToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :payment_status, :integer
    add_column :payments, :expired_date, :timestamp
    add_column :payments, :message_title, :string
    add_column :payments, :message_body, :string
  end
end
