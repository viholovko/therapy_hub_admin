class AddAbout < ActiveRecord::Migration[5.1]
  def change
    create_table :abouts do |t|
      t.text :body
    end
  end
end
