class AddPaymentTypeNameToPayment < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_plans, :type_name, :string
  end
end
