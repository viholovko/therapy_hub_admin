class AddOrderToPaymentPlans < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_plans, :order, :integer
  end
end
