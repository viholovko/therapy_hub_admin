class AddAndroidContent < ActiveRecord::Migration[5.1]
  def change
    add_column :terms, :body_android, :text
    add_column :privacy_policies, :body_android, :text
    add_column :abouts, :body_android, :text
  end
end
