class Social < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :facebook_id, :string
    add_column :users, :twitter_id, :string
    add_column :users, :google_plus_id, :string
    add_column :users, :wonde_id, :string
    add_column :users, :from_social, :boolean
  end
end
