class AddRefreshTokenToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :system_settings, :android_token_refresh, :string
    add_column :system_settings, :android_key, :string
  end
end
