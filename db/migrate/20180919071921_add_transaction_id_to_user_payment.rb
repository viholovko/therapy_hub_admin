class AddTransactionIdToUserPayment < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :transaction_id, :string
    add_index  :payments, :transaction_id, unique: true
  end
end
