class AddCreatedByAdminToPost < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :is_admin_created, :boolean, default: false
  end
end
