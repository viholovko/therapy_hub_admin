class AddFieldForSendedPushes < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications, :push_send, :boolean, default: false
    add_column :payments, :payment_at, :timestamp
  end
end
