class ValidationToPayment < ActiveRecord::Migration[5.1]
  def change
    change_column :payments, :payment_at, :timestamp, null: false
    change_column :payments, :transaction_id, :string, null: false, unique: true
  end
end
