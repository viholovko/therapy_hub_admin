class ChangeUniqColumsInPayment < ActiveRecord::Migration[5.1]
  def change
    remove_column :payments, :transaction_id, :string
    add_column :payments, :transaction_id, :string
  end
end
