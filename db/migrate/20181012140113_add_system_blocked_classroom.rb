class AddSystemBlockedClassroom < ActiveRecord::Migration[5.1]
  def change
    add_column :classrooms, :system_blocked, :boolean, default: false
  end
end
