Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each { |seed| load seed }

puts 'Adding countries ...'
seed_countries
puts 'Country added'

puts 'Adding Payment Plans ...'
seed_payment_plans
puts 'Payment Plans added'

puts 'Adding About ...'
seed_abouts
puts 'About added'

puts 'Adding Terms & Conditions ...'
seed_terms_and_conditions
puts 'Terms & Conditions added'

puts 'Adding Privacy & Policies ...'
seed_privacy_and_policies
puts 'Privacy & Policies added'

puts 'Create admin ...'
User.create email: 'admin@gmail.com',
            password: 'secret',
            password_confirmation: 'secret',
            phone_number: '132132132',
            role_id: Role.get_admin.id,
            first_name: 'Advertisement',
            last_name: 'iDyslexic',
            country: Country.first

# puts 'Storing to S3'
# system_setting = SystemSetting.first_or_create
#
# classes = [
#     'User:avatar', 'Chat:avatar', 'Classroom:avatar',
#     'Classroom:assessment_file', 'Country:flag', 'Notification:avatar',
#     'SystemSetting:ios_push_certificate','Attachment:file'
# ]

# system_setting.movetos3(classes)