def seed_payment_plans

  PaymentPlan.destroy_all

  payment_plans_attributes = [
      { name: 'Purchase 1 classroom',  type_name: 'Subscription Type 1', description: 'Create classroom for one Student, Parent , Teacher, and Caseworker',price: 19.99, classroom_count: 1, active: true, apple_id: 'com.idyslexic.oneclassroom', android_id: ''},
      { name: 'Purchase 2 classrooms', type_name: 'Subscription Type 2', description: 'Create classroom for one Student, Parent , Teacher, and Caseworker',price: 29.99, classroom_count: 2, active: true, apple_id: 'com.idyslexic.twoclassrooms', android_id: ''},
      { name: 'Purchase 3 classrooms', type_name: 'Subscription Type 3', description: 'Create classroom for one Student, Parent , Teacher, and Caseworker',price: 39.99, classroom_count: 3, active: true, apple_id: 'com.idyslexic.threeclassrooms', android_id: ''},
      { name: 'Purchase 4 classrooms', type_name: 'Subscription Type 4', description: 'Create classroom for one Student, Parent , Teacher, and Caseworker',price: 49.99, classroom_count: 4, active: true, apple_id: 'com.idyslexic.fourclassrooms', android_id: ''}
  ]

  payment_plans_attributes.each do |attributes|
    c = PaymentPlan.create attributes
    puts c.name, c.errors.full_messages unless c.valid?
    print '.'
  end
end