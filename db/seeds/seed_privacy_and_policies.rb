def seed_privacy_and_policies

  PrivacyPolicy.destroy_all

  privacies_attributes = [
      { body: 'Purchase 1 classroom', body_android: ''},
  ]

  privacies_attributes.each do |attributes|
    PrivacyPolicy.create attributes
  end
end