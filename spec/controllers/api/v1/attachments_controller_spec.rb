require 'rails_helper'
require 'rack/test'

RSpec.describe ::Api::V1::AttachmentsController, type: :controller do
  render_views

  describe '#create' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :not_upgraded }

    it 'should return create attachment' do
      sign_in user: user

      post :create, params: { attachment_type: 'audio',user: user,
                              file: Rack::Test::UploadedFile.new(
                                  "#{Rails.root}/spec/fixtures/factory_image.png",
                                  'image/png'
                              )
      }
      expect(response.status).to be 200
      expect(response_body['message']).to eq('Attachment is created')
      expect(Attachment.count).to be 1
      expect(Attachment.first.id).to be 1
    end

    it 'should return validation errors' do
      sign_in user: user

      post :create, params: { user: user }

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00500')
      expect(response_body['errors'][0]['message']).to eq('File can\'t be blank')
      expect(response_body['errors'][1]['code']).to eq('E00501')
      expect(response_body['errors'][1]['message']).to eq('Attachment type can\'t be blank')
    end
  end

  describe '#destroy' do
    let(:user) { create :user, :not_upgraded }
    let(:response_body) { JSON.parse(response.body) }

    it 'should return destroy attachment' do
      sign_in user: user
      attachment = create :attachment, :photo, user: user
      expect(Attachment.count).to be 1

      delete :destroy, params: {id: attachment.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Attachment is destroyed')
      expect(Attachment.count).to be 0
    end

    it 'should return error if user not owner attachment' do
      sign_in user: user
      user2 = create :user, :not_upgraded
      attachment = create :attachment, :photo, user: user2
      expect(Attachment.count).to be 1

      delete :destroy, params: {id: attachment.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Attachment.count).to be 1
    end

    it 'should return error if attachment not found' do
      sign_in user: user

      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end
  end


end
