require 'rails_helper'

RSpec.describe ::Api::V1::BlockListsController, type: :controller do
  render_views

  describe '#create' do
    let(:user1) {create :user, :student}
    let(:user2) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return block user' do
      sign_in user: user1

      post :create, params: {id: user2.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq("You successfully block #{user2.first_name} #{user2.last_name}.")
      expect(user1.blocked_users.include?(user2)).to be true
    end

    it 'should return error if block on to yourself' do
      sign_in user: user1

      post :create, params: {id: user1.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00901')
      expect(response_body['errors'][0]['message']).to eq('You can\'t block to yourself.')
      expect(user1.blocked_users.include?(user1)).to be false
    end

    it 'should return error if already blocked on to user' do
      sign_in user: user1

      post :create, params: {id: user2.id}
      post :create, params: {id: user2.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00900')
      expect(response_body['errors'][0]['message']).to eq("You already blocking #{user2.first_name} #{user2.last_name}.")
      expect(user1.blocked_users.count).to be 1
    end

    it 'should return error if user not found' do
      sign_in user: user1
      post :create, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      delete :destroy, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return remove from friend after block user' do
      sign_in user: user1
      create :friendship, user: user1, friend: user2
      create :friendship, user: user2, friend: user1
      expect(user1.friends.include?(user2)).to be true

      post :create, params: {id: user2.id}

      expect(response.status).to be 200
      expect(user1.friends.include?(user2)).to be false
    end
  end

  describe '#destroy' do
    let(:response_body) {JSON.parse(response.body)}
    let(:user1) {create :user, :not_upgraded}
    let(:user2) {create :user, :not_upgraded}

    it 'should return unblock user' do
      sign_in user: user1
      user1.blocked_users<<user2
      expect(user1.blocked_users.include?(user2)).to be true

      delete :destroy, params: {id: user2.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq("You successfully unblock #{user2.first_name} #{user2.last_name}.")
      expect(user1.followed.include?(user2)).to be false
    end

    it 'should return error if user not blocked' do
      sign_in user: user1
      delete :destroy, params: {id: user2.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00902')
      expect(response_body['errors'][0]['message']).to eq("You have not been blocked #{user2.first_name} #{user2.last_name} before.")
    end

    it 'should return error if user not found' do
      sign_in user: user1
      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      delete :destroy, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end
end
