require 'rails_helper'

RSpec.describe Api::V1::ChatMessagesController, type: :controller do
  render_views

  describe '#create' do
    let(:response_body) {JSON.parse(response.body)}

    it 'should create message' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      created_chat = chat.updated_at
      post :create, params: { message: 'Test Message', ui_id: 'test_ui_id', chat_id: chat.id, message_type: 'text' }

      chat.reload
      expect(response.status).to be(200)
      expect(response_body['chat_message']['id']).to be > 0
      expect(response_body['message']).to eq('Message is saved.')
      expect(chat.updated_at).to be > created_chat
    end

    it 'should return validation error for share_id' do
      chat = create :chat, :direct

      sign_in user: chat.users.first

      post :create, params: { message: 'Test Message', chat_id: chat.id, ui_id: 'test_ui_id', message_type: 'forward' }

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00612')
    end

    it 'should return validation error for ui_id' do
      chat = create :chat, :direct

      sign_in user: chat.users.first

      post :create, params: { message: 'Test Message', chat_id: chat.id, message_type: 'text' }

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00611')
    end

    it 'should create message with photo' do
      chat = create :chat, :direct

      sign_in user: chat.users.first

      photo_attachment = create :attachment, :photo

      post :create, params: { message: 'Test Message', ui_id: 'test_ui_id', chat_id: chat.id, message_type: 'photo', attachment_ids: [photo_attachment.id] }

      expect(response.status).to be(200)
      expect(response_body['chat_message']['id']).to be > 0
      expect(response_body['message']).to eq('Message is saved.')
      expect(Attachment.count).to be 1
      expect(Attachment.first.attachable_id).to be(response_body['chat_message']['id'])
      expect(Attachment.first.attachable_type).to eq('ChatMessage')
    end

    it 'should create message with file' do
      chat = create :chat, :direct

      sign_in user: chat.users.first

      photo_attachment = create :attachment, :other

      post :create, params: { message: 'Test Message', ui_id: 'test_ui_id', chat_id: chat.id, message_type: 'file', attachment_ids: [photo_attachment.id] }

      expect(response.status).to be(200)
      expect(response_body['chat_message']['id']).to be > 0
      expect(response_body['message']).to eq('Message is saved.')
      expect(Attachment.count).to be 1
      expect(Attachment.first.attachable_id).to be(response_body['chat_message']['id'])
      expect(Attachment.first.attachable_type).to eq('ChatMessage')
    end

    it 'should create message with share post' do
      chat = create :chat, :direct

      sign_in user: chat.users.first

      post_object = create :post, :link

      post :create, params: { message: 'Test Message Post', ui_id: 'test_ui_id', chat_id: chat.id, message_type: 'post', share_id: post_object.id  }

      expect(response.status).to be(200)
      expect(response_body['chat_message']['id']).to be > 0
      expect(response_body['message']).to eq('Message is saved.')
    end

    it 'should create message with link' do
      chat = create :chat, :direct

      sign_in user: chat.users.first

      post :create, params: {message: 'test', ui_id: 'uiid', chat_id: chat.id, message_type: 'text', link: 'http://microsoft.com/derevo'}

      expect(response.status).to be 200
      expect(response_body['chat_message']['id']).to_not be_nil

      link = ChatMessage.find(response_body['chat_message']['id']).link
      expect(link).to_not be_nil
      LinkThumbnailerWorker.new.perform(link.id)
      link.reload
      expect(link.url).to eq('http://microsoft.com/derevo')
      expect(link.title).to eq('Microsoft title')
    end

    it 'should return error if user not in this chat' do
      chat = create :chat, :direct
      user = create :user, :student
      sign_in user: user

      created_chat = chat.updated_at
      post :create, params: { message: 'Test Message', ui_id: 'test_ui_id', chat_id: chat.id, message_type: 'text' }

      chat.reload
      expect(response.status).to be(403)
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end
  end

  describe '#destroy' do
    it 'should delete message' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      post :create, params: { message: 'Test Message', ui_id: 'test_ui_id', chat_id: chat.id, message_type: 'text' }

      message_id = JSON.parse(response.body)['chat_message']['id']

      delete :destroy, params: {id: message_id}
      expect(response.status).to be 200

      get :show, params: {id: message_id}

      response_body = JSON.parse(response.body)
      expect(response_body['chat_message']['message_status']).to eq('deleted')
    end
  end

  describe '#show' do
    let(:response_body) { JSON.parse(response.body) }

    it 'should return share content for post type' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      post_for_message = create :post, :photo, user: chat.users.first

      post :create, params: { message: 'Test Message', ui_id: 'test_ui_id',
                              chat_id: chat.id, message_type: 'post', share_id: post_for_message.id}
      get :show, params: {id: response_body['chat_message']['id']}

      expect(response.status).to be(200)
      expect(response_body['chat_message']['message_type']).to eq('post')
      expect(response_body['chat_message']['message_status']).to eq('created')
      expect(response_body['chat_message']['share'].size).to be > 0
      expect(response_body['chat_message']['share_id']).to eq(post_for_message.id)
    end

    it 'should return share content for contact type' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      post :create, params: { message: 'Test Message', ui_id: 'test_ui_id',
                              chat_id: chat.id, message_type: 'contact', share_id: chat.users.first.id}

      get :show, params: {id: response_body['chat_message']['id']}

      expect(response.status).to be(200)
      expect(response_body['chat_message']['message_type']).to eq('contact')
      expect(response_body['chat_message']['share'].size).to be > 0
      expect(response_body['chat_message']['share_id']).to eq(chat.users.first.id)
    end

    it 'should return share content for reply type' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      message = create :chat_message, :message, chat: chat, user: chat.users.first
      post :create, params: { ui_id: 'test_ui_id', chat_id: chat.id,
                              message_type: 'reply', share_id: message.id}

      get :show, params: {id: response_body['chat_message']['id']}

      expect(response.status).to be(200)
      expect(response_body['chat_message']['message']).to be nil
      expect(response_body['chat_message']['message_type']).to eq('reply')
      expect(response_body['chat_message']['share'].size).to be > 0
      expect(response_body['chat_message']['share_id']).to eq(message.id)
    end

    it 'should return share content for forward type' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      message = create :chat_message, :message, chat: chat, user: chat.users.first
      post :create, params: { message: 'Test Message Forward', ui_id: 'test_ui_id',
                              chat_id: chat.id, message_type: 'forward', share_id: message.id}

      get :show, params: {id: response_body['chat_message']['id']}

      expect(response.status).to be(200)
      expect(response_body['chat_message']['message']).to eq('Test Message Forward')
      expect(response_body['chat_message']['message_type']).to eq('forward')
      expect(response_body['chat_message']['share'].size).to be > 0
      expect(response_body['chat_message']['share_id']).to eq(message.id)
    end
  end

  describe '#update' do
    it 'should return correct message_status after' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      message = create :chat_message, :link, chat: chat, user: chat.users.first

      put :update, params: {id: message.id, link: 'http://microsoft.com/derevo', message: 'newmessage'}
      LinkThumbnailerWorker.new.perform(message.link.id)

      expect(response.status).to be(200)

      message.reload
      message.link.reload
      expect(message.message_status).to eq('updated')
      expect(message.message).to eq('newmessage')
      expect(message.link.url).to eq('http://microsoft.com/derevo')
      expect(message.link.title).to eq('Microsoft title')
    end

    it 'should return update message with invalid url' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      message = create :chat_message, :link, chat: chat, user: chat.users.first

      put :update, params: {id: message.id, link: 'microsoft.com/derevo', message: 'newmessage'}
      LinkThumbnailerWorker.new.perform(message.link.id)
      expect(response.status).to be(200)

      message.reload
      message.link.reload
      expect(message.message_status).to eq('updated')
      expect(message.message).to eq('newmessage')
      expect(message.link.url).to eq('microsoft.com/derevo')
      expect(message.link.title).to be_nil
    end
  end

  describe '#index' do
    let(:response_body) { JSON.parse(response.body) }

    it 'should return messages list with filters by datetime' do
      chat = create :chat, :groups

      sign_in user: chat.users.first
      create :chat_message, :message, created_at: DateTime.now + 1.hour, chat: chat
      message2 = create :chat_message, :message, created_at: DateTime.now + 2.hours, chat: chat
      message3 = create :chat_message, :message, created_at: DateTime.now + 3.hours, chat: chat
      create :chat_message, :message, created_at: DateTime.now + 4.hours, chat: chat

      get :index, params: {from_date: DateTime.now + 70.minutes, to_date: DateTime.now + 3.hours + 30.minutes, id: chat.id}

      expect(response.status).to be 200
      expect(response_body['chat_messages'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['chat_messages'][0]['id']).to be message3.id
      expect(response_body['chat_messages'][1]['id']).to be message2.id
    end

    it 'should return empty list if user not in the same chat' do
      chat1 = create :chat, :groups
      chat2 = create :chat, :groups

      sign_in user: chat2.users.first
      create :chat_message, :message, created_at: DateTime.now + 1.hour, chat: chat1
      message2 = create :chat_message, :message, created_at: DateTime.now + 2.hours, chat: chat1
      message3 = create :chat_message, :message, created_at: DateTime.now + 3.hours, chat: chat1
      create :chat_message, :message, created_at: DateTime.now + 4.hours, chat: chat1

      get :index, params: {id: chat1.id}

      expect(response.status).to be 200
      expect(response_body['chat_messages'].count).to be 0
      expect(response_body['count']).to be 0
      expect(response_body['chat_messages']).to eq([])
    end
  end
end
