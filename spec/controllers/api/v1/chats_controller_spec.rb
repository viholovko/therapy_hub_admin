require 'rails_helper'

RSpec.describe Api::V1::ChatsController, type: :controller do
  render_views
  require 'rails_helper'

  describe '#create' do
    it 'should create direct chat' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded

      sign_in user: user1

      post :create, params: {
          title: 'chat_test',
          chat_type: 'direct',
          avatar: '',
          user_ids: [user2.id]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(response_body['id']).to be > 0
    end

    it 'should create groups chat' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded

      sign_in user: user1

      post :create, params: {
          title: 'chat_test',
          chat_type: 'groups',
          avatar: '',
          user_ids: [user2.id, user3.id]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(response_body['id']).to be > 0
    end

    it 'should render direct chat with wrong user count' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded

      sign_in user: user1

      post :create, params: {
          title: 'chat_test',
          chat_type: 'direct',
          avatar: '',
          user_ids: [user2.id, user3.id]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00603')
      expect(response_body['errors'][0]['message']).to eq("Can't to be more then 2 user for direct chat")
    end

    it 'should return error if user added to chat yourself' do
      user1 = create :user, :not_upgraded
      sign_in user: user1

      post :create, params: {
          title: 'chat_test',
          chat_type: 'direct',
          avatar: '',
          user_ids: [user1.id]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00603')
      expect(response_body['errors'][0]['message']).to eq("Can't to be more then 2 user for direct chat")
    end

    it 'should return error if owner is blocked or being blocking' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded

      user2.blocked_users << user1
      sign_in user: user1

      post :create, params: {
          title: 'block_test',
          chat_type: 'groups',
          avatar: '',
          user_ids: [user1.id, user2.id]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00610')
      expect(response_body['errors'][0]['message']).to eq("Can't create chat with blocked users")
    end
  end

  describe '#update' do
    it 'should update user' do
      chat = create :chat, :groups
      sign_in user: chat.users.first

      put :update, params: {id: chat.id,
          title: 'chat_test_new',
          chat_type: 'groups',
          avatar: '',
          user_ids: [chat.users[1].id, chat.users[0].id]
      }

      chat.reload
      expect(response.status).to be(200)
      expect(chat.users.size).to be(2)
    end

  end

  describe '#destroy' do
    it 'should delete chat' do
      chat = create :chat, :direct
      sign_in user: chat.users.first

      delete :destroy, params: {id: chat.id}

      expect(response.status).to be 200
    end

  end

  describe '#index' do
    it 'should return chats with filters by datetime' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded
      chat1 = create :chat, :groups, users: [user1, user2, user3], created_at: DateTime.now + 1.hour,  updated_at: DateTime.now + 1.hour
      chat2 = create :chat, :groups, users: [user1, user2, user3], created_at: DateTime.now + 2.hours, updated_at: DateTime.now + 2.hours
      chat3 = create :chat, :groups, users: [user1, user2, user3], created_at: DateTime.now + 3.hours, updated_at: DateTime.now + 3.hours
      chat4 = create :chat, :groups, users: [user1, user2, user3], created_at: DateTime.now + 4.hours, updated_at: DateTime.now + 4.hours
      sign_in user: user1

      get :index, params: {from_date: (DateTime.now + 70.minutes), to_date: (DateTime.now + 3.hours + 30.minute) }
      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['chats'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['chats'][0]['id']).to be chat3.id
      expect(response_body['chats'][1]['id']).to be chat2.id
    end

    it 'should return chats with last message' do
      user1 = create(:user, :not_upgraded)
      user2 = create(:user, :not_upgraded)
      sign_in user: user1

      chat = create :chat, :direct, owner: user1, users: [user1, user2]
      create :chat_message, :message, user: user1, chat: chat, message: 'Message1'
      create :chat_message, :message, user: user1, chat: chat, message: 'Message2'

      get :index
      response_body = JSON.parse(response.body)
      expect(response_body['chats'][0]['message']['user']['id']).to eq(user1.id)
      expect(response_body['chats'][0]['message']['message']).to eq('Message2')
      expect(response_body['chats'][0]['users'].size).to eq(2)
      expect(response_body['chats'][0]['read_messages']).to eq(nil)
    end

    it 'should changed updated_at for chat' do
      user1 = create(:user, :not_upgraded)
      user2 = create(:user, :not_upgraded)
      sign_in user: user1

      chat = create :chat, :direct, owner: user1, users: [user1, user2]

      updated_at = chat.updated_at

      create :chat_message, :message, user: user1, chat: chat, message: 'Message1'
      create :chat_message, :message, user: user1, chat: chat, message: 'Message2'

      chat.reload
      expect(chat.updated_at).not_to eq(updated_at)
    end

  end

  describe '#leave_chat' do
    it 'should delete current_user from group chat' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded
      sign_in user: user1

      chat = create :chat, :groups, owner: user1, users: [user1, user2, user3]
      get :leave_chat, params: { id: chat.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['msg']).to eq('user left chat')

      get :index
      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['chats'].count).to be 0
    end

    it 'should add user to leave_user in direct chat' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded
      sign_in user: user1

      chat = create :chat, :direct, owner: user1, users: [user1, user2]
      get :leave_chat, params: { id: chat.id }
      get :index
      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['chats'].count).to be 0
    end

    it 'should delete chat if chat has no users' do
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded

      sign_in user: user1
      chat = create :chat, :direct, owner: user1, users: [user1, user2]
      get :leave_chat, params: { id: chat.id }

      sign_in user: user2
      get :leave_chat, params: { id: chat.id }

      expect(user1.chats.size).to be 0
    end
  end
end
