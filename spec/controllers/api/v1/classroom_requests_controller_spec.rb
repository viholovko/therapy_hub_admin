require 'rails_helper'

RSpec.describe ::Api::V1::ClassroomRequestsController, type: :controller do
  render_views

  describe '#update' do
    let(:teacher) {create :user, :teacher}
    let(:student) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return accept invitation to classroom' do
      sign_in user: teacher
      classroom = create :classroom, :with_members, teacher: teacher
      expect(classroom.teacher_accepted).to be false

      put :update, params: {id: classroom.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('You accepted invitation to classroom')
      classroom.reload
      expect(classroom.teacher_accepted).to be true
    end

    it 'should return error if classroom not found' do
      sign_in user: teacher

      put :update, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not invited to classroom' do
      sign_in user: teacher
      classroom = create :classroom, :with_members

      put :update, params: {id: classroom}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end
  end

  describe '#destroy' do
    let(:teacher) {create :user, :teacher}
    let(:student) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return reject invitation to classroom' do
      sign_in user: teacher
      classroom = create :classroom, :with_members, teacher: teacher
      expect(classroom.teacher_accepted).to be false

      delete :destroy, params: {id: classroom.id}
      classroom.reload

      expect(response.status).to be 200
      expect(response_body['message']).to eq('You rejected invitation to classroom.')
      expect(classroom).to_not be_nil
      expect(classroom.teacher_id).to be_nil
      expect(classroom.teacher_accepted).to be false
    end

    it 'should return leave from classroom' do
      sign_in user: teacher
      classroom = create :classroom, :with_members, teacher: teacher, teacher_accepted: true
      expect(classroom.teacher_accepted).to be true

      delete :destroy, params: {id: classroom.id}
      classroom.reload

      expect(response.status).to be 200
      expect(response_body['message']).to eq('You leaved from classroom.')
      expect(classroom.teacher_id).to be_nil
      expect(classroom.teacher_accepted).to be false
    end

    it 'should return destroy classroom if leave classroom\'s owner' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student, owner: student
      expect(classroom.owner_id).to be student.id

      delete :destroy, params: {id: classroom.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('You leaved from classroom. Classroom is destroyed.')
      expect(Classroom.count).to be 0
    end

    it 'should return error if classroom not found' do
      sign_in user: teacher

      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user is not participant in classroom' do
      sign_in user: teacher
      classroom = create :classroom, :with_members

      delete :destroy, params: {id: classroom}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    describe 'create notifications' do
      it 'should return leave from classroom' do
        sign_in user: teacher
        classroom = create :classroom, :with_members, teacher: teacher, teacher_accepted: true

        expect{
          delete :destroy, params: {id: classroom.id}
        }.to change{
          Notification.where(from_user_id: teacher.id, user_id: classroom.owner_id,
                             notification_type: :classroom,
                             message_type: :classroom_leave_user).count
        }.by 1

      end
    end
  end

end
