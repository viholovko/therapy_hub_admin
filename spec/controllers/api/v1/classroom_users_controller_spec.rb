require 'rails_helper'

RSpec.describe Api::V1::ClassroomUsersController, type: :controller do
  render_views

  describe '#update' do
    let(:student) {create :user, :student}
    let(:parent) {create :user, :parent}
    let(:teacher) {create :user, :teacher}
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return added users to classroom' do
      sign_in user: student
      create :friendship, user: student, friend: parent
      create :friendship, user: parent, friend: student

      create :friendship, user: student, friend: teacher
      create :friendship, user: teacher, friend: student

      create :friendship, user: student, friend: caseworker
      create :friendship, user: caseworker, friend: student

      classroom = create :classroom, :empty, student: student, parent: nil,
                         teacher: nil, caseworker: nil
      expect(Notification.count).to be 0
      expect(classroom.chats.last.user_ids).to eq([student.id])

      expect{
        put :update, params: {id: classroom.id, parent_id: parent.id,
                                   teacher_id: teacher.id, caseworker_id: caseworker.id}
      }.to change{Notification.count}.from(0).to(3)

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Users have been added to the classroom')
      classroom.reload
      expect(classroom.parent_id).to be parent.id
      expect(classroom.teacher_id).to be teacher.id
      expect(classroom.caseworker_id).to be caseworker.id
      expect(classroom.parent_accepted).to be false
      expect(classroom.teacher_accepted).to be false
      expect(classroom.caseworker_accepted).to be false

      expect(classroom.chats.last.user_ids).to eq([student.id, parent.id, teacher.id, caseworker.id])
    end

    it 'should return error if given user is not your friend' do
      sign_in user: student

      classroom = create :classroom, :empty, student: student, parent: nil,
                         teacher: nil, caseworker: nil
      expect(Notification.count).to be 0
      expect(classroom.chats.last.user_ids).to eq([student.id])

      put :update, params: {id: classroom.id, parent_id: parent.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00701')
      expect(response_body['errors'][0]['message']).to eq('Parent is not your friend.')
      expect(Notification.count).to be 0
      expect(classroom.chats.last.user_ids).to eq([student.id])
    end

    it 'should return error if given user with inappropriate role' do
      sign_in user: student

      classroom = create :classroom, :empty, student: student, parent: nil,
                         teacher: nil, caseworker: nil
      expect(Notification.count).to be 0
      expect(classroom.chats.last.user_ids).to eq([student.id])

      put :update, params: {id: classroom.id, parent_id: teacher.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00705')
      expect(response_body['errors'][0]['message']).to eq('Given user is not parent.')
      expect(Notification.count).to be 0
      expect(classroom.chats.last.user_ids).to eq([student.id])
    end

    it 'should return error if other user already exist' do
      sign_in user: student

      classroom = create :classroom, :with_members, student: student
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])

      put :update, params: {id: classroom.id, parent_id: parent.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00710')
      expect(response_body['errors'][0]['message']).to eq('parent_id is already exist.')
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])
    end

    it 'should return error if classroom not found' do
      sign_in user: student

      put :update, params: {id: -1, parent_id: parent.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not found' do
      sign_in user: student
      classroom = create :classroom, :empty, student: student
      expect(classroom.chats.last.user_ids).to eq([student.id])

      put :update, params: {id: classroom.id, teacher_id: -1}

      expect(response.status).to be 400
      classroom.reload
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
      expect(classroom.teacher).to be_nil
      expect(classroom.chats.last.user_ids).to eq([student.id])
    end

    it 'should return error if user not authorized' do
      classroom = create :classroom, :empty, student: student
      expect(classroom.chats.last.user_ids).to eq([student.id])

      put :update, params: {id: classroom.id, teacher_id: teacher.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
      classroom.reload
      expect(classroom.teacher).to be_nil
      expect(Notification.count).to be 0
      expect(classroom.chats.last.user_ids).to eq([student.id])
    end

    it 'should return error if user not owner' do
      sign_in user: student
      classroom = create :classroom, :empty
      expect(classroom.chats.last.user_ids).to eq([classroom.owner.id])

      put :update, params: {id: classroom.id, teacher_id: teacher.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(["You are not authorized to access this page."])
      classroom.reload
      expect(classroom.teacher).to be_nil
      expect(Notification.count).to be 0
      expect(classroom.chats.last.user_ids).to eq([classroom.owner.id])
    end

    describe 'create notifications' do
      it 'should return added users to classroom' do
        sign_in user: student
        create :friendship, user: student, friend: parent
        create :friendship, user: parent, friend: student

        create :friendship, user: student, friend: teacher
        create :friendship, user: teacher, friend: student

        create :friendship, user: student, friend: caseworker
        create :friendship, user: caseworker, friend: student

        classroom = create :classroom, :empty, student: student, parent: nil,
                           teacher: nil, caseworker: nil

        expect{
          put :update, params: {id: classroom.id, parent_id: parent.id,
                                teacher_id: teacher.id, caseworker_id: caseworker.id}
        }.to change{
          Notification.where(from_user_id: student.id,
                             notification_type: :classroom_invites,
                             message_type: :classroom_invite_user).count
        }.by 3
      end
    end
  end

  describe '#destroy' do
    let(:student) {create :user, :student}
    let(:parent) {create :user, :parent}
    let(:teacher) {create :user, :teacher}
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return removed users from classroom' do
      sign_in user: student
      create :friendship, user: student, friend: parent
      create :friendship, user: parent, friend: student
      create :friendship, user: student, friend: teacher
      create :friendship, user: teacher, friend: student
      create :friendship, user: student, friend: caseworker
      create :friendship, user: caseworker, friend: student
      classroom = create :classroom, :with_members, student: student, parent: parent,
                         teacher: teacher, caseworker: caseworker
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, parent.id, teacher.id, caseworker.id])

      expect{
        delete :destroy, params: {id: classroom.id, parent_id: parent.id,
                                       teacher_id: teacher.id,
                                  caseworker_id: caseworker.id}
      }.to change{Notification.count}.from(3).to(6)


      expect(response.status).to be 200
      expect(response_body['message']).to eq('Users have been removed from this classroom')
      classroom.reload
      expect(classroom.parent_id).to be_nil
      expect(classroom.teacher_id).to be_nil
      expect(classroom.caseworker_id).to be_nil
      expect(classroom.parent_accepted).to be false
      expect(classroom.teacher_accepted).to be false
      expect(classroom.caseworker_accepted).to be false
      expect(classroom.chats.last.user_ids).to eq([student.id])
    end

    it 'should return error if given user not equals user in classroom' do
      sign_in user: student

      classroom = create :classroom, :with_members, student: student
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])

      delete :destroy, params: {id: classroom.id, parent_id: parent.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00712')
      expect(response_body['errors'][0]['message']).to eq('parent_id is wrong.')
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])
    end

    it 'should return error if classroom not found' do
      sign_in user: student

      delete :destroy, params: {id: -1, parent_id: parent.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      classroom = create :classroom, :with_members, student: student
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])

      delete :destroy, params: {id: classroom.id, teacher_id: teacher.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
      classroom.reload
      expect(classroom.teacher).to_not be_nil
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])
    end

    it 'should return error if user removed yourself' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])

      delete :destroy, params: {id: classroom.id, student_id: student.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00711')
      expect(response_body['errors'][0]['message']).to eq('You can\'t to remove yourself.')
      classroom.reload
      expect(classroom.student).to_not be_nil
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([student.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])
    end

    it 'should return error if user not owner' do
      sign_in user: student
      classroom = create :classroom, :with_members
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([classroom.owner.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])

      delete :destroy, params: {id: classroom.id, teacher_id: teacher.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(["You are not authorized to access this page."])
      classroom.reload
      expect(classroom.teacher).to_not be_nil
      expect(Notification.count).to be 3
      expect(classroom.chats.last.user_ids).to eq([classroom.owner.id, classroom.parent.id, classroom.teacher.id, classroom.caseworker.id])
    end

    describe 'create notifications' do
      it 'should return removed users from classroom' do
        sign_in user: student
        create :friendship, user: student, friend: parent
        create :friendship, user: parent, friend: student
        create :friendship, user: student, friend: teacher
        create :friendship, user: teacher, friend: student
        create :friendship, user: student, friend: caseworker
        create :friendship, user: caseworker, friend: student
        classroom = create :classroom, :with_members, student: student, parent: parent,
                           teacher: teacher, caseworker: caseworker

        expect{
          delete :destroy, params: {id: classroom.id, parent_id: parent.id,
                                    teacher_id: teacher.id,
                                    caseworker_id: caseworker.id}
        }.to change{
          Notification.where(from_user_id: student.id,
                                       notification_type: :classroom,
                                       message_type: :classroom_remove_user).count
        }.by 3
      end
    end
  end
end
