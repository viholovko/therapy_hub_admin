require 'rails_helper'

RSpec.describe ::Api::V1::ClassroomsController, type: :controller do
  render_views

  describe '#create' do
    let(:student) {create :user, :student}
    let(:parent) {create :user, :parent}
    let(:teacher) {create :user, :teacher}
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return classroom' do
      sign_in user: student

      create :friendship, user: student, friend: parent
      create :friendship, friend: student, user: parent
      create :friendship, user: student, friend: teacher
      create :friendship, friend: student, user: teacher
      create :friendship, user: student, friend: caseworker
      create :friendship, friend: student, user: caseworker

      post :create, params: {parent_id: parent.id, teacher_id: teacher.id,
                                         caseworker_id: caseworker.id,
                             chats_attributes: [{title: 'clasroom_chat', chat_type: 'classroom',
                                                user_ids: [parent.id, teacher.id, caseworker.id]
                             }]
      }
      expect(response.status).to be 200
      expect(response_body['message']).to eq('Classroom is created')
      expect(Classroom.last.owner_id).to be student.id
      expect(Classroom.last.student_id).to be student.id
      expect(Classroom.last.student_accepted).to be true
      expect(Classroom.last.parent_id).to be parent.id
      expect(Classroom.last.parent_accepted).to be false
      expect(Classroom.last.teacher_id).to be teacher.id
      expect(Classroom.last.teacher_accepted).to be false
      expect(Classroom.last.caseworker_id).to be caseworker.id
      expect(Classroom.last.caseworker_accepted).to be false
      expect(Chat.count).to be 1
      expect(Chat.find_by_id(response_body['chat_id'])).to_not be_nil
      expect(Chat.find_by_id(response_body['chat_id']).users.count).to be 4
      expect(Chat.find_by_id(response_body['chat_id']).user_ids).to eq([student.id, parent.id, teacher.id, caseworker.id])
    end

    it 'should return empty classroom' do
      sign_in user: student
      post :create, params: { chats_attributes: [{title: 'clasroom_chat', chat_type: 'classroom', user_ids: nil}]}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Classroom is created')
      expect(Classroom.last.owner_id).to be student.id
      expect(Classroom.last.student_id).to be student.id
      expect(Classroom.last.student_accepted).to be true
      expect(Classroom.last.parent_id).to be_nil
      expect(Classroom.last.teacher_id).to be_nil
      expect(Classroom.last.caseworker_id).to be_nil
    end

    it 'should return validation errors' do
      sign_in user: student

      post :create, params: {parent_id: parent.id, teacher_id: teacher.id,
                             caseworker_id: caseworker.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00701')
      expect(response_body['errors'][0]['message']).to eq('Parent is not your friend.')
      expect(response_body['errors'][1]['code']).to eq('E00702')
      expect(response_body['errors'][1]['message']).to eq('Teacher is not your friend.')
      expect(response_body['errors'][2]['code']).to eq('E00703')
      expect(response_body['errors'][2]['message']).to eq('Caseworker is not your friend.')
    end

    it 'should return errors if not matching roles' do
      sign_in user: student
      create :friendship, user: student, friend: parent
      create :friendship, friend: student, user: parent
      create :friendship, user: student, friend: teacher
      create :friendship, friend: student, user: teacher
      create :friendship, user: student, friend: caseworker
      create :friendship, friend: student, user: caseworker

      post :create, params: {parent_id: teacher.id, caseworker_id: parent.id,
                             teacher_id: caseworker.id,
                             chats_attributes: [
                                 {title: 'clasroom_chat', chat_type: 'classroom',
                                  user_ids: [parent.id, teacher.id, caseworker.id]
                                 }
                             ]
      }
      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00705')
      expect(response_body['errors'][0]['message']).to eq('Given user is not parent.')
      expect(response_body['errors'][1]['code']).to eq('E00706')
      expect(response_body['errors'][1]['message']).to eq('Given user is not teacher.')
      expect(response_body['errors'][2]['code']).to eq('E00707')
      expect(response_body['errors'][2]['message']).to eq('Given user is not caseworker.')
    end

    it 'should return error if creator is not parent or student' do
      sign_in user: teacher

      post :create, params: {student_id: student.id, parent_id: parent.id,
                             caseworker_id: caseworker.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    it 'should return error if user not authorized' do
      post :create, params: {student_id: student.id, parent_id: parent.id,
                             caseworker_id: caseworker.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    describe 'create notifications' do
      it 'should return classroom' do
        sign_in user: student

        create :friendship, user: student, friend: parent
        create :friendship, friend: student, user: parent
        create :friendship, user: student, friend: teacher
        create :friendship, friend: student, user: teacher
        create :friendship, user: student, friend: caseworker
        create :friendship, friend: student, user: caseworker

        expect{
          post :create, params: {parent_id: parent.id, teacher_id: teacher.id,
                                 caseworker_id: caseworker.id,
                                 chats_attributes: [{title: 'clasroom_chat', chat_type: 'classroom',
                                                     user_ids: [parent.id, teacher.id, caseworker.id]
                                                    }]
          }
        }.to change{
          Notification.where(from_user_id: student.id,
                             notification_type: :classroom_invites,
                             message_type: :classroom_invite_user).count
        }.by 3
      end
    end
  end

  describe '#update' do
    let(:student) {create :user, :student}
    let(:parent) {create :user, :parent}
    let(:teacher) {create :user, :teacher}
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return update classroom' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student

      put :update, params: {id: classroom.id, name: 'newname'}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Classroom is updated')
      expect(Classroom.first.name).to eq('newname')
    end

    it 'should return validation errors' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student
      # create :friendship, user: student, friend: classroom.teacher
      # create :friendship, friend: student, user: classroom.teacher

      put :update, params: {id: classroom.id,
                            parent_id: caseworker.id, caseworker_id: classroom.teacher.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00705')
      expect(response_body['errors'][0]['message']).to eq('Given user is not parent.')
      expect(response_body['errors'][1]['code']).to eq('E00701')
      expect(response_body['errors'][1]['message']).to eq('Parent is not your friend.')
      expect(response_body['errors'][2]['code']).to eq('E00707')
      expect(response_body['errors'][2]['message']).to eq('Given user is not caseworker.')
    end

    it 'should return error if updater is not creator' do
      sign_in user: teacher
      classroom = create :classroom, :with_members

      put :update, params: {id: classroom.id,
                            parent_id: caseworker.id, caseworker_id: teacher.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    it 'should return error if user not authorized' do
      put :update, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if classroom not found' do
      sign_in user: student

      put :update, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end
  end

  describe '#show' do
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return classroom if user accepted invite to classroom' do
      sign_in user: caseworker
      classroom = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: true

      get :show, params: {id: classroom.id}

      expect(response.status).to be 200
      expect(response_body['classroom']['id']).to be classroom.id
      expect(response_body['classroom']['chat_id']).to be > 0
      expect(response_body['classroom']['avatar']).to_not be_nil
      expect(response_body['classroom']['avatar_preview']).to_not be_nil
    end

    it 'should return error if classroom not found' do
      sign_in user: caseworker

      get :show, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user is not authorized' do
      classroom = create :classroom, :with_members, caseworker: caseworker

      get :show, params: {id: classroom.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if user not invited to classroom' do
      user = create :user, :student
      sign_in user: user
      classroom1 = create :classroom, :with_members


      get :show, params: {id: classroom1.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    it 'should return error if user not accepted invite to classroom' do
      user = create :user, :teacher
      sign_in user: user
      classroom = create :classroom, :with_members, teacher: user, teacher_accepted: false

      get :show, params: {id: classroom.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end
  end

  describe '#index' do
    let(:caseworker) {create :user, :caseworker}
    let(:student) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return accepted classrooms list' do
      sign_in user: caseworker
      classroom1 = create :classroom, :with_members, caseworker: caseworker
      classroom2 = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: true
      classroom3 = create :classroom, :with_members, caseworker: caseworker
      classroom4 = create :classroom

      get :index, params: {accepted: true}

      expect(response.status).to be 200
      expect(response_body['classrooms'].count).to be 1
      expect(response_body['count']).to be 1
      expect(response_body['invites_count']).to be 2

      expect(response_body['classrooms'][0]['id']).to be classroom2.id
      expect(response_body['classrooms'][0]['role_in_classroom']).to eq(caseworker.role.name)
      expect(response_body['classrooms'][0]['status_in_classroom']).to eq(true)
      expect(response_body['classrooms'][0]['chat_id']).to be > 0
    end

    it 'should return not accepted classrooms list' do
      sign_in user: caseworker
      classroom1 = create :classroom, :with_members, caseworker: caseworker
      classroom2 = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: true
      classroom3 = create :classroom, :with_members, caseworker: caseworker
      classroom4 = create :classroom

      get :index, params: {accepted: false}

      expect(response.status).to be 200
      expect(response_body['classrooms'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['invites_count']).to be 2

      expect(response_body['classrooms'][0]['id']).to be classroom3.id
      expect(response_body['classrooms'][0]['role_in_classroom']).to eq(caseworker.role.name)
      expect(response_body['classrooms'][0]['status_in_classroom']).to eq(false)
      expect(response_body['classrooms'][0]['chat_id']).to be > 0

      expect(response_body['classrooms'][1]['id']).to be classroom1.id
      expect(response_body['classrooms'][1]['role_in_classroom']).to eq(caseworker.role.name)
      expect(response_body['classrooms'][1]['status_in_classroom']).to eq(false)
      expect(response_body['classrooms'][1]['chat_id']).to be > 0
    end

    it 'should return empty classroom list' do
      sign_in user: caseworker
      create :classroom, :with_members
      create :classroom, :with_members
      create :classroom, :with_members

      get :index

      expect(response.status).to be 200
      expect(response_body['classrooms'].count).to be 0
      expect(response_body['count']).to be 0
      expect(response_body['classrooms']).to eq([])
    end

    it 'should return error if user is not authorized' do
      create :classroom, :with_members
      create :classroom, :with_members

      get :index

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end

  describe '#tasks_and_events' do
    let(:teacher) {create :user, :teacher}
    let(:parent) {create :user, :parent}
    let(:caseworker) {create :user, :caseworker}
    let(:student) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return list of tasks and events' do
      classroom = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: true, parent: parent,
                         parent_accepted: true, student: student, student_accepted: true
      sign_in user: teacher

      task1 = create :task, classroom: classroom
      task2 = create :task, classroom: classroom

      sign_in user: student

      event1 = create :event, classroom: classroom, owner: student
      event2 = create :event, classroom: classroom, owner: student

      get :tasks_and_events, params: {id: classroom.id, sort_type: 'desc'}

      expect(response.status).to be 200
      expect(response_body['count_events']).to be 2
      expect(response_body['count_tasks']).to be 2
      expect(response_body['events'][0]['id']).to eq(event2.id)
      expect(response_body['events'][1]['id']).to eq(event1.id)
      expect(response_body['tasks'][0]['id']).to eq(task1.id)
      expect(response_body['tasks'][1]['id']).to eq(task2.id)
    end

    it 'should return list of tasks and events with correct date range' do
      classroom = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: true, parent: parent,
                         parent_accepted: true, student: student, student_accepted: true
      sign_in user: teacher

      task1 = create :task, classroom: classroom, due_date: Time.now - 5.day
      task2 = create :task, classroom: classroom, due_date: Time.now
      task3 = create :task, classroom: classroom, due_date: Time.now + 2.day

      sign_in user: student

      event1 = create :event, classroom: classroom, owner: student, time_from: Time.now - 6.day, time_to: Time.now + 5.day
      event2 = create :event, classroom: classroom, owner: student, time_from: Time.now - 2.day, time_to: Time.now
      event3 = create :event, classroom: classroom, owner: student, time_from: Time.now - 2.day, time_to: Time.now - 1.day
      event4 = create :event, classroom: classroom, owner: student, time_from: Time.now - 1.day, time_to: Time.now + 1.day
      event5 = create :event, classroom: classroom, owner: student, time_from: Time.now, time_to: Time.now + 2.day

      get :tasks_and_events, params: {id: classroom.id, sort_type: 'desc', from_date: Time.now - 2, to_date: Time.now + 2}

      expect(response.status).to be 200
      expect(response_body['count_events']).to be 2
      expect(response_body['count_tasks']).to be 1

      expect(response_body['events'][0]['id']).to eq(event5.id)
      expect(response_body['events'][1]['id']).to eq(event2.id)

      expect(response_body['tasks'][0]['id']).to eq(task2.id)
    end

    it 'should error for not autorized to access for user' do
      classroom = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: false,
                         parent: parent, parent_accepted: false,
                         student: student, student_accepted: true,
                         teacher: teacher, teacher_accepted: true
      sign_in user: student

      create :task, classroom: classroom
      create :event, classroom: classroom, owner: student

      sign_in user: parent

      get :days_for_tasks_and_events, params: {id: classroom.id}

      expect(response.status).to be 403
      expect(response_body['errors'][0]).to eq('You are not authorized to access this page.')

      sign_in user: caseworker

      get :days_for_tasks_and_events, params: {id: classroom.id}

      expect(response.status).to be 403
      expect(response_body['errors'][0]).to eq('You are not authorized to access this page.')
    end

  end

  describe '#days_for_tasks_and_events' do
    let(:teacher) {create :user, :teacher}
    let(:parent) {create :user, :parent}
    let(:caseworker) {create :user, :caseworker}
    let(:student) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return list of dates' do
      classroom = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: true, parent: parent,
                         parent_accepted: true, student: student, student_accepted: true
      sign_in user: teacher

      task1 = create :task, classroom: classroom, due_date: Time.now + 1.day
      task2 = create :task, classroom: classroom, due_date: Time.now + 2.day

      sign_in user: student

      event1 = create :event, classroom: classroom, owner: student, time_from: Time.now - 1.day, time_to: Time.now + 1.day
      event2 = create :event, classroom: classroom, owner: student, time_from: Time.now - 2.day, time_to: Time.now + 2.day

      get :days_for_tasks_and_events, params: {id: classroom.id}

      expect(response.status).to be 200
      expect(response_body['events'].count).to be 3
      expect(response_body['tasks'].count).to be 2
    end

    it 'should error for not autorized to access for user' do
      classroom = create :classroom, :with_members, caseworker: caseworker, caseworker_accepted: true, parent: parent,
                         parent_accepted: false, student: student, student_accepted: true
      sign_in user: teacher

      create :task, classroom: classroom
      create :event, classroom: classroom, owner: student

      sign_in user: parent

      get :days_for_tasks_and_events, params: {id: classroom.id}

      expect(response.status).to be 403
      expect(response_body['errors'][0]).to eq('You are not authorized to access this page.')
    end
  end

  describe '#assessments' do
    let(:student) {create :user, :student}
    let(:parent) {create :user, :parent}
    let(:teacher) {create :user, :teacher}
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should add assessment_link to classroom' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student, parent_id: parent.id, parent_accepted: true, teacher_id: teacher.id, teacher_accepted: true

      sign_in user: classroom.parent

      params = {
        id: classroom.id,
        assessment_title: 'test_1',
        assessment_link: "google.com",
      }

      put :assessments, params: params

      classroom.reload

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Assessment is saved')
      expect(classroom.assessment_title).to eq(params[:assessment_title])
      expect(classroom.assessment_link).to eq(params[:assessment_link])
    end

    it 'should return error to change link in assessment by other user' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student, parent_id: parent.id, parent_accepted: true, teacher_id: teacher.id, teacher_accepted: true,
                         caseworker_id: caseworker.id, caseworker_accepted: true, assessment_title: 'test_1', assessment_link: "google.com", assessment_owner_id: parent.id

      sign_in user: classroom.caseworker

      params = {
        id: classroom.id,
        assessment_title: 'test_1',
        assessment_link: "google.com"
      }

      put :assessments, params: params

      expect(response.status).to be 403
      expect(response_body['errors'][0]).to eq('You are not authorized to access this page.')
    end

    it 'should change link in assessment by assessment_owner' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student, parent_id: parent.id, parent_accepted: true, teacher_id: teacher.id, teacher_accepted: true,
                         caseworker_id: caseworker.id, caseworker_accepted: true, assessment_title: 'test_1', assessment_link: "google.com", assessment_owner_id: parent.id

      sign_in user: classroom.parent

      params = {
        id: classroom.id,
        assessment_title: 'test_2',
        assessment_link: "www.youtube.com"
      }

      put :assessments, params: params

      classroom.reload

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Assessment is saved')
      expect(classroom.assessment_title).to eq(params[:assessment_title])
      expect(classroom.assessment_link).to eq(params[:assessment_link])
    end

    describe 'create notifications' do
      it 'should add assessment_link to classroom' do
        classroom = create :classroom, :with_members,
                           student: student, parent_id: parent.id,
                           parent_accepted: true, teacher_id: teacher.id,
                           teacher_accepted: true

        sign_in user: classroom.parent

        params = {
            id: classroom.id,
            assessment_title: 'test_1',
            assessment_link: "google.com",
        }

        expect{
          put :assessments, params: params
        }.to change{
          Notification.where(from_user_id: classroom.parent.id,
                             notification_type: :classroom,
                             message_type: :classroom_create_assessment).count
        }.by 2
      end
    end

  end

  describe '#delete_assessments' do
    let(:student) {create :user, :student}
    let(:parent) {create :user, :parent}
    let(:teacher) {create :user, :teacher}
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should add assessment_link to classroom' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student, parent_id: parent.id, parent_accepted: true, teacher_id: teacher.id, teacher_accepted: true,
                         caseworker_id: caseworker.id, caseworker_accepted: true, assessment_title: 'test_1', assessment_link: "google.com", assessment_owner_id: parent.id

      sign_in user: classroom.parent

      params = {
        id: classroom.id
      }

      delete :delete_assessments, params: params

      classroom.reload

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Assessment is deleted')
      expect(classroom.assessment_title).to eq(nil)
      expect(classroom.assessment_link).to eq(nil)
    end

    it 'should return error to change link in assessment by other user' do
      sign_in user: student
      classroom = create :classroom, :with_members, student: student, parent_id: parent.id, parent_accepted: true, teacher_id: teacher.id, teacher_accepted: true,
                         caseworker_id: caseworker.id, caseworker_accepted: true, assessment_title: 'test_1', assessment_link: "google.com", assessment_owner_id: parent.id

      sign_in user: classroom.caseworker

      params = {
        id: classroom.id
      }

      delete :delete_assessments, params: params

      expect(response.status).to be 403
      expect(response_body['errors'][0]).to eq('You are not authorized to access this page.')
    end
  end


end
