require 'rails_helper'

RSpec.describe Api::V1::CodeController, type: :controller do
  render_views
  describe '#create' do
    it 'should send code via sms' do
      country = create :country
      phone_number = '12345'

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(0)

      post :create, params: {country_id: country.id, phone_number: phone_number}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(response_body['message']).to eq("Code sent.")

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)

      code_request = CodeRequest.first
      expect(code_request.code).to_not be(nil)
      expect(code_request.country_id).to eq(country.id)
      expect(code_request.phone_number).to eq(phone_number)
      expect(code_request.attempts_count).to eq(1)
    end

    it 'should raise user not found on login' do
      country = create :country
      phone_number = '12345'

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(0)

      post :create, params: {country_id: country.id, phone_number: phone_number, for: 'signin'}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq("E00107")

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(0)
    end

    it 'should not allow to send code in 1 minute after first attempt' do
      country = create :country
      phone_number = '12345'
      code_request = create :code_request, country_id: country.id, phone_number: phone_number, attempts_count: 1, updated_at: Time.now

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)

      Timecop.travel Time.now + 30.seconds do
        post :create, params: {country_id: country.id, phone_number: phone_number}

        response_body = JSON.parse(response.body)

        expect(response.status).to be(400)
        expect(response_body['errors'][0]['code']).to eq("E00100")

        expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)

        code_request = CodeRequest.first
        expect(code_request.code).to_not be(nil)
        expect(code_request.country_id).to eq(country.id)
        expect(code_request.phone_number).to eq(phone_number)
        expect(code_request.attempts_count).to eq(1)
      end
    end

    it 'should not allow to send code in 3 minutes after second attempt' do
      country = create :country
      phone_number = '12345'
      code_request = create :code_request, country_id: country.id, phone_number: phone_number, attempts_count: 2, updated_at: Time.now

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)

      Timecop.travel Time.now + 30.seconds do
        post :create, params: {country_id: country.id, phone_number: phone_number}

        response_body = JSON.parse(response.body)

        expect(response.status).to be(400)
        expect(response_body['errors'][0]['code']).to eq("E00100")

        expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)

        code_request = CodeRequest.first
        expect(code_request.code).to_not be(nil)
        expect(code_request.country_id).to eq(country.id)
        expect(code_request.phone_number).to eq(phone_number)
        expect(code_request.attempts_count).to eq(2)
      end
    end

    it 'should not allow to send code in 10 minutes after third attempt' do
      country = create :country
      phone_number = '12345'
      code_request = create :code_request, country_id: country.id, phone_number: phone_number, attempts_count: 3, updated_at: Time.now

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)

      Timecop.travel Time.now + 30.seconds do
        post :create, params: {country_id: country.id, phone_number: phone_number}

        response_body = JSON.parse(response.body)

        expect(response.status).to be(400)
        expect(response_body['errors'][0]['code']).to eq("E00100")

        expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)

        code_request = CodeRequest.first
        expect(code_request.code).to_not be(nil)
        expect(code_request.country_id).to eq(country.id)
        expect(code_request.phone_number).to eq(phone_number)
        expect(code_request.attempts_count).to eq(3)
      end
    end
  end
end
