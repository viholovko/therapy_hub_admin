require 'rails_helper'

RSpec.describe Api::V1::CommentsController, type: :controller do
  render_views

  describe '#create' do
    let(:response_body) {JSON.parse(response.body)}
    let(:post1) {create :post, :photo}
    let(:user) {create :user, :student}

    it 'should return comment to post' do
      sign_in user: user

      post :create, params: {message: 'Message', commentable_type: 'Post', commentable_id: post1.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Comment successfully created.')
      expect(post1.comments.count).to be 1
    end

    it 'should return error if commentable type is wrong' do
      sign_in user: user
      post1 = create :post, :link

      post :create, params: {commentable_id: post1.id, commentable_type: 'wrongtype', message: 'message'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01003')
      expect(response_body['errors'][0]['message']).to eq('Commentable type is wrong.')
    end

    it 'should return validation errors' do
      sign_in user: user

      post :create
      expect(response.status).to be 400

      expect(response_body['errors'][0]['code']).to eq('E01000')
      expect(response_body['errors'][0]['message']).to eq('Message can\'t be blank')

      expect(response_body['errors'][1]['code']).to eq('E01001')
      expect(response_body['errors'][1]['message']).to eq('Commentable id can\'t be blank')

      expect(response_body['errors'][2]['code']).to eq('E01002')
      expect(response_body['errors'][2]['message']).to eq('Commentable type can\'t be blank')
    end

    it 'should return errors if user not authorized' do
      post :create, params: {message: 'Message', commentable_type: 'Post', commentable_id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if commentable not found' do
      sign_in user: user

      post :create, params: {commentable_id: -1, commentable_type: 'Post', message: 'message'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    describe 'create notifications' do
      it 'should return comment to post' do
        sign_in user: user

        expect{
          post :create, params: {message: 'Message', commentable_type: 'Post', commentable_id: post1.id}
        }.to change{Notification.where(user_id: post1.user_id, from_user_id: user.id,
                                       message_type: :post_create_comment,
                                       notification_type: :posts).count
        }.by 1
      end

      it 'should return comment to comment' do
        sign_in user: user
        comment1 = create :comment, commentable: post1, post: post1

        expect{
          post :create, params: {message: 'Message', commentable_type: 'Comment', commentable_id: comment1.id}
        }.to change{Notification.where(user_id: comment1.user_id, from_user_id: user.id,
                                       message_type: :post_comment_create_reply,
                                       notification_type: :posts).count
        }.by 1
      end
    end

    it 'should return comment_member_id to post' do
      sign_in user: user

      member = create :user, :not_upgraded

      post :create, params: {message: 'Message', commentable_type: 'Post', commentable_id: post1.id, comment_member_id: member.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Comment successfully created.')
      expect(response_body['comment']['comment_member']['id']).to eq(member.id)
      expect(post1.comments.count).to be 1
    end
  end

  describe '#index' do
    let(:response_body) {JSON.parse(response.body)}
    let(:post1) {create :post, :link}
    let(:user) {create :user, :student}

    it 'should return comments list' do
      sign_in user: user
      create :comment,  :post
      comment2 = create :comment,  commentable: post1
      comment3 = create :comment,  commentable: post1
      comment4 = create :comment,  commentable: post1
      create :comment,  commentable: comment4
      create :comment,  commentable: comment4

      get :index, params: {commentable_id: post1.id, commentable_type: 'Post'}

      expect(response.status).to be 200
      expect(response_body['comments'].count).to be 3
      expect(response_body['count']).to be 3
      expect(response_body['comments'][0]['id']).to be comment4.id
      expect(response_body['comments'][0]['number_of_replies']).to be 2

      expect(response_body['comments'][1]['id']).to be comment3.id
      expect(response_body['comments'][1]['number_of_replies']).to be 0

      expect(response_body['comments'][2]['id']).to be comment2.id
      expect(response_body['comments'][2]['number_of_replies']).to be 0

    end

    it 'should return comments list with limit' do
      sign_in user: user
      comment1 = create :comment,  commentable: post1
      comment2 = create :comment,  commentable: post1
      comment3 = create :comment,  commentable: post1
      comment4 = create :comment,  commentable: post1
      comment5 = create :comment,  commentable: post1

      get :index, params: {commentable_id: post1.id, commentable_type: 'Post', from_id: comment1.id, to_id: comment5.id, limit: 2}

      expect(response.status).to be 200
      expect(response_body['comments'].count).to be 2
      expect(response_body['count']).to be 5
      expect(response_body['comments'][0]['id']).to be comment4.id
      expect(response_body['comments'][1]['id']).to be comment3.id
    end

    it 'should return comments list with comment_member_id' do
      sign_in user: user

      member = create :user, :student

      comment1 = create :comment,  commentable_id: post1.id, commentable_type: 'Post', comment_member_id: member.id
      comment2 = create :comment,  commentable_id: post1.id, commentable_type: 'Post'

      get :index

      expect(response.status).to be 200
      expect(response_body['comments'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['comments'][0]['id']).to be comment2.id
      expect(response_body['comments'][1]['id']).to be comment1.id
      expect(response_body['comments'][1]['comment_member']['id']).to eq(comment1.comment_member_id)
    end

    it 'should return empty list' do
      sign_in user: user

      get :index, params: {commentable_id: post1.id, comentable_type: 'Post'}

      expect(response.status).to be 200
      expect(response_body['comments']).to eq([])
    end

    it 'should return comments to comment list' do
      sign_in user: user
      comment1 = create :comment,  commentable: post1
      comment2 = create :comment,  commentable: comment1
      comment3 = create :comment,  commentable: comment1
      comment4 = create :comment,  commentable: comment1
      comment5 = create :comment,  commentable: comment1

      get :index, params: {commentable_id: comment1.id, commentable_type: 'Comment'}

      expect(response.status).to be 200
      expect(response_body['comments'].count).to be 4
      expect(response_body['count']).to be 4
      expect(response_body['comments'][0]['id']).to be comment5.id
      expect(response_body['comments'][1]['id']).to be comment4.id
      expect(response_body['comments'][2]['id']).to be comment3.id
      expect(response_body['comments'][3]['id']).to be comment2.id
    end
  end
end
