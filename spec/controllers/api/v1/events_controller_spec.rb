require 'rails_helper'

RSpec.describe ::Api::V1::EventsController, type: :controller do
  render_views

  describe '#create' do
    let(:classroom) {create :classroom}
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return event' do
      sign_in user: user

      post :create, params: {title: 'Title', description: 'Description',
                             classroom_id: classroom.id, time_from: Time.now + 5.hours, time_to: Time.now + 10.hours
      }

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Event is created')
    end

    it 'should return validation errors' do
      sign_in user: user

      post :create, params: {}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01300')
      expect(response_body['errors'][0]['message']).to eq('Title can\'t be blank.')

      expect(response_body['errors'][1]['code']).to eq('E01301')
      expect(response_body['errors'][1]['message']).to eq('Description can\'t be blank.')

      expect(response_body['errors'][2]['code']).to eq('E01302')
      expect(response_body['errors'][2]['message']).to eq('From date can\'t be blank.')

      expect(response_body['errors'][3]['code']).to eq('E01303')
      expect(response_body['errors'][3]['message']).to eq('To date can\'t be blank.')

      expect(response_body['errors'][4]['code']).to eq('E01304')
      expect(response_body['errors'][4]['message']).to eq('Classroom can\'t be blank.')
    end

    it 'should return error if user not authorized' do

      post :create, params: {title: 'Title', description: 'Description'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if user is not_upgraded' do
      user = create :user, :not_upgraded
      sign_in user: user

      post :create, params: {title: 'Title', description: 'Description',
                             classroom_id: classroom.id, time_from: Time.now + 5.hours, time_to: Time.now + 10.hours
      }

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    describe 'create notifications' do
      it 'should return event' do
        classroom1 = create :classroom, :with_members, student: user, parent_accepted: true, teacher_accepted: true
        sign_in user: user

        expect{
          post :create, params: {title: 'Title', description: 'Description',
                                 classroom_id: classroom1.id, time_from: Time.now + 5.hours, time_to: Time.now + 10.hours
          }
        }.to change{
          Notification.where(from_user_id: user.id,
                             notification_type: :classroom,
                             message_type: :classroom_create_event).count
        }.by 2
      end
    end
  end

  describe '#destroy' do
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return delete event' do
      sign_in user: user
      event = create :event, owner: user

      delete :destroy, params: {id: event.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Event is deleted')
      expect(Task.count).to be 0
    end

    it 'should return error if destroyer not owner' do
      sign_in user: user
      event = create :event
      expect(event.owner_id).to_not be user.id

      delete :destroy, params: {id: event.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Event.count).to_not be 0
    end

    it 'should return error if user not authorized' do
      event = create :event
      delete :destroy, params: {id: event.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if event not found' do
      sign_in user: user

      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end
  end

  describe '#update' do
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return update event' do
      sign_in user: user
      event = create :event, owner: user

      put :update, params: {id: event.id, title: 'newtitle', description: 'newdescription',
                            time_from: Time.new(2000) + 1.day, time_to: Time.new(2000) + 2.days}
      event.reload
      expect(response.status).to be 200
      expect(response_body['message']).to eq('Event is updated')
      expect(event.title).to eq('newtitle')
      expect(event.description).to eq('newdescription')
      expect(event.time_from).to eq(Time.new(2000) + 1.day)
      expect(event.time_to).to eq(Time.new(2000) + 2.days)
    end

    it 'should return validation errors' do
      sign_in user: user

      post :create, params: {title: '', description: '', time_from: '', time_to: ''}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01300')
      expect(response_body['errors'][0]['message']).to eq('Title can\'t be blank.')

      expect(response_body['errors'][1]['code']).to eq('E01301')
      expect(response_body['errors'][1]['message']).to eq('Description can\'t be blank.')

      expect(response_body['errors'][2]['code']).to eq('E01302')
      expect(response_body['errors'][2]['message']).to eq('From date can\'t be blank.')

      expect(response_body['errors'][3]['code']).to eq('E01303')
      expect(response_body['errors'][3]['message']).to eq('To date can\'t be blank.')
    end

    it 'should return error if user not owner' do
      sign_in user: user
      event = create :event
      expect(event.owner_id).to_not be user.id

      delete :destroy, params: {id: event.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Event.count).to_not be 0
    end

    it 'should return error if user not authorized' do
      event = create :event
      delete :destroy, params: {id: event.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end

  describe '#index' do
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return events list' do
      sign_in user: user
      classroom = create :classroom, student: user
      event1 = create :event, classroom: classroom, time_from: Time.now - 1.day, time_to: Time.now + 1.day
      event2 = create :event, classroom: classroom, time_from: Time.now - 2.day, time_to: Time.now + 2.day
      event3 = create :event, classroom: classroom, time_from: Time.now + 1.day, time_to: Time.now + 2.hours
      event4 = create :event, classroom: classroom, time_from: Time.now + 2.day, time_to: Time.now + 2.hours
      event5 = create :event, classroom: classroom, time_from: Time.now - (1.day + 1.hour), time_to: Time.now + 7.hours

      get :index, params: {classroom_id: classroom.id, date_from: (Time.now - (1.day + 30.minutes)).utc, date_to: (Time.now + 6.hours).utc}

      expect(response.status).to be 200
      expect(response_body['events'].count).to be 3
      expect(response_body['count']).to be 3
      expect(response_body['events'][0]['id']).to be event4.id
      expect(response_body['events'][1]['id']).to be event3.id
      expect(response_body['events'][2]['id']).to be event1.id
    end

    it 'should return error if user not authorized' do
      get :index

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return empty event list' do
      sign_in user: user

      get :index

      expect(response.status).to be 200
      expect(response_body['events'].count).to be 0
      expect(response_body['count']).to be 0
      expect(response_body['events']).to eq([])
    end
  end
end
