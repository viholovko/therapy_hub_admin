require 'rails_helper'

RSpec.describe ::Api::V1::FollowingsController, type: :controller do
  render_views

  describe '#create' do
    let(:response_body) {JSON.parse(response.body)}
    let(:user1) {create :user, :student}
    let(:user2) {create :user, :teacher}

    it 'should return follow user' do
      sign_in user: user1
      post :create, params: {follow_user_id: user2.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq("You successfully follow to #{user2.first_name} #{user2.last_name}.")
      expect(user1.followed.count).to be 1
    end

    it 'should return error if follow on to yourself' do
      sign_in user: user1
      post :create, params: {follow_user_id: user1.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00301')
      expect(response_body['errors'][0]['message']).to eq('You can\'t follow on to yourself.')
      expect(user1.followed.count).to be 0
    end

    it 'should return error if already following on to user' do
      sign_in user: user1
      post :create, params: {follow_user_id: user2.id}
      post :create, params: {follow_user_id: user2.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq("E00302")
      expect(response_body['errors'][0]['message']).to eq("You already following to #{user2.first_name} #{user2.last_name}.")
      expect(user1.followed.count).to be 1
    end

    it 'should return error if user not found' do
      sign_in user: user1
      post :create, params: {follow_user_id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      delete :create, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if not_upgraded user following' do
      not_upgraded = create :user, :not_upgraded
      sign_in user: not_upgraded

      post :create, params: {follow_user_id: user1.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      end

    it 'should return error if user following to not_upgraded user' do
      not_upgraded = create :user, :not_upgraded
      sign_in user: user1

      post :create, params: {follow_user_id: not_upgraded.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    describe 'create notifications' do
      it 'should return follow user' do
        sign_in user: user1

        expect{
          post :create, params: {follow_user_id: user2.id}
        }.to change{Notification.where(user_id: user2.id, from_user_id: user1.id,
                                       notification_type: :friend_requests,
                                       message_type: :friend_request_follow).count
        }.by 1
      end
    end
  end

  describe '#destroy' do
    let(:response_body) {JSON.parse(response.body)}
    let(:user1) {create :user, :student}
    let(:user2) {create :user, :teacher}

    it 'should return unfollow user' do
      sign_in user: user1
      user1.followed<<user2
      expect(user1.followed.count).to be 1

      delete :destroy, params: {id: user2.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq("You successfully unfollow from #{user2.first_name} #{user2.last_name}.")
      expect(user1.followed.count).to be 0
    end

    it 'should return error if user not followed' do
      sign_in user: user1
      delete :destroy, params: {id: user2.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00303')
      expect(response_body['errors'][0]['message']).to eq("You have not been followed to #{user2.first_name} #{user2.last_name} before.")
    end

    it 'should return error if user not found' do
      sign_in user: user1
      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      delete :destroy, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    describe 'create notifications' do
      it 'should return unfollow user' do
        sign_in user: user1
        user1.followed<<user2
        expect(user1.followed.count).to be 1



        expect{
          delete :destroy, params: {id: user2.id}
        }.to change{Notification.where(user_id: user2.id, from_user_id: user1.id,
                                       notification_type: :friend_requests,
                                       message_type: :friend_request_unfollow).count
        }.by 1
      end
    end
  end
end
