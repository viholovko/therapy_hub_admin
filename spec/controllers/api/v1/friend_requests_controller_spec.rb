require 'rails_helper'

RSpec.describe ::Api::V1::FriendRequestsController, type: :controller do
  render_views

  describe '#create' do
    let(:response_body) {JSON.parse(response.body).with_indifferent_access}
    let(:user1) {create :user, :not_upgraded}
    let(:user2) {create :user, :not_upgraded}

    it 'should return invite to friends' do
      sign_in user: user1

      post :create, params: {user_id: user2.id}

      expect(response.status).to be 200
      expect(response_body[:message]).to eq("Invitation is sent to #{user2.first_name} #{user2.last_name}.")
      expect(Friendship.count).to be 2

      friendship1 = Friendship.find_by_user_id user1
      friendship2 = Friendship.find_by_user_id user2

      expect(friendship1.status).to eq('requested')
      expect(friendship2.status).to eq('not_accepted')
    end

    it 'should return error if user not found' do
      sign_in user: user1

      post :create, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq('E00001')
    end

    it 'shouldn\'t invite twice' do
      sign_in user: user1

      post :create, params: {user_id: user2.id}
      post :create, params: {user_id: user2.id}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00200")
      expect(Friendship.count).to be 2
    end

    it 'should render error if there is no session' do
      post :create, params: {user_id: user2.id}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00002")
    end

    it 'should return error if user invite mentor' do
      sign_in user: user1
      mentor = create :user, :mentor

      post :create, params: {user_id: mentor.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      end

    it 'should return error if mentor invite user' do
      mentor = create :user, :mentor
      sign_in user: mentor

      post :create, params: {user_id: user1.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    describe 'create notifications' do
      it 'should return invite to friend' do
        sign_in user: user1

        expect{
          post :create, params: {user_id: user2.id}
        }.to change{Notification.where(user_id: user2.id, from_user_id: user1.id,
                                       notification_type: :friend_requests,
                                       message_type: :friend_request_invite).count
        }.by 1
      end
    end
  end

  describe '#update' do
    let(:response_body) {JSON.parse(response.body).with_indifferent_access}
    let(:user1) {create :user, :not_upgraded}
    let(:user2) {create :user, :not_upgraded}

    it 'should accept invitation' do
      friendship1 = create :friendship, user: user1, friend: user2, status: 'requested'
      friendship2 = create :friendship, user: user2, friend: user1, status: 'not_accepted'
      sign_in user: user2

      put :update, params: {id: user1.id}
      friendship1.reload
      friendship2.reload

      expect(response.status).to be 200
      expect(response_body[:message]).to eq('You accepted invitation')
      expect(friendship1.status).to eq('accepted')
      expect(friendship2.status).to eq('accepted')
    end

    it 'should render error if there is no invitation' do
      sign_in user: user2

      put :update, params: {id: user1.id}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00201")
    end

    it 'should render error if there is no session' do
      put :update, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00002")
    end

    it 'shouldn\'t accept already acepted invite' do
      sign_in user: user2
      friendship1 = create :friendship, user: user1, friend: user2, status: 'accepted'
      friendship2 = create :friendship, user: user2, friend: user1, status: 'accepted'

      put :update, params: {id: user1.id}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00202")
    end

    describe 'create notifications' do
      it 'should return accept invitation' do
        friendship1 = create :friendship, user: user1, friend: user2, status: 'requested'
        friendship2 = create :friendship, user: user2, friend: user1, status: 'not_accepted'
        sign_in user: user2

        expect{
          put :update, params: {id: user1.id}
        }.to change{Notification.where(user_id: user1.id, from_user_id: user2.id,
                                       notification_type: :friend_requests,
                                       message_type: :friend_request_accept).count
        }.by 1
      end
    end
  end

  describe '#destroy' do
    let(:response_body) {JSON.parse(response.body).with_indifferent_access}
    let(:user1) {create :user, :not_upgraded}
    let(:user2) {create :user, :not_upgraded}

    it 'should return invitation is rejected' do
      create :friendship, user: user1, friend: user2, status: 'requested'
      friendship2 = create :friendship, user: user2, friend: user1, status: 'not_accepted'
      sign_in user: user2

      delete :destroy, params: {id: user1.id}

      expect(response.status).to be 200
      expect(Friendship.count).to be 0
    end

    it 'should render error if there is no invitation' do
      sign_in user: user2

      delete :destroy, params: {id: user1.id}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00201")
    end

    it 'should render error if there is no session' do
      delete :destroy, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00002")
    end

    it 'shouldn\'t reject already acepted invite' do
      sign_in user: user2
      friendship1 = create :friendship, user: user1, friend: user2, status: 'accepted'
      friendship2 = create :friendship, user: user2, friend: user1, status: 'accepted'

      delete :destroy, params: {id: user1.id}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq("E00203")
    end
  end

end