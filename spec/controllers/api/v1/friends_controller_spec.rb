require 'rails_helper'

RSpec.describe ::Api::V1::FriendsController, type: :controller do
  render_views

  describe '#destroy' do
    let(:response_body) {JSON.parse(response.body).with_indifferent_access}
    let(:user1) {create :user, :not_upgraded}
    let(:user2) {create :user, :not_upgraded}

    it 'should return friend is removed from friends list' do
      sign_in user: user1

      create :friendship, user: user1, friend: user2, status: 'accepted', accepted_at: Time.now
      create :friendship, user: user2, friend: user1, status: 'accepted', accepted_at: Time.now

      delete :destroy, params: {id: user2.id}

      expect(response.status).to be 200
      expect(response_body[:message]).to eq('User has been removed from your friends')
      expect(Friendship.count).to be 0
    end

    it 'should return error if user does not exist' do
      sign_in user: user1
      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq('E00001')
      end

    it 'should return error if user does not exist in friend list' do
      sign_in user: user1
      delete :destroy, params: {id: user2.id}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq('E00201')
    end

    it 'should return error if user not authorize' do
      delete :destroy, params: {id: 1}

      expect(response.status).to be 400
      expect(response_body[:errors][0][:code]).to eq('E00002')
    end
  end
end
