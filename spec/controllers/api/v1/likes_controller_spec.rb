require 'rails_helper'

RSpec.describe Api::V1::LikesController, type: :controller do
  render_views

  describe '#create' do
    let(:response_body) {JSON.parse(response.body)}
    let(:post1) {create :post, :photo}
    let(:user) {create :user, :student}

    it 'should return like to post' do
      sign_in user: user

      post :create, params: {likeable_type: 'Post', likeable_id: post1.id}
      expect(response.status).to be 200
      expect(response_body['message']).to eq('Liked.')
      expect(post1.likes.count).to be 1
    end

    it 'should return error if likeable type is wrong' do
      sign_in user: user
      post1 = create :post, :link

      post :create, params: {likeable_id: post1.id, likeable_type: 'wrongtype'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01103')
      expect(response_body['errors'][0]['message']).to eq('Likeable type is wrong.')
    end

    it 'should return validation errors' do
      sign_in user: user

      post :create

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01100')
      expect(response_body['errors'][0]['message']).to eq('Likeable id can\'t be blank.')

      expect(response_body['errors'][1]['code']).to eq('E01101')
      expect(response_body['errors'][1]['message']).to eq('Likeable type can\'t be blank.')
    end

    it 'should return errors if user not authorized' do
      post :create, params: {likeable_type: 'Post', likeable_id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if likeable not found' do
      sign_in user: user

      post :create, params: {likeable_id: -1, likeable_type: 'Post'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if double like' do
      like = create :like, :post
      sign_in user: like.user

      post :create, params: {likeable_type: 'Post', likeable_id: like.likeable.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01102')
      expect(response_body['errors'][0]['message']).to eq('Likeable is already exist.')
    end

    describe 'create notifications' do
      it 'should return create notification' do
        sign_in user: user

        expect{
          post :create, params: { likeable_type: 'Post', likeable_id: post1.id }
        }.to change{
          Notification.where(user_id: post1.user_id, from_user_id: user.id,
                             notification_type: :posts,
                             message_type: :post_create_like).count
        }.by 1
      end
    end
  end

  describe '#destroy' do
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}
    let(:like) {create :like, :post}

    it 'should return unlike' do
      sign_in user: like.user
      expect(Like.count).to be 1
      expect(Like.first.id).to be like.id
      delete :destroy, params: {likeable_type: 'Post', likeable_id: like.likeable.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Unliked.')
      expect(Like.count).to be 0
    end

    it 'should return error if user have not been liked before' do
      sign_in user: user
      post1 = create :post, :link

      delete :destroy, params: {likeable_id: post1.id, likeable_type: 'Post'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01104')
      expect(response_body['errors'][0]['message']).to eq("You have not been liked this Post before.")
    end

    it 'should return error if likeable not found' do
      sign_in user: user

      delete :destroy, params: {likeable_id: -1, likeable_type: 'Post'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq("Record not found.")
    end

    it 'should return error if likeable type is wrong' do
      sign_in user: user
      post1 = create :post, :link

      delete :destroy, params: {likeable_id: post1.id, likeable_type: 'wrongtype'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01103')
      expect(response_body['errors'][0]['message']).to eq('Likeable type is wrong.')
    end

    it 'should return errors if user not authorized' do
      delete :destroy, params: {likeable_type: 'Post', likeable_id: 1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end
end
