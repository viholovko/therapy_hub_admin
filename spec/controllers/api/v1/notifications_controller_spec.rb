require 'rails_helper'

RSpec.describe ::Api::V1::NotificationsController, type: :controller do
  render_views

  describe '#index' do
    let(:student) {create :user, :student}
    let(:parent) {create :user, :parent}
    let(:teacher) {create :user, :teacher}
    let(:caseworker) {create :user, :caseworker}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return notifications list' do
      sign_in user: student
      post1 = create :post, :link,
                     user: parent,
                     text: 'My post',
                     tagged_user_ids: [student.id]
      comment1 = create :comment, message: 'Comment to post', commentable: post1, user: teacher, post_id: post1.id
      comment2 = create :comment, message: 'Comment to comment', commentable: comment1, user: student, post_id: post1.id
      comment3 = create :comment, message: 'Comment to comment', commentable: comment2, user: caseworker, post_id: post1.id

      get :index

      expect(response.status).to be 200
      expect(response_body['notifications'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['notifications'][0]['title']).to eq("#{caseworker.first_name} #{caseworker.last_name}")
      expect(response_body['notifications'][0]['body']).to eq("replied to your comment in post My post.")
      expect(response_body['notifications'][1]['title']).to eq("#{parent.first_name} #{parent.last_name}")
      expect(response_body['notifications'][1]['body']).to eq("tagged you in the post My post.")
    end
  end

end
