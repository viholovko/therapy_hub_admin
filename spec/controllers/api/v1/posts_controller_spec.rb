require 'rails_helper'

RSpec.describe ::Api::V1::PostsController, type: :controller do
  render_views

  describe '#create' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :not_upgraded }

    it 'should return create post with url' do
      sign_in user: user

      post :create, params: { text: 'Post text', link: 'http://google.com/derevo', user: user }

      expect(response.status).to be 200
      expect(Post.find(response_body['post']['id'])).to_not be_nil

      link = Post.find(response_body['post']['id']).link
      expect(link).to_not be_nil
      LinkThumbnailerWorker.new.perform(link.id)
      link.reload
      expect(link.url).to eq('http://google.com/derevo')
      expect(link.title).to eq('Google title')
    end

    it 'should return create post with invalid url' do
      sign_in user: user

      post :create, params: { text: 'Post text', link: 'www.google.com/derevo', user: user }

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is created')
      expect(Post.find(response_body['post']['id'])).to_not be_nil

      link = Post.find(response_body['post']['id']).link
      expect(link).to_not be_nil
      LinkThumbnailerWorker.new.perform(link.id)
      link.reload
      expect(link.url).to eq('www.google.com/derevo')
      expect(link.title).to be_nil
    end

    it 'should return create post with attachments' do
      sign_in user: user
      photo_attachment = create :attachment, :photo
      video_attachment = create :attachment, :video

      post :create, params: { text: 'Post text',
                              attachment_ids: [photo_attachment.id, video_attachment.id], user: user }

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is created')

      expect(Attachment.where(id: photo_attachment.id)).to_not be_nil
      expect(Attachment.where(id: photo_attachment.id).last.attachable_id).to be (response_body['post']['id'])
      expect(Attachment.where(id: photo_attachment.id).last.attachable_type).to eq('Post')

      expect(Attachment.where(id: video_attachment.id)).to_not be_nil
      expect(Attachment.where(id: video_attachment.id).last.attachable_id).to be (response_body['post']['id'])
      expect(Attachment.where(id: video_attachment.id).last.attachable_type).to eq('Post')

      expect(response_body['post']['attachments'][0]['url']).to_not be_nil
      expect(response_body['post']['attachments'][0]['url_preview']).to_not be_nil
      expect(response_body['post']['attachments'][1]['url']).to_not be_nil
      expect(response_body['post']['attachments'][1]['url_preview']).to_not be_nil
    end

    it 'should return create post with tagged users' do
      sign_in user: user
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded

      post :create, params: { link: 'http://link.ua', user: user, tagged_user_ids: [user2.id, user3.id] }

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is created')

      expect(response_body['post']['tagged_users_count']).to be 2
    end

    describe 'create notifications' do
      it 'should return create post by mentor' do
        mentor = create :user, :mentor
        mentor.followers<<user
        sign_in user: mentor

        expect{
          post :create, params: { user: mentor, text: 'Derevo' }
        }.to change{
          Notification.where(from_user_id: mentor.id, notification_type: :mentor_posts, message_type: :post_mentor_create_post).count
        }.by 1
      end

      it 'should return create post with tagged users' do
        sign_in user: user
        user2 = create :user, :not_upgraded
        user3 = create :user, :not_upgraded

        expect{
          post :create, params: { link: 'http://link.ua', user: user, tagged_user_ids: [user2.id, user3.id] }
        }.to change{
          Notification.where(from_user_id: user.id, notification_type: :posts, message_type: :post_tagged_users).count
        }.by 2
      end

    end



    it 'should return validation errors' do
      sign_in user: user

      post :create, params: { user: user }

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00500')
      expect(response_body['errors'][0]['message']).to eq('Text can\'t be blank')
    end

    it 'should return error if user not authorized' do
      post :create
      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end

  describe '#update' do
    let(:user) { create :user, :not_upgraded }
    let(:response_body) { JSON.parse(response.body) }

    it 'should return update post with url' do
      sign_in user: user
      post = create :post, :link, user: user

      put :update, params: { id: post.id, link: 'http://microsoft.com/derevo', text: 'newtext' }

      expect(response.status).to be 200
      LinkThumbnailerWorker.new.perform(post.link.id)
      post.reload
      post.link.reload
      expect(post.text).to eq('newtext')
      expect(post.link.url).to eq('http://microsoft.com/derevo')
      expect(post.link.title).to eq('Microsoft title')
    end

    it 'should return update post with invalid link' do
      sign_in user: user
      post = create :post, :link, user: user

      put :update, params: { id: post.id, link: 'microsoft.com/derevo' }

      expect(response.status).to be 200
      LinkThumbnailerWorker.new.perform(post.link.id)
      post.link.reload
      expect(post.link.url).to eq('microsoft.com/derevo')
      expect(post.link.title).to be_nil
    end

    it 'should return update post with attachment' do
      sign_in user: user
      post = create :post, :photo, user: user, text: 'Post text'
      expect(Attachment.where(attachable_id: post.id, attachable_type: 'Post').count).to be 2
      photo1 = create :attachment, :photo
      photo2 = create :attachment, :photo
      photo3 = create :attachment, :photo

      put :update, params: { id: post.id, text: 'newtext',
                             attachment_ids: [photo1.id, photo2.id, photo3.id]
      }

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is updated')
      expect(response_body['post']['text']).to eq('newtext')
      expect(Attachment.where(attachable_id: post.id, attachable_type: 'Post').count).to be 3
      expect(response_body['post']['attachments'].count).to be 3
      expect(response_body['post']['attachments'][0]['id']).to be photo1.id
      expect(response_body['post']['attachments'][1]['id']).to be photo2.id
      expect(response_body['post']['attachments'][2]['id']).to be photo3.id
    end

    it 'should return update post with tagged users' do
      sign_in user: user
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded
      user4 = create :user, :not_upgraded
      user5 = create :user, :not_upgraded
      post = create :post, :photo, user: user, tagged_user_ids: [user2.id, user3.id, user4.id]
      expect(post.tagged_users.count).to be 3

      put :update, params: {id: post.id, tagged_user_ids: [user2.id, user5.id]}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is updated')
      expect(response_body['post']['tagged_users_count']).to be 2
    end


    it 'should return error if user not owner of post' do
      sign_in user: user
      user2 = create :user, :not_upgraded
      post = create :post, :photo, user: user2

      put :update, params: {id: post.id, text: 'newtext'}
      post.reload

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(post.text).not_to eq('newtext')
    end

    it 'should return error if post not found' do
      sign_in user: user
      put :update, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end
  end

  describe '#destroy' do
    let(:user) { create :user, :not_upgraded }
    let(:response_body) { JSON.parse(response.body) }

    it 'should return destroy post' do
      sign_in user: user
      post = create :post, :photo, user: user
      expect(Post.where(id: post.id)).to_not be_nil

      delete :destroy, params: {id: post.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is destroyed')
      expect(Post.where(id: post.id).count).to be 0
    end

    it 'should return error if user not owner post' do
      sign_in user: user
      user2 = create :user, :not_upgraded
      post = create :post, :photo, user: user2
      expect(Post.where(id: post.id)).to_not be_nil

      delete :destroy, params: {id: post.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Post.where(id: post.id)).to_not be_nil
    end

    it 'should return error if post not found' do
      sign_in user: user

      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end
  end

  describe '#index' do
    let(:user) { create :user, :student }
    let(:response_body) { JSON.parse(response.body) }

    it 'should return posts with order desc' do
      post1 = create :post, :photo, tagged_user_ids: []
      post2 = create :post, :video
      post3 = create :post, :audio

      create :like, :post, likeable: post1
      create :like, :post, likeable: post1
      comment1 = create :comment, :post, commentable: post1
      create :comment, :post, commentable: comment1
      create :like, :post, likeable: post2, user: user
      create :comment, :post, commentable: post2
      sign_in user: user

      get :index

      expect(response.status).to be 200
      expect(response_body['posts'].count).to be 3
      expect(response_body['count']).to be 3
      expect(response_body['posts'][0]['id']).to be post3.id
      expect(response_body['posts'][1]['id']).to be post2.id
      expect(response_body['posts'][2]['id']).to be post1.id

      expect(response_body['posts'][0]['attachments'].count).to be 2
      expect(response_body['posts'][0]['attachments'][0]['type']).to eq('audio')
      expect(response_body['posts'][0]['attachments'][0]['url']).to_not be_nil
      expect(response_body['posts'][0]['attachments'][0]['url_preview']).to be_nil
      expect(response_body['posts'][0]['tagged_users_count']).to be 2
      expect(response_body['posts'][0]['comments_count']).to be 0
      expect(response_body['posts'][0]['likes_count']).to be 0
      expect(response_body['posts'][0]['is_liked']).to be false

      expect(response_body['posts'][1]['attachments'].count).to be 2
      expect(response_body['posts'][1]['attachments'][0]['type']).to eq('video')
      expect(response_body['posts'][1]['attachments'][0]['url']).to_not be_nil
      expect(response_body['posts'][1]['attachments'][0]['url_preview']).to_not be_nil
      expect(response_body['posts'][1]['tagged_users_count']).to be 2
      expect(response_body['posts'][1]['comments_count']).to be 1
      expect(response_body['posts'][1]['likes_count']).to be 1
      expect(response_body['posts'][1]['is_liked']).to be true

      expect(response_body['posts'][2]['attachments'].count).to be 2
      expect(response_body['posts'][2]['attachments'][0]['type']).to eq('photo')
      expect(response_body['posts'][2]['attachments'][0]['url']).to_not be_nil
      expect(response_body['posts'][2]['attachments'][0]['url_preview']).to_not be_nil
      expect(response_body['posts'][2]['tagged_users_count']).to be 0
      expect(response_body['posts'][2]['comments_count']).to be 1
      expect(response_body['posts'][2]['likes_count']).to be 2
      expect(response_body['posts'][2]['is_liked']).to be false
    end

    it 'should return empty posts array' do
      sign_in user: user

      get :index

      expect(response.status).to be 200
      expect(response_body['posts'].count).to be 0
      expect(response_body['posts']).to eq([])
    end

    it 'should return posts list without blocked user posts' do
      sign_in user: user
      user2 = create :user, :teacher
      post1 = create :post, :photo, user: user2
      post2 = create :post, :video, user: user2
      post3 = create :post, :audio
      post4 = create :post, :link
      user.blocked_users<<user2
      expect(user.blocked_users.include?(user2)).to be true

      get :index

      expect(response.status).to be 200
      expect(response_body['posts'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['posts'][0]['id']).to be post4.id
      expect(response_body['posts'][1]['id']).to be post3.id
    end

    it 'should return posts list without reports posts' do
      sign_in user: user
      post1 = create :post, :photo
      post2 = create :post, :video
      post3 = create :post, :link
      post4 = create :post, :audio
      post1.reports.create(user: user, report_type: :spam)
      post3.reports.create(user: user, report_type: :spam)

      get :index

      expect(response.status).to be 200
      expect(response_body['posts'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['posts'][0]['id']).to be post4.id
      expect(response_body['posts'][1]['id']).to be post2.id
    end

    it 'should return classroom\'s posts' do
      post1 = create :post, :classroom
      post2 = create :post, :photo, classroom_id: post1.classroom_id
      post3 = create :post, :classroom
      post4 = create :post, :link
      sign_in user: user
      expect(Post.where(classroom_id: post1.classroom_id).count).to be 2

      get :index, params: {classroom_id: post1.classroom_id}

      expect(response.status).to be 200
      expect(response_body['posts'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['posts'][0]['id']).to be post2.id
      expect(response_body['posts'][1]['id']).to be post1.id
    end

    it 'should return user\'s posts' do
      post1 = create :post, :text
      post2 = create :post, :photo, user_id: post1.user_id
      post3 = create :post, :text
      sign_in user: user
      expect(Post.where(user_id: post1.user_id).count).to be 2

      get :index, params: {user_id: post1.user_id}

      expect(response.status).to be 200
      expect(response_body['posts'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['posts'][0]['id']).to be post2.id
      expect(response_body['posts'][1]['id']).to be post1.id
    end

    it 'should return posts with dates filter' do
      post1 = create :post, :text, created_at: Time.now.utc - 5.hours
      post2 = create :post, :photo, created_at: Time.now.utc - 3.hours
      post3 = create :post, :text, created_at: Time.now.utc - 1.hour
      sign_in user: user

      get :index, params: {from_date: Time.now.utc - 4.hours, to_date: Time.now.utc}

      expect(response.status).to be 200
      expect(response_body['posts'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['posts'][0]['id']).to be post3.id
      expect(response_body['posts'][1]['id']).to be post2.id
    end
  end

  describe '#show' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :not_upgraded }

    it 'should return photo post' do
      post = create :post, :photo
      
      sign_in user: user

      get :show, params: {id: post.id}

      expect(response.status).to be 200
      expect(response_body['post']['id']).to be post.id
      expect(response_body['post']['attachments'].count).to be 2
      expect(response_body['post']['attachments'][0]['type']).to eq('photo')
      expect(response_body['post']['attachments'][0]['url']).to_not be_nil
      expect(response_body['post']['attachments'][0]['url_preview']).to_not be_nil
      expect(response_body['post']['tagged_users_count']).to be 2
      end

    it 'should return post with correct order of attachments' do
      sign_in user: user
      photo_attachment = create :attachment, :photo
      video_attachment = create :attachment, :video

      post :create, params: { text: 'Post text',
                              attachment_ids: [photo_attachment.id, video_attachment.id], user: user }

      post_id = JSON.parse(response.body)['post']['id']
      get :show, params: {id: post_id}

      response_content = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_content['post']['id']).to be post_id
      expect(response_content['post']['attachments'][0]['id']).to eq(photo_attachment.id)
      expect(response_content['post']['attachments'][1]['id']).to eq(video_attachment.id)
    end

    it 'should return video post' do
      post = create :post, :video
      sign_in user: user

      get :show, params: {id: post.id}

      expect(response.status).to be 200
      expect(response_body['post']['id']).to be post.id
      expect(response_body['post']['attachments'].count).to be 2
      expect(response_body['post']['attachments'][0]['type']).to eq('video')
      expect(response_body['post']['attachments'][0]['url']).to_not be_nil
      expect(response_body['post']['attachments'][0]['url_preview']).to_not be_nil
      expect(response_body['post']['tagged_users_count']).to be 2
    end

    it 'should return post with likes and comments without my like' do
      post = create :post, :photo
      user1 = create :user, :student
      user2 = create :user, :student
      user3 = create :user, :student
      user4 = create :user, :student
      create :like, :post, likeable: post, user: user1
      create :like, :post, likeable: post, user: user2
      create :like, :post, likeable: post, user: user3
      create :like, :post, likeable: post, user: user4
      create :comment, :post, commentable: post

      sign_in user: user

      get :show, params: {id: post.id}

      expect(response.status).to be 200
      expect(response_body['post']['id']).to be post.id
      expect(response_body['post']['comments_count']).to be 1
      expect(response_body['post']['likes_count']).to be 4
      expect(response_body['post']['is_liked']).to be false
      expect(response_body['post']['who_liked'].count).to be 3
      expect(response_body['post']['who_liked'][0]['id']).to be user4.id
      expect(response_body['post']['who_liked'][1]['id']).to be user3.id
      expect(response_body['post']['who_liked'][2]['id']).to be user2.id
    end

    it 'should return post with my like' do
      post = create :post, :photo
      user1 = create :user, :student
      create :like, :post, likeable: post, user: user1
      create :like, :post, likeable: post, user: user

      sign_in user: user

      get :show, params: {id: post.id}

      expect(response.status).to be 200
      expect(response_body['post']['id']).to be post.id
      expect(response_body['post']['likes_count']).to be 2
      expect(response_body['post']['is_liked']).to be true
      expect(response_body['post']['who_liked'].count).to be 2
      expect(response_body['post']['who_liked'][0]['id']).to be user.id
      expect(response_body['post']['who_liked'][1]['id']).to be user1.id
    end

    it 'should return error if post not found' do
      sign_in user: user

      get :show, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      post = create :post, :photo

      get :show, params: {id: post.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end
end
