require 'rails_helper'

RSpec.describe Api::V1::ReadMessagesController, type: :controller do
  render_views
  require 'rails_helper'

  describe '#mark_read_all' do
    it 'should delete all messages' do
      user1 = create(:user, :not_upgraded)
      user2 = create(:user, :not_upgraded)
      sign_in user: user1

      chat = create :chat, :direct, owner: user1, users: [user1, user2]
      create :chat_message, :message, user: user1, chat: chat, message: 'Message1'
      create :chat_message, :message, user: user1, chat: chat, message: 'Message2'

      delete :mark_read_all, params: {id: chat.id, user_id: chat.users.first.id}

      # All messages marked as read

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['message']).to eq('All messages marked as read')
    end

  end

end
