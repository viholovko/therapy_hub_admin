require 'rails_helper'

RSpec.describe ::Api::V1::ReportsController, type: :controller do
  render_views

  describe '#create' do
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}
    let(:post1) {create :post, :link}

    it 'should return reporting post' do
      sign_in user: user

      post :create, params: {post_id: post1.id, report_type: 'spam'}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is reported')
      expect(post1.reports.count).to be 1
    end

    it 'should return validation errors' do
      sign_in user: user

      post :create, params: {}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01201')
      expect(response_body['errors'][0]['message']).to eq('Post can\'t be blank.')

      expect(response_body['errors'][1]['code']).to eq('E01202')
      expect(response_body['errors'][1]['message']).to eq('Report type can\'t be blank.')

      expect(response_body['errors'][2]['code']).to eq('E01204')
      expect(response_body['errors'][2]['message']).to eq('Report type is not valid.')

      expect(post1.reports.count).to be 0
    end

    it 'should return error if double report' do
      sign_in user: user
      report = create :report, post: post1, user: user
      expect(post1.reports.count).to be 1
      post :create, params: {post_id: post1.id, user_id: user.id, report_type: 'spam'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01203')
      expect(response_body['errors'][0]['message']).to eq('Report is already exist.')
      expect(post1.reports.count).to be 1
    end

    it 'should return errors if user not authorized' do
      post :create, params: {}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if post not found' do
      sign_in user: user

      post :create, params: {post_id: -1, report_type: 'spam'}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01201')
      expect(response_body['errors'][0]['message']).to eq('Post can\'t be blank.')
    end
  end

  describe '#destroy' do
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}
    let(:post1) {create :post, :link}

    it 'should return unreport' do
      sign_in user: user
      report = create :report, post: post1, user: user
      expect(post1.reports.count).to be 1

      delete :destroy, params: {post_id: post1.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Post is unreported.')
      expect(post1.reports.count).to be 0
    end

    it 'should return error if user have not been report before' do
      sign_in user: user

      delete :destroy, params: {post_id: post1.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E01205')
      expect(response_body['errors'][0]['message']).to eq("You have not been reported this Post before.")
    end

    it 'should return error if Post not found' do
      sign_in user: user

      delete :destroy, params: {post_id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq("Record not found.")
    end

    it 'should return errors if user not authorized' do
      delete :destroy, params: {}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end

end
