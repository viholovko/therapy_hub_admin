require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  render_views
  describe '#create' do
    it 'should create session' do
      country = create :country
      phone_number = '123456789'
      code_request = create :code_request, country_id: country.id, phone_number: phone_number
      user = create :user, :not_upgraded, country_id: country.id, phone_number: phone_number

      post :create, params: {country_id: country.id, phone_number: phone_number, code: code_request.code}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(response_body['session_token']).to_not be_nil
      expect(response_body['user']['id']).to eq(user.id)

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(0)
    end

    it 'should not create session if there is no request code' do
      country = create :country
      phone_number = '123456789'

      user = create :user, :not_upgraded, country_id: country.id, phone_number: phone_number

      post :create, params: {country_id: country.id, phone_number: phone_number, code: 'wrong_code'}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq("E00101")
    end
  end

  describe '#destroy' do
    it 'should destroy session' do
      user = create :user, :not_upgraded
      session = sign_in user: user

      delete :destroy, params: {session_token: session.token}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(user.sessions.count).to eq(0)
    end

    it 'should render unauthorized if there is no session' do
      delete :destroy, params: {session_token: ''}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq("E00002")
    end
  end
end
