require 'rails_helper'

RSpec.describe ::Api::V1::TasksController, type: :controller do
  render_views

  describe '#create' do
    let(:classroom) {create :classroom, :with_members}
    let(:teacher) {create :user, :teacher}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return task' do
      sign_in user: classroom.teacher

      post :create, params: {title: 'Title', description: 'Description',
                             classroom_id: classroom.id, due_date: Time.now + 5.hours
      }

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Task is created')
    end

    it 'should return validation errors' do
      sign_in user: classroom.teacher

      post :create, params: {}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00800')
      expect(response_body['errors'][0]['message']).to eq('Title can\'t be blank.')

      expect(response_body['errors'][1]['code']).to eq('E00801')
      expect(response_body['errors'][1]['message']).to eq('Description can\'t be blank.')

      expect(response_body['errors'][2]['code']).to eq('E00802')
      expect(response_body['errors'][2]['message']).to eq('Due date can\'t be blank.')

      expect(response_body['errors'][3]['code']).to eq('E00803')
      expect(response_body['errors'][3]['message']).to eq('Classroom can\'t be blank.')
    end

    it 'should return error if user not teacher or caseworker' do
      student = create :user, :student
      sign_in user: student

      post :create, params: {title: 'Title', description: 'Description',
                             classroom_id: classroom.id, due_date: Time.now + 5.hours
      }

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    it 'should return error if user not authorized' do

      post :create, params: {title: 'Title', description: 'Description',
                             classroom_id: classroom.id, due_date: Time.now + 5.hours
      }

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    describe 'create notifications' do
      it 'should return task' do
        classroom = create :classroom, :with_members, teacher_accepted: true, caseworker_accepted: true, student_accepted: true, parent_accepted: false
        sign_in user: classroom.teacher

        expect{
          post :create, params: {title: 'Title', description: 'Description',
                                 classroom_id: classroom.id, due_date: Time.now + 5.hours
          }
        }.to change{
          Notification.where(from_user_id: classroom.teacher.id,
                             notification_type: :classroom,
                             message_type: :classroom_create_task).count
        }.by 2
      end
    end
  end

  describe '#destroy' do
    let(:teacher) {create :user, :teacher}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return delete task' do
      classroom = create :classroom, :with_members, teacher_accepted: true, caseworker_accepted: true
      task = create :task, classroom: classroom, owner: classroom.teacher
      sign_in user: classroom.caseworker
      expect(task.deleted).to be false

      delete :destroy, params: {id: task.id}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Task marked as deleted')
      task.reload
      expect(task.deleted).to be true
    end

    it 'should return error if destroyer not teacher or caseworker' do
      classroom = create :classroom, :with_members, parent_accepted: true
      sign_in user: classroom.parent
      task = create :task, classroom: classroom, owner: classroom.teacher

      delete :destroy, params: {id: task.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Task.count).to_not be 0
    end

    it 'should return error if destroyer teacher but not in the same classroom' do
      classroom = create :classroom, :with_members
      sign_in user: teacher
      task = create :task, classroom: classroom, owner: classroom.teacher

      delete :destroy, params: {id: task.id}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Task.count).to_not be 0
    end

    it 'should return error if user not authorized' do
      task = create :task
      delete :destroy, params: {id: task.id}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if task not found' do
      sign_in user: teacher

      delete :destroy, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end
  end

  describe '#destroy_selected' do
    let(:teacher) {create :user, :teacher}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return delete tasks' do
      classroom = create :classroom, :with_members, caseworker_accepted: true
      sign_in user: classroom.caseworker
      task1 = create :task, owner: classroom.teacher, classroom: classroom
      task2 = create :task, owner: classroom.teacher, classroom: classroom
      task3 = create :task, owner: classroom.teacher, classroom: classroom

      delete :destroy_selected, params: {task_ids: [task1.id, task2.id]}

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Tasks have been deleted')
      expect(Task.count).to be 1
      expect(Task.find_by_id(task3.id)).to_not be_nil
    end

    it 'should return error if destroyer not teacher' do
      classroom = create :classroom, :with_members, student_accepted: true
      sign_in user: classroom.student
      task1 = create :task, classroom: classroom, owner: classroom.teacher
      task2 = create :task, classroom: classroom, owner: classroom.caseworker

      delete :destroy_selected, params: {task_ids: [task1.id, task2.id]}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Task.count).to_not be 0
    end

    it 'should return error if destroyer teacher but not in the same classroom' do
      sign_in user: teacher
      task1 = create :task
      task2 = create :task

      delete :destroy_selected, params: {task_ids: [task1.id, task2.id]}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      expect(Task.count).to_not be 0
    end

    it 'should return error if user not authorized' do
      task1 = create :task
      task2 = create :task
      delete :destroy_selected, params: {task_ids: [task1.id, task2.id]}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end

  describe '#update' do
    let(:teacher) {create :user, :teacher}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return update task' do
      classroom = create :classroom, :with_members, teacher_accepted: true, caseworker_accepted: true
      sign_in user: classroom.caseworker
      task = create :task, owner: classroom.teacher, classroom: classroom

      put :update, params: {id: task.id, title: 'newtitle', description: 'newdescription',
                            due_date: Time.new(2000) + 1.day}
      task.reload
      expect(response.status).to be 200
      expect(response_body['message']).to eq('Task is updated')
      expect(task.title).to eq('newtitle')
      expect(task.description).to eq('newdescription')
      expect(task.due_date).to eq(Time.new(2000) + 1.day)
    end

    it 'should return task marked as complete' do
      task = create :task
      student = create :user, :student
      sign_in user: student

      put :mark_complete, params: {id: task.id, completed: true}
      task.reload

      expect(response.status).to be 200
      expect(response_body['message']).to eq('Task is marked as complete')
      expect(task.completed).to be true
    end

    it 'should return error if user who marked as complete is not a student or parent' do
      sign_in user: teacher
      task = create :task, owner: teacher

      put :mark_complete, params: {id: task.id, completed: true}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end

    it 'should return error if task not found' do
      sign_in user: teacher

      put :update, params: {id: -1}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      task = create :task, owner: teacher

      put :update, params: {id: task.id, title: 'newtitle', description: 'newdescription',
                            due_date: Time.new(2000) + 1.day}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return validation errors' do
      classroom = create :classroom, :with_members, caseworker_accepted: true
      task = create :task, classroom: classroom, owner: classroom.teacher
      sign_in user: classroom.caseworker

      put :update, params: {id: task.id, title: '', description: '', due_date: ''}

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00800')
      expect(response_body['errors'][0]['message']).to eq('Title can\'t be blank.')
      expect(response_body['errors'][1]['code']).to eq('E00801')
      expect(response_body['errors'][1]['message']).to eq('Description can\'t be blank.')
      expect(response_body['errors'][2]['code']).to eq('E00802')
      expect(response_body['errors'][2]['message']).to eq('Due date can\'t be blank.')

    end

    it 'should return error if updater not teacher' do
      classroom = create :classroom, :with_members, student_accepted: true
      sign_in user: classroom.student
      task = create :task, classroom: classroom, owner: classroom.teacher

      put :update, params: {id: task.id, title: 'newtitle', description: 'newdescription', due_date: Time.now + 1.month}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      task.reload
      expect(task.title).to_not eq('newtitle')
      expect(task.description).to_not eq('newdescription')
      expect(task.due_date).to_not eq(Time.now + 1.month)
    end

    it 'should return error if updater teacher but not in the same classroom' do
      sign_in user: teacher
      task = create :task

      put :update, params: {id: task.id, title: 'newtitle', description: 'newdescription', due_date: Time.now + 1.month}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      task.reload
      expect(task.title).to_not eq('newtitle')
      expect(task.description).to_not eq('newdescription')
      expect(task.due_date).to_not eq(Time.now + 1.month)
    end

    describe 'create notifications' do
      it 'should return task marked as complete' do
        classroom = create :classroom, :with_members, teacher_accepted: true, caseworker_accepted: true, parent_accepted: false
        task = create :task, classroom: classroom
        student = create :user, :student
        sign_in user: student
        expect{
          put :mark_complete, params: {id: task.id, completed: true}
        }.to change{
          Notification.where(from_user_id: student.id,
                             notification_type: :classroom,
                             message_type: :classroom_complete_task).count
        }.by 2
      end
    end
  end

  describe '#index' do
    let(:user) {create :user, :student}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return tasks list' do
      classroom = create :classroom, student: user
      sign_in user: user
      task1 = create :task, classroom: classroom, due_date: Time.now.utc + 1.day
      task2 = create :task, classroom: classroom, due_date: Time.now.utc + 3.day
      task3 = create :task, classroom: classroom, due_date: Time.now.utc + 2.day
      task4 = create :task, classroom: classroom, due_date: Time.now.utc + 4.day
      task5 = create :task, classroom: classroom, due_date: Time.now.utc + 5.day

      get :index, params: {classroom_id: classroom.id,
                           from_date: (Time.now.utc + 1.day + 2.hours),
                           to_date: (Time.now.utc + 4.days + 2.hours)}

      expect(response.status).to be 200
      expect(response_body['tasks'].count).to be 3
      expect(response_body['count']).to be 3
      expect(response_body['tasks'][0]['id']).to be task3.id
      expect(response_body['tasks'][1]['id']).to be task2.id
      expect(response_body['tasks'][2]['id']).to be task4.id
    end

    it 'should return empty list' do
      sign_in user: user
      classroom = create :classroom, student: user

      get :index, params: {classroom_id: classroom.id}

      expect(response.status).to be 200
      expect(response_body['tasks']).to eq([])
    end

    it 'should return error if user not authorized' do
      get :index

      expect(response.status).to be 400
      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end
  end
end
