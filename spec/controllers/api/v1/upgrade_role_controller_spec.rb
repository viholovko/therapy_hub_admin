require 'rails_helper'

RSpec.describe Api::V1::UpgradeRoleController, type: :controller do
  render_views
  require 'rails_helper'

  describe '#create' do
    it 'should upgrade user to teacher' do
      user = create :user, :not_upgraded
      sign_in user: user

      expect(user.role.name).to eq('not_upgraded')
      expect(user.role_verified).to eq(true)

      post :create, params: {
          role: 'teacher'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)


      user.reload
      expect(user.role.name).to eq('teacher')
      expect(user.role_verified).to eq(false)
    end

    it 'should upgrade user to caseworker' do
      user = create :user, :not_upgraded
      sign_in user: user

      expect(user.role.name).to eq('not_upgraded')
      expect(user.role_verified).to eq(true)

      post :create, params: {
          role: 'caseworker'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)

      user.reload
      expect(user.role.name).to eq('caseworker')
      expect(user.role_verified).to eq(false)
    end

    it 'should require purchase when upgrade to parent' do
      user = create :user, :not_upgraded
      sign_in user: user

      expect(user.role.name).to eq('not_upgraded')
      expect(user.role_verified).to eq(true)

      post :create, params: {
          role: 'parent'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00401')

      user.reload
      expect(user.role.name).to eq('not_upgraded')
      expect(user.role_verified).to eq(true)
    end

    it 'should require purchase when upgrade to student' do
      user = create :user, :not_upgraded
      sign_in user: user

      expect(user.role.name).to eq('not_upgraded')
      expect(user.role_verified).to eq(true)

      post :create, params: {
          role: 'student'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00401')

      user.reload
      expect(user.role.name).to eq('not_upgraded')
      expect(user.role_verified).to eq(true)
    end

    describe 'ios' do
      it 'should upgrade user to parent via ios in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'parent',
            ios_token: 'test_success_ios_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(200)

        user.reload
        expect(user.role.name).to eq('parent')
        expect(user.role_verified).to eq(true)
      end

      xit 'should fail when trying to upgrade user to parent via ios in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'parent',
            ios_token: 'test_bad_ios_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(400)
        expect(response_body['errors'][0]['code']).to eq('E00402')

        user.reload
        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)
      end

      it 'should upgrade user to student via ios in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'student',
            ios_token: 'test_success_ios_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(200)

        user.reload
        expect(user.role.name).to eq('student')
        expect(user.role_verified).to eq(true)
      end

      xit 'should fail when trying to upgrade user to student via ios in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'student',
            ios_token: 'test_bad_ios_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(400)
        expect(response_body['errors'][0]['code']).to eq('E00402')

        user.reload
        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)
      end
    end

    describe 'android' do
      it 'should upgrade user to parent via android in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'parent',
            android_token: 'test_success_android_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(200)

        user.reload
        expect(user.role.name).to eq('parent')
        expect(user.role_verified).to eq(true)
      end

      xit 'should fail when trying to upgrade user to parent via android in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'parent',
            android_token: 'test_bad_android_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(400)
        expect(response_body['errors'][0]['code']).to eq('E00403')

        user.reload
        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)
      end

      it 'should upgrade user to student via android in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'student',
            android_token: 'test_success_android_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(200)

        user.reload
        expect(user.role.name).to eq('student')
        expect(user.role_verified).to eq(true)
      end

      xit 'should fail when trying to upgrade user to student via android in app purchase' do
        user = create :user, :not_upgraded
        sign_in user: user

        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)

        post :create, params: {
            role: 'student',
            android_token: 'test_bad_android_purchase_token'
        }

        response_body = JSON.parse(response.body)

        expect(response.status).to be(400)
        expect(response_body['errors'][0]['code']).to eq('E00403')

        user.reload
        expect(user.role.name).to eq('not_upgraded')
        expect(user.role_verified).to eq(true)
      end
    end

    it 'should respond with role not found error' do
      user = create :user, :not_upgraded
      sign_in user: user

      post :create, params: {
          role: 'not existing role'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00400')

      user.reload
      expect(user.role.name).to eq('not_upgraded')
    end

    it 'should render unauthorized' do
      post :create, params: {
          role: 'parent'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00002')
    end

    describe 'change role_verified' do
      it 'should return role_verified is true if user is already in classroom with role for which he changed' do
        user = create :user, :not_upgraded
        classroom1 = create :classroom, :with_members, teacher: user, teacher_accepted: true
        classroom2 = create :classroom, :with_members, parent: user, parent_accepted: true
        classroom3 = create :classroom, :with_members, student: user
        sign_in user: user

        post :create, params: {role: 'teacher'}
        user.reload
        classroom1.reload
        classroom2.reload
        classroom3.reload
        expect(response.status).to be 200
        expect(user.role.name).to eq('teacher')
        expect(user.role_verified).to be true
        expect(classroom1.teacher_id).to be user.id
        expect(classroom2.parent_id).to be_nil
        expect(classroom2.parent_accepted).to be false
        expect(classroom3.student_id).to be_nil
        expect(classroom3.student_accepted).to be false
      end

      it 'should return role_verified is true if user receives an invitation to role he has changed' do
        user = create :user, :not_upgraded
        sign_in user: user
        post :create, params: {role: 'teacher'}
        expect(response.status).to be 200
        user.reload

        expect(user.role.name).to eq('teacher')
        expect(user.role_verified).to be false

        classroom = create :classroom, :with_members, teacher: user
        classroom.update_attribute :teacher_accepted, true
        classroom.save
        user.reload

        expect(user.role_verified).to be true
      end
    end
  end
end
