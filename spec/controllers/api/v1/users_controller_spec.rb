require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::UsersController, type: :controller do
  render_views
  require 'rails_helper'

  describe '#create' do
    it 'should create user' do
      country = create :country
      phone_number = '123456789'
      code_request = create :code_request, country_id: country.id, phone_number: phone_number

      post :create, params: {
          country_id: country.id,
          phone_number: phone_number,
          code: code_request.code,
          last_name: 'user',
          first_name: 'user'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(response_body['session_token']).to_not be_nil
      expect(response_body['user']['first_name']).to eq('user')
      expect(response_body['user']['last_name']).to eq('user')

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(0)
    end

    it 'should render error if code not found' do
      country = create :country
      phone_number = '12345'

      post :create, params: {
          country_id: country.id,
          phone_number: phone_number,
          code: 'code',
          last_name: '',
          first_name: ''
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00102')
    end

    it 'should render validation errors' do
      country = create :country
      phone_number = '123456789'
      code_request = create :code_request, country_id: country.id, phone_number: phone_number

      post :create, params: {
          country_id: country.id,
          phone_number: phone_number,
          code: code_request.code,
          last_name: '',
          first_name: ''
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00103')
      expect(response_body['errors'][1]['code']).to eq('E00110')
      expect(response_body['errors'][2]['code']).to eq('E00104')
      expect(response_body['errors'][3]['code']).to eq('E00111')

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)
    end

    it 'should render validation error for already registered phone number' do
      country = create :country
      phone_number = '123456789'
      code_request = create :code_request, country_id: country.id, phone_number: phone_number
      user = create :user, :not_upgraded, country_id: country.id, phone_number: phone_number

      post :create, params: {
          country_id: country.id,
          phone_number: phone_number,
          code: code_request.code,
          last_name: 'test',
          first_name: 'test'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00106')

      expect(CodeRequest.where(country_id: country.id, phone_number: phone_number).count).to eq(1)
    end
  end

  describe '#update' do
    it 'should create user' do
      country = create :country
      user = create :user, :not_upgraded, country_id: country.id, phone_number: '123456789'
      sign_in user: user

      put :update, params: {
          last_name: 'userupdated',
          first_name: 'userupdated',
          about_me: 'test updated'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(response_body['user']['first_name']).to eq('userupdated')
      expect(response_body['user']['last_name']).to eq('userupdated')
      expect(response_body['user']['about_me']).to eq('test updated')
    end

    it 'should render validation errors' do
      country = create :country
      user = create :user, :not_upgraded, country_id: country.id, phone_number: '123456789'
      sign_in user: user

      put :update, params: {
          last_name: '',
          first_name: ''
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00103')
      expect(response_body['errors'][1]['code']).to eq('E00110')
      expect(response_body['errors'][2]['code']).to eq('E00104')
      expect(response_body['errors'][3]['code']).to eq('E00111')
    end
  end

  describe '#index' do
    let(:response_body) {JSON.parse(response.body)}
    let(:user1) {create :user, :not_upgraded}
    let(:user2) {create :user, :not_upgraded}
    let(:user3) {create :user, :not_upgraded}
    let(:user4) {create :user, :not_upgraded}

    it 'should return users list ordered by distance' do
      user =  create :user, :not_upgraded
      sign_in user: user

      create :user, :not_upgraded, latitude: 43.1, longitude: 24.1
      create :user, :not_upgraded, latitude: 43.11, longitude: 24.11
      create :user, :not_upgraded, latitude: 24, longitude: 28

      get :index, params: {sort_column: 'distance', sort_type: 'asc', latitude: 43, longitude: 24}
      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['count']).to be 3
      expect(response_body['users'][0]['distance'] < response_body['users'][1]['distance']).to be(true)
      expect(response_body['users'][0]['avatar']).to_not be_nil
      expect(response_body['users'][0]['avatar_preview']).to_not be_nil
    end

    it 'should return friends list' do

      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded
      user4 = create :user, :not_upgraded

      create :friendship, user: user1, friend: user2, status: 'requested'
      create :friendship, user: user2, friend: user1, status: 'not_accepted'
      create :friendship, user: user1, friend: user3, status: 'not_accepted'
      create :friendship, user: user3, friend: user1, status: 'requested'
      create :friendship, user: user4, friend: user1, status: 'accepted', accepted_at: Time.now
      create :friendship, user: user1, friend: user4, status: 'accepted', accepted_at: Time.now

      sign_in user: user1

      get :index

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 3

      expect(response_body['users'][0]['id']).to be user4.id
      expect(response_body['users'][1]['id']).to be user3.id
      expect(response_body['users'][2]['id']).to be user2.id

      expect(response_body['users'][0]['friendship_status']).to eq('accepted')
      expect(response_body['users'][1]['friendship_status']).to eq('not_accepted')
      expect(response_body['users'][2]['friendship_status']).to eq('requested')

      expect(response_body['users'][0]['friendship_accepted_at']).to_not be_nil
      expect(response_body['users'][1]['friendship_accepted_at']).to be_nil
      expect(response_body['users'][2]['friendship_accepted_at']).to be_nil
    end

    it 'should return tagged users list' do
      sign_in user: user1
      post1 = create :post, :link, tagged_user_ids: [user2.id, user3.id]
      expect(post1.tagged_users.count).to be 2

      get :index, params: {tagged_users: true, post_id: post1.id}

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['users'][0]['id']).to be user2.id
      expect(response_body['users'][1]['id']).to be user3.id
    end

    it 'should recommendations list' do

      user = create :user, :not_upgraded
      user1 = create :user, :not_upgraded
      user2 = create :user, :not_upgraded
      user3 = create :user, :not_upgraded
      user4 = create :user, :not_upgraded

      create :friendship, user: user, friend: user1, status: 'accepted'
      create :friendship, user: user1, friend: user, status: 'accepted'

      create :friendship, user: user, friend: user2, status: 'accepted'
      create :friendship, user: user2, friend: user, status: 'accepted'


      create :friendship, user: user1, friend: user3, status: 'accepted'
      create :friendship, user: user3, friend: user1, status: 'accepted'

      create :friendship, user: user2, friend: user3, status: 'accepted'
      create :friendship, user: user3, friend: user2, status: 'accepted'

      create :friendship, user: user2, friend: user4, status: 'accepted'
      create :friendship, user: user4, friend: user2, status: 'accepted'

      create :friendship, user: user1, friend: user4, status: 'accepted'
      create :friendship, user: user4, friend: user1, status: 'accepted'



      sign_in user: user
      get :index, params: {recommendations: true}

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 2
      expect(response_body['count']).to be 2

      expect(response_body['users'][0]['id']).to be user4.id
      expect(response_body['users'][1]['id']).to be user3.id

      expect(response_body['users'][0]['friendship_status']).to eq('not_connected')
      expect(response_body['users'][1]['friendship_status']).to eq('not_connected')
    end

    it 'should return liked list' do
      sign_in user: user1
      post1 = create :post, :link
      like1 = create :like, likeable: post1, user: user1
      like2 = create :like, likeable: post1, user: user2
      like3 = create :like, likeable: post1, user: user3
      like4 = create :like, :post, user: user4
      expect(Like.count).to be 4

      get :index, params: {likes: true, likeable_id: post1.id, likeable_type: 'Post'}

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['users'][0]['id']).to be user2.id
      expect(response_body['users'][1]['id']).to be user3.id
    end

    it 'should return my followers list' do
      sign_in user: user1
      user2.followed<<user1
      user3.followed<<user1
      user1.followed<<user4
      expect(user1.followed.count).to be 1
      expect(user1.followers.count).to be 2

      get :index, params: {followship_status: ['followed']}

      expect(response.status).to be 200
      expect(response_body['users'][0]['id']).to eq(user3.id)
      expect(response_body['users'][1]['id']).to eq(user2.id)
      expect(response_body['count']).to be 2
    end

    it 'should return my following list' do
      sign_in user: user1
      user1.followed<<user2
      user1.followed<<user3
      user4.followed<<user1
      expect(user1.followed.count).to be 2
      expect(user1.followers.count).to be 1
      expect(user1.followed.ids).to eq [user2.id, user3.id]

      get :index, params: {followship_status: ['followers']}

      expect(response.status).to be 200
      expect(response_body['users'][0]['id']).to eq(user3.id)
      expect(response_body['users'][1]['id']).to eq(user2.id)
      expect(response_body['count']).to be 2
      end

    it 'should return my not connected users list' do
      sign_in user: user1
      user5 = create :user, :not_upgraded
      user1.followed<<user2
      user5.followed<<user1
      user3.followed<<user4
      user4.followed<<user3
      expect(user1.followed.count).to be 1
      expect(user1.followers.count).to be 1

      get :index, params: {followship_status: ['not_connected']}

      expect(response.status).to be 200
      expect(response_body['users'][0]['id']).to eq(user4.id)
      expect(response_body['users'][1]['id']).to eq(user3.id)
      expect(response_body['count']).to be 2
    end

    it 'should return other user followers list' do
      sign_in user: user1
      user1.followed<<user2
      user3.followed<<user2
      user2.followed<<user4
      expect(user2.followed.count).to be 1
      expect(user2.followers.count).to be 2
      expect(user2.followers.ids).to eq [user1.id, user3.id]

      get :index, params: {followship_status: ['followed'], followship_user_id: user2.id}

      expect(response.status).to be 200
      expect(response_body['users'][0]['id']).to be user3.id
      expect(response_body['users'][1]['id']).to be user1.id
      expect(response_body['count']).to be 2
    end

    it 'should return other user following list' do
      sign_in user: user1
      user2.followed<<user1
      user2.followed<<user3
      user4.followed<<user2
      expect(user2.followed.count).to be 2
      expect(user2.followers.count).to be 1
      expect(user2.followed.ids).to eq [user1.id, user3.id]

      get :index, params: {followship_status: ['followers'], followship_user_id: user2.id}

      expect(response.status).to be 200
      expect(response_body['users'][0]['id']).to be user3.id
      expect(response_body['users'][1]['id']).to be user1.id
      expect(response_body['count']).to be 2
    end

    it 'should return users list without blocked users' do
      sign_in user: user1
      user4
      user5 = create :user, :student
      user6 = create :user, :student

      user1.blocked_users<<user2
      user3.blocked_users<<user1

      get :index

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 3
      expect(response_body['count']).to be 3
      expect(response_body['users'][0]['id']).to be user6.id
      expect(response_body['users'][1]['id']).to be user5.id
      expect(response_body['users'][2]['id']).to be user4.id
    end

    it 'should return users list without ignore users' do
      sign_in user: user1
      user2
      user3
      user4
      user5 = create :user, :student

      get :index, params: {ignore_users: [user3.id, user5.id]}

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['users'][0]['id']).to be user4.id
      expect(response_body['users'][1]['id']).to be user2.id
    end

    it 'should return users list with geo_search setting' do
      sign_in user: user1
      user2.setting.update_attribute :geo_search, :everyone
      user3.setting.update_attribute :geo_search, :friends
      user4.setting.update_attribute :geo_search, :no_one
      user5 = create :user, :student
      user5.setting.update_attribute :geo_search, :friends
      create :friendship, user: user1, friend: user3, status: :accepted
      create :friendship, user: user3, friend: user1, status: :accepted

      get :index

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 2
      expect(response_body['count']).to be 2
      expect(response_body['users'][0]['id']).to be user3.id
      expect(response_body['users'][1]['id']).to be user2.id
    end
  end

  describe '#show' do
    let(:user1) {create :user, :not_upgraded}
    let(:user2) {create :user, :not_upgraded}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return user by id' do
      sign_in user: user1

      get :show, params: {id: user2.id}
      expect(response.status).to be 200

      expect(response_body['user']['id']).to be user2.id
      expect(response_body['user']['phone_number']).to eq user2.phone_number
      expect(response_body['user']['avatar']).to_not be_nil
      expect(response_body['user']['avatar_preview']).to_not be_nil
    end

    it 'should return user by id with relationships statuses' do
      sign_in user: user1
      user3 = create :user, :not_upgraded
      user4 = create :user, :not_upgraded
      user5 = create :user, :not_upgraded
      user6 = create :user, :not_upgraded

      user1.followed<<user2
      user2.followed<<user3
      user2.followed<<user4
      user4.followed<<user2
      create :friendship, user: user2
      create :friendship, user: user1, friend: user2, status: 'requested'
      create :friendship, user: user2, friend: user1, status: 'not_accepted'

      get :show, params: {id: user2.id}
      expect(response.status).to be 200

      expect(response_body['user']['id']).to be user2.id
      expect(response_body['user']['phone_number']).to eq user2.phone_number
      expect(response_body['user']['friendship_status']).to eq('requested')
      expect(response_body['user']['follower_status']).to eq false
      expect(response_body['user']['followed_status']).to eq true
    end

    it 'should return error if user not found' do
      sign_in user: user1

      get :show, params: {id: -1}
      expect(response.status).to be 400

      expect(response_body['errors'][0]['code']).to eq('E00001')
      expect(response_body['errors'][0]['message']).to eq('Record not found.')
      end

    it 'should return error if user not login' do

      get :show, params: {id: user1.id}
      expect(response.status).to be 400

      expect(response_body['errors'][0]['code']).to eq('E00002')
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if current_user blocked other user' do
      sign_in user: user1
      user1.blocked_users<<user2

      get :show, params: {id: user2}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
      end

    it 'should return error if other user blocked current_user' do
      sign_in user: user1
      user2.blocked_users<<user1

      get :show, params: {id: user2}

      expect(response.status).to be 403
      expect(response_body['errors']).to eq(['You are not authorized to access this page.'])
    end
  end

  describe '#role changed' do
    it 'should respond with error when role was changed' do

      user = create :user, :teacher

      sign_in user: user

      request.headers["Role"] = "not_upgraded"
      get :index, params: {}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['code']).to eq('E00003')
      expect(response_body['errors'][0]['role']).to eq('teacher')
    end
  end
end
