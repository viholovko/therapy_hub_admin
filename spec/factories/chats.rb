FactoryBot.define do
  factory :chat do
    sequence(:title) { |n| "chat#{n}" }

    trait :direct do
      chat_type 'direct'
      owner { create(:user, :not_upgraded) }
      users { [create(:user, :not_upgraded), create(:user, :not_upgraded)] }
    end

    trait :groups do
      chat_type 'groups'
      owner { create(:user, :not_upgraded) }
      users { [create(:user, :not_upgraded), create(:user, :not_upgraded)] }
    end

    trait :classroom do
      chat_type 'classroom'
      owner { create(:user, :not_upgraded) }
      users { [create(:user, :not_upgraded), create(:user, :not_upgraded)] }
      classroom_id -1
    end

  end
end