FactoryBot.define do
  factory :classroom do
    student {FactoryBot.create(:user, :student) }
    avatar {File.new("#{Rails.root}/spec/fixtures/factory_image.png")}

    before(:create) do |classroom|
      classroom.owner_id = classroom.student_id
    end

    sequence(:name) {|n| "Classroom#{n}" }

    trait(:with_members) do
      parent {FactoryBot.create(:user, :parent) }
      teacher {FactoryBot.create(:user, :teacher) }
      caseworker {FactoryBot.create(:user, :caseworker) }

      before(:create) do |classroom|
        classroom.owner_id = classroom.student_id
        create :friendship, user: classroom.owner, friend: classroom.parent
        create :friendship, friend: classroom.owner, user: classroom.parent
        create :friendship, user: classroom.owner, friend: classroom.teacher
        create :friendship, friend: classroom.owner, user: classroom.teacher
        create :friendship, user: classroom.owner, friend: classroom.caseworker
        create :friendship, friend: classroom.owner, user: classroom.caseworker
      end
      after(:create) do |classroom|
        FactoryBot.create :chat, :classroom, owner: classroom.owner, users: [classroom.owner, classroom.parent, classroom.teacher, classroom.caseworker], classroom_id: classroom.id
      end
    end

    trait(:empty) do
      after(:create) do |classroom|
        FactoryBot.create :chat, :classroom, owner: classroom.owner, users: [classroom.owner], classroom_id: classroom.id
      end
    end
  end
end