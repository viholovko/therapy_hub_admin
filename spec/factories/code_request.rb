FactoryBot.define do
  factory :code_request do
    country {FactoryBot.create :country}
    phone_number '12345'
    attempts_count 1
  end
end
