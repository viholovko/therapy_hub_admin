FactoryBot.define do
  factory :comment do
    message 'Comment message'
    user { FactoryBot.create(:user, :not_upgraded) }
    trait :post do
      commentable {FactoryBot.create(:post, :link)}
    end
  end
end