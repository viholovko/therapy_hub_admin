FactoryBot.define do
  factory :country do
    selected false
    flag { File.open("#{Rails.root}/spec/fixtures/factory_image.png") }
    sequence(:phone_code) {|n| n}
    sequence(:name) {|n| "country_#{n}"}
    alpha2_code " AF"
    alpha3_code "AFG"
    sequence(:numeric_code) {|n| n}
  end
end
