FactoryBot.define do
  factory :event do
    title 'Title'
    description 'Description'
    classroom {FactoryBot.create(:classroom)}
    time_from Time.now + 5.hours
    time_to Time.now + 15.hours
    owner {FactoryBot.create(:user, :student)}
  end
end