FactoryBot.define do
  factory :friendship do
    user {FactoryBot.create(:user, :student) }
    friend {FactoryBot.create(:user, :student) }
    status 'accepted'
  end
end