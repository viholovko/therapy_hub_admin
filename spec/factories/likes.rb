FactoryBot.define do
  factory :like do
    user {FactoryBot.create(:user, :student)}
    trait :post do
      likeable {FactoryBot.create(:post, :link)}
    end
  end
end