FactoryBot.define do
  factory :chat_message do
    sequence(:message)     { |n| "message#{n}"  }
    sequence(:ui_id)     { |n| "ui_id#{n}"  }
    user {FactoryBot.create(:user, :student) }
    chat {FactoryBot.create(:chat, :direct) }
    message_status 'created'

    trait :message do
      message_type 'text'
    end

    trait :link do
      message_type 'text'
      link {FactoryBot.create(:link)}
    end

    trait :photo do
      message_type 'photo'
    end

    trait :video do
      message_type 'video'
    end

    trait :post do
      message_type 'post'
    end
  end
end