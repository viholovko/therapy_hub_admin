FactoryBot.define do
  factory :post do
    user { FactoryBot.create(:user, :not_upgraded) }
    tagged_user_ids {[FactoryBot.create(:user, :not_upgraded).id, FactoryBot.create(:user, :not_upgraded).id]}

    trait :link do
      link {FactoryBot.create(:link) }
    end

    trait :text do
      text 'Post text'
    end

    trait :classroom do
      text 'Post classroom'
      before(:create) do |post|
        post.classroom_id = FactoryBot.create(:classroom, :empty, student: post.user).id
      end
    end

    trait :photo do
      text 'Post photo text'
      before(:create) do |post|
        post.attachments_attributes = [
            {attachable_type: 'Post', attachment_type: 'photo',user: post.user,
             file: File.new("#{Rails.root}/spec/fixtures/factory_image.png")
             },
            {attachable_type: 'Post', attachment_type: 'photo', user: post.user,
             file: File.new("#{Rails.root}/spec/fixtures/factory_image.png")
            }
        ]
      end
    end

    trait :video do
      text 'Post video text'
      before(:create) do |post|
        post.attachments_attributes = [
            {attachable_type: 'Post', attachment_type: 'video',user: post.user,
             file: File.new("#{Rails.root}/spec/fixtures/factory_video.mp4")
             },
            {attachable_type: 'Post', attachment_type: 'video', user: post.user,
             file: File.new("#{Rails.root}/spec/fixtures/factory_video.mp4")
            }
        ]
      end
    end

    trait :audio do
      text 'Post audio text'
      before(:create) do |post|
        post.attachments_attributes = [
            {attachable_type: 'Post', attachment_type: 'audio',user: post.user,
             file: File.new("#{Rails.root}/spec/fixtures/factory_audio.mp3")
             },
            {attachable_type: 'Post', attachment_type: 'audio', user: post.user,
             file: File.new("#{Rails.root}/spec/fixtures/factory_audio.mp3")
            }
        ]
      end
    end
  end
end