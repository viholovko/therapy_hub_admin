FactoryBot.define do
  factory :report do
    post {FactoryBot.create(:post, :link)}
    user {FactoryBot.create(:user, :student)}
    report_type 'spam'
  end
end