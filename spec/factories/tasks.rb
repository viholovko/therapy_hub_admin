FactoryBot.define do
  factory :task do
    title 'Title'
    description 'Description'
    classroom {FactoryBot.create(:classroom, :empty)}
    due_date Time.now + 5.hours
    owner {FactoryBot.create(:user, :teacher)}
    completed false
  end
end