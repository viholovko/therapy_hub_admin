FactoryBot.define do
  factory :user do
    abc = 'abcdefg'
    sequence(:first_name)     { |n| "user#{abc[n]}"  }
    sequence(:last_name)      { |n| "user#{abc[n]}"  }
    sequence(:phone_number)   { |n| "12345678#{n}"   }
    country             {FactoryBot.create(:country) }

    password                'secret!'
    password_confirmation   'secret!'
    avatar {File.new("#{Rails.root}/spec/fixtures/factory_image.png")}

    trait :not_upgraded do
      role { Role.get_not_upgraded }
    end

    trait :teacher do
      role { Role.get_teacher }
    end

    trait :student do
      role { Role.get_student }
    end

    trait :parent do
          role { Role.get_parent }
        end

    trait :caseworker do
      role { Role.get_caseworker }
    end

    trait :mentor do
      role { Role.get_mentor }
    end

    trait :admin do
      role { Role.get_admin }
    end
  end
end