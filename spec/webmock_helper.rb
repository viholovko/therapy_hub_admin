RSpec.configure do |config|
  config.before(:each) do

    stub_request(:post, "https://sandbox.itunes.apple.com/verifyReceipt")
        .with(body: /test_success_ios_purchase_token/)
        .to_return(
            status: 200,
            body: {
                status: 0,
                receipt: {
                    in_app: [
                        product_id: 'com.foxtrapp.booklava.subscription_premium',
                        purchase_date_ms: 1493123304,
                        expires_date_ms: 1493209733,
                        is_trial_period: true
                    ]
                }
            }.to_json,
            headers: {})

    stub_request(:post, "https://sandbox.itunes.apple.com/verifyReceipt")
        .with(body: /test_bad_ios_purchase_token/)
        .to_return(
            status: 200,
            body: {"status"=>21002}.to_json,
            headers: {})

    stub_request(:post, "https://accounts.google.com/o/oauth2/token")
        .to_return(:status => 200, :body => {}.to_json, :headers => {})

    stub_request(:get, "https://www.googleapis.com/androidpublisher/v2/applications//purchases/products//tokens/test_success_android_purchase_token?access_token=").
        to_return(:status => 200, :body => {}.to_json, :headers => {})

    stub_request(:get, "https://www.googleapis.com/androidpublisher/v2/applications//purchases/products//tokens/test_bad_android_purchase_token?access_token=").
        to_return(:status => 200, :body => {error: {}}.to_json, :headers => {})

    html1 = File.open(File.dirname(__FILE__) + '/fixtures/factory_page_google.html').read
    stub_request(:get, "http://google.com/derevo").
        to_return(status: 200, body: html1, headers: {})

    html2 = File.open(File.dirname(__FILE__) + '/fixtures/factory_page_microsoft.html').read
    stub_request(:get, "http://microsoft.com/derevo").
        to_return(status: 200, body: html2, headers: {})

  end
end